processTreeMapJson (  {"id":"Clover database Thu Sep 27 2012 21:40:43 CEST0","name":"","data":{
    "$area":2287.0,"$color":50.240486,"title":
    " 2287 Elements, 50.2% Coverage"},"children":[{"id":
      "org.apache.http.nio.client1822","name":"org.apache.http.nio.client",
      "data":{"$area":0.0,"$color":-100.0,"title":
        "org.apache.http.nio.client 0 Elements,  -  Coverage"},"children":[{
          "id":"HttpAsyncClient1822","name":"HttpAsyncClient","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/client/HttpAsyncClient.html#HttpAsyncClient",
            "title":"HttpAsyncClient 0 Elements,  -  Coverage"},"children":[]}]},
    {"id":"org.apache.http.nio.conn.scheme2111","name":
      "org.apache.http.nio.conn.scheme","data":{"$area":95.0,"$color":
        37.894737,"title":
        "org.apache.http.nio.conn.scheme 95 Elements, 37.9% Coverage"},
      "children":[{"id":"AsyncScheme2111","name":"AsyncScheme","data":{
            "$area":49.0,"$color":24.489796,"path":
            "org/apache/http/nio/conn/scheme/AsyncScheme.html#AsyncScheme",
            "title":"AsyncScheme 49 Elements, 24.5% Coverage"},"children":[]},
        {"id":"AsyncSchemeRegistry2160","name":"AsyncSchemeRegistry","data":{
            "$area":46.0,"$color":52.173912,"path":
            "org/apache/http/nio/conn/scheme/AsyncSchemeRegistry.html#AsyncSchemeRegistry",
            "title":"AsyncSchemeRegistry 46 Elements, 52.2% Coverage"},
          "children":[]},{"id":"LayeringStrategy2206","name":
          "LayeringStrategy","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/conn/scheme/LayeringStrategy.html#LayeringStrategy",
            "title":"LayeringStrategy 0 Elements,  -  Coverage"},"children":[]}]},
    {"id":"org.apache.http.nio.conn2111","name":"org.apache.http.nio.conn",
      "data":{"$area":0.0,"$color":-100.0,"title":
        "org.apache.http.nio.conn 0 Elements,  -  Coverage"},"children":[{
          "id":"ClientAsyncConnection2111","name":"ClientAsyncConnection",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/conn/ClientAsyncConnection.html#ClientAsyncConnection",
            "title":"ClientAsyncConnection 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ClientAsyncConnectionFactory2111","name":
          "ClientAsyncConnectionFactory","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/nio/conn/ClientAsyncConnectionFactory.html#ClientAsyncConnectionFactory",
            "title":
            "ClientAsyncConnectionFactory 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ClientAsyncConnectionManager2111","name":
          "ClientAsyncConnectionManager","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/nio/conn/ClientAsyncConnectionManager.html#ClientAsyncConnectionManager",
            "title":
            "ClientAsyncConnectionManager 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ManagedClientAsyncConnection2111","name":
          "ManagedClientAsyncConnection","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/nio/conn/ManagedClientAsyncConnection.html#ManagedClientAsyncConnection",
            "title":
            "ManagedClientAsyncConnection 0 Elements,  -  Coverage"},
          "children":[]}]},{"id":"org.apache.http.impl.nio.client0","name":
      "org.apache.http.impl.nio.client","data":{"$area":996.0,"$color":
        61.947792,"title":
        "org.apache.http.impl.nio.client 996 Elements, 61.9% Coverage"},
      "children":[{"id":"AbstractHttpAsyncClient0","name":
          "AbstractHttpAsyncClient","data":{"$area":281.0,"$color":68.3274,
            "path":
            "org/apache/http/impl/nio/client/AbstractHttpAsyncClient.html#AbstractHttpAsyncClient",
            "title":"AbstractHttpAsyncClient 281 Elements, 68.3% Coverage"},
          "children":[]},{"id":"DefaultAsyncRequestDirector281","name":
          "DefaultAsyncRequestDirector","data":{"$area":591.0,"$color":
            58.88325,"path":
            "org/apache/http/impl/nio/client/DefaultAsyncRequestDirector.html#DefaultAsyncRequestDirector",
            "title":
            "DefaultAsyncRequestDirector 591 Elements, 58.9% Coverage"},
          "children":[]},{"id":
          "DefaultAsyncRequestDirector.InternalFutureCallback681","name":
          "DefaultAsyncRequestDirector.InternalFutureCallback","data":{
            "$area":6.0,"$color":33.333336,"path":
            "org/apache/http/impl/nio/client/DefaultAsyncRequestDirector.html#DefaultAsyncRequestDirector.InternalFutureCallback",
            "title":
            "DefaultAsyncRequestDirector.InternalFutureCallback 6 Elements, 33.3% Coverage"},
          "children":[]},{"id":"DefaultHttpAsyncClient878","name":
          "DefaultHttpAsyncClient","data":{"$area":34.0,"$color":85.29411,
            "path":
            "org/apache/http/impl/nio/client/DefaultHttpAsyncClient.html#DefaultHttpAsyncClient",
            "title":"DefaultHttpAsyncClient 34 Elements, 85.3% Coverage"},
          "children":[]},{"id":"DefaultResultCallback912","name":
          "DefaultResultCallback","data":{"$area":15.0,"$color":80.0,"path":
            "org/apache/http/impl/nio/client/DefaultResultCallback.html#DefaultResultCallback",
            "title":"DefaultResultCallback 15 Elements, 80% Coverage"},
          "children":[]},{"id":"InternalIOReactorExceptionHandler927","name":
          "InternalIOReactorExceptionHandler","data":{"$area":9.0,"$color":
            0.0,"path":
            "org/apache/http/impl/nio/client/InternalIOReactorExceptionHandler.html#InternalIOReactorExceptionHandler",
            "title":
            "InternalIOReactorExceptionHandler 9 Elements, 0% Coverage"},
          "children":[]},{"id":"LoggingAsyncRequestExecutor936","name":
          "LoggingAsyncRequestExecutor","data":{"$area":60.0,"$color":
            56.666668,"path":
            "org/apache/http/impl/nio/client/LoggingAsyncRequestExecutor.html#LoggingAsyncRequestExecutor",
            "title":
            "LoggingAsyncRequestExecutor 60 Elements, 56.7% Coverage"},
          "children":[]},{"id":"ResultCallback996","name":"ResultCallback",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/impl/nio/client/ResultCallback.html#ResultCallback",
            "title":"ResultCallback 0 Elements,  -  Coverage"},"children":[]}]},
    {"id":"org.apache.http.impl.nio.conn996","name":
      "org.apache.http.impl.nio.conn","data":{"$area":826.0,"$color":
        35.472153,"title":
        "org.apache.http.impl.nio.conn 826 Elements, 35.5% Coverage"},
      "children":[{"id":"AsyncSchemeRegistryFactory996","name":
          "AsyncSchemeRegistryFactory","data":{"$area":5.0,"$color":0.0,
            "path":
            "org/apache/http/impl/nio/conn/AsyncSchemeRegistryFactory.html#AsyncSchemeRegistryFactory",
            "title":"AsyncSchemeRegistryFactory 5 Elements, 0% Coverage"},
          "children":[]},{"id":"DefaultClientAsyncConnection1001","name":
          "DefaultClientAsyncConnection","data":{"$area":61.0,"$color":
            24.590164,"path":
            "org/apache/http/impl/nio/conn/DefaultClientAsyncConnection.html#DefaultClientAsyncConnection",
            "title":
            "DefaultClientAsyncConnection 61 Elements, 24.6% Coverage"},
          "children":[]},{"id":"DefaultClientAsyncConnectionFactory1062",
          "name":"DefaultClientAsyncConnectionFactory","data":{"$area":
            10.0,"$color":100.0,"path":
            "org/apache/http/impl/nio/conn/DefaultClientAsyncConnectionFactory.html#DefaultClientAsyncConnectionFactory",
            "title":
            "DefaultClientAsyncConnectionFactory 10 Elements, 100% Coverage"},
          "children":[]},{"id":"DefaultHttpAsyncRoutePlanner1072","name":
          "DefaultHttpAsyncRoutePlanner","data":{"$area":39.0,"$color":
            74.35898,"path":
            "org/apache/http/impl/nio/conn/DefaultHttpAsyncRoutePlanner.html#DefaultHttpAsyncRoutePlanner",
            "title":
            "DefaultHttpAsyncRoutePlanner 39 Elements, 74.4% Coverage"},
          "children":[]},{"id":"HttpNIOConnPool1111","name":
          "HttpNIOConnPool","data":{"$area":25.0,"$color":84.0,"path":
            "org/apache/http/impl/nio/conn/HttpNIOConnPool.html#HttpNIOConnPool",
            "title":"HttpNIOConnPool 25 Elements, 84% Coverage"},"children":[]},
        {"id":"HttpNIOConnPoolFactory1136","name":"HttpNIOConnPoolFactory",
          "data":{"$area":2.0,"$color":100.0,"path":
            "org/apache/http/impl/nio/conn/HttpNIOConnPoolFactory.html#HttpNIOConnPoolFactory",
            "title":"HttpNIOConnPoolFactory 2 Elements, 100% Coverage"},
          "children":[]},{"id":"HttpPoolEntry1138","name":"HttpPoolEntry",
          "data":{"$area":30.0,"$color":73.333336,"path":
            "org/apache/http/impl/nio/conn/HttpPoolEntry.html#HttpPoolEntry",
            "title":"HttpPoolEntry 30 Elements, 73.3% Coverage"},"children":[]},
        {"id":"LoggingIOSession1168","name":"LoggingIOSession","data":{
            "$area":104.0,"$color":0.0,"path":
            "org/apache/http/impl/nio/conn/LoggingIOSession.html#LoggingIOSession",
            "title":"LoggingIOSession 104 Elements, 0% Coverage"},"children":
          []},{"id":"LoggingIOSession.LoggingByteChannel1272","name":
          "LoggingIOSession.LoggingByteChannel","data":{"$area":38.0,
            "$color":0.0,"path":
            "org/apache/http/impl/nio/conn/LoggingIOSession.html#LoggingIOSession.LoggingByteChannel",
            "title":
            "LoggingIOSession.LoggingByteChannel 38 Elements, 0% Coverage"},
          "children":[]},{"id":"ManagedClientAsyncConnectionImpl1310","name":
          "ManagedClientAsyncConnectionImpl","data":{"$area":252.0,"$color":
            45.238094,"path":
            "org/apache/http/impl/nio/conn/ManagedClientAsyncConnectionImpl.html#ManagedClientAsyncConnectionImpl",
            "title":
            "ManagedClientAsyncConnectionImpl 252 Elements, 45.2% Coverage"},
          "children":[]},{"id":"PoolingClientAsyncConnectionManager1562",
          "name":"PoolingClientAsyncConnectionManager","data":{"$area":
            172.0,"$color":41.279068,"path":
            "org/apache/http/impl/nio/conn/PoolingClientAsyncConnectionManager.html#PoolingClientAsyncConnectionManager",
            "title":
            "PoolingClientAsyncConnectionManager 172 Elements, 41.3% Coverage"},
          "children":[]},{"id":
          "PoolingClientAsyncConnectionManager.InternalPoolEntryCallback1734",
          "name":
          "PoolingClientAsyncConnectionManager.InternalPoolEntryCallback",
          "data":{"$area":22.0,"$color":40.909092,"path":
            "org/apache/http/impl/nio/conn/PoolingClientAsyncConnectionManager.html#PoolingClientAsyncConnectionManager.InternalPoolEntryCallback",
            "title":
            "PoolingClientAsyncConnectionManager.InternalPoolEntryCallback 22 Elements, 40.9% Coverage"},
          "children":[]},{"id":"Wire1756","name":"Wire","data":{"$area":
            66.0,"$color":0.0,"path":
            "org/apache/http/impl/nio/conn/Wire.html#Wire","title":
            "Wire 66 Elements, 0% Coverage"},"children":[]}]},{"id":
      "org.apache.http.nio.client.util2104","name":
      "org.apache.http.nio.client.util","data":{"$area":7.0,"$color":
        71.42857,"title":
        "org.apache.http.nio.client.util 7 Elements, 71.4% Coverage"},
      "children":[{"id":"HttpAsyncClientUtils2104","name":
          "HttpAsyncClientUtils","data":{"$area":7.0,"$color":71.42857,
            "path":
            "org/apache/http/nio/client/util/HttpAsyncClientUtils.html#HttpAsyncClientUtils",
            "title":"HttpAsyncClientUtils 7 Elements, 71.4% Coverage"},
          "children":[]}]},{"id":"org.apache.http.nio.conn.ssl2206","name":
      "org.apache.http.nio.conn.ssl","data":{"$area":81.0,"$color":
        24.691359,"title":
        "org.apache.http.nio.conn.ssl 81 Elements, 24.7% Coverage"},
      "children":[{"id":"SSLLayeringStrategy2206","name":
          "SSLLayeringStrategy","data":{"$area":64.0,"$color":25.0,"path":
            "org/apache/http/nio/conn/ssl/SSLLayeringStrategy.html#SSLLayeringStrategy",
            "title":"SSLLayeringStrategy 64 Elements, 25% Coverage"},
          "children":[]},{"id":
          "SSLLayeringStrategy.InternalSSLSetupHandler2270","name":
          "SSLLayeringStrategy.InternalSSLSetupHandler","data":{"$area":
            4.0,"$color":100.0,"path":
            "org/apache/http/nio/conn/ssl/SSLLayeringStrategy.html#SSLLayeringStrategy.InternalSSLSetupHandler",
            "title":
            "SSLLayeringStrategy.InternalSSLSetupHandler 4 Elements, 100% Coverage"},
          "children":[]},{"id":"TrustManagerDecorator2274","name":
          "TrustManagerDecorator","data":{"$area":13.0,"$color":0.0,"path":
            "org/apache/http/nio/conn/ssl/TrustManagerDecorator.html#TrustManagerDecorator",
            "title":"TrustManagerDecorator 13 Elements, 0% Coverage"},
          "children":[]}]},{"id":"org.apache.http.nio.client.methods1822",
      "name":"org.apache.http.nio.client.methods","data":{"$area":282.0,
        "$color":63.120567,"title":
        "org.apache.http.nio.client.methods 282 Elements, 63.1% Coverage"},
      "children":[{"id":"AsyncByteConsumer1822","name":"AsyncByteConsumer",
          "data":{"$area":23.0,"$color":91.30435,"path":
            "org/apache/http/nio/client/methods/AsyncByteConsumer.html#AsyncByteConsumer",
            "title":"AsyncByteConsumer 23 Elements, 91.3% Coverage"},
          "children":[]},{"id":"AsyncCharConsumer1845","name":
          "AsyncCharConsumer","data":{"$area":53.0,"$color":86.79245,"path":
            "org/apache/http/nio/client/methods/AsyncCharConsumer.html#AsyncCharConsumer",
            "title":"AsyncCharConsumer 53 Elements, 86.8% Coverage"},
          "children":[]},{"id":"BaseZeroCopyRequestProducer1898","name":
          "BaseZeroCopyRequestProducer","data":{"$area":60.0,"$color":
            81.666664,"path":
            "org/apache/http/nio/client/methods/BaseZeroCopyRequestProducer.html#BaseZeroCopyRequestProducer",
            "title":
            "BaseZeroCopyRequestProducer 60 Elements, 81.7% Coverage"},
          "children":[]},{"id":"HttpAsyncMethods1958","name":
          "HttpAsyncMethods","data":{"$area":81.0,"$color":28.395063,"path":
            "org/apache/http/nio/client/methods/HttpAsyncMethods.html#HttpAsyncMethods",
            "title":"HttpAsyncMethods 81 Elements, 28.4% Coverage"},
          "children":[]},{"id":"HttpAsyncMethods.RequestProducerImpl2039",
          "name":"HttpAsyncMethods.RequestProducerImpl","data":{"$area":
            4.0,"$color":100.0,"path":
            "org/apache/http/nio/client/methods/HttpAsyncMethods.html#HttpAsyncMethods.RequestProducerImpl",
            "title":
            "HttpAsyncMethods.RequestProducerImpl 4 Elements, 100% Coverage"},
          "children":[]},{"id":"ZeroCopyConsumer2043","name":
          "ZeroCopyConsumer","data":{"$area":45.0,"$color":77.77778,"path":
            "org/apache/http/nio/client/methods/ZeroCopyConsumer.html#ZeroCopyConsumer",
            "title":"ZeroCopyConsumer 45 Elements, 77.8% Coverage"},
          "children":[]},{"id":"ZeroCopyPost2088","name":"ZeroCopyPost",
          "data":{"$area":8.0,"$color":0.0,"path":
            "org/apache/http/nio/client/methods/ZeroCopyPost.html#ZeroCopyPost",
            "title":"ZeroCopyPost 8 Elements, 0% Coverage"},"children":[]},{
          "id":"ZeroCopyPut2096","name":"ZeroCopyPut","data":{"$area":8.0,
            "$color":0.0,"path":
            "org/apache/http/nio/client/methods/ZeroCopyPut.html#ZeroCopyPut",
            "title":"ZeroCopyPut 8 Elements, 0% Coverage"},"children":[]}]}]}

 ); 