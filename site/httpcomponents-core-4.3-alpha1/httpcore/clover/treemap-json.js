processTreeMapJson (  {"id":"Clover database Wed Nov 28 2012 22:43:03 CET0","name":"","data":{
    "$area":7681.0,"$color":69.50918,"title":
    " 7681 Elements, 69.5% Coverage"},"children":[{"id":
      "org.apache.http.impl.entity1831","name":
      "org.apache.http.impl.entity","data":{"$area":150.0,"$color":
        99.333336,"title":
        "org.apache.http.impl.entity 150 Elements, 99.3% Coverage"},
      "children":[{"id":"DisallowIdentityContentLengthStrategy1831","name":
          "DisallowIdentityContentLengthStrategy","data":{"$area":10.0,
            "$color":100.0,"path":
            "org/apache/http/impl/entity/DisallowIdentityContentLengthStrategy.html#DisallowIdentityContentLengthStrategy",
            "title":
            "DisallowIdentityContentLengthStrategy 10 Elements, 100% Coverage"},
          "children":[]},{"id":"EntityDeserializer1841","name":
          "EntityDeserializer","data":{"$area":36.0,"$color":100.0,"path":
            "org/apache/http/impl/entity/EntityDeserializer.html#EntityDeserializer",
            "title":"EntityDeserializer 36 Elements, 100% Coverage"},
          "children":[]},{"id":"EntitySerializer1877","name":
          "EntitySerializer","data":{"$area":21.0,"$color":100.0,"path":
            "org/apache/http/impl/entity/EntitySerializer.html#EntitySerializer",
            "title":"EntitySerializer 21 Elements, 100% Coverage"},
          "children":[]},{"id":"LaxContentLengthStrategy1898","name":
          "LaxContentLengthStrategy","data":{"$area":44.0,"$color":
            97.72727,"path":
            "org/apache/http/impl/entity/LaxContentLengthStrategy.html#LaxContentLengthStrategy",
            "title":"LaxContentLengthStrategy 44 Elements, 97.7% Coverage"},
          "children":[]},{"id":"StrictContentLengthStrategy1942","name":
          "StrictContentLengthStrategy","data":{"$area":39.0,"$color":
            100.0,"path":
            "org/apache/http/impl/entity/StrictContentLengthStrategy.html#StrictContentLengthStrategy",
            "title":
            "StrictContentLengthStrategy 39 Elements, 100% Coverage"},
          "children":[]}]},{"id":"org.apache.http.io3577","name":
      "org.apache.http.io","data":{"$area":0.0,"$color":-100.0,"title":
        "org.apache.http.io 0 Elements,  -  Coverage"},"children":[{"id":
          "BufferInfo3577","name":"BufferInfo","data":{"$area":0.0,"$color":
            -100.0,"path":"org/apache/http/io/BufferInfo.html#BufferInfo",
            "title":"BufferInfo 0 Elements,  -  Coverage"},"children":[]},{
          "id":"EofSensor3577","name":"EofSensor","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/io/EofSensor.html#EofSensor","title":
            "EofSensor 0 Elements,  -  Coverage"},"children":[]},{"id":
          "HttpMessageParser3577","name":"HttpMessageParser","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/io/HttpMessageParser.html#HttpMessageParser",
            "title":"HttpMessageParser 0 Elements,  -  Coverage"},"children":
          []},{"id":"HttpMessageParserFactory3577","name":
          "HttpMessageParserFactory","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/io/HttpMessageParserFactory.html#HttpMessageParserFactory",
            "title":"HttpMessageParserFactory 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpMessageWriter3577","name":
          "HttpMessageWriter","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/io/HttpMessageWriter.html#HttpMessageWriter",
            "title":"HttpMessageWriter 0 Elements,  -  Coverage"},"children":
          []},{"id":"HttpMessageWriterFactory3577","name":
          "HttpMessageWriterFactory","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/io/HttpMessageWriterFactory.html#HttpMessageWriterFactory",
            "title":"HttpMessageWriterFactory 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpTransportMetrics3577","name":
          "HttpTransportMetrics","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/io/HttpTransportMetrics.html#HttpTransportMetrics",
            "title":"HttpTransportMetrics 0 Elements,  -  Coverage"},
          "children":[]},{"id":"SessionInputBuffer3577","name":
          "SessionInputBuffer","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/io/SessionInputBuffer.html#SessionInputBuffer",
            "title":"SessionInputBuffer 0 Elements,  -  Coverage"},
          "children":[]},{"id":"SessionOutputBuffer3577","name":
          "SessionOutputBuffer","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/io/SessionOutputBuffer.html#SessionOutputBuffer",
            "title":"SessionOutputBuffer 0 Elements,  -  Coverage"},
          "children":[]}]},{"id":"org.apache.http.message3577","name":
      "org.apache.http.message","data":{"$area":1476.0,"$color":91.39566,
        "title":"org.apache.http.message 1476 Elements, 91.4% Coverage"},
      "children":[{"id":"AbstractHttpMessage3577","name":
          "AbstractHttpMessage","data":{"$area":55.0,"$color":96.36363,
            "path":
            "org/apache/http/message/AbstractHttpMessage.html#AbstractHttpMessage",
            "title":"AbstractHttpMessage 55 Elements, 96.4% Coverage"},
          "children":[]},{"id":"BasicHeader3632","name":"BasicHeader","data":
          {"$area":18.0,"$color":100.0,"path":
            "org/apache/http/message/BasicHeader.html#BasicHeader","title":
            "BasicHeader 18 Elements, 100% Coverage"},"children":[]},{"id":
          "BasicHeaderElement3650","name":"BasicHeaderElement","data":{
            "$area":63.0,"$color":100.0,"path":
            "org/apache/http/message/BasicHeaderElement.html#BasicHeaderElement",
            "title":"BasicHeaderElement 63 Elements, 100% Coverage"},
          "children":[]},{"id":"BasicHeaderElementIterator3713","name":
          "BasicHeaderElementIterator","data":{"$area":74.0,"$color":
            87.83784,"path":
            "org/apache/http/message/BasicHeaderElementIterator.html#BasicHeaderElementIterator",
            "title":
            "BasicHeaderElementIterator 74 Elements, 87.8% Coverage"},
          "children":[]},{"id":"BasicHeaderIterator3787","name":
          "BasicHeaderIterator","data":{"$area":36.0,"$color":100.0,"path":
            "org/apache/http/message/BasicHeaderIterator.html#BasicHeaderIterator",
            "title":"BasicHeaderIterator 36 Elements, 100% Coverage"},
          "children":[]},{"id":"BasicHeaderValueFormatter3823","name":
          "BasicHeaderValueFormatter","data":{"$area":180.0,"$color":
            92.77778,"path":
            "org/apache/http/message/BasicHeaderValueFormatter.html#BasicHeaderValueFormatter",
            "title":
            "BasicHeaderValueFormatter 180 Elements, 92.8% Coverage"},
          "children":[]},{"id":"BasicHeaderValueParser4003","name":
          "BasicHeaderValueParser","data":{"$area":194.0,"$color":97.42268,
            "path":
            "org/apache/http/message/BasicHeaderValueParser.html#BasicHeaderValueParser",
            "title":"BasicHeaderValueParser 194 Elements, 97.4% Coverage"},
          "children":[]},{"id":"BasicHttpEntityEnclosingRequest4197","name":
          "BasicHttpEntityEnclosingRequest","data":{"$area":13.0,"$color":
            100.0,"path":
            "org/apache/http/message/BasicHttpEntityEnclosingRequest.html#BasicHttpEntityEnclosingRequest",
            "title":
            "BasicHttpEntityEnclosingRequest 13 Elements, 100% Coverage"},
          "children":[]},{"id":"BasicHttpRequest4210","name":
          "BasicHttpRequest","data":{"$area":22.0,"$color":90.909096,"path":
            "org/apache/http/message/BasicHttpRequest.html#BasicHttpRequest",
            "title":"BasicHttpRequest 22 Elements, 90.9% Coverage"},
          "children":[]},{"id":"BasicHttpResponse4232","name":
          "BasicHttpResponse","data":{"$area":73.0,"$color":83.56164,"path":
            "org/apache/http/message/BasicHttpResponse.html#BasicHttpResponse",
            "title":"BasicHttpResponse 73 Elements, 83.6% Coverage"},
          "children":[]},{"id":"BasicLineFormatter4305","name":
          "BasicLineFormatter","data":{"$area":111.0,"$color":98.1982,"path":
            
            "org/apache/http/message/BasicLineFormatter.html#BasicLineFormatter",
            "title":"BasicLineFormatter 111 Elements, 98.2% Coverage"},
          "children":[]},{"id":"BasicLineParser4416","name":
          "BasicLineParser","data":{"$area":213.0,"$color":76.52582,"path":
            "org/apache/http/message/BasicLineParser.html#BasicLineParser",
            "title":"BasicLineParser 213 Elements, 76.5% Coverage"},
          "children":[]},{"id":"BasicListHeaderIterator4629","name":
          "BasicListHeaderIterator","data":{"$area":49.0,"$color":
            87.755104,"path":
            "org/apache/http/message/BasicListHeaderIterator.html#BasicListHeaderIterator",
            "title":"BasicListHeaderIterator 49 Elements, 87.8% Coverage"},
          "children":[]},{"id":"BasicNameValuePair4678","name":
          "BasicNameValuePair","data":{"$area":37.0,"$color":100.0,"path":
            "org/apache/http/message/BasicNameValuePair.html#BasicNameValuePair",
            "title":"BasicNameValuePair 37 Elements, 100% Coverage"},
          "children":[]},{"id":"BasicRequestLine4715","name":
          "BasicRequestLine","data":{"$area":15.0,"$color":100.0,"path":
            "org/apache/http/message/BasicRequestLine.html#BasicRequestLine",
            "title":"BasicRequestLine 15 Elements, 100% Coverage"},
          "children":[]},{"id":"BasicStatusLine4730","name":
          "BasicStatusLine","data":{"$area":15.0,"$color":100.0,"path":
            "org/apache/http/message/BasicStatusLine.html#BasicStatusLine",
            "title":"BasicStatusLine 15 Elements, 100% Coverage"},"children":
          []},{"id":"BasicTokenIterator4745","name":"BasicTokenIterator",
          "data":{"$area":123.0,"$color":100.0,"path":
            "org/apache/http/message/BasicTokenIterator.html#BasicTokenIterator",
            "title":"BasicTokenIterator 123 Elements, 100% Coverage"},
          "children":[]},{"id":"BufferedHeader4868","name":
          "BufferedHeader","data":{"$area":32.0,"$color":100.0,"path":
            "org/apache/http/message/BufferedHeader.html#BufferedHeader",
            "title":"BufferedHeader 32 Elements, 100% Coverage"},"children":[]},
        {"id":"HeaderGroup4900","name":"HeaderGroup","data":{"$area":112.0,
            "$color":94.64286,"path":
            "org/apache/http/message/HeaderGroup.html#HeaderGroup","title":
            "HeaderGroup 112 Elements, 94.6% Coverage"},"children":[]},{"id":
          "HeaderValueFormatter5012","name":"HeaderValueFormatter","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/message/HeaderValueFormatter.html#HeaderValueFormatter",
            "title":"HeaderValueFormatter 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HeaderValueParser5012","name":
          "HeaderValueParser","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/message/HeaderValueParser.html#HeaderValueParser",
            "title":"HeaderValueParser 0 Elements,  -  Coverage"},"children":
          []},{"id":"LineFormatter5012","name":"LineFormatter","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/message/LineFormatter.html#LineFormatter",
            "title":"LineFormatter 0 Elements,  -  Coverage"},"children":[]},
        {"id":"LineParser5012","name":"LineParser","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/message/LineParser.html#LineParser","title":
            "LineParser 0 Elements,  -  Coverage"},"children":[]},{"id":
          "ParserCursor5012","name":"ParserCursor","data":{"$area":41.0,
            "$color":51.219513,"path":
            "org/apache/http/message/ParserCursor.html#ParserCursor","title":
            "ParserCursor 41 Elements, 51.2% Coverage"},"children":[]}]},{
      "id":"org.apache.http.protocol5902","name":
      "org.apache.http.protocol","data":{"$area":1041.0,"$color":66.186356,
        "title":"org.apache.http.protocol 1041 Elements, 66.2% Coverage"},
      "children":[{"id":"BasicHttpContext5902","name":"BasicHttpContext",
          "data":{"$area":42.0,"$color":73.809525,"path":
            "org/apache/http/protocol/BasicHttpContext.html#BasicHttpContext",
            "title":"BasicHttpContext 42 Elements, 73.8% Coverage"},
          "children":[]},{"id":"BasicHttpProcessor5944","name":
          "BasicHttpProcessor","data":{"$area":114.0,"$color":23.684212,
            "path":
            "org/apache/http/protocol/BasicHttpProcessor.html#BasicHttpProcessor",
            "title":"BasicHttpProcessor 114 Elements, 23.7% Coverage"},
          "children":[]},{"id":"ChainBuilder6058","name":"ChainBuilder",
          "data":{"$area":60.0,"$color":80.0,"path":
            "org/apache/http/protocol/ChainBuilder.html#ChainBuilder",
            "title":"ChainBuilder 60 Elements, 80% Coverage"},"children":[]},
        {"id":"DefaultedHttpContext6118","name":"DefaultedHttpContext",
          "data":{"$area":23.0,"$color":0.0,"path":
            "org/apache/http/protocol/DefaultedHttpContext.html#DefaultedHttpContext",
            "title":"DefaultedHttpContext 23 Elements, 0% Coverage"},
          "children":[]},{"id":"ExecutionContext6141","name":
          "ExecutionContext","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/protocol/ExecutionContext.html#ExecutionContext",
            "title":"ExecutionContext 0 Elements,  -  Coverage"},"children":[]},
        {"id":"HTTP6141","name":"HTTP","data":{"$area":3.0,"$color":
            66.66667,"path":"org/apache/http/protocol/HTTP.html#HTTP",
            "title":"HTTP 3 Elements, 66.7% Coverage"},"children":[]},{"id":
          "HttpContext6144","name":"HttpContext","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/protocol/HttpContext.html#HttpContext","title":
            "HttpContext 0 Elements,  -  Coverage"},"children":[]},{"id":
          "HttpCoreContext6144","name":"HttpCoreContext","data":{"$area":
            46.0,"$color":71.73913,"path":
            "org/apache/http/protocol/HttpCoreContext.html#HttpCoreContext",
            "title":"HttpCoreContext 46 Elements, 71.7% Coverage"},
          "children":[]},{"id":"HttpDateGenerator6190","name":
          "HttpDateGenerator","data":{"$area":12.0,"$color":100.0,"path":
            "org/apache/http/protocol/HttpDateGenerator.html#HttpDateGenerator",
            "title":"HttpDateGenerator 12 Elements, 100% Coverage"},
          "children":[]},{"id":"HttpExpectationVerifier6202","name":
          "HttpExpectationVerifier","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/protocol/HttpExpectationVerifier.html#HttpExpectationVerifier",
            "title":"HttpExpectationVerifier 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpProcessor6202","name":"HttpProcessor",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/protocol/HttpProcessor.html#HttpProcessor",
            "title":"HttpProcessor 0 Elements,  -  Coverage"},"children":[]},
        {"id":"HttpProcessorBuilder6202","name":"HttpProcessorBuilder",
          "data":{"$area":86.0,"$color":0.0,"path":
            "org/apache/http/protocol/HttpProcessorBuilder.html#HttpProcessorBuilder",
            "title":"HttpProcessorBuilder 86 Elements, 0% Coverage"},
          "children":[]},{"id":"HttpRequestExecutor6288","name":
          "HttpRequestExecutor","data":{"$area":102.0,"$color":100.0,"path":
            "org/apache/http/protocol/HttpRequestExecutor.html#HttpRequestExecutor",
            "title":"HttpRequestExecutor 102 Elements, 100% Coverage"},
          "children":[]},{"id":"HttpRequestHandler6390","name":
          "HttpRequestHandler","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/protocol/HttpRequestHandler.html#HttpRequestHandler",
            "title":"HttpRequestHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpRequestHandlerMapper6390","name":
          "HttpRequestHandlerMapper","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/protocol/HttpRequestHandlerMapper.html#HttpRequestHandlerMapper",
            "title":"HttpRequestHandlerMapper 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpRequestHandlerRegistry6390","name":
          "HttpRequestHandlerRegistry","data":{"$area":14.0,"$color":0.0,
            "path":
            "org/apache/http/protocol/HttpRequestHandlerRegistry.html#HttpRequestHandlerRegistry",
            "title":"HttpRequestHandlerRegistry 14 Elements, 0% Coverage"},
          "children":[]},{"id":"HttpRequestHandlerResolver6404","name":
          "HttpRequestHandlerResolver","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/protocol/HttpRequestHandlerResolver.html#HttpRequestHandlerResolver",
            "title":"HttpRequestHandlerResolver 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpRequestInterceptorList6404","name":
          "HttpRequestInterceptorList","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/protocol/HttpRequestInterceptorList.html#HttpRequestInterceptorList",
            "title":"HttpRequestInterceptorList 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpResponseInterceptorList6404","name":
          "HttpResponseInterceptorList","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/protocol/HttpResponseInterceptorList.html#HttpResponseInterceptorList",
            "title":"HttpResponseInterceptorList 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpService6404","name":"HttpService","data":
          {"$area":127.0,"$color":74.01575,"path":
            "org/apache/http/protocol/HttpService.html#HttpService","title":
            "HttpService 127 Elements, 74% Coverage"},"children":[]},{"id":
          "HttpService.HttpRequestHandlerResolverAdapter6531","name":
          "HttpService.HttpRequestHandlerResolverAdapter","data":{"$area":
            4.0,"$color":0.0,"path":
            "org/apache/http/protocol/HttpService.html#HttpService.HttpRequestHandlerResolverAdapter",
            "title":
            "HttpService.HttpRequestHandlerResolverAdapter 4 Elements, 0% Coverage"},
          "children":[]},{"id":"ImmutableHttpProcessor6535","name":
          "ImmutableHttpProcessor","data":{"$area":62.0,"$color":41.935482,
            "path":
            "org/apache/http/protocol/ImmutableHttpProcessor.html#ImmutableHttpProcessor",
            "title":"ImmutableHttpProcessor 62 Elements, 41.9% Coverage"},
          "children":[]},{"id":"RequestConnControl6597","name":
          "RequestConnControl","data":{"$area":13.0,"$color":100.0,"path":
            "org/apache/http/protocol/RequestConnControl.html#RequestConnControl",
            "title":"RequestConnControl 13 Elements, 100% Coverage"},
          "children":[]},{"id":"RequestContent6610","name":
          "RequestContent","data":{"$area":47.0,"$color":100.0,"path":
            "org/apache/http/protocol/RequestContent.html#RequestContent",
            "title":"RequestContent 47 Elements, 100% Coverage"},"children":[]},
        {"id":"RequestDate6657","name":"RequestDate","data":{"$area":9.0,
            "$color":100.0,"path":
            "org/apache/http/protocol/RequestDate.html#RequestDate","title":
            "RequestDate 9 Elements, 100% Coverage"},"children":[]},{"id":
          "RequestExpectContinue6666","name":"RequestExpectContinue","data":{
            "$area":20.0,"$color":90.0,"path":
            "org/apache/http/protocol/RequestExpectContinue.html#RequestExpectContinue",
            "title":"RequestExpectContinue 20 Elements, 90% Coverage"},
          "children":[]},{"id":"RequestTargetHost6686","name":
          "RequestTargetHost","data":{"$area":37.0,"$color":100.0,"path":
            "org/apache/http/protocol/RequestTargetHost.html#RequestTargetHost",
            "title":"RequestTargetHost 37 Elements, 100% Coverage"},
          "children":[]},{"id":"RequestUserAgent6723","name":
          "RequestUserAgent","data":{"$area":24.0,"$color":91.66667,"path":
            "org/apache/http/protocol/RequestUserAgent.html#RequestUserAgent",
            "title":"RequestUserAgent 24 Elements, 91.7% Coverage"},
          "children":[]},{"id":"ResponseConnControl6747","name":
          "ResponseConnControl","data":{"$area":39.0,"$color":100.0,"path":
            "org/apache/http/protocol/ResponseConnControl.html#ResponseConnControl",
            "title":"ResponseConnControl 39 Elements, 100% Coverage"},
          "children":[]},{"id":"ResponseContent6786","name":
          "ResponseContent","data":{"$area":47.0,"$color":100.0,"path":
            "org/apache/http/protocol/ResponseContent.html#ResponseContent",
            "title":"ResponseContent 47 Elements, 100% Coverage"},"children":
          []},{"id":"ResponseDate6833","name":"ResponseDate","data":{"$area":
            10.0,"$color":100.0,"path":
            "org/apache/http/protocol/ResponseDate.html#ResponseDate",
            "title":"ResponseDate 10 Elements, 100% Coverage"},"children":[]},
        {"id":"ResponseServer6843","name":"ResponseServer","data":{"$area":
            14.0,"$color":100.0,"path":
            "org/apache/http/protocol/ResponseServer.html#ResponseServer",
            "title":"ResponseServer 14 Elements, 100% Coverage"},"children":[]},
        {"id":"SyncBasicHttpContext6857","name":"SyncBasicHttpContext",
          "data":{"$area":12.0,"$color":0.0,"path":
            "org/apache/http/protocol/SyncBasicHttpContext.html#SyncBasicHttpContext",
            "title":"SyncBasicHttpContext 12 Elements, 0% Coverage"},
          "children":[]},{"id":"UriHttpRequestHandlerMapper6869","name":
          "UriHttpRequestHandlerMapper","data":{"$area":27.0,"$color":
            92.59259,"path":
            "org/apache/http/protocol/UriHttpRequestHandlerMapper.html#UriHttpRequestHandlerMapper",
            "title":
            "UriHttpRequestHandlerMapper 27 Elements, 92.6% Coverage"},
          "children":[]},{"id":"UriPatternMatcher6896","name":
          "UriPatternMatcher","data":{"$area":47.0,"$color":70.21276,"path":
            "org/apache/http/protocol/UriPatternMatcher.html#UriPatternMatcher",
            "title":"UriPatternMatcher 47 Elements, 70.2% Coverage"},
          "children":[]}]},{"id":"org.apache.http.impl.io1981","name":
      "org.apache.http.impl.io","data":{"$area":1502.0,"$color":61.31824,
        "title":"org.apache.http.impl.io 1502 Elements, 61.3% Coverage"},
      "children":[{"id":"AbstractMessageParser1981","name":
          "AbstractMessageParser","data":{"$area":99.0,"$color":84.84849,
            "path":
            "org/apache/http/impl/io/AbstractMessageParser.html#AbstractMessageParser",
            "title":"AbstractMessageParser 99 Elements, 84.8% Coverage"},
          "children":[]},{"id":"AbstractMessageWriter2080","name":
          "AbstractMessageWriter","data":{"$area":25.0,"$color":64.0,"path":
            "org/apache/http/impl/io/AbstractMessageWriter.html#AbstractMessageWriter",
            "title":"AbstractMessageWriter 25 Elements, 64% Coverage"},
          "children":[]},{"id":"AbstractSessionInputBuffer2105","name":
          "AbstractSessionInputBuffer","data":{"$area":246.0,"$color":0.0,
            "path":
            "org/apache/http/impl/io/AbstractSessionInputBuffer.html#AbstractSessionInputBuffer",
            "title":"AbstractSessionInputBuffer 246 Elements, 0% Coverage"},
          "children":[]},{"id":"AbstractSessionOutputBuffer2351","name":
          "AbstractSessionOutputBuffer","data":{"$area":175.0,"$color":0.0,
            "path":
            "org/apache/http/impl/io/AbstractSessionOutputBuffer.html#AbstractSessionOutputBuffer",
            "title":
            "AbstractSessionOutputBuffer 175 Elements, 0% Coverage"},
          "children":[]},{"id":"ChunkedInputStream2526","name":
          "ChunkedInputStream","data":{"$area":139.0,"$color":94.96403,
            "path":
            "org/apache/http/impl/io/ChunkedInputStream.html#ChunkedInputStream",
            "title":"ChunkedInputStream 139 Elements, 95% Coverage"},
          "children":[]},{"id":"ChunkedOutputStream2665","name":
          "ChunkedOutputStream","data":{"$area":66.0,"$color":96.969696,
            "path":
            "org/apache/http/impl/io/ChunkedOutputStream.html#ChunkedOutputStream",
            "title":"ChunkedOutputStream 66 Elements, 97% Coverage"},
          "children":[]},{"id":"ContentLengthInputStream2731","name":
          "ContentLengthInputStream","data":{"$area":87.0,"$color":
            93.10345,"path":
            "org/apache/http/impl/io/ContentLengthInputStream.html#ContentLengthInputStream",
            "title":"ContentLengthInputStream 87 Elements, 93.1% Coverage"},
          "children":[]},{"id":"ContentLengthOutputStream2818","name":
          "ContentLengthOutputStream","data":{"$area":39.0,"$color":100.0,
            "path":
            "org/apache/http/impl/io/ContentLengthOutputStream.html#ContentLengthOutputStream",
            "title":"ContentLengthOutputStream 39 Elements, 100% Coverage"},
          "children":[]},{"id":"DefaultHttpRequestParser2857","name":
          "DefaultHttpRequestParser","data":{"$area":24.0,"$color":75.0,
            "path":
            "org/apache/http/impl/io/DefaultHttpRequestParser.html#DefaultHttpRequestParser",
            "title":"DefaultHttpRequestParser 24 Elements, 75% Coverage"},
          "children":[]},{"id":"DefaultHttpRequestParserFactory2881","name":
          "DefaultHttpRequestParserFactory","data":{"$area":12.0,"$color":
            83.33333,"path":
            "org/apache/http/impl/io/DefaultHttpRequestParserFactory.html#DefaultHttpRequestParserFactory",
            "title":
            "DefaultHttpRequestParserFactory 12 Elements, 83.3% Coverage"},
          "children":[]},{"id":"DefaultHttpRequestWriter2893","name":
          "DefaultHttpRequestWriter","data":{"$area":7.0,"$color":71.42857,
            "path":
            "org/apache/http/impl/io/DefaultHttpRequestWriter.html#DefaultHttpRequestWriter",
            "title":"DefaultHttpRequestWriter 7 Elements, 71.4% Coverage"},
          "children":[]},{"id":"DefaultHttpRequestWriterFactory2900","name":
          "DefaultHttpRequestWriterFactory","data":{"$area":9.0,"$color":
            88.88889,"path":
            "org/apache/http/impl/io/DefaultHttpRequestWriterFactory.html#DefaultHttpRequestWriterFactory",
            "title":
            "DefaultHttpRequestWriterFactory 9 Elements, 88.9% Coverage"},
          "children":[]},{"id":"DefaultHttpResponseParser2909","name":
          "DefaultHttpResponseParser","data":{"$area":24.0,"$color":75.0,
            "path":
            "org/apache/http/impl/io/DefaultHttpResponseParser.html#DefaultHttpResponseParser",
            "title":"DefaultHttpResponseParser 24 Elements, 75% Coverage"},
          "children":[]},{"id":"DefaultHttpResponseParserFactory2933","name":
          "DefaultHttpResponseParserFactory","data":{"$area":12.0,"$color":
            83.33333,"path":
            "org/apache/http/impl/io/DefaultHttpResponseParserFactory.html#DefaultHttpResponseParserFactory",
            "title":
            "DefaultHttpResponseParserFactory 12 Elements, 83.3% Coverage"},
          "children":[]},{"id":"DefaultHttpResponseWriter2945","name":
          "DefaultHttpResponseWriter","data":{"$area":7.0,"$color":
            71.42857,"path":
            "org/apache/http/impl/io/DefaultHttpResponseWriter.html#DefaultHttpResponseWriter",
            "title":"DefaultHttpResponseWriter 7 Elements, 71.4% Coverage"},
          "children":[]},{"id":"DefaultHttpResponseWriterFactory2952","name":
          "DefaultHttpResponseWriterFactory","data":{"$area":9.0,"$color":
            88.88889,"path":
            "org/apache/http/impl/io/DefaultHttpResponseWriterFactory.html#DefaultHttpResponseWriterFactory",
            "title":
            "DefaultHttpResponseWriterFactory 9 Elements, 88.9% Coverage"},
          "children":[]},{"id":"HttpRequestParser2961","name":
          "HttpRequestParser","data":{"$area":14.0,"$color":0.0,"path":
            "org/apache/http/impl/io/HttpRequestParser.html#HttpRequestParser",
            "title":"HttpRequestParser 14 Elements, 0% Coverage"},"children":
          []},{"id":"HttpRequestWriter2975","name":"HttpRequestWriter",
          "data":{"$area":5.0,"$color":0.0,"path":
            "org/apache/http/impl/io/HttpRequestWriter.html#HttpRequestWriter",
            "title":"HttpRequestWriter 5 Elements, 0% Coverage"},"children":[]},
        {"id":"HttpResponseParser2980","name":"HttpResponseParser","data":{
            "$area":14.0,"$color":0.0,"path":
            "org/apache/http/impl/io/HttpResponseParser.html#HttpResponseParser",
            "title":"HttpResponseParser 14 Elements, 0% Coverage"},
          "children":[]},{"id":"HttpResponseWriter2994","name":
          "HttpResponseWriter","data":{"$area":5.0,"$color":0.0,"path":
            "org/apache/http/impl/io/HttpResponseWriter.html#HttpResponseWriter",
            "title":"HttpResponseWriter 5 Elements, 0% Coverage"},"children":
          []},{"id":"HttpTransportMetricsImpl2999","name":
          "HttpTransportMetricsImpl","data":{"$area":10.0,"$color":
            60.000004,"path":
            "org/apache/http/impl/io/HttpTransportMetricsImpl.html#HttpTransportMetricsImpl",
            "title":"HttpTransportMetricsImpl 10 Elements, 60% Coverage"},
          "children":[]},{"id":"IdentityInputStream3009","name":
          "IdentityInputStream","data":{"$area":23.0,"$color":91.30435,
            "path":
            "org/apache/http/impl/io/IdentityInputStream.html#IdentityInputStream",
            "title":"IdentityInputStream 23 Elements, 91.3% Coverage"},
          "children":[]},{"id":"IdentityOutputStream3032","name":
          "IdentityOutputStream","data":{"$area":25.0,"$color":100.0,"path":
            "org/apache/http/impl/io/IdentityOutputStream.html#IdentityOutputStream",
            "title":"IdentityOutputStream 25 Elements, 100% Coverage"},
          "children":[]},{"id":"SessionInputBufferImpl3057","name":
          "SessionInputBufferImpl","data":{"$area":236.0,"$color":96.61017,
            "path":
            "org/apache/http/impl/io/SessionInputBufferImpl.html#SessionInputBufferImpl",
            "title":"SessionInputBufferImpl 236 Elements, 96.6% Coverage"},
          "children":[]},{"id":"SessionOutputBufferImpl3293","name":
          "SessionOutputBufferImpl","data":{"$area":145.0,"$color":
            98.62069,"path":
            "org/apache/http/impl/io/SessionOutputBufferImpl.html#SessionOutputBufferImpl",
            "title":"SessionOutputBufferImpl 145 Elements, 98.6% Coverage"},
          "children":[]},{"id":"SocketInputBuffer3438","name":
          "SocketInputBuffer","data":{"$area":33.0,"$color":0.0,"path":
            "org/apache/http/impl/io/SocketInputBuffer.html#SocketInputBuffer",
            "title":"SocketInputBuffer 33 Elements, 0% Coverage"},"children":
          []},{"id":"SocketOutputBuffer3471","name":"SocketOutputBuffer",
          "data":{"$area":12.0,"$color":0.0,"path":
            "org/apache/http/impl/io/SocketOutputBuffer.html#SocketOutputBuffer",
            "title":"SocketOutputBuffer 12 Elements, 0% Coverage"},
          "children":[]}]},{"id":"org.apache.http.pool5358","name":
      "org.apache.http.pool","data":{"$area":544.0,"$color":91.3603,"title":
        "org.apache.http.pool 544 Elements, 91.4% Coverage"},"children":[{
          "id":"AbstractConnPool5358","name":"AbstractConnPool","data":{
            "$area":272.0,"$color":92.27941,"path":
            "org/apache/http/pool/AbstractConnPool.html#AbstractConnPool",
            "title":"AbstractConnPool 272 Elements, 92.3% Coverage"},
          "children":[]},{"id":"ConnFactory5630","name":"ConnFactory","data":
          {"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/pool/ConnFactory.html#ConnFactory","title":
            "ConnFactory 0 Elements,  -  Coverage"},"children":[]},{"id":
          "ConnPool5630","name":"ConnPool","data":{"$area":0.0,"$color":
            -100.0,"path":"org/apache/http/pool/ConnPool.html#ConnPool",
            "title":"ConnPool 0 Elements,  -  Coverage"},"children":[]},{
          "id":"ConnPoolControl5630","name":"ConnPoolControl","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/pool/ConnPoolControl.html#ConnPoolControl",
            "title":"ConnPoolControl 0 Elements,  -  Coverage"},"children":[]},
        {"id":"PoolEntry5630","name":"PoolEntry","data":{"$area":57.0,
            "$color":96.49123,"path":
            "org/apache/http/pool/PoolEntry.html#PoolEntry","title":
            "PoolEntry 57 Elements, 96.5% Coverage"},"children":[]},{"id":
          "PoolEntryFuture5687","name":"PoolEntryFuture","data":{"$area":
            76.0,"$color":84.210526,"path":
            "org/apache/http/pool/PoolEntryFuture.html#PoolEntryFuture",
            "title":"PoolEntryFuture 76 Elements, 84.2% Coverage"},
          "children":[]},{"id":"PoolStats5763","name":"PoolStats","data":{
            "$area":26.0,"$color":53.846157,"path":
            "org/apache/http/pool/PoolStats.html#PoolStats","title":
            "PoolStats 26 Elements, 53.8% Coverage"},"children":[]},{"id":
          "RouteSpecificPool5789","name":"RouteSpecificPool","data":{"$area":
            113.0,"$color":100.0,"path":
            "org/apache/http/pool/RouteSpecificPool.html#RouteSpecificPool",
            "title":"RouteSpecificPool 113 Elements, 100% Coverage"},
          "children":[]}]},{"id":"org.apache.http.impl851","name":
      "org.apache.http.impl","data":{"$area":980.0,"$color":40.612244,
        "title":"org.apache.http.impl 980 Elements, 40.6% Coverage"},
      "children":[{"id":"AbstractHttpClientConnection851","name":
          "AbstractHttpClientConnection","data":{"$area":80.0,"$color":0.0,
            "path":
            "org/apache/http/impl/AbstractHttpClientConnection.html#AbstractHttpClientConnection",
            "title":
            "AbstractHttpClientConnection 80 Elements, 0% Coverage"},
          "children":[]},{"id":"AbstractHttpServerConnection931","name":
          "AbstractHttpServerConnection","data":{"$area":72.0,"$color":0.0,
            "path":
            "org/apache/http/impl/AbstractHttpServerConnection.html#AbstractHttpServerConnection",
            "title":
            "AbstractHttpServerConnection 72 Elements, 0% Coverage"},
          "children":[]},{"id":"BHttpConnectionBase1003","name":
          "BHttpConnectionBase","data":{"$area":200.0,"$color":65.5,"path":
            "org/apache/http/impl/BHttpConnectionBase.html#BHttpConnectionBase",
            "title":"BHttpConnectionBase 200 Elements, 65.5% Coverage"},
          "children":[]},{"id":"DefaultBHttpClientConnection1203","name":
          "DefaultBHttpClientConnection","data":{"$area":55.0,"$color":
            87.27273,"path":
            "org/apache/http/impl/DefaultBHttpClientConnection.html#DefaultBHttpClientConnection",
            "title":
            "DefaultBHttpClientConnection 55 Elements, 87.3% Coverage"},
          "children":[]},{"id":"DefaultBHttpServerConnection1258","name":
          "DefaultBHttpServerConnection","data":{"$area":52.0,"$color":
            82.69231,"path":
            "org/apache/http/impl/DefaultBHttpServerConnection.html#DefaultBHttpServerConnection",
            "title":
            "DefaultBHttpServerConnection 52 Elements, 82.7% Coverage"},
          "children":[]},{"id":"DefaultConnectionReuseStrategy1310","name":
          "DefaultConnectionReuseStrategy","data":{"$area":64.0,"$color":
            96.875,"path":
            "org/apache/http/impl/DefaultConnectionReuseStrategy.html#DefaultConnectionReuseStrategy",
            "title":
            "DefaultConnectionReuseStrategy 64 Elements, 96.9% Coverage"},
          "children":[]},{"id":"DefaultHttpClientConnection1374","name":
          "DefaultHttpClientConnection","data":{"$area":15.0,"$color":0.0,
            "path":
            "org/apache/http/impl/DefaultHttpClientConnection.html#DefaultHttpClientConnection",
            "title":"DefaultHttpClientConnection 15 Elements, 0% Coverage"},
          "children":[]},{"id":"DefaultHttpRequestFactory1389","name":
          "DefaultHttpRequestFactory","data":{"$area":39.0,"$color":
            48.71795,"path":
            "org/apache/http/impl/DefaultHttpRequestFactory.html#DefaultHttpRequestFactory",
            "title":
            "DefaultHttpRequestFactory 39 Elements, 48.7% Coverage"},
          "children":[]},{"id":"DefaultHttpResponseFactory1428","name":
          "DefaultHttpResponseFactory","data":{"$area":15.0,"$color":100.0,
            "path":
            "org/apache/http/impl/DefaultHttpResponseFactory.html#DefaultHttpResponseFactory",
            "title":
            "DefaultHttpResponseFactory 15 Elements, 100% Coverage"},
          "children":[]},{"id":"DefaultHttpServerConnection1443","name":
          "DefaultHttpServerConnection","data":{"$area":19.0,"$color":0.0,
            "path":
            "org/apache/http/impl/DefaultHttpServerConnection.html#DefaultHttpServerConnection",
            "title":"DefaultHttpServerConnection 19 Elements, 0% Coverage"},
          "children":[]},{"id":"EnglishReasonPhraseCatalog1462","name":
          "EnglishReasonPhraseCatalog","data":{"$area":64.0,"$color":100.0,
            "path":
            "org/apache/http/impl/EnglishReasonPhraseCatalog.html#EnglishReasonPhraseCatalog",
            "title":
            "EnglishReasonPhraseCatalog 64 Elements, 100% Coverage"},
          "children":[]},{"id":"HttpConnectionMetricsImpl1526","name":
          "HttpConnectionMetricsImpl","data":{"$area":76.0,"$color":
            15.789473,"path":
            "org/apache/http/impl/HttpConnectionMetricsImpl.html#HttpConnectionMetricsImpl",
            "title":
            "HttpConnectionMetricsImpl 76 Elements, 15.8% Coverage"},
          "children":[]},{"id":"NoConnectionReuseStrategy1602","name":
          "NoConnectionReuseStrategy","data":{"$area":4.0,"$color":100.0,
            "path":
            "org/apache/http/impl/NoConnectionReuseStrategy.html#NoConnectionReuseStrategy",
            "title":"NoConnectionReuseStrategy 4 Elements, 100% Coverage"},
          "children":[]},{"id":"SocketHttpClientConnection1606","name":
          "SocketHttpClientConnection","data":{"$area":112.0,"$color":0.0,
            "path":
            "org/apache/http/impl/SocketHttpClientConnection.html#SocketHttpClientConnection",
            "title":"SocketHttpClientConnection 112 Elements, 0% Coverage"},
          "children":[]},{"id":"SocketHttpServerConnection1718","name":
          "SocketHttpServerConnection","data":{"$area":113.0,"$color":0.0,
            "path":
            "org/apache/http/impl/SocketHttpServerConnection.html#SocketHttpServerConnection",
            "title":"SocketHttpServerConnection 113 Elements, 0% Coverage"},
          "children":[]}]},{"id":"org.apache.http.params5053","name":
      "org.apache.http.params","data":{"$area":305.0,"$color":19.016394,
        "title":"org.apache.http.params 305 Elements, 19% Coverage"},
      "children":[{"id":"AbstractHttpParams5053","name":
          "AbstractHttpParams","data":{"$area":50.0,"$color":14.0,"path":
            "org/apache/http/params/AbstractHttpParams.html#AbstractHttpParams",
            "title":"AbstractHttpParams 50 Elements, 14% Coverage"},
          "children":[]},{"id":"BasicHttpParams5103","name":
          "BasicHttpParams","data":{"$area":44.0,"$color":50.0,"path":
            "org/apache/http/params/BasicHttpParams.html#BasicHttpParams",
            "title":"BasicHttpParams 44 Elements, 50% Coverage"},"children":[]},
        {"id":"CoreConnectionPNames5147","name":"CoreConnectionPNames",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/params/CoreConnectionPNames.html#CoreConnectionPNames",
            "title":"CoreConnectionPNames 0 Elements,  -  Coverage"},
          "children":[]},{"id":"CoreProtocolPNames5147","name":
          "CoreProtocolPNames","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/params/CoreProtocolPNames.html#CoreProtocolPNames",
            "title":"CoreProtocolPNames 0 Elements,  -  Coverage"},
          "children":[]},{"id":"DefaultedHttpParams5147","name":
          "DefaultedHttpParams","data":{"$area":34.0,"$color":76.47059,
            "path":
            "org/apache/http/params/DefaultedHttpParams.html#DefaultedHttpParams",
            "title":"DefaultedHttpParams 34 Elements, 76.5% Coverage"},
          "children":[]},{"id":"HttpAbstractParamBean5181","name":
          "HttpAbstractParamBean","data":{"$area":3.0,"$color":0.0,"path":
            "org/apache/http/params/HttpAbstractParamBean.html#HttpAbstractParamBean",
            "title":"HttpAbstractParamBean 3 Elements, 0% Coverage"},
          "children":[]},{"id":"HttpConnectionParamBean5184","name":
          "HttpConnectionParamBean","data":{"$area":14.0,"$color":0.0,"path":
            
            "org/apache/http/params/HttpConnectionParamBean.html#HttpConnectionParamBean",
            "title":"HttpConnectionParamBean 14 Elements, 0% Coverage"},
          "children":[]},{"id":"HttpConnectionParams5198","name":
          "HttpConnectionParams","data":{"$area":50.0,"$color":0.0,"path":
            "org/apache/http/params/HttpConnectionParams.html#HttpConnectionParams",
            "title":"HttpConnectionParams 50 Elements, 0% Coverage"},
          "children":[]},{"id":"HttpParamConfig5248","name":
          "HttpParamConfig","data":{"$area":11.0,"$color":0.0,"path":
            "org/apache/http/params/HttpParamConfig.html#HttpParamConfig",
            "title":"HttpParamConfig 11 Elements, 0% Coverage"},"children":[]},
        {"id":"HttpParams5259","name":"HttpParams","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/params/HttpParams.html#HttpParams","title":
            "HttpParams 0 Elements,  -  Coverage"},"children":[]},{"id":
          "HttpParamsNames5259","name":"HttpParamsNames","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/params/HttpParamsNames.html#HttpParamsNames",
            "title":"HttpParamsNames 0 Elements,  -  Coverage"},"children":[]},
        {"id":"HttpProtocolParamBean5259","name":"HttpProtocolParamBean",
          "data":{"$area":12.0,"$color":0.0,"path":
            "org/apache/http/params/HttpProtocolParamBean.html#HttpProtocolParamBean",
            "title":"HttpProtocolParamBean 12 Elements, 0% Coverage"},
          "children":[]},{"id":"HttpProtocolParams5271","name":
          "HttpProtocolParams","data":{"$area":69.0,"$color":4.347826,"path":
            
            "org/apache/http/params/HttpProtocolParams.html#HttpProtocolParams",
            "title":"HttpProtocolParams 69 Elements, 4.3% Coverage"},
          "children":[]},{"id":"SyncBasicHttpParams5340","name":
          "SyncBasicHttpParams","data":{"$area":18.0,"$color":0.0,"path":
            "org/apache/http/params/SyncBasicHttpParams.html#SyncBasicHttpParams",
            "title":"SyncBasicHttpParams 18 Elements, 0% Coverage"},
          "children":[]}]},{"id":"org.apache.http.entity422","name":
      "org.apache.http.entity","data":{"$area":429.0,"$color":86.247086,
        "title":"org.apache.http.entity 429 Elements, 86.2% Coverage"},
      "children":[{"id":"AbstractHttpEntity422","name":
          "AbstractHttpEntity","data":{"$area":29.0,"$color":96.55172,"path":
            
            "org/apache/http/entity/AbstractHttpEntity.html#AbstractHttpEntity",
            "title":"AbstractHttpEntity 29 Elements, 96.6% Coverage"},
          "children":[]},{"id":"BasicHttpEntity451","name":
          "BasicHttpEntity","data":{"$area":35.0,"$color":80.0,"path":
            "org/apache/http/entity/BasicHttpEntity.html#BasicHttpEntity",
            "title":"BasicHttpEntity 35 Elements, 80% Coverage"},"children":[]},
        {"id":"BufferedHttpEntity486","name":"BufferedHttpEntity","data":{
            "$area":32.0,"$color":100.0,"path":
            "org/apache/http/entity/BufferedHttpEntity.html#BufferedHttpEntity",
            "title":"BufferedHttpEntity 32 Elements, 100% Coverage"},
          "children":[]},{"id":"ByteArrayEntity518","name":
          "ByteArrayEntity","data":{"$area":44.0,"$color":86.36364,"path":
            "org/apache/http/entity/ByteArrayEntity.html#ByteArrayEntity",
            "title":"ByteArrayEntity 44 Elements, 86.4% Coverage"},
          "children":[]},{"id":"ContentLengthStrategy562","name":
          "ContentLengthStrategy","data":{"$area":0.0,"$color":-100.0,"path":
            
            "org/apache/http/entity/ContentLengthStrategy.html#ContentLengthStrategy",
            "title":"ContentLengthStrategy 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ContentProducer562","name":
          "ContentProducer","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/entity/ContentProducer.html#ContentProducer",
            "title":"ContentProducer 0 Elements,  -  Coverage"},"children":[]},
        {"id":"ContentType562","name":"ContentType","data":{"$area":77.0,
            "$color":96.1039,"path":
            "org/apache/http/entity/ContentType.html#ContentType","title":
            "ContentType 77 Elements, 96.1% Coverage"},"children":[]},{"id":
          "EntityTemplate639","name":"EntityTemplate","data":{"$area":16.0,
            "$color":100.0,"path":
            "org/apache/http/entity/EntityTemplate.html#EntityTemplate",
            "title":"EntityTemplate 16 Elements, 100% Coverage"},"children":[]},
        {"id":"FileEntity655","name":"FileEntity","data":{"$area":36.0,
            "$color":66.66667,"path":
            "org/apache/http/entity/FileEntity.html#FileEntity","title":
            "FileEntity 36 Elements, 66.7% Coverage"},"children":[]},{"id":
          "HttpEntityWrapper691","name":"HttpEntityWrapper","data":{"$area":
            21.0,"$color":90.47619,"path":
            "org/apache/http/entity/HttpEntityWrapper.html#HttpEntityWrapper",
            "title":"HttpEntityWrapper 21 Elements, 90.5% Coverage"},
          "children":[]},{"id":"InputStreamEntity712","name":
          "InputStreamEntity","data":{"$area":45.0,"$color":86.666664,"path":
            
            "org/apache/http/entity/InputStreamEntity.html#InputStreamEntity",
            "title":"InputStreamEntity 45 Elements, 86.7% Coverage"},
          "children":[]},{"id":"SerializableEntity757","name":
          "SerializableEntity","data":{"$area":44.0,"$color":90.909096,
            "path":
            "org/apache/http/entity/SerializableEntity.html#SerializableEntity",
            "title":"SerializableEntity 44 Elements, 90.9% Coverage"},
          "children":[]},{"id":"StringEntity801","name":"StringEntity",
          "data":{"$area":50.0,"$color":64.0,"path":
            "org/apache/http/entity/StringEntity.html#StringEntity","title":
            "StringEntity 50 Elements, 64% Coverage"},"children":[]}]},{"id":
      "org.apache.http.annotation186","name":"org.apache.http.annotation",
      "data":{"$area":0.0,"$color":-100.0,"title":
        "org.apache.http.annotation 0 Elements,  -  Coverage"},"children":[{
          "id":"GuardedBy186","name":"GuardedBy","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/annotation/GuardedBy.html#GuardedBy","title":
            "GuardedBy 0 Elements,  -  Coverage"},"children":[]},{"id":
          "Immutable186","name":"Immutable","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/annotation/Immutable.html#Immutable","title":
            "Immutable 0 Elements,  -  Coverage"},"children":[]},{"id":
          "NotThreadSafe186","name":"NotThreadSafe","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/annotation/NotThreadSafe.html#NotThreadSafe",
            "title":"NotThreadSafe 0 Elements,  -  Coverage"},"children":[]},
        {"id":"ThreadSafe186","name":"ThreadSafe","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/annotation/ThreadSafe.html#ThreadSafe","title":
            "ThreadSafe 0 Elements,  -  Coverage"},"children":[]}]},{"id":
      "org.apache.http.impl.pool3483","name":"org.apache.http.impl.pool",
      "data":{"$area":94.0,"$color":55.31915,"title":
        "org.apache.http.impl.pool 94 Elements, 55.3% Coverage"},"children":[
        {"id":"BasicConnFactory3483","name":"BasicConnFactory","data":{
            "$area":77.0,"$color":55.84416,"path":
            "org/apache/http/impl/pool/BasicConnFactory.html#BasicConnFactory",
            "title":"BasicConnFactory 77 Elements, 55.8% Coverage"},
          "children":[]},{"id":"BasicConnPool3560","name":"BasicConnPool",
          "data":{"$area":10.0,"$color":40.0,"path":
            "org/apache/http/impl/pool/BasicConnPool.html#BasicConnPool",
            "title":"BasicConnPool 10 Elements, 40% Coverage"},"children":[]},
        {"id":"BasicPoolEntry3570","name":"BasicPoolEntry","data":{"$area":
            7.0,"$color":71.42857,"path":
            "org/apache/http/impl/pool/BasicPoolEntry.html#BasicPoolEntry",
            "title":"BasicPoolEntry 7 Elements, 71.4% Coverage"},"children":[]}]},
    {"id":"org.apache.http.config271","name":"org.apache.http.config","data":
      {"$area":151.0,"$color":52.317883,"title":
        "org.apache.http.config 151 Elements, 52.3% Coverage"},"children":[{
          "id":"ConnectionConfig271","name":"ConnectionConfig","data":{
            "$area":22.0,"$color":63.636364,"path":
            "org/apache/http/config/ConnectionConfig.html#ConnectionConfig",
            "title":"ConnectionConfig 22 Elements, 63.6% Coverage"},
          "children":[]},{"id":"ConnectionConfig.Builder293","name":
          "ConnectionConfig.Builder","data":{"$area":30.0,"$color":20.0,
            "path":
            "org/apache/http/config/ConnectionConfig.html#ConnectionConfig.Builder",
            "title":"ConnectionConfig.Builder 30 Elements, 20% Coverage"},
          "children":[]},{"id":"Lookup323","name":"Lookup","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/config/Lookup.html#Lookup","title":
            "Lookup 0 Elements,  -  Coverage"},"children":[]},{"id":
          "MessageConstraints323","name":"MessageConstraints","data":{
            "$area":18.0,"$color":55.555557,"path":
            "org/apache/http/config/MessageConstraints.html#MessageConstraints",
            "title":"MessageConstraints 18 Elements, 55.6% Coverage"},
          "children":[]},{"id":"MessageConstraints.Builder341","name":
          "MessageConstraints.Builder","data":{"$area":11.0,"$color":
            45.454548,"path":
            "org/apache/http/config/MessageConstraints.html#MessageConstraints.Builder",
            "title":
            "MessageConstraints.Builder 11 Elements, 45.5% Coverage"},
          "children":[]},{"id":"Registry352","name":"Registry","data":{
            "$area":11.0,"$color":81.818184,"path":
            "org/apache/http/config/Registry.html#Registry","title":
            "Registry 11 Elements, 81.8% Coverage"},"children":[]},{"id":
          "RegistryBuilder363","name":"RegistryBuilder","data":{"$area":
            14.0,"$color":85.71429,"path":
            "org/apache/http/config/RegistryBuilder.html#RegistryBuilder",
            "title":"RegistryBuilder 14 Elements, 85.7% Coverage"},
          "children":[]},{"id":"SocketConfig377","name":"SocketConfig",
          "data":{"$area":25.0,"$color":60.000004,"path":
            "org/apache/http/config/SocketConfig.html#SocketConfig","title":
            "SocketConfig 25 Elements, 60% Coverage"},"children":[]},{"id":
          "SocketConfig.Builder402","name":"SocketConfig.Builder","data":{
            "$area":20.0,"$color":40.0,"path":
            "org/apache/http/config/SocketConfig.html#SocketConfig.Builder",
            "title":"SocketConfig.Builder 20 Elements, 40% Coverage"},
          "children":[]}]},{"id":"org.apache.http.util6943","name":
      "org.apache.http.util","data":{"$area":738.0,"$color":72.22222,"title":
        "org.apache.http.util 738 Elements, 72.2% Coverage"},"children":[{
          "id":"Args6943","name":"Args","data":{"$area":65.0,"$color":
            100.0,"path":"org/apache/http/util/Args.html#Args","title":
            "Args 65 Elements, 100% Coverage"},"children":[]},{"id":
          "Asserts7008","name":"Asserts","data":{"$area":20.0,"$color":
            90.0,"path":"org/apache/http/util/Asserts.html#Asserts","title":
            "Asserts 20 Elements, 90% Coverage"},"children":[]},{"id":
          "ByteArrayBuffer7028","name":"ByteArrayBuffer","data":{"$area":
            126.0,"$color":98.4127,"path":
            "org/apache/http/util/ByteArrayBuffer.html#ByteArrayBuffer",
            "title":"ByteArrayBuffer 126 Elements, 98.4% Coverage"},
          "children":[]},{"id":"CharArrayBuffer7154","name":
          "CharArrayBuffer","data":{"$area":179.0,"$color":97.765366,"path":
            "org/apache/http/util/CharArrayBuffer.html#CharArrayBuffer",
            "title":"CharArrayBuffer 179 Elements, 97.8% Coverage"},
          "children":[]},{"id":"CharsetUtils7333","name":"CharsetUtils",
          "data":{"$area":16.0,"$color":0.0,"path":
            "org/apache/http/util/CharsetUtils.html#CharsetUtils","title":
            "CharsetUtils 16 Elements, 0% Coverage"},"children":[]},{"id":
          "EncodingUtils7349","name":"EncodingUtils","data":{"$area":29.0,
            "$color":89.655174,"path":
            "org/apache/http/util/EncodingUtils.html#EncodingUtils","title":
            "EncodingUtils 29 Elements, 89.7% Coverage"},"children":[]},{
          "id":"EntityUtils7378","name":"EntityUtils","data":{"$area":
            113.0,"$color":62.83186,"path":
            "org/apache/http/util/EntityUtils.html#EntityUtils","title":
            "EntityUtils 113 Elements, 62.8% Coverage"},"children":[]},{"id":
          "ExceptionUtils7491","name":"ExceptionUtils","data":{"$area":
            12.0,"$color":0.0,"path":
            "org/apache/http/util/ExceptionUtils.html#ExceptionUtils",
            "title":"ExceptionUtils 12 Elements, 0% Coverage"},"children":[]},
        {"id":"LangUtils7503","name":"LangUtils","data":{"$area":36.0,
            "$color":97.22222,"path":
            "org/apache/http/util/LangUtils.html#LangUtils","title":
            "LangUtils 36 Elements, 97.2% Coverage"},"children":[]},{"id":
          "NetUtils7539","name":"NetUtils","data":{"$area":12.0,"$color":
            0.0,"path":"org/apache/http/util/NetUtils.html#NetUtils","title":
            "NetUtils 12 Elements, 0% Coverage"},"children":[]},{"id":
          "TextUtils7551","name":"TextUtils","data":{"$area":19.0,"$color":
            100.0,"path":"org/apache/http/util/TextUtils.html#TextUtils",
            "title":"TextUtils 19 Elements, 100% Coverage"},"children":[]},{
          "id":"VersionInfo7570","name":"VersionInfo","data":{"$area":
            111.0,"$color":0.0,"path":
            "org/apache/http/util/VersionInfo.html#VersionInfo","title":
            "VersionInfo 111 Elements, 0% Coverage"},"children":[]}]},{"id":
      "org.apache.http0","name":"org.apache.http","data":{"$area":186.0,
        "$color":89.24731,"title":
        "org.apache.http 186 Elements, 89.2% Coverage"},"children":[{"id":
          "ConnectionClosedException0","name":"ConnectionClosedException",
          "data":{"$area":2.0,"$color":100.0,"path":
            "org/apache/http/ConnectionClosedException.html#ConnectionClosedException",
            "title":"ConnectionClosedException 2 Elements, 100% Coverage"},
          "children":[]},{"id":"ConnectionReuseStrategy2","name":
          "ConnectionReuseStrategy","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/ConnectionReuseStrategy.html#ConnectionReuseStrategy",
            "title":"ConnectionReuseStrategy 0 Elements,  -  Coverage"},
          "children":[]},{"id":"Consts2","name":"Consts","data":{"$area":
            1.0,"$color":0.0,"path":"org/apache/http/Consts.html#Consts",
            "title":"Consts 1 Elements, 0% Coverage"},"children":[]},{"id":
          "ContentTooLongException3","name":"ContentTooLongException","data":
          {"$area":2.0,"$color":0.0,"path":
            "org/apache/http/ContentTooLongException.html#ContentTooLongException",
            "title":"ContentTooLongException 2 Elements, 0% Coverage"},
          "children":[]},{"id":"FormattedHeader5","name":"FormattedHeader",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/FormattedHeader.html#FormattedHeader","title":
            "FormattedHeader 0 Elements,  -  Coverage"},"children":[]},{"id":
          "Header5","name":"Header","data":{"$area":0.0,"$color":-100.0,
            "path":"org/apache/http/Header.html#Header","title":
            "Header 0 Elements,  -  Coverage"},"children":[]},{"id":
          "HeaderElement5","name":"HeaderElement","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/HeaderElement.html#HeaderElement","title":
            "HeaderElement 0 Elements,  -  Coverage"},"children":[]},{"id":
          "HeaderElementIterator5","name":"HeaderElementIterator","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/HeaderElementIterator.html#HeaderElementIterator",
            "title":"HeaderElementIterator 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HeaderIterator5","name":"HeaderIterator",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/HeaderIterator.html#HeaderIterator","title":
            "HeaderIterator 0 Elements,  -  Coverage"},"children":[]},{"id":
          "HttpClientConnection5","name":"HttpClientConnection","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/HttpClientConnection.html#HttpClientConnection",
            "title":"HttpClientConnection 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpConnection5","name":"HttpConnection",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/HttpConnection.html#HttpConnection","title":
            "HttpConnection 0 Elements,  -  Coverage"},"children":[]},{"id":
          "HttpConnectionMetrics5","name":"HttpConnectionMetrics","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/HttpConnectionMetrics.html#HttpConnectionMetrics",
            "title":"HttpConnectionMetrics 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpEntity5","name":"HttpEntity","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/HttpEntity.html#HttpEntity","title":
            "HttpEntity 0 Elements,  -  Coverage"},"children":[]},{"id":
          "HttpEntityEnclosingRequest5","name":
          "HttpEntityEnclosingRequest","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/HttpEntityEnclosingRequest.html#HttpEntityEnclosingRequest",
            "title":"HttpEntityEnclosingRequest 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpException5","name":"HttpException",
          "data":{"$area":7.0,"$color":100.0,"path":
            "org/apache/http/HttpException.html#HttpException","title":
            "HttpException 7 Elements, 100% Coverage"},"children":[]},{"id":
          "HttpHeaders12","name":"HttpHeaders","data":{"$area":1.0,"$color":
            0.0,"path":"org/apache/http/HttpHeaders.html#HttpHeaders",
            "title":"HttpHeaders 1 Elements, 0% Coverage"},"children":[]},{
          "id":"HttpHost13","name":"HttpHost","data":{"$area":64.0,"$color":
            96.875,"path":"org/apache/http/HttpHost.html#HttpHost","title":
            "HttpHost 64 Elements, 96.9% Coverage"},"children":[]},{"id":
          "HttpInetConnection77","name":"HttpInetConnection","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/HttpInetConnection.html#HttpInetConnection",
            "title":"HttpInetConnection 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpMessage77","name":"HttpMessage","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/HttpMessage.html#HttpMessage","title":
            "HttpMessage 0 Elements,  -  Coverage"},"children":[]},{"id":
          "HttpRequest77","name":"HttpRequest","data":{"$area":0.0,"$color":
            -100.0,"path":"org/apache/http/HttpRequest.html#HttpRequest",
            "title":"HttpRequest 0 Elements,  -  Coverage"},"children":[]},{
          "id":"HttpRequestFactory77","name":"HttpRequestFactory","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/HttpRequestFactory.html#HttpRequestFactory",
            "title":"HttpRequestFactory 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpRequestInterceptor77","name":
          "HttpRequestInterceptor","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/HttpRequestInterceptor.html#HttpRequestInterceptor",
            "title":"HttpRequestInterceptor 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpResponse77","name":"HttpResponse","data":
          {"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/HttpResponse.html#HttpResponse","title":
            "HttpResponse 0 Elements,  -  Coverage"},"children":[]},{"id":
          "HttpResponseFactory77","name":"HttpResponseFactory","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/HttpResponseFactory.html#HttpResponseFactory",
            "title":"HttpResponseFactory 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpResponseInterceptor77","name":
          "HttpResponseInterceptor","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/HttpResponseInterceptor.html#HttpResponseInterceptor",
            "title":"HttpResponseInterceptor 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpServerConnection77","name":
          "HttpServerConnection","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/HttpServerConnection.html#HttpServerConnection",
            "title":"HttpServerConnection 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpStatus77","name":"HttpStatus","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/HttpStatus.html#HttpStatus","title":
            "HttpStatus 0 Elements,  -  Coverage"},"children":[]},{"id":
          "HttpVersion77","name":"HttpVersion","data":{"$area":23.0,"$color":
            82.608696,"path":
            "org/apache/http/HttpVersion.html#HttpVersion","title":
            "HttpVersion 23 Elements, 82.6% Coverage"},"children":[]},{"id":
          "MalformedChunkCodingException100","name":
          "MalformedChunkCodingException","data":{"$area":4.0,"$color":
            100.0,"path":
            "org/apache/http/MalformedChunkCodingException.html#MalformedChunkCodingException",
            "title":
            "MalformedChunkCodingException 4 Elements, 100% Coverage"},
          "children":[]},{"id":"MessageConstraintException104","name":
          "MessageConstraintException","data":{"$area":2.0,"$color":100.0,
            "path":
            "org/apache/http/MessageConstraintException.html#MessageConstraintException",
            "title":"MessageConstraintException 2 Elements, 100% Coverage"},
          "children":[]},{"id":"MethodNotSupportedException106","name":
          "MethodNotSupportedException","data":{"$area":4.0,"$color":100.0,
            "path":
            "org/apache/http/MethodNotSupportedException.html#MethodNotSupportedException",
            "title":
            "MethodNotSupportedException 4 Elements, 100% Coverage"},
          "children":[]},{"id":"NameValuePair110","name":"NameValuePair",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/NameValuePair.html#NameValuePair","title":
            "NameValuePair 0 Elements,  -  Coverage"},"children":[]},{"id":
          "NoHttpResponseException110","name":"NoHttpResponseException",
          "data":{"$area":2.0,"$color":100.0,"path":
            "org/apache/http/NoHttpResponseException.html#NoHttpResponseException",
            "title":"NoHttpResponseException 2 Elements, 100% Coverage"},
          "children":[]},{"id":"ParseException112","name":"ParseException",
          "data":{"$area":4.0,"$color":50.0,"path":
            "org/apache/http/ParseException.html#ParseException","title":
            "ParseException 4 Elements, 50% Coverage"},"children":[]},{"id":
          "ProtocolException116","name":"ProtocolException","data":{"$area":
            6.0,"$color":100.0,"path":
            "org/apache/http/ProtocolException.html#ProtocolException",
            "title":"ProtocolException 6 Elements, 100% Coverage"},
          "children":[]},{"id":"ProtocolVersion122","name":
          "ProtocolVersion","data":{"$area":58.0,"$color":86.206894,"path":
            "org/apache/http/ProtocolVersion.html#ProtocolVersion","title":
            "ProtocolVersion 58 Elements, 86.2% Coverage"},"children":[]},{
          "id":"ReasonPhraseCatalog180","name":"ReasonPhraseCatalog","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/ReasonPhraseCatalog.html#ReasonPhraseCatalog",
            "title":"ReasonPhraseCatalog 0 Elements,  -  Coverage"},
          "children":[]},{"id":"RequestLine180","name":"RequestLine","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/RequestLine.html#RequestLine","title":
            "RequestLine 0 Elements,  -  Coverage"},"children":[]},{"id":
          "StatusLine180","name":"StatusLine","data":{"$area":0.0,"$color":
            -100.0,"path":"org/apache/http/StatusLine.html#StatusLine",
            "title":"StatusLine 0 Elements,  -  Coverage"},"children":[]},{
          "id":"TokenIterator180","name":"TokenIterator","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/TokenIterator.html#TokenIterator","title":
            "TokenIterator 0 Elements,  -  Coverage"},"children":[]},{"id":
          "TruncatedChunkException180","name":"TruncatedChunkException",
          "data":{"$area":2.0,"$color":100.0,"path":
            "org/apache/http/TruncatedChunkException.html#TruncatedChunkException",
            "title":"TruncatedChunkException 2 Elements, 100% Coverage"},
          "children":[]},{"id":"UnsupportedHttpVersionException182","name":
          "UnsupportedHttpVersionException","data":{"$area":4.0,"$color":
            100.0,"path":
            "org/apache/http/UnsupportedHttpVersionException.html#UnsupportedHttpVersionException",
            "title":
            "UnsupportedHttpVersionException 4 Elements, 100% Coverage"},
          "children":[]}]},{"id":"org.apache.http.concurrent186","name":
      "org.apache.http.concurrent","data":{"$area":85.0,"$color":91.76471,
        "title":"org.apache.http.concurrent 85 Elements, 91.8% Coverage"},
      "children":[{"id":"BasicFuture186","name":"BasicFuture","data":{
            "$area":85.0,"$color":91.76471,"path":
            "org/apache/http/concurrent/BasicFuture.html#BasicFuture",
            "title":"BasicFuture 85 Elements, 91.8% Coverage"},"children":[]},
        {"id":"Cancellable271","name":"Cancellable","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/concurrent/Cancellable.html#Cancellable",
            "title":"Cancellable 0 Elements,  -  Coverage"},"children":[]},{
          "id":"FutureCallback271","name":"FutureCallback","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/concurrent/FutureCallback.html#FutureCallback",
            "title":"FutureCallback 0 Elements,  -  Coverage"},"children":[]}]}]}

 ); 