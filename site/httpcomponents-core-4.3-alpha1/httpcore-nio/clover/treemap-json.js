processTreeMapJson (  {"id":"Clover database Wed Nov 28 2012 22:44:43 CET0","name":"","data":{
    "$area":8684.0,"$color":66.68586,"title":
    " 8684 Elements, 66.7% Coverage"},"children":[{"id":
      "org.apache.http.impl.nio0","name":"org.apache.http.impl.nio","data":{
        "$area":1089.0,"$color":38.934803,"title":
        "org.apache.http.impl.nio 1089 Elements, 38.9% Coverage"},"children":
      [{"id":"DefaultClientIOEventDispatch0","name":
          "DefaultClientIOEventDispatch","data":{"$area":28.0,"$color":0.0,
            "path":
            "org/apache/http/impl/nio/DefaultClientIOEventDispatch.html#DefaultClientIOEventDispatch",
            "title":
            "DefaultClientIOEventDispatch 28 Elements, 0% Coverage"},
          "children":[]},{"id":"DefaultHttpClientIODispatch28","name":
          "DefaultHttpClientIODispatch","data":{"$area":35.0,"$color":
            45.714287,"path":
            "org/apache/http/impl/nio/DefaultHttpClientIODispatch.html#DefaultHttpClientIODispatch",
            "title":
            "DefaultHttpClientIODispatch 35 Elements, 45.7% Coverage"},
          "children":[]},{"id":"DefaultHttpServerIODispatch63","name":
          "DefaultHttpServerIODispatch","data":{"$area":34.0,"$color":
            55.88235,"path":
            "org/apache/http/impl/nio/DefaultHttpServerIODispatch.html#DefaultHttpServerIODispatch",
            "title":
            "DefaultHttpServerIODispatch 34 Elements, 55.9% Coverage"},
          "children":[]},{"id":"DefaultNHttpClientConnection97","name":
          "DefaultNHttpClientConnection","data":{"$area":152.0,"$color":
            72.368416,"path":
            "org/apache/http/impl/nio/DefaultNHttpClientConnection.html#DefaultNHttpClientConnection",
            "title":
            "DefaultNHttpClientConnection 152 Elements, 72.4% Coverage"},
          "children":[]},{"id":"DefaultNHttpClientConnectionFactory249",
          "name":"DefaultNHttpClientConnectionFactory","data":{"$area":
            43.0,"$color":46.51163,"path":
            "org/apache/http/impl/nio/DefaultNHttpClientConnectionFactory.html#DefaultNHttpClientConnectionFactory",
            "title":
            "DefaultNHttpClientConnectionFactory 43 Elements, 46.5% Coverage"},
          "children":[]},{"id":"DefaultNHttpServerConnection292","name":
          "DefaultNHttpServerConnection","data":{"$area":156.0,"$color":
            77.5641,"path":
            "org/apache/http/impl/nio/DefaultNHttpServerConnection.html#DefaultNHttpServerConnection",
            "title":
            "DefaultNHttpServerConnection 156 Elements, 77.6% Coverage"},
          "children":[]},{"id":"DefaultNHttpServerConnectionFactory448",
          "name":"DefaultNHttpServerConnectionFactory","data":{"$area":
            43.0,"$color":20.930233,"path":
            "org/apache/http/impl/nio/DefaultNHttpServerConnectionFactory.html#DefaultNHttpServerConnectionFactory",
            "title":
            "DefaultNHttpServerConnectionFactory 43 Elements, 20.9% Coverage"},
          "children":[]},{"id":"DefaultServerIOEventDispatch491","name":
          "DefaultServerIOEventDispatch","data":{"$area":27.0,"$color":0.0,
            "path":
            "org/apache/http/impl/nio/DefaultServerIOEventDispatch.html#DefaultServerIOEventDispatch",
            "title":
            "DefaultServerIOEventDispatch 27 Elements, 0% Coverage"},
          "children":[]},{"id":"NHttpClientEventHandlerAdaptor518","name":
          "NHttpClientEventHandlerAdaptor","data":{"$area":33.0,"$color":
            0.0,"path":
            "org/apache/http/impl/nio/NHttpClientEventHandlerAdaptor.html#NHttpClientEventHandlerAdaptor",
            "title":
            "NHttpClientEventHandlerAdaptor 33 Elements, 0% Coverage"},
          "children":[]},{"id":"NHttpConnectionBase551","name":
          "NHttpConnectionBase","data":{"$area":233.0,"$color":51.502144,
            "path":
            "org/apache/http/impl/nio/NHttpConnectionBase.html#NHttpConnectionBase",
            "title":"NHttpConnectionBase 233 Elements, 51.5% Coverage"},
          "children":[]},{"id":"NHttpServerEventHandlerAdaptor784","name":
          "NHttpServerEventHandlerAdaptor","data":{"$area":33.0,"$color":
            0.0,"path":
            "org/apache/http/impl/nio/NHttpServerEventHandlerAdaptor.html#NHttpServerEventHandlerAdaptor",
            "title":
            "NHttpServerEventHandlerAdaptor 33 Elements, 0% Coverage"},
          "children":[]},{"id":"SSLClientIOEventDispatch817","name":
          "SSLClientIOEventDispatch","data":{"$area":67.0,"$color":0.0,
            "path":
            "org/apache/http/impl/nio/SSLClientIOEventDispatch.html#SSLClientIOEventDispatch",
            "title":"SSLClientIOEventDispatch 67 Elements, 0% Coverage"},
          "children":[]},{"id":"SSLNHttpClientConnectionFactory884","name":
          "SSLNHttpClientConnectionFactory","data":{"$area":65.0,"$color":
            0.0,"path":
            "org/apache/http/impl/nio/SSLNHttpClientConnectionFactory.html#SSLNHttpClientConnectionFactory",
            "title":
            "SSLNHttpClientConnectionFactory 65 Elements, 0% Coverage"},
          "children":[]},{"id":"SSLNHttpServerConnectionFactory949","name":
          "SSLNHttpServerConnectionFactory","data":{"$area":65.0,"$color":
            0.0,"path":
            "org/apache/http/impl/nio/SSLNHttpServerConnectionFactory.html#SSLNHttpServerConnectionFactory",
            "title":
            "SSLNHttpServerConnectionFactory 65 Elements, 0% Coverage"},
          "children":[]},{"id":"SSLServerIOEventDispatch1014","name":
          "SSLServerIOEventDispatch","data":{"$area":66.0,"$color":0.0,
            "path":
            "org/apache/http/impl/nio/SSLServerIOEventDispatch.html#SSLServerIOEventDispatch",
            "title":"SSLServerIOEventDispatch 66 Elements, 0% Coverage"},
          "children":[]},{"id":"SessionHttpContext1080","name":
          "SessionHttpContext","data":{"$area":9.0,"$color":100.0,"path":
            "org/apache/http/impl/nio/SessionHttpContext.html#SessionHttpContext",
            "title":"SessionHttpContext 9 Elements, 100% Coverage"},
          "children":[]}]},{"id":"org.apache.http.impl.nio.codecs1089",
      "name":"org.apache.http.impl.nio.codecs","data":{"$area":796.0,
        "$color":82.412056,"title":
        "org.apache.http.impl.nio.codecs 796 Elements, 82.4% Coverage"},
      "children":[{"id":"AbstractContentDecoder1089","name":
          "AbstractContentDecoder","data":{"$area":10.0,"$color":100.0,
            "path":
            "org/apache/http/impl/nio/codecs/AbstractContentDecoder.html#AbstractContentDecoder",
            "title":"AbstractContentDecoder 10 Elements, 100% Coverage"},
          "children":[]},{"id":"AbstractContentEncoder1099","name":
          "AbstractContentEncoder","data":{"$area":17.0,"$color":100.0,
            "path":
            "org/apache/http/impl/nio/codecs/AbstractContentEncoder.html#AbstractContentEncoder",
            "title":"AbstractContentEncoder 17 Elements, 100% Coverage"},
          "children":[]},{"id":"AbstractMessageParser1116","name":
          "AbstractMessageParser","data":{"$area":118.0,"$color":89.830505,
            "path":
            "org/apache/http/impl/nio/codecs/AbstractMessageParser.html#AbstractMessageParser",
            "title":"AbstractMessageParser 118 Elements, 89.8% Coverage"},
          "children":[]},{"id":"AbstractMessageWriter1234","name":
          "AbstractMessageWriter","data":{"$area":26.0,"$color":65.38461,
            "path":
            "org/apache/http/impl/nio/codecs/AbstractMessageWriter.html#AbstractMessageWriter",
            "title":"AbstractMessageWriter 26 Elements, 65.4% Coverage"},
          "children":[]},{"id":"ChunkDecoder1260","name":"ChunkDecoder",
          "data":{"$area":176.0,"$color":96.02273,"path":
            "org/apache/http/impl/nio/codecs/ChunkDecoder.html#ChunkDecoder",
            "title":"ChunkDecoder 176 Elements, 96% Coverage"},"children":[]},
        {"id":"ChunkEncoder1436","name":"ChunkEncoder","data":{"$area":
            67.0,"$color":85.07463,"path":
            "org/apache/http/impl/nio/codecs/ChunkEncoder.html#ChunkEncoder",
            "title":"ChunkEncoder 67 Elements, 85.1% Coverage"},"children":[]},
        {"id":"DefaultHttpRequestParser1503","name":
          "DefaultHttpRequestParser","data":{"$area":17.0,"$color":
            76.47059,"path":
            "org/apache/http/impl/nio/codecs/DefaultHttpRequestParser.html#DefaultHttpRequestParser",
            "title":"DefaultHttpRequestParser 17 Elements, 76.5% Coverage"},
          "children":[]},{"id":"DefaultHttpRequestParserFactory1520","name":
          "DefaultHttpRequestParserFactory","data":{"$area":12.0,"$color":
            83.33333,"path":
            "org/apache/http/impl/nio/codecs/DefaultHttpRequestParserFactory.html#DefaultHttpRequestParserFactory",
            "title":
            "DefaultHttpRequestParserFactory 12 Elements, 83.3% Coverage"},
          "children":[]},{"id":"DefaultHttpRequestWriter1532","name":
          "DefaultHttpRequestWriter","data":{"$area":7.0,"$color":71.42857,
            "path":
            "org/apache/http/impl/nio/codecs/DefaultHttpRequestWriter.html#DefaultHttpRequestWriter",
            "title":"DefaultHttpRequestWriter 7 Elements, 71.4% Coverage"},
          "children":[]},{"id":"DefaultHttpRequestWriterFactory1539","name":
          "DefaultHttpRequestWriterFactory","data":{"$area":9.0,"$color":
            88.88889,"path":
            "org/apache/http/impl/nio/codecs/DefaultHttpRequestWriterFactory.html#DefaultHttpRequestWriterFactory",
            "title":
            "DefaultHttpRequestWriterFactory 9 Elements, 88.9% Coverage"},
          "children":[]},{"id":"DefaultHttpResponseParser1548","name":
          "DefaultHttpResponseParser","data":{"$area":17.0,"$color":
            76.47059,"path":
            "org/apache/http/impl/nio/codecs/DefaultHttpResponseParser.html#DefaultHttpResponseParser",
            "title":
            "DefaultHttpResponseParser 17 Elements, 76.5% Coverage"},
          "children":[]},{"id":"DefaultHttpResponseParserFactory1565","name":
          "DefaultHttpResponseParserFactory","data":{"$area":12.0,"$color":
            83.33333,"path":
            "org/apache/http/impl/nio/codecs/DefaultHttpResponseParserFactory.html#DefaultHttpResponseParserFactory",
            "title":
            "DefaultHttpResponseParserFactory 12 Elements, 83.3% Coverage"},
          "children":[]},{"id":"DefaultHttpResponseWriter1577","name":
          "DefaultHttpResponseWriter","data":{"$area":7.0,"$color":
            71.42857,"path":
            "org/apache/http/impl/nio/codecs/DefaultHttpResponseWriter.html#DefaultHttpResponseWriter",
            "title":"DefaultHttpResponseWriter 7 Elements, 71.4% Coverage"},
          "children":[]},{"id":"DefaultHttpResponseWriterFactory1584","name":
          "DefaultHttpResponseWriterFactory","data":{"$area":9.0,"$color":
            88.88889,"path":
            "org/apache/http/impl/nio/codecs/DefaultHttpResponseWriterFactory.html#DefaultHttpResponseWriterFactory",
            "title":
            "DefaultHttpResponseWriterFactory 9 Elements, 88.9% Coverage"},
          "children":[]},{"id":"HttpRequestParser1593","name":
          "HttpRequestParser","data":{"$area":8.0,"$color":0.0,"path":
            "org/apache/http/impl/nio/codecs/HttpRequestParser.html#HttpRequestParser",
            "title":"HttpRequestParser 8 Elements, 0% Coverage"},"children":[]},
        {"id":"HttpRequestWriter1601","name":"HttpRequestWriter","data":{
            "$area":5.0,"$color":0.0,"path":
            "org/apache/http/impl/nio/codecs/HttpRequestWriter.html#HttpRequestWriter",
            "title":"HttpRequestWriter 5 Elements, 0% Coverage"},"children":[]},
        {"id":"HttpResponseParser1606","name":"HttpResponseParser","data":{
            "$area":8.0,"$color":0.0,"path":
            "org/apache/http/impl/nio/codecs/HttpResponseParser.html#HttpResponseParser",
            "title":"HttpResponseParser 8 Elements, 0% Coverage"},"children":
          []},{"id":"HttpResponseWriter1614","name":"HttpResponseWriter",
          "data":{"$area":5.0,"$color":0.0,"path":
            "org/apache/http/impl/nio/codecs/HttpResponseWriter.html#HttpResponseWriter",
            "title":"HttpResponseWriter 5 Elements, 0% Coverage"},"children":
          []},{"id":"IdentityDecoder1619","name":"IdentityDecoder","data":{
            "$area":68.0,"$color":79.411766,"path":
            "org/apache/http/impl/nio/codecs/IdentityDecoder.html#IdentityDecoder",
            "title":"IdentityDecoder 68 Elements, 79.4% Coverage"},
          "children":[]},{"id":"IdentityEncoder1687","name":
          "IdentityEncoder","data":{"$area":32.0,"$color":43.75,"path":
            "org/apache/http/impl/nio/codecs/IdentityEncoder.html#IdentityEncoder",
            "title":"IdentityEncoder 32 Elements, 43.8% Coverage"},
          "children":[]},{"id":"LengthDelimitedDecoder1719","name":
          "LengthDelimitedDecoder","data":{"$area":106.0,"$color":86.79245,
            "path":
            "org/apache/http/impl/nio/codecs/LengthDelimitedDecoder.html#LengthDelimitedDecoder",
            "title":"LengthDelimitedDecoder 106 Elements, 86.8% Coverage"},
          "children":[]},{"id":"LengthDelimitedEncoder1825","name":
          "LengthDelimitedEncoder","data":{"$area":60.0,"$color":80.0,"path":
            
            "org/apache/http/impl/nio/codecs/LengthDelimitedEncoder.html#LengthDelimitedEncoder",
            "title":"LengthDelimitedEncoder 60 Elements, 80% Coverage"},
          "children":[]}]},{"id":"org.apache.http.impl.nio.reactor1947",
      "name":"org.apache.http.impl.nio.reactor","data":{"$area":1991.0,
        "$color":61.828224,"title":
        "org.apache.http.impl.nio.reactor 1991 Elements, 61.8% Coverage"},
      "children":[{"id":"AbstractIODispatch1947","name":
          "AbstractIODispatch","data":{"$area":96.0,"$color":66.66667,"path":
            
            "org/apache/http/impl/nio/reactor/AbstractIODispatch.html#AbstractIODispatch",
            "title":"AbstractIODispatch 96 Elements, 66.7% Coverage"},
          "children":[]},{"id":"AbstractIOReactor2043","name":
          "AbstractIOReactor","data":{"$area":268.0,"$color":65.29851,"path":
            
            "org/apache/http/impl/nio/reactor/AbstractIOReactor.html#AbstractIOReactor",
            "title":"AbstractIOReactor 268 Elements, 65.3% Coverage"},
          "children":[]},{"id":"AbstractMultiworkerIOReactor2311","name":
          "AbstractMultiworkerIOReactor","data":{"$area":236.0,"$color":
            74.57627,"path":
            "org/apache/http/impl/nio/reactor/AbstractMultiworkerIOReactor.html#AbstractMultiworkerIOReactor",
            "title":
            "AbstractMultiworkerIOReactor 236 Elements, 74.6% Coverage"},
          "children":[]},{"id":"AbstractMultiworkerIOReactor.Worker2547",
          "name":"AbstractMultiworkerIOReactor.Worker","data":{"$area":
            10.0,"$color":100.0,"path":
            "org/apache/http/impl/nio/reactor/AbstractMultiworkerIOReactor.html#AbstractMultiworkerIOReactor.Worker",
            "title":
            "AbstractMultiworkerIOReactor.Worker 10 Elements, 100% Coverage"},
          "children":[]},{"id":
          "AbstractMultiworkerIOReactor.DefaultThreadFactory2557","name":
          "AbstractMultiworkerIOReactor.DefaultThreadFactory","data":{
            "$area":2.0,"$color":100.0,"path":
            "org/apache/http/impl/nio/reactor/AbstractMultiworkerIOReactor.html#AbstractMultiworkerIOReactor.DefaultThreadFactory",
            "title":
            "AbstractMultiworkerIOReactor.DefaultThreadFactory 2 Elements, 100% Coverage"},
          "children":[]},{"id":"BaseIOReactor2559","name":"BaseIOReactor",
          "data":{"$area":87.0,"$color":78.16092,"path":
            "org/apache/http/impl/nio/reactor/BaseIOReactor.html#BaseIOReactor",
            "title":"BaseIOReactor 87 Elements, 78.2% Coverage"},"children":[]},
        {"id":"ChannelEntry2646","name":"ChannelEntry","data":{"$area":
            17.0,"$color":100.0,"path":
            "org/apache/http/impl/nio/reactor/ChannelEntry.html#ChannelEntry",
            "title":"ChannelEntry 17 Elements, 100% Coverage"},"children":[]},
        {"id":"DefaultConnectingIOReactor2663","name":
          "DefaultConnectingIOReactor","data":{"$area":140.0,"$color":
            60.000004,"path":
            "org/apache/http/impl/nio/reactor/DefaultConnectingIOReactor.html#DefaultConnectingIOReactor",
            "title":
            "DefaultConnectingIOReactor 140 Elements, 60% Coverage"},
          "children":[]},{"id":"DefaultListeningIOReactor2803","name":
          "DefaultListeningIOReactor","data":{"$area":143.0,"$color":
            60.13986,"path":
            "org/apache/http/impl/nio/reactor/DefaultListeningIOReactor.html#DefaultListeningIOReactor",
            "title":
            "DefaultListeningIOReactor 143 Elements, 60.1% Coverage"},
          "children":[]},{"id":"ExceptionEvent2946","name":
          "ExceptionEvent","data":{"$area":20.0,"$color":60.000004,"path":
            "org/apache/http/impl/nio/reactor/ExceptionEvent.html#ExceptionEvent",
            "title":"ExceptionEvent 20 Elements, 60% Coverage"},"children":[]},
        {"id":"IOReactorConfig2966","name":"IOReactorConfig","data":{"$area":
            75.0,"$color":42.666668,"path":
            "org/apache/http/impl/nio/reactor/IOReactorConfig.html#IOReactorConfig",
            "title":"IOReactorConfig 75 Elements, 42.7% Coverage"},
          "children":[]},{"id":"IOReactorConfig.Builder3037","name":
          "IOReactorConfig.Builder","data":{"$area":43.0,"$color":37.2093,
            "path":
            "org/apache/http/impl/nio/reactor/IOReactorConfig.html#IOReactorConfig.Builder",
            "title":"IOReactorConfig.Builder 43 Elements, 37.2% Coverage"},
          "children":[]},{"id":"IOSessionImpl3084","name":"IOSessionImpl",
          "data":{"$area":202.0,"$color":47.524754,"path":
            "org/apache/http/impl/nio/reactor/IOSessionImpl.html#IOSessionImpl",
            "title":"IOSessionImpl 202 Elements, 47.5% Coverage"},"children":
          []},{"id":"InterestOpEntry3286","name":"InterestOpEntry","data":{
            "$area":13.0,"$color":0.0,"path":
            "org/apache/http/impl/nio/reactor/InterestOpEntry.html#InterestOpEntry",
            "title":"InterestOpEntry 13 Elements, 0% Coverage"},"children":[]},
        {"id":"InterestOpsCallback3299","name":"InterestOpsCallback","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/impl/nio/reactor/InterestOpsCallback.html#InterestOpsCallback",
            "title":"InterestOpsCallback 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ListenerEndpointClosedCallback3299","name":
          "ListenerEndpointClosedCallback","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/impl/nio/reactor/ListenerEndpointClosedCallback.html#ListenerEndpointClosedCallback",
            "title":
            "ListenerEndpointClosedCallback 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ListenerEndpointImpl3299","name":
          "ListenerEndpointImpl","data":{"$area":80.0,"$color":72.5,"path":
            "org/apache/http/impl/nio/reactor/ListenerEndpointImpl.html#ListenerEndpointImpl",
            "title":"ListenerEndpointImpl 80 Elements, 72.5% Coverage"},
          "children":[]},{"id":"SSLIOSession3379","name":"SSLIOSession",
          "data":{"$area":26.0,"$color":0.0,"path":
            "org/apache/http/impl/nio/reactor/SSLIOSession.html#SSLIOSession",
            "title":"SSLIOSession 26 Elements, 0% Coverage"},"children":[]},{
          "id":"SSLIOSessionHandler3405","name":"SSLIOSessionHandler","data":
          {"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/impl/nio/reactor/SSLIOSessionHandler.html#SSLIOSessionHandler",
            "title":"SSLIOSessionHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"SSLIOSessionHandlerAdaptor3405","name":
          "SSLIOSessionHandlerAdaptor","data":{"$area":11.0,"$color":0.0,
            "path":
            "org/apache/http/impl/nio/reactor/SSLIOSessionHandlerAdaptor.html#SSLIOSessionHandlerAdaptor",
            "title":"SSLIOSessionHandlerAdaptor 11 Elements, 0% Coverage"},
          "children":[]},{"id":"SSLMode3416","name":"SSLMode","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/impl/nio/reactor/SSLMode.html#SSLMode","title":
            "SSLMode 0 Elements,  -  Coverage"},"children":[]},{"id":
          "SSLSetupHandler3416","name":"SSLSetupHandler","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/impl/nio/reactor/SSLSetupHandler.html#SSLSetupHandler",
            "title":"SSLSetupHandler 0 Elements,  -  Coverage"},"children":[]},
        {"id":"SSLSetupHandlerAdaptor3416","name":"SSLSetupHandlerAdaptor",
          "data":{"$area":11.0,"$color":0.0,"path":
            "org/apache/http/impl/nio/reactor/SSLSetupHandlerAdaptor.html#SSLSetupHandlerAdaptor",
            "title":"SSLSetupHandlerAdaptor 11 Elements, 0% Coverage"},
          "children":[]},{"id":"SessionClosedCallback3427","name":
          "SessionClosedCallback","data":{"$area":0.0,"$color":-100.0,"path":
            
            "org/apache/http/impl/nio/reactor/SessionClosedCallback.html#SessionClosedCallback",
            "title":"SessionClosedCallback 0 Elements,  -  Coverage"},
          "children":[]},{"id":"SessionHandle3427","name":"SessionHandle",
          "data":{"$area":27.0,"$color":0.0,"path":
            "org/apache/http/impl/nio/reactor/SessionHandle.html#SessionHandle",
            "title":"SessionHandle 27 Elements, 0% Coverage"},"children":[]},
        {"id":"SessionInputBufferImpl3454","name":"SessionInputBufferImpl",
          "data":{"$area":182.0,"$color":81.86813,"path":
            "org/apache/http/impl/nio/reactor/SessionInputBufferImpl.html#SessionInputBufferImpl",
            "title":"SessionInputBufferImpl 182 Elements, 81.9% Coverage"},
          "children":[]},{"id":"SessionOutputBufferImpl3636","name":
          "SessionOutputBufferImpl","data":{"$area":160.0,"$color":76.875,
            "path":
            "org/apache/http/impl/nio/reactor/SessionOutputBufferImpl.html#SessionOutputBufferImpl",
            "title":"SessionOutputBufferImpl 160 Elements, 76.9% Coverage"},
          "children":[]},{"id":"SessionRequestHandle3796","name":
          "SessionRequestHandle","data":{"$area":9.0,"$color":77.77778,
            "path":
            "org/apache/http/impl/nio/reactor/SessionRequestHandle.html#SessionRequestHandle",
            "title":"SessionRequestHandle 9 Elements, 77.8% Coverage"},
          "children":[]},{"id":"SessionRequestImpl3805","name":
          "SessionRequestImpl","data":{"$area":133.0,"$color":42.105263,
            "path":
            "org/apache/http/impl/nio/reactor/SessionRequestImpl.html#SessionRequestImpl",
            "title":"SessionRequestImpl 133 Elements, 42.1% Coverage"},
          "children":[]}]},{"id":"org.apache.http.impl.nio.ssl3938","name":
      "org.apache.http.impl.nio.ssl","data":{"$area":51.0,"$color":0.0,
        "title":"org.apache.http.impl.nio.ssl 51 Elements, 0% Coverage"},
      "children":[{"id":"SSLClientIOEventDispatch3938","name":
          "SSLClientIOEventDispatch","data":{"$area":26.0,"$color":0.0,
            "path":
            "org/apache/http/impl/nio/ssl/SSLClientIOEventDispatch.html#SSLClientIOEventDispatch",
            "title":"SSLClientIOEventDispatch 26 Elements, 0% Coverage"},
          "children":[]},{"id":"SSLServerIOEventDispatch3964","name":
          "SSLServerIOEventDispatch","data":{"$area":25.0,"$color":0.0,
            "path":
            "org/apache/http/impl/nio/ssl/SSLServerIOEventDispatch.html#SSLServerIOEventDispatch",
            "title":"SSLServerIOEventDispatch 25 Elements, 0% Coverage"},
          "children":[]}]},{"id":"org.apache.http.nio.util8134","name":
      "org.apache.http.nio.util","data":{"$area":550.0,"$color":66.36364,
        "title":"org.apache.http.nio.util 550 Elements, 66.4% Coverage"},
      "children":[{"id":"BufferInfo8134","name":"BufferInfo","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/nio/util/BufferInfo.html#BufferInfo","title":
            "BufferInfo 0 Elements,  -  Coverage"},"children":[]},{"id":
          "ByteBufferAllocator8134","name":"ByteBufferAllocator","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/util/ByteBufferAllocator.html#ByteBufferAllocator",
            "title":"ByteBufferAllocator 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ContentInputBuffer8134","name":
          "ContentInputBuffer","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/util/ContentInputBuffer.html#ContentInputBuffer",
            "title":"ContentInputBuffer 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ContentOutputBuffer8134","name":
          "ContentOutputBuffer","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/util/ContentOutputBuffer.html#ContentOutputBuffer",
            "title":"ContentOutputBuffer 0 Elements,  -  Coverage"},
          "children":[]},{"id":"DirectByteBufferAllocator8134","name":
          "DirectByteBufferAllocator","data":{"$area":4.0,"$color":100.0,
            "path":
            "org/apache/http/nio/util/DirectByteBufferAllocator.html#DirectByteBufferAllocator",
            "title":"DirectByteBufferAllocator 4 Elements, 100% Coverage"},
          "children":[]},{"id":"ExpandableBuffer8138","name":
          "ExpandableBuffer","data":{"$area":72.0,"$color":70.83333,"path":
            "org/apache/http/nio/util/ExpandableBuffer.html#ExpandableBuffer",
            "title":"ExpandableBuffer 72 Elements, 70.8% Coverage"},
          "children":[]},{"id":"HeapByteBufferAllocator8210","name":
          "HeapByteBufferAllocator","data":{"$area":4.0,"$color":100.0,
            "path":
            "org/apache/http/nio/util/HeapByteBufferAllocator.html#HeapByteBufferAllocator",
            "title":"HeapByteBufferAllocator 4 Elements, 100% Coverage"},
          "children":[]},{"id":"SharedInputBuffer8214","name":
          "SharedInputBuffer","data":{"$area":180.0,"$color":57.77778,"path":
            
            "org/apache/http/nio/util/SharedInputBuffer.html#SharedInputBuffer",
            "title":"SharedInputBuffer 180 Elements, 57.8% Coverage"},
          "children":[]},{"id":"SharedOutputBuffer8394","name":
          "SharedOutputBuffer","data":{"$area":173.0,"$color":61.849712,
            "path":
            "org/apache/http/nio/util/SharedOutputBuffer.html#SharedOutputBuffer",
            "title":"SharedOutputBuffer 173 Elements, 61.8% Coverage"},
          "children":[]},{"id":"SimpleInputBuffer8567","name":
          "SimpleInputBuffer","data":{"$area":66.0,"$color":93.93939,"path":
            "org/apache/http/nio/util/SimpleInputBuffer.html#SimpleInputBuffer",
            "title":"SimpleInputBuffer 66 Elements, 93.9% Coverage"},
          "children":[]},{"id":"SimpleOutputBuffer8633","name":
          "SimpleOutputBuffer","data":{"$area":51.0,"$color":64.70589,"path":
            
            "org/apache/http/nio/util/SimpleOutputBuffer.html#SimpleOutputBuffer",
            "title":"SimpleOutputBuffer 51 Elements, 64.7% Coverage"},
          "children":[]}]},{"id":"org.apache.http.nio3989","name":
      "org.apache.http.nio","data":{"$area":14.0,"$color":100.0,"title":
        "org.apache.http.nio 14 Elements, 100% Coverage"},"children":[{"id":
          "ContentDecoder3989","name":"ContentDecoder","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/nio/ContentDecoder.html#ContentDecoder","title":
            "ContentDecoder 0 Elements,  -  Coverage"},"children":[]},{"id":
          "ContentDecoderChannel3989","name":"ContentDecoderChannel","data":{
            "$area":7.0,"$color":100.0,"path":
            "org/apache/http/nio/ContentDecoderChannel.html#ContentDecoderChannel",
            "title":"ContentDecoderChannel 7 Elements, 100% Coverage"},
          "children":[]},{"id":"ContentEncoder3996","name":
          "ContentEncoder","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/ContentEncoder.html#ContentEncoder","title":
            "ContentEncoder 0 Elements,  -  Coverage"},"children":[]},{"id":
          "ContentEncoderChannel3996","name":"ContentEncoderChannel","data":{
            "$area":7.0,"$color":100.0,"path":
            "org/apache/http/nio/ContentEncoderChannel.html#ContentEncoderChannel",
            "title":"ContentEncoderChannel 7 Elements, 100% Coverage"},
          "children":[]},{"id":"FileContentDecoder4003","name":
          "FileContentDecoder","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/FileContentDecoder.html#FileContentDecoder",
            "title":"FileContentDecoder 0 Elements,  -  Coverage"},
          "children":[]},{"id":"FileContentEncoder4003","name":
          "FileContentEncoder","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/FileContentEncoder.html#FileContentEncoder",
            "title":"FileContentEncoder 0 Elements,  -  Coverage"},
          "children":[]},{"id":"IOControl4003","name":"IOControl","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/IOControl.html#IOControl","title":
            "IOControl 0 Elements,  -  Coverage"},"children":[]},{"id":
          "NHttpClientConnection4003","name":"NHttpClientConnection","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/NHttpClientConnection.html#NHttpClientConnection",
            "title":"NHttpClientConnection 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpClientEventHandler4003","name":
          "NHttpClientEventHandler","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/nio/NHttpClientEventHandler.html#NHttpClientEventHandler",
            "title":"NHttpClientEventHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpClientHandler4003","name":
          "NHttpClientHandler","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/NHttpClientHandler.html#NHttpClientHandler",
            "title":"NHttpClientHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpClientIOTarget4003","name":
          "NHttpClientIOTarget","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/NHttpClientIOTarget.html#NHttpClientIOTarget",
            "title":"NHttpClientIOTarget 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpConnection4003","name":
          "NHttpConnection","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/NHttpConnection.html#NHttpConnection",
            "title":"NHttpConnection 0 Elements,  -  Coverage"},"children":[]},
        {"id":"NHttpConnectionFactory4003","name":"NHttpConnectionFactory",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/NHttpConnectionFactory.html#NHttpConnectionFactory",
            "title":"NHttpConnectionFactory 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpMessageParser4003","name":
          "NHttpMessageParser","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/NHttpMessageParser.html#NHttpMessageParser",
            "title":"NHttpMessageParser 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpMessageParserFactory4003","name":
          "NHttpMessageParserFactory","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/nio/NHttpMessageParserFactory.html#NHttpMessageParserFactory",
            "title":"NHttpMessageParserFactory 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpMessageWriter4003","name":
          "NHttpMessageWriter","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/NHttpMessageWriter.html#NHttpMessageWriter",
            "title":"NHttpMessageWriter 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpMessageWriterFactory4003","name":
          "NHttpMessageWriterFactory","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/nio/NHttpMessageWriterFactory.html#NHttpMessageWriterFactory",
            "title":"NHttpMessageWriterFactory 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpServerConnection4003","name":
          "NHttpServerConnection","data":{"$area":0.0,"$color":-100.0,"path":
            
            "org/apache/http/nio/NHttpServerConnection.html#NHttpServerConnection",
            "title":"NHttpServerConnection 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpServerEventHandler4003","name":
          "NHttpServerEventHandler","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/nio/NHttpServerEventHandler.html#NHttpServerEventHandler",
            "title":"NHttpServerEventHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpServerIOTarget4003","name":
          "NHttpServerIOTarget","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/NHttpServerIOTarget.html#NHttpServerIOTarget",
            "title":"NHttpServerIOTarget 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpServiceHandler4003","name":
          "NHttpServiceHandler","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/NHttpServiceHandler.html#NHttpServiceHandler",
            "title":"NHttpServiceHandler 0 Elements,  -  Coverage"},
          "children":[]}]},{"id":"org.apache.http.nio.params4353","name":
      "org.apache.http.nio.params","data":{"$area":32.0,"$color":0.0,"title":
        "org.apache.http.nio.params 32 Elements, 0% Coverage"},"children":[{
          "id":"NIOReactorPNames4353","name":"NIOReactorPNames","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/params/NIOReactorPNames.html#NIOReactorPNames",
            "title":"NIOReactorPNames 0 Elements,  -  Coverage"},"children":[]},
        {"id":"NIOReactorParamBean4353","name":"NIOReactorParamBean","data":{
            "$area":6.0,"$color":0.0,"path":
            "org/apache/http/nio/params/NIOReactorParamBean.html#NIOReactorParamBean",
            "title":"NIOReactorParamBean 6 Elements, 0% Coverage"},
          "children":[]},{"id":"NIOReactorParams4359","name":
          "NIOReactorParams","data":{"$area":26.0,"$color":0.0,"path":
            "org/apache/http/nio/params/NIOReactorParams.html#NIOReactorParams",
            "title":"NIOReactorParams 26 Elements, 0% Coverage"},"children":[]}]},
    {"id":"org.apache.http.nio.reactor.ssl7792","name":
      "org.apache.http.nio.reactor.ssl","data":{"$area":342.0,"$color":
        70.76023,"title":
        "org.apache.http.nio.reactor.ssl 342 Elements, 70.8% Coverage"},
      "children":[{"id":"SSLIOSession7792","name":"SSLIOSession","data":{
            "$area":334.0,"$color":71.257484,"path":
            "org/apache/http/nio/reactor/ssl/SSLIOSession.html#SSLIOSession",
            "title":"SSLIOSession 334 Elements, 71.3% Coverage"},"children":[]},
        {"id":"SSLIOSession.InternalByteChannel8126","name":
          "SSLIOSession.InternalByteChannel","data":{"$area":8.0,"$color":
            50.0,"path":
            "org/apache/http/nio/reactor/ssl/SSLIOSession.html#SSLIOSession.InternalByteChannel",
            "title":
            "SSLIOSession.InternalByteChannel 8 Elements, 50% Coverage"},
          "children":[]},{"id":"SSLMode8134","name":"SSLMode","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/ssl/SSLMode.html#SSLMode","title":
            "SSLMode 0 Elements,  -  Coverage"},"children":[]},{"id":
          "SSLSetupHandler8134","name":"SSLSetupHandler","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/ssl/SSLSetupHandler.html#SSLSetupHandler",
            "title":"SSLSetupHandler 0 Elements,  -  Coverage"},"children":[]}]},
    {"id":"org.apache.http.nio.reactor7784","name":
      "org.apache.http.nio.reactor","data":{"$area":8.0,"$color":62.5,
        "title":"org.apache.http.nio.reactor 8 Elements, 62.5% Coverage"},
      "children":[{"id":"ConnectingIOReactor7784","name":
          "ConnectingIOReactor","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/ConnectingIOReactor.html#ConnectingIOReactor",
            "title":"ConnectingIOReactor 0 Elements,  -  Coverage"},
          "children":[]},{"id":"EventMask7784","name":"EventMask","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/EventMask.html#EventMask","title":
            "EventMask 0 Elements,  -  Coverage"},"children":[]},{"id":
          "IOEventDispatch7784","name":"IOEventDispatch","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/IOEventDispatch.html#IOEventDispatch",
            "title":"IOEventDispatch 0 Elements,  -  Coverage"},"children":[]},
        {"id":"IOReactor7784","name":"IOReactor","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/nio/reactor/IOReactor.html#IOReactor","title":
            "IOReactor 0 Elements,  -  Coverage"},"children":[]},{"id":
          "IOReactorException7784","name":"IOReactorException","data":{
            "$area":8.0,"$color":62.5,"path":
            "org/apache/http/nio/reactor/IOReactorException.html#IOReactorException",
            "title":"IOReactorException 8 Elements, 62.5% Coverage"},
          "children":[]},{"id":"IOReactorExceptionHandler7792","name":
          "IOReactorExceptionHandler","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/nio/reactor/IOReactorExceptionHandler.html#IOReactorExceptionHandler",
            "title":"IOReactorExceptionHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"IOReactorStatus7792","name":
          "IOReactorStatus","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/IOReactorStatus.html#IOReactorStatus",
            "title":"IOReactorStatus 0 Elements,  -  Coverage"},"children":[]},
        {"id":"IOSession7792","name":"IOSession","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/nio/reactor/IOSession.html#IOSession","title":
            "IOSession 0 Elements,  -  Coverage"},"children":[]},{"id":
          "ListenerEndpoint7792","name":"ListenerEndpoint","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/ListenerEndpoint.html#ListenerEndpoint",
            "title":"ListenerEndpoint 0 Elements,  -  Coverage"},"children":[]},
        {"id":"ListeningIOReactor7792","name":"ListeningIOReactor","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/ListeningIOReactor.html#ListeningIOReactor",
            "title":"ListeningIOReactor 0 Elements,  -  Coverage"},
          "children":[]},{"id":"SessionBufferStatus7792","name":
          "SessionBufferStatus","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/SessionBufferStatus.html#SessionBufferStatus",
            "title":"SessionBufferStatus 0 Elements,  -  Coverage"},
          "children":[]},{"id":"SessionInputBuffer7792","name":
          "SessionInputBuffer","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/SessionInputBuffer.html#SessionInputBuffer",
            "title":"SessionInputBuffer 0 Elements,  -  Coverage"},
          "children":[]},{"id":"SessionOutputBuffer7792","name":
          "SessionOutputBuffer","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/SessionOutputBuffer.html#SessionOutputBuffer",
            "title":"SessionOutputBuffer 0 Elements,  -  Coverage"},
          "children":[]},{"id":"SessionRequest7792","name":
          "SessionRequest","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/SessionRequest.html#SessionRequest",
            "title":"SessionRequest 0 Elements,  -  Coverage"},"children":[]},
        {"id":"SessionRequestCallback7792","name":"SessionRequestCallback",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/SessionRequestCallback.html#SessionRequestCallback",
            "title":"SessionRequestCallback 0 Elements,  -  Coverage"},
          "children":[]},{"id":"SocketAccessor7792","name":
          "SocketAccessor","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/SocketAccessor.html#SocketAccessor",
            "title":"SocketAccessor 0 Elements,  -  Coverage"},"children":[]}]},
    {"id":"org.apache.http.nio.protocol4900","name":
      "org.apache.http.nio.protocol","data":{"$area":2884.0,"$color":
        76.907074,"title":
        "org.apache.http.nio.protocol 2884 Elements, 76.9% Coverage"},
      "children":[{"id":"AbstractAsyncRequestConsumer4900","name":
          "AbstractAsyncRequestConsumer","data":{"$area":46.0,"$color":
            89.13044,"path":
            "org/apache/http/nio/protocol/AbstractAsyncRequestConsumer.html#AbstractAsyncRequestConsumer",
            "title":
            "AbstractAsyncRequestConsumer 46 Elements, 89.1% Coverage"},
          "children":[]},{"id":"AbstractAsyncResponseConsumer4946","name":
          "AbstractAsyncResponseConsumer","data":{"$area":51.0,"$color":
            98.039215,"path":
            "org/apache/http/nio/protocol/AbstractAsyncResponseConsumer.html#AbstractAsyncResponseConsumer",
            "title":
            "AbstractAsyncResponseConsumer 51 Elements, 98% Coverage"},
          "children":[]},{"id":"AsyncNHttpClientHandler4997","name":
          "AsyncNHttpClientHandler","data":{"$area":241.0,"$color":
            66.390045,"path":
            "org/apache/http/nio/protocol/AsyncNHttpClientHandler.html#AsyncNHttpClientHandler",
            "title":"AsyncNHttpClientHandler 241 Elements, 66.4% Coverage"},
          "children":[]},{"id":
          "AsyncNHttpClientHandler.ClientConnState5238","name":
          "AsyncNHttpClientHandler.ClientConnState","data":{"$area":49.0,
            "$color":95.918365,"path":
            "org/apache/http/nio/protocol/AsyncNHttpClientHandler.html#AsyncNHttpClientHandler.ClientConnState",
            "title":
            "AsyncNHttpClientHandler.ClientConnState 49 Elements, 95.9% Coverage"},
          "children":[]},{"id":"AsyncNHttpServiceHandler5287","name":
          "AsyncNHttpServiceHandler","data":{"$area":292.0,"$color":
            67.12329,"path":
            "org/apache/http/nio/protocol/AsyncNHttpServiceHandler.html#AsyncNHttpServiceHandler",
            "title":
            "AsyncNHttpServiceHandler 292 Elements, 67.1% Coverage"},
          "children":[]},{"id":
          "AsyncNHttpServiceHandler.ServerConnState5579","name":
          "AsyncNHttpServiceHandler.ServerConnState","data":{"$area":61.0,
            "$color":83.60656,"path":
            "org/apache/http/nio/protocol/AsyncNHttpServiceHandler.html#AsyncNHttpServiceHandler.ServerConnState",
            "title":
            "AsyncNHttpServiceHandler.ServerConnState 61 Elements, 83.6% Coverage"},
          "children":[]},{"id":
          "AsyncNHttpServiceHandler.ResponseTriggerImpl5640","name":
          "AsyncNHttpServiceHandler.ResponseTriggerImpl","data":{"$area":
            29.0,"$color":58.62069,"path":
            "org/apache/http/nio/protocol/AsyncNHttpServiceHandler.html#AsyncNHttpServiceHandler.ResponseTriggerImpl",
            "title":
            "AsyncNHttpServiceHandler.ResponseTriggerImpl 29 Elements, 58.6% Coverage"},
          "children":[]},{"id":"BasicAsyncRequestConsumer5669","name":
          "BasicAsyncRequestConsumer","data":{"$area":27.0,"$color":
            85.18519,"path":
            "org/apache/http/nio/protocol/BasicAsyncRequestConsumer.html#BasicAsyncRequestConsumer",
            "title":
            "BasicAsyncRequestConsumer 27 Elements, 85.2% Coverage"},
          "children":[]},{"id":"BasicAsyncRequestExecutionHandler5696",
          "name":"BasicAsyncRequestExecutionHandler","data":{"$area":100.0,
            "$color":80.0,"path":
            "org/apache/http/nio/protocol/BasicAsyncRequestExecutionHandler.html#BasicAsyncRequestExecutionHandler",
            "title":
            "BasicAsyncRequestExecutionHandler 100 Elements, 80% Coverage"},
          "children":[]},{"id":"BasicAsyncRequestHandler5796","name":
          "BasicAsyncRequestHandler","data":{"$area":9.0,"$color":100.0,
            "path":
            "org/apache/http/nio/protocol/BasicAsyncRequestHandler.html#BasicAsyncRequestHandler",
            "title":"BasicAsyncRequestHandler 9 Elements, 100% Coverage"},
          "children":[]},{"id":"BasicAsyncRequestProducer5805","name":
          "BasicAsyncRequestProducer","data":{"$area":54.0,"$color":
            87.03704,"path":
            "org/apache/http/nio/protocol/BasicAsyncRequestProducer.html#BasicAsyncRequestProducer",
            "title":"BasicAsyncRequestProducer 54 Elements, 87% Coverage"},
          "children":[]},{"id":"BasicAsyncResponseConsumer5859","name":
          "BasicAsyncResponseConsumer","data":{"$area":27.0,"$color":
            85.18519,"path":
            "org/apache/http/nio/protocol/BasicAsyncResponseConsumer.html#BasicAsyncResponseConsumer",
            "title":
            "BasicAsyncResponseConsumer 27 Elements, 85.2% Coverage"},
          "children":[]},{"id":"BasicAsyncResponseProducer5886","name":
          "BasicAsyncResponseProducer","data":{"$area":38.0,"$color":
            92.10526,"path":
            "org/apache/http/nio/protocol/BasicAsyncResponseProducer.html#BasicAsyncResponseProducer",
            "title":
            "BasicAsyncResponseProducer 38 Elements, 92.1% Coverage"},
          "children":[]},{"id":"BufferingHttpClientHandler5924","name":
          "BufferingHttpClientHandler","data":{"$area":24.0,"$color":0.0,
            "path":
            "org/apache/http/nio/protocol/BufferingHttpClientHandler.html#BufferingHttpClientHandler",
            "title":"BufferingHttpClientHandler 24 Elements, 0% Coverage"},
          "children":[]},{"id":
          "BufferingHttpClientHandler.ExecutionHandlerAdaptor5948","name":
          "BufferingHttpClientHandler.ExecutionHandlerAdaptor","data":{
            "$area":13.0,"$color":0.0,"path":
            "org/apache/http/nio/protocol/BufferingHttpClientHandler.html#BufferingHttpClientHandler.ExecutionHandlerAdaptor",
            "title":
            "BufferingHttpClientHandler.ExecutionHandlerAdaptor 13 Elements, 0% Coverage"},
          "children":[]},{"id":"BufferingHttpServiceHandler5961","name":
          "BufferingHttpServiceHandler","data":{"$area":30.0,"$color":
            66.66667,"path":
            "org/apache/http/nio/protocol/BufferingHttpServiceHandler.html#BufferingHttpServiceHandler",
            "title":
            "BufferingHttpServiceHandler 30 Elements, 66.7% Coverage"},
          "children":[]},{"id":
          "BufferingHttpServiceHandler.RequestHandlerResolverAdaptor5991",
          "name":
          "BufferingHttpServiceHandler.RequestHandlerResolverAdaptor","data":
          {"$area":7.0,"$color":71.42857,"path":
            "org/apache/http/nio/protocol/BufferingHttpServiceHandler.html#BufferingHttpServiceHandler.RequestHandlerResolverAdaptor",
            "title":
            "BufferingHttpServiceHandler.RequestHandlerResolverAdaptor 7 Elements, 71.4% Coverage"},
          "children":[]},{"id":
          "BufferingHttpServiceHandler.RequestHandlerAdaptor5998","name":
          "BufferingHttpServiceHandler.RequestHandlerAdaptor","data":{
            "$area":7.0,"$color":71.42857,"path":
            "org/apache/http/nio/protocol/BufferingHttpServiceHandler.html#BufferingHttpServiceHandler.RequestHandlerAdaptor",
            "title":
            "BufferingHttpServiceHandler.RequestHandlerAdaptor 7 Elements, 71.4% Coverage"},
          "children":[]},{"id":"ErrorResponseProducer6005","name":
          "ErrorResponseProducer","data":{"$area":24.0,"$color":95.83333,
            "path":
            "org/apache/http/nio/protocol/ErrorResponseProducer.html#ErrorResponseProducer",
            "title":"ErrorResponseProducer 24 Elements, 95.8% Coverage"},
          "children":[]},{"id":"EventListener6029","name":"EventListener",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/protocol/EventListener.html#EventListener",
            "title":"EventListener 0 Elements,  -  Coverage"},"children":[]},
        {"id":"HttpAsyncExchange6029","name":"HttpAsyncExchange","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/protocol/HttpAsyncExchange.html#HttpAsyncExchange",
            "title":"HttpAsyncExchange 0 Elements,  -  Coverage"},"children":
          []},{"id":"HttpAsyncExpectationVerifier6029","name":
          "HttpAsyncExpectationVerifier","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/nio/protocol/HttpAsyncExpectationVerifier.html#HttpAsyncExpectationVerifier",
            "title":
            "HttpAsyncExpectationVerifier 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpAsyncRequestConsumer6029","name":
          "HttpAsyncRequestConsumer","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/nio/protocol/HttpAsyncRequestConsumer.html#HttpAsyncRequestConsumer",
            "title":"HttpAsyncRequestConsumer 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpAsyncRequestExecutionHandler6029","name":
          "HttpAsyncRequestExecutionHandler","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/nio/protocol/HttpAsyncRequestExecutionHandler.html#HttpAsyncRequestExecutionHandler",
            "title":
            "HttpAsyncRequestExecutionHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpAsyncRequestExecutor6029","name":
          "HttpAsyncRequestExecutor","data":{"$area":240.0,"$color":
            95.416664,"path":
            "org/apache/http/nio/protocol/HttpAsyncRequestExecutor.html#HttpAsyncRequestExecutor",
            "title":
            "HttpAsyncRequestExecutor 240 Elements, 95.4% Coverage"},
          "children":[]},{"id":"HttpAsyncRequestExecutor.State6269","name":
          "HttpAsyncRequestExecutor.State","data":{"$area":55.0,"$color":
            98.18182,"path":
            "org/apache/http/nio/protocol/HttpAsyncRequestExecutor.html#HttpAsyncRequestExecutor.State",
            "title":
            "HttpAsyncRequestExecutor.State 55 Elements, 98.2% Coverage"},
          "children":[]},{"id":"HttpAsyncRequestHandler6324","name":
          "HttpAsyncRequestHandler","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/nio/protocol/HttpAsyncRequestHandler.html#HttpAsyncRequestHandler",
            "title":"HttpAsyncRequestHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpAsyncRequestHandlerMapper6324","name":
          "HttpAsyncRequestHandlerMapper","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/nio/protocol/HttpAsyncRequestHandlerMapper.html#HttpAsyncRequestHandlerMapper",
            "title":
            "HttpAsyncRequestHandlerMapper 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpAsyncRequestHandlerRegistry6324","name":
          "HttpAsyncRequestHandlerRegistry","data":{"$area":12.0,"$color":
            0.0,"path":
            "org/apache/http/nio/protocol/HttpAsyncRequestHandlerRegistry.html#HttpAsyncRequestHandlerRegistry",
            "title":
            "HttpAsyncRequestHandlerRegistry 12 Elements, 0% Coverage"},
          "children":[]},{"id":"HttpAsyncRequestHandlerResolver6336","name":
          "HttpAsyncRequestHandlerResolver","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/nio/protocol/HttpAsyncRequestHandlerResolver.html#HttpAsyncRequestHandlerResolver",
            "title":
            "HttpAsyncRequestHandlerResolver 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpAsyncRequestProducer6336","name":
          "HttpAsyncRequestProducer","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/nio/protocol/HttpAsyncRequestProducer.html#HttpAsyncRequestProducer",
            "title":"HttpAsyncRequestProducer 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpAsyncRequester6336","name":
          "HttpAsyncRequester","data":{"$area":57.0,"$color":92.98246,"path":
            
            "org/apache/http/nio/protocol/HttpAsyncRequester.html#HttpAsyncRequester",
            "title":"HttpAsyncRequester 57 Elements, 93% Coverage"},
          "children":[]},{"id":
          "HttpAsyncRequester.ConnRequestCallback6392","name":
          "HttpAsyncRequester.ConnRequestCallback","data":{"$area":35.0,
            "$color":94.28571,"path":
            "org/apache/http/nio/protocol/HttpAsyncRequester.html#HttpAsyncRequester.ConnRequestCallback",
            "title":
            "HttpAsyncRequester.ConnRequestCallback 35 Elements, 94.3% Coverage"},
          "children":[]},{"id":
          "HttpAsyncRequester.RequestExecutionCallback6427","name":
          "HttpAsyncRequester.RequestExecutionCallback","data":{"$area":
            17.0,"$color":100.0,"path":
            "org/apache/http/nio/protocol/HttpAsyncRequester.html#HttpAsyncRequester.RequestExecutionCallback",
            "title":
            "HttpAsyncRequester.RequestExecutionCallback 17 Elements, 100% Coverage"},
          "children":[]},{"id":"HttpAsyncResponseConsumer6445","name":
          "HttpAsyncResponseConsumer","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/nio/protocol/HttpAsyncResponseConsumer.html#HttpAsyncResponseConsumer",
            "title":"HttpAsyncResponseConsumer 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpAsyncResponseProducer6445","name":
          "HttpAsyncResponseProducer","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/nio/protocol/HttpAsyncResponseProducer.html#HttpAsyncResponseProducer",
            "title":"HttpAsyncResponseProducer 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpAsyncService6445","name":
          "HttpAsyncService","data":{"$area":336.0,"$color":92.85714,"path":
            "org/apache/http/nio/protocol/HttpAsyncService.html#HttpAsyncService",
            "title":"HttpAsyncService 336 Elements, 92.9% Coverage"},
          "children":[]},{"id":"HttpAsyncService.State6781","name":
          "HttpAsyncService.State","data":{"$area":71.0,"$color":100.0,
            "path":
            "org/apache/http/nio/protocol/HttpAsyncService.html#HttpAsyncService.State",
            "title":"HttpAsyncService.State 71 Elements, 100% Coverage"},
          "children":[]},{"id":"HttpAsyncService.Exchange6852","name":
          "HttpAsyncService.Exchange","data":{"$area":46.0,"$color":
            76.08696,"path":
            "org/apache/http/nio/protocol/HttpAsyncService.html#HttpAsyncService.Exchange",
            "title":
            "HttpAsyncService.Exchange 46 Elements, 76.1% Coverage"},
          "children":[]},{"id":
          "HttpAsyncService.HttpAsyncRequestHandlerResolverAdapter6898",
          "name":"HttpAsyncService.HttpAsyncRequestHandlerResolverAdapter",
          "data":{"$area":4.0,"$color":0.0,"path":
            "org/apache/http/nio/protocol/HttpAsyncService.html#HttpAsyncService.HttpAsyncRequestHandlerResolverAdapter",
            "title":
            "HttpAsyncService.HttpAsyncRequestHandlerResolverAdapter 4 Elements, 0% Coverage"},
          "children":[]},{"id":"HttpRequestExecutionHandler6902","name":
          "HttpRequestExecutionHandler","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/nio/protocol/HttpRequestExecutionHandler.html#HttpRequestExecutionHandler",
            "title":"HttpRequestExecutionHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"MessageState6902","name":"MessageState",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/protocol/MessageState.html#MessageState",
            "title":"MessageState 0 Elements,  -  Coverage"},"children":[]},{
          "id":"NHttpHandlerBase6902","name":"NHttpHandlerBase","data":{
            "$area":44.0,"$color":43.18182,"path":
            "org/apache/http/nio/protocol/NHttpHandlerBase.html#NHttpHandlerBase",
            "title":"NHttpHandlerBase 44 Elements, 43.2% Coverage"},
          "children":[]},{"id":"NHttpRequestExecutionHandler6946","name":
          "NHttpRequestExecutionHandler","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/nio/protocol/NHttpRequestExecutionHandler.html#NHttpRequestExecutionHandler",
            "title":
            "NHttpRequestExecutionHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpRequestHandler6946","name":
          "NHttpRequestHandler","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/protocol/NHttpRequestHandler.html#NHttpRequestHandler",
            "title":"NHttpRequestHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpRequestHandlerRegistry6946","name":
          "NHttpRequestHandlerRegistry","data":{"$area":12.0,"$color":0.0,
            "path":
            "org/apache/http/nio/protocol/NHttpRequestHandlerRegistry.html#NHttpRequestHandlerRegistry",
            "title":"NHttpRequestHandlerRegistry 12 Elements, 0% Coverage"},
          "children":[]},{"id":"NHttpRequestHandlerResolver6958","name":
          "NHttpRequestHandlerResolver","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/nio/protocol/NHttpRequestHandlerResolver.html#NHttpRequestHandlerResolver",
            "title":"NHttpRequestHandlerResolver 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpResponseTrigger6958","name":
          "NHttpResponseTrigger","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/protocol/NHttpResponseTrigger.html#NHttpResponseTrigger",
            "title":"NHttpResponseTrigger 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NullNHttpEntity6958","name":
          "NullNHttpEntity","data":{"$area":19.0,"$color":57.894737,"path":
            "org/apache/http/nio/protocol/NullNHttpEntity.html#NullNHttpEntity",
            "title":"NullNHttpEntity 19 Elements, 57.9% Coverage"},
          "children":[]},{"id":"NullRequestConsumer6977","name":
          "NullRequestConsumer","data":{"$area":23.0,"$color":52.173912,
            "path":
            "org/apache/http/nio/protocol/NullRequestConsumer.html#NullRequestConsumer",
            "title":"NullRequestConsumer 23 Elements, 52.2% Coverage"},
          "children":[]},{"id":"NullRequestHandler7000","name":
          "NullRequestHandler","data":{"$area":8.0,"$color":100.0,"path":
            "org/apache/http/nio/protocol/NullRequestHandler.html#NullRequestHandler",
            "title":"NullRequestHandler 8 Elements, 100% Coverage"},
          "children":[]},{"id":"SimpleNHttpRequestHandler7008","name":
          "SimpleNHttpRequestHandler","data":{"$area":3.0,"$color":100.0,
            "path":
            "org/apache/http/nio/protocol/SimpleNHttpRequestHandler.html#SimpleNHttpRequestHandler",
            "title":"SimpleNHttpRequestHandler 3 Elements, 100% Coverage"},
          "children":[]},{"id":"ThrottlingHttpClientHandler7011","name":
          "ThrottlingHttpClientHandler","data":{"$area":302.0,"$color":
            65.894035,"path":
            "org/apache/http/nio/protocol/ThrottlingHttpClientHandler.html#ThrottlingHttpClientHandler",
            "title":
            "ThrottlingHttpClientHandler 302 Elements, 65.9% Coverage"},
          "children":[]},{"id":
          "ThrottlingHttpClientHandler.ClientConnState7313","name":
          "ThrottlingHttpClientHandler.ClientConnState","data":{"$area":
            52.0,"$color":90.38461,"path":
            "org/apache/http/nio/protocol/ThrottlingHttpClientHandler.html#ThrottlingHttpClientHandler.ClientConnState",
            "title":
            "ThrottlingHttpClientHandler.ClientConnState 52 Elements, 90.4% Coverage"},
          "children":[]},{"id":"ThrottlingHttpServiceHandler7365","name":
          "ThrottlingHttpServiceHandler","data":{"$area":346.0,"$color":
            63.872833,"path":
            "org/apache/http/nio/protocol/ThrottlingHttpServiceHandler.html#ThrottlingHttpServiceHandler",
            "title":
            "ThrottlingHttpServiceHandler 346 Elements, 63.9% Coverage"},
          "children":[]},{"id":
          "ThrottlingHttpServiceHandler.ServerConnState7711","name":
          "ThrottlingHttpServiceHandler.ServerConnState","data":{"$area":
            49.0,"$color":81.63265,"path":
            "org/apache/http/nio/protocol/ThrottlingHttpServiceHandler.html#ThrottlingHttpServiceHandler.ServerConnState",
            "title":
            "ThrottlingHttpServiceHandler.ServerConnState 49 Elements, 81.6% Coverage"},
          "children":[]},{"id":"UriHttpAsyncRequestHandlerMapper7760","name":
          "UriHttpAsyncRequestHandlerMapper","data":{"$area":24.0,"$color":
            91.66667,"path":
            "org/apache/http/nio/protocol/UriHttpAsyncRequestHandlerMapper.html#UriHttpAsyncRequestHandlerMapper",
            "title":
            "UriHttpAsyncRequestHandlerMapper 24 Elements, 91.7% Coverage"},
          "children":[]}]},{"id":"org.apache.http.nio.pool4385","name":
      "org.apache.http.nio.pool","data":{"$area":515.0,"$color":92.62135,
        "title":"org.apache.http.nio.pool 515 Elements, 92.6% Coverage"},
      "children":[{"id":"AbstractNIOConnPool4385","name":
          "AbstractNIOConnPool","data":{"$area":362.0,"$color":91.71271,
            "path":
            "org/apache/http/nio/pool/AbstractNIOConnPool.html#AbstractNIOConnPool",
            "title":"AbstractNIOConnPool 362 Elements, 91.7% Coverage"},
          "children":[]},{"id":
          "AbstractNIOConnPool.InternalSessionRequestCallback4747","name":
          "AbstractNIOConnPool.InternalSessionRequestCallback","data":{
            "$area":8.0,"$color":25.0,"path":
            "org/apache/http/nio/pool/AbstractNIOConnPool.html#AbstractNIOConnPool.InternalSessionRequestCallback",
            "title":
            "AbstractNIOConnPool.InternalSessionRequestCallback 8 Elements, 25% Coverage"},
          "children":[]},{"id":"LeaseRequest4755","name":"LeaseRequest",
          "data":{"$area":27.0,"$color":100.0,"path":
            "org/apache/http/nio/pool/LeaseRequest.html#LeaseRequest",
            "title":"LeaseRequest 27 Elements, 100% Coverage"},"children":[]},
        {"id":"NIOConnFactory4782","name":"NIOConnFactory","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/nio/pool/NIOConnFactory.html#NIOConnFactory",
            "title":"NIOConnFactory 0 Elements,  -  Coverage"},"children":[]},
        {"id":"RouteSpecificPool4782","name":"RouteSpecificPool","data":{
            "$area":118.0,"$color":98.305084,"path":
            "org/apache/http/nio/pool/RouteSpecificPool.html#RouteSpecificPool",
            "title":"RouteSpecificPool 118 Elements, 98.3% Coverage"},
          "children":[]}]},{"id":"org.apache.http.impl.nio.pool1885","name":
      "org.apache.http.impl.nio.pool","data":{"$area":62.0,"$color":
        51.6129,"title":
        "org.apache.http.impl.nio.pool 62 Elements, 51.6% Coverage"},
      "children":[{"id":"BasicNIOConnFactory1885","name":
          "BasicNIOConnFactory","data":{"$area":32.0,"$color":50.0,"path":
            "org/apache/http/impl/nio/pool/BasicNIOConnFactory.html#BasicNIOConnFactory",
            "title":"BasicNIOConnFactory 32 Elements, 50% Coverage"},
          "children":[]},{"id":"BasicNIOConnPool1917","name":
          "BasicNIOConnPool","data":{"$area":23.0,"$color":39.130436,"path":
            "org/apache/http/impl/nio/pool/BasicNIOConnPool.html#BasicNIOConnPool",
            "title":"BasicNIOConnPool 23 Elements, 39.1% Coverage"},
          "children":[]},{"id":"BasicNIOPoolEntry1940","name":
          "BasicNIOPoolEntry","data":{"$area":7.0,"$color":100.0,"path":
            "org/apache/http/impl/nio/pool/BasicNIOPoolEntry.html#BasicNIOPoolEntry",
            "title":"BasicNIOPoolEntry 7 Elements, 100% Coverage"},
          "children":[]}]},{"id":"org.apache.http.nio.entity4003","name":
      "org.apache.http.nio.entity","data":{"$area":350.0,"$color":
        36.285713,"title":
        "org.apache.http.nio.entity 350 Elements, 36.3% Coverage"},
      "children":[{"id":"BufferingNHttpEntity4003","name":
          "BufferingNHttpEntity","data":{"$area":36.0,"$color":50.0,"path":
            "org/apache/http/nio/entity/BufferingNHttpEntity.html#BufferingNHttpEntity",
            "title":"BufferingNHttpEntity 36 Elements, 50% Coverage"},
          "children":[]},{"id":"ConsumingNHttpEntity4039","name":
          "ConsumingNHttpEntity","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/entity/ConsumingNHttpEntity.html#ConsumingNHttpEntity",
            "title":"ConsumingNHttpEntity 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ConsumingNHttpEntityTemplate4039","name":
          "ConsumingNHttpEntityTemplate","data":{"$area":17.0,"$color":0.0,
            "path":
            "org/apache/http/nio/entity/ConsumingNHttpEntityTemplate.html#ConsumingNHttpEntityTemplate",
            "title":
            "ConsumingNHttpEntityTemplate 17 Elements, 0% Coverage"},
          "children":[]},{"id":"ContentBufferEntity4056","name":
          "ContentBufferEntity","data":{"$area":13.0,"$color":69.230774,
            "path":
            "org/apache/http/nio/entity/ContentBufferEntity.html#ContentBufferEntity",
            "title":"ContentBufferEntity 13 Elements, 69.2% Coverage"},
          "children":[]},{"id":"ContentInputStream4069","name":
          "ContentInputStream","data":{"$area":26.0,"$color":76.92308,"path":
            
            "org/apache/http/nio/entity/ContentInputStream.html#ContentInputStream",
            "title":"ContentInputStream 26 Elements, 76.9% Coverage"},
          "children":[]},{"id":"ContentListener4095","name":
          "ContentListener","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/entity/ContentListener.html#ContentListener",
            "title":"ContentListener 0 Elements,  -  Coverage"},"children":[]},
        {"id":"ContentOutputStream4095","name":"ContentOutputStream","data":{
            "$area":17.0,"$color":76.47059,"path":
            "org/apache/http/nio/entity/ContentOutputStream.html#ContentOutputStream",
            "title":"ContentOutputStream 17 Elements, 76.5% Coverage"},
          "children":[]},{"id":"EntityAsyncContentProducer4112","name":
          "EntityAsyncContentProducer","data":{"$area":29.0,"$color":
            17.241379,"path":
            "org/apache/http/nio/entity/EntityAsyncContentProducer.html#EntityAsyncContentProducer",
            "title":
            "EntityAsyncContentProducer 29 Elements, 17.2% Coverage"},
          "children":[]},{"id":"HttpAsyncContentProducer4141","name":
          "HttpAsyncContentProducer","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/nio/entity/HttpAsyncContentProducer.html#HttpAsyncContentProducer",
            "title":"HttpAsyncContentProducer 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NByteArrayEntity4141","name":
          "NByteArrayEntity","data":{"$area":56.0,"$color":50.0,"path":
            "org/apache/http/nio/entity/NByteArrayEntity.html#NByteArrayEntity",
            "title":"NByteArrayEntity 56 Elements, 50% Coverage"},"children":
          []},{"id":"NFileEntity4197","name":"NFileEntity","data":{"$area":
            71.0,"$color":0.0,"path":
            "org/apache/http/nio/entity/NFileEntity.html#NFileEntity",
            "title":"NFileEntity 71 Elements, 0% Coverage"},"children":[]},{
          "id":"NHttpEntityWrapper4268","name":"NHttpEntityWrapper","data":{
            "$area":26.0,"$color":0.0,"path":
            "org/apache/http/nio/entity/NHttpEntityWrapper.html#NHttpEntityWrapper",
            "title":"NHttpEntityWrapper 26 Elements, 0% Coverage"},
          "children":[]},{"id":"NStringEntity4294","name":"NStringEntity",
          "data":{"$area":47.0,"$color":72.34042,"path":
            "org/apache/http/nio/entity/NStringEntity.html#NStringEntity",
            "title":"NStringEntity 47 Elements, 72.3% Coverage"},"children":[]},
        {"id":"ProducingNHttpEntity4341","name":"ProducingNHttpEntity",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/entity/ProducingNHttpEntity.html#ProducingNHttpEntity",
            "title":"ProducingNHttpEntity 0 Elements,  -  Coverage"},
          "children":[]},{"id":"SkipContentListener4341","name":
          "SkipContentListener","data":{"$area":12.0,"$color":0.0,"path":
            "org/apache/http/nio/entity/SkipContentListener.html#SkipContentListener",
            "title":"SkipContentListener 12 Elements, 0% Coverage"},
          "children":[]}]}]}

 ); 