processTreeMapJson (  {"id":"Clover database Wed Nov 28 2012 22:36:05 CET0","name":"","data":{
    "$area":8689.0,"$color":68.76511,"title":
    " 8689 Elements, 68.8% Coverage"},"children":[{"id":
      "org.apache.http.impl.nio0","name":"org.apache.http.impl.nio","data":{
        "$area":1009.0,"$color":50.247772,"title":
        "org.apache.http.impl.nio 1009 Elements, 50.2% Coverage"},"children":
      [{"id":"DefaultClientIOEventDispatch0","name":
          "DefaultClientIOEventDispatch","data":{"$area":34.0,"$color":0.0,
            "path":
            "org/apache/http/impl/nio/DefaultClientIOEventDispatch.html#DefaultClientIOEventDispatch",
            "title":
            "DefaultClientIOEventDispatch 34 Elements, 0% Coverage"},
          "children":[]},{"id":"DefaultHttpClientIODispatch34","name":
          "DefaultHttpClientIODispatch","data":{"$area":37.0,"$color":
            54.05405,"path":
            "org/apache/http/impl/nio/DefaultHttpClientIODispatch.html#DefaultHttpClientIODispatch",
            "title":
            "DefaultHttpClientIODispatch 37 Elements, 54.1% Coverage"},
          "children":[]},{"id":"DefaultHttpServerIODispatch71","name":
          "DefaultHttpServerIODispatch","data":{"$area":36.0,"$color":
            63.88889,"path":
            "org/apache/http/impl/nio/DefaultHttpServerIODispatch.html#DefaultHttpServerIODispatch",
            "title":
            "DefaultHttpServerIODispatch 36 Elements, 63.9% Coverage"},
          "children":[]},{"id":"DefaultNHttpClientConnection107","name":
          "DefaultNHttpClientConnection","data":{"$area":145.0,"$color":
            79.31035,"path":
            "org/apache/http/impl/nio/DefaultNHttpClientConnection.html#DefaultNHttpClientConnection",
            "title":
            "DefaultNHttpClientConnection 145 Elements, 79.3% Coverage"},
          "children":[]},{"id":"DefaultNHttpClientConnectionFactory252",
          "name":"DefaultNHttpClientConnectionFactory","data":{"$area":
            26.0,"$color":76.92308,"path":
            "org/apache/http/impl/nio/DefaultNHttpClientConnectionFactory.html#DefaultNHttpClientConnectionFactory",
            "title":
            "DefaultNHttpClientConnectionFactory 26 Elements, 76.9% Coverage"},
          "children":[]},{"id":"DefaultNHttpServerConnection278","name":
          "DefaultNHttpServerConnection","data":{"$area":147.0,"$color":
            85.03401,"path":
            "org/apache/http/impl/nio/DefaultNHttpServerConnection.html#DefaultNHttpServerConnection",
            "title":
            "DefaultNHttpServerConnection 147 Elements, 85% Coverage"},
          "children":[]},{"id":"DefaultNHttpServerConnectionFactory425",
          "name":"DefaultNHttpServerConnectionFactory","data":{"$area":
            26.0,"$color":69.230774,"path":
            "org/apache/http/impl/nio/DefaultNHttpServerConnectionFactory.html#DefaultNHttpServerConnectionFactory",
            "title":
            "DefaultNHttpServerConnectionFactory 26 Elements, 69.2% Coverage"},
          "children":[]},{"id":"DefaultServerIOEventDispatch451","name":
          "DefaultServerIOEventDispatch","data":{"$area":33.0,"$color":0.0,
            "path":
            "org/apache/http/impl/nio/DefaultServerIOEventDispatch.html#DefaultServerIOEventDispatch",
            "title":
            "DefaultServerIOEventDispatch 33 Elements, 0% Coverage"},
          "children":[]},{"id":"NHttpClientEventHandlerAdaptor484","name":
          "NHttpClientEventHandlerAdaptor","data":{"$area":33.0,"$color":
            0.0,"path":
            "org/apache/http/impl/nio/NHttpClientEventHandlerAdaptor.html#NHttpClientEventHandlerAdaptor",
            "title":
            "NHttpClientEventHandlerAdaptor 33 Elements, 0% Coverage"},
          "children":[]},{"id":"NHttpConnectionBase517","name":
          "NHttpConnectionBase","data":{"$area":215.0,"$color":61.860466,
            "path":
            "org/apache/http/impl/nio/NHttpConnectionBase.html#NHttpConnectionBase",
            "title":"NHttpConnectionBase 215 Elements, 61.9% Coverage"},
          "children":[]},{"id":"NHttpServerEventHandlerAdaptor732","name":
          "NHttpServerEventHandlerAdaptor","data":{"$area":33.0,"$color":
            0.0,"path":
            "org/apache/http/impl/nio/NHttpServerEventHandlerAdaptor.html#NHttpServerEventHandlerAdaptor",
            "title":
            "NHttpServerEventHandlerAdaptor 33 Elements, 0% Coverage"},
          "children":[]},{"id":"SSLClientIOEventDispatch765","name":
          "SSLClientIOEventDispatch","data":{"$area":76.0,"$color":0.0,
            "path":
            "org/apache/http/impl/nio/SSLClientIOEventDispatch.html#SSLClientIOEventDispatch",
            "title":"SSLClientIOEventDispatch 76 Elements, 0% Coverage"},
          "children":[]},{"id":"SSLNHttpClientConnectionFactory841","name":
          "SSLNHttpClientConnectionFactory","data":{"$area":42.0,"$color":
            52.380955,"path":
            "org/apache/http/impl/nio/SSLNHttpClientConnectionFactory.html#SSLNHttpClientConnectionFactory",
            "title":
            "SSLNHttpClientConnectionFactory 42 Elements, 52.4% Coverage"},
          "children":[]},{"id":"SSLNHttpServerConnectionFactory883","name":
          "SSLNHttpServerConnectionFactory","data":{"$area":42.0,"$color":
            52.380955,"path":
            "org/apache/http/impl/nio/SSLNHttpServerConnectionFactory.html#SSLNHttpServerConnectionFactory",
            "title":
            "SSLNHttpServerConnectionFactory 42 Elements, 52.4% Coverage"},
          "children":[]},{"id":"SSLServerIOEventDispatch925","name":
          "SSLServerIOEventDispatch","data":{"$area":75.0,"$color":0.0,
            "path":
            "org/apache/http/impl/nio/SSLServerIOEventDispatch.html#SSLServerIOEventDispatch",
            "title":"SSLServerIOEventDispatch 75 Elements, 0% Coverage"},
          "children":[]},{"id":"SessionHttpContext1000","name":
          "SessionHttpContext","data":{"$area":9.0,"$color":100.0,"path":
            "org/apache/http/impl/nio/SessionHttpContext.html#SessionHttpContext",
            "title":"SessionHttpContext 9 Elements, 100% Coverage"},
          "children":[]}]},{"id":"org.apache.http.impl.nio.codecs1009",
      "name":"org.apache.http.impl.nio.codecs","data":{"$area":768.0,
        "$color":85.02605,"title":
        "org.apache.http.impl.nio.codecs 768 Elements, 85% Coverage"},
      "children":[{"id":"AbstractContentDecoder1009","name":
          "AbstractContentDecoder","data":{"$area":19.0,"$color":100.0,
            "path":
            "org/apache/http/impl/nio/codecs/AbstractContentDecoder.html#AbstractContentDecoder",
            "title":"AbstractContentDecoder 19 Elements, 100% Coverage"},
          "children":[]},{"id":"AbstractContentEncoder1028","name":
          "AbstractContentEncoder","data":{"$area":26.0,"$color":100.0,
            "path":
            "org/apache/http/impl/nio/codecs/AbstractContentEncoder.html#AbstractContentEncoder",
            "title":"AbstractContentEncoder 26 Elements, 100% Coverage"},
          "children":[]},{"id":"AbstractMessageParser1054","name":
          "AbstractMessageParser","data":{"$area":110.0,"$color":97.27273,
            "path":
            "org/apache/http/impl/nio/codecs/AbstractMessageParser.html#AbstractMessageParser",
            "title":"AbstractMessageParser 110 Elements, 97.3% Coverage"},
          "children":[]},{"id":"AbstractMessageWriter1164","name":
          "AbstractMessageWriter","data":{"$area":25.0,"$color":80.0,"path":
            "org/apache/http/impl/nio/codecs/AbstractMessageWriter.html#AbstractMessageWriter",
            "title":"AbstractMessageWriter 25 Elements, 80% Coverage"},
          "children":[]},{"id":"ChunkDecoder1189","name":"ChunkDecoder",
          "data":{"$area":179.0,"$color":96.089386,"path":
            "org/apache/http/impl/nio/codecs/ChunkDecoder.html#ChunkDecoder",
            "title":"ChunkDecoder 179 Elements, 96.1% Coverage"},"children":[]},
        {"id":"ChunkEncoder1368","name":"ChunkEncoder","data":{"$area":
            67.0,"$color":85.07463,"path":
            "org/apache/http/impl/nio/codecs/ChunkEncoder.html#ChunkEncoder",
            "title":"ChunkEncoder 67 Elements, 85.1% Coverage"},"children":[]},
        {"id":"DefaultHttpRequestParser1435","name":
          "DefaultHttpRequestParser","data":{"$area":11.0,"$color":100.0,
            "path":
            "org/apache/http/impl/nio/codecs/DefaultHttpRequestParser.html#DefaultHttpRequestParser",
            "title":"DefaultHttpRequestParser 11 Elements, 100% Coverage"},
          "children":[]},{"id":"DefaultHttpRequestWriter1446","name":
          "DefaultHttpRequestWriter","data":{"$area":5.0,"$color":100.0,
            "path":
            "org/apache/http/impl/nio/codecs/DefaultHttpRequestWriter.html#DefaultHttpRequestWriter",
            "title":"DefaultHttpRequestWriter 5 Elements, 100% Coverage"},
          "children":[]},{"id":"DefaultHttpResponseParser1451","name":
          "DefaultHttpResponseParser","data":{"$area":11.0,"$color":100.0,
            "path":
            "org/apache/http/impl/nio/codecs/DefaultHttpResponseParser.html#DefaultHttpResponseParser",
            "title":"DefaultHttpResponseParser 11 Elements, 100% Coverage"},
          "children":[]},{"id":"DefaultHttpResponseWriter1462","name":
          "DefaultHttpResponseWriter","data":{"$area":5.0,"$color":100.0,
            "path":
            "org/apache/http/impl/nio/codecs/DefaultHttpResponseWriter.html#DefaultHttpResponseWriter",
            "title":"DefaultHttpResponseWriter 5 Elements, 100% Coverage"},
          "children":[]},{"id":"HttpRequestParser1467","name":
          "HttpRequestParser","data":{"$area":11.0,"$color":0.0,"path":
            "org/apache/http/impl/nio/codecs/HttpRequestParser.html#HttpRequestParser",
            "title":"HttpRequestParser 11 Elements, 0% Coverage"},"children":
          []},{"id":"HttpRequestWriter1478","name":"HttpRequestWriter",
          "data":{"$area":5.0,"$color":0.0,"path":
            "org/apache/http/impl/nio/codecs/HttpRequestWriter.html#HttpRequestWriter",
            "title":"HttpRequestWriter 5 Elements, 0% Coverage"},"children":[]},
        {"id":"HttpResponseParser1483","name":"HttpResponseParser","data":{
            "$area":11.0,"$color":0.0,"path":
            "org/apache/http/impl/nio/codecs/HttpResponseParser.html#HttpResponseParser",
            "title":"HttpResponseParser 11 Elements, 0% Coverage"},
          "children":[]},{"id":"HttpResponseWriter1494","name":
          "HttpResponseWriter","data":{"$area":5.0,"$color":0.0,"path":
            "org/apache/http/impl/nio/codecs/HttpResponseWriter.html#HttpResponseWriter",
            "title":"HttpResponseWriter 5 Elements, 0% Coverage"},"children":
          []},{"id":"IdentityDecoder1499","name":"IdentityDecoder","data":{
            "$area":71.0,"$color":80.28169,"path":
            "org/apache/http/impl/nio/codecs/IdentityDecoder.html#IdentityDecoder",
            "title":"IdentityDecoder 71 Elements, 80.3% Coverage"},
          "children":[]},{"id":"IdentityEncoder1570","name":
          "IdentityEncoder","data":{"$area":32.0,"$color":43.75,"path":
            "org/apache/http/impl/nio/codecs/IdentityEncoder.html#IdentityEncoder",
            "title":"IdentityEncoder 32 Elements, 43.8% Coverage"},
          "children":[]},{"id":"LengthDelimitedDecoder1602","name":
          "LengthDelimitedDecoder","data":{"$area":112.0,"$color":87.5,
            "path":
            "org/apache/http/impl/nio/codecs/LengthDelimitedDecoder.html#LengthDelimitedDecoder",
            "title":"LengthDelimitedDecoder 112 Elements, 87.5% Coverage"},
          "children":[]},{"id":"LengthDelimitedEncoder1714","name":
          "LengthDelimitedEncoder","data":{"$area":63.0,"$color":80.952385,
            "path":
            "org/apache/http/impl/nio/codecs/LengthDelimitedEncoder.html#LengthDelimitedEncoder",
            "title":"LengthDelimitedEncoder 63 Elements, 81% Coverage"},
          "children":[]}]},{"id":"org.apache.http.impl.nio.reactor1834",
      "name":"org.apache.http.impl.nio.reactor","data":{"$area":1941.0,
        "$color":62.648117,"title":
        "org.apache.http.impl.nio.reactor 1941 Elements, 62.6% Coverage"},
      "children":[{"id":"AbstractIODispatch1834","name":
          "AbstractIODispatch","data":{"$area":96.0,"$color":66.66667,"path":
            
            "org/apache/http/impl/nio/reactor/AbstractIODispatch.html#AbstractIODispatch",
            "title":"AbstractIODispatch 96 Elements, 66.7% Coverage"},
          "children":[]},{"id":"AbstractIOReactor1930","name":
          "AbstractIOReactor","data":{"$area":283.0,"$color":65.371025,
            "path":
            "org/apache/http/impl/nio/reactor/AbstractIOReactor.html#AbstractIOReactor",
            "title":"AbstractIOReactor 283 Elements, 65.4% Coverage"},
          "children":[]},{"id":"AbstractMultiworkerIOReactor2213","name":
          "AbstractMultiworkerIOReactor","data":{"$area":260.0,"$color":
            70.76923,"path":
            "org/apache/http/impl/nio/reactor/AbstractMultiworkerIOReactor.html#AbstractMultiworkerIOReactor",
            "title":
            "AbstractMultiworkerIOReactor 260 Elements, 70.8% Coverage"},
          "children":[]},{"id":"AbstractMultiworkerIOReactor.Worker2473",
          "name":"AbstractMultiworkerIOReactor.Worker","data":{"$area":
            10.0,"$color":100.0,"path":
            "org/apache/http/impl/nio/reactor/AbstractMultiworkerIOReactor.html#AbstractMultiworkerIOReactor.Worker",
            "title":
            "AbstractMultiworkerIOReactor.Worker 10 Elements, 100% Coverage"},
          "children":[]},{"id":
          "AbstractMultiworkerIOReactor.DefaultThreadFactory2483","name":
          "AbstractMultiworkerIOReactor.DefaultThreadFactory","data":{
            "$area":2.0,"$color":100.0,"path":
            "org/apache/http/impl/nio/reactor/AbstractMultiworkerIOReactor.html#AbstractMultiworkerIOReactor.DefaultThreadFactory",
            "title":
            "AbstractMultiworkerIOReactor.DefaultThreadFactory 2 Elements, 100% Coverage"},
          "children":[]},{"id":"BaseIOReactor2485","name":"BaseIOReactor",
          "data":{"$area":93.0,"$color":77.41935,"path":
            "org/apache/http/impl/nio/reactor/BaseIOReactor.html#BaseIOReactor",
            "title":"BaseIOReactor 93 Elements, 77.4% Coverage"},"children":[]},
        {"id":"ChannelEntry2578","name":"ChannelEntry","data":{"$area":
            20.0,"$color":90.0,"path":
            "org/apache/http/impl/nio/reactor/ChannelEntry.html#ChannelEntry",
            "title":"ChannelEntry 20 Elements, 90% Coverage"},"children":[]},
        {"id":"DefaultConnectingIOReactor2598","name":
          "DefaultConnectingIOReactor","data":{"$area":146.0,"$color":
            60.273975,"path":
            "org/apache/http/impl/nio/reactor/DefaultConnectingIOReactor.html#DefaultConnectingIOReactor",
            "title":
            "DefaultConnectingIOReactor 146 Elements, 60.3% Coverage"},
          "children":[]},{"id":"DefaultListeningIOReactor2744","name":
          "DefaultListeningIOReactor","data":{"$area":146.0,"$color":
            60.958904,"path":
            "org/apache/http/impl/nio/reactor/DefaultListeningIOReactor.html#DefaultListeningIOReactor",
            "title":"DefaultListeningIOReactor 146 Elements, 61% Coverage"},
          "children":[]},{"id":"ExceptionEvent2890","name":
          "ExceptionEvent","data":{"$area":20.0,"$color":60.000004,"path":
            "org/apache/http/impl/nio/reactor/ExceptionEvent.html#ExceptionEvent",
            "title":"ExceptionEvent 20 Elements, 60% Coverage"},"children":[]},
        {"id":"IOReactorConfig2910","name":"IOReactorConfig","data":{"$area":
            70.0,"$color":51.428574,"path":
            "org/apache/http/impl/nio/reactor/IOReactorConfig.html#IOReactorConfig",
            "title":"IOReactorConfig 70 Elements, 51.4% Coverage"},
          "children":[]},{"id":"IOSessionImpl2980","name":"IOSessionImpl",
          "data":{"$area":205.0,"$color":47.317074,"path":
            "org/apache/http/impl/nio/reactor/IOSessionImpl.html#IOSessionImpl",
            "title":"IOSessionImpl 205 Elements, 47.3% Coverage"},"children":
          []},{"id":"InterestOpEntry3185","name":"InterestOpEntry","data":{
            "$area":16.0,"$color":0.0,"path":
            "org/apache/http/impl/nio/reactor/InterestOpEntry.html#InterestOpEntry",
            "title":"InterestOpEntry 16 Elements, 0% Coverage"},"children":[]},
        {"id":"InterestOpsCallback3201","name":"InterestOpsCallback","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/impl/nio/reactor/InterestOpsCallback.html#InterestOpsCallback",
            "title":"InterestOpsCallback 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ListenerEndpointClosedCallback3201","name":
          "ListenerEndpointClosedCallback","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/impl/nio/reactor/ListenerEndpointClosedCallback.html#ListenerEndpointClosedCallback",
            "title":
            "ListenerEndpointClosedCallback 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ListenerEndpointImpl3201","name":
          "ListenerEndpointImpl","data":{"$area":86.0,"$color":69.76744,
            "path":
            "org/apache/http/impl/nio/reactor/ListenerEndpointImpl.html#ListenerEndpointImpl",
            "title":"ListenerEndpointImpl 86 Elements, 69.8% Coverage"},
          "children":[]},{"id":"SSLIOSession3287","name":"SSLIOSession",
          "data":{"$area":26.0,"$color":0.0,"path":
            "org/apache/http/impl/nio/reactor/SSLIOSession.html#SSLIOSession",
            "title":"SSLIOSession 26 Elements, 0% Coverage"},"children":[]},{
          "id":"SSLIOSessionHandler3313","name":"SSLIOSessionHandler","data":
          {"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/impl/nio/reactor/SSLIOSessionHandler.html#SSLIOSessionHandler",
            "title":"SSLIOSessionHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"SSLIOSessionHandlerAdaptor3313","name":
          "SSLIOSessionHandlerAdaptor","data":{"$area":11.0,"$color":0.0,
            "path":
            "org/apache/http/impl/nio/reactor/SSLIOSessionHandlerAdaptor.html#SSLIOSessionHandlerAdaptor",
            "title":"SSLIOSessionHandlerAdaptor 11 Elements, 0% Coverage"},
          "children":[]},{"id":"SSLMode3324","name":"SSLMode","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/impl/nio/reactor/SSLMode.html#SSLMode","title":
            "SSLMode 0 Elements,  -  Coverage"},"children":[]},{"id":
          "SSLSetupHandler3324","name":"SSLSetupHandler","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/impl/nio/reactor/SSLSetupHandler.html#SSLSetupHandler",
            "title":"SSLSetupHandler 0 Elements,  -  Coverage"},"children":[]},
        {"id":"SSLSetupHandlerAdaptor3324","name":"SSLSetupHandlerAdaptor",
          "data":{"$area":11.0,"$color":0.0,"path":
            "org/apache/http/impl/nio/reactor/SSLSetupHandlerAdaptor.html#SSLSetupHandlerAdaptor",
            "title":"SSLSetupHandlerAdaptor 11 Elements, 0% Coverage"},
          "children":[]},{"id":"SessionClosedCallback3335","name":
          "SessionClosedCallback","data":{"$area":0.0,"$color":-100.0,"path":
            
            "org/apache/http/impl/nio/reactor/SessionClosedCallback.html#SessionClosedCallback",
            "title":"SessionClosedCallback 0 Elements,  -  Coverage"},
          "children":[]},{"id":"SessionHandle3335","name":"SessionHandle",
          "data":{"$area":30.0,"$color":0.0,"path":
            "org/apache/http/impl/nio/reactor/SessionHandle.html#SessionHandle",
            "title":"SessionHandle 30 Elements, 0% Coverage"},"children":[]},
        {"id":"SessionInputBufferImpl3365","name":"SessionInputBufferImpl",
          "data":{"$area":143.0,"$color":91.60839,"path":
            "org/apache/http/impl/nio/reactor/SessionInputBufferImpl.html#SessionInputBufferImpl",
            "title":"SessionInputBufferImpl 143 Elements, 91.6% Coverage"},
          "children":[]},{"id":"SessionOutputBufferImpl3508","name":
          "SessionOutputBufferImpl","data":{"$area":116.0,"$color":
            87.93104,"path":
            "org/apache/http/impl/nio/reactor/SessionOutputBufferImpl.html#SessionOutputBufferImpl",
            "title":"SessionOutputBufferImpl 116 Elements, 87.9% Coverage"},
          "children":[]},{"id":"SessionRequestHandle3624","name":
          "SessionRequestHandle","data":{"$area":12.0,"$color":66.66667,
            "path":
            "org/apache/http/impl/nio/reactor/SessionRequestHandle.html#SessionRequestHandle",
            "title":"SessionRequestHandle 12 Elements, 66.7% Coverage"},
          "children":[]},{"id":"SessionRequestImpl3636","name":
          "SessionRequestImpl","data":{"$area":139.0,"$color":41.72662,
            "path":
            "org/apache/http/impl/nio/reactor/SessionRequestImpl.html#SessionRequestImpl",
            "title":"SessionRequestImpl 139 Elements, 41.7% Coverage"},
          "children":[]}]},{"id":"org.apache.http.impl.nio.ssl3775","name":
      "org.apache.http.impl.nio.ssl","data":{"$area":63.0,"$color":0.0,
        "title":"org.apache.http.impl.nio.ssl 63 Elements, 0% Coverage"},
      "children":[{"id":"SSLClientIOEventDispatch3775","name":
          "SSLClientIOEventDispatch","data":{"$area":32.0,"$color":0.0,
            "path":
            "org/apache/http/impl/nio/ssl/SSLClientIOEventDispatch.html#SSLClientIOEventDispatch",
            "title":"SSLClientIOEventDispatch 32 Elements, 0% Coverage"},
          "children":[]},{"id":"SSLServerIOEventDispatch3807","name":
          "SSLServerIOEventDispatch","data":{"$area":31.0,"$color":0.0,
            "path":
            "org/apache/http/impl/nio/ssl/SSLServerIOEventDispatch.html#SSLServerIOEventDispatch",
            "title":"SSLServerIOEventDispatch 31 Elements, 0% Coverage"},
          "children":[]}]},{"id":"org.apache.http.nio.util8176","name":
      "org.apache.http.nio.util","data":{"$area":513.0,"$color":67.64133,
        "title":"org.apache.http.nio.util 513 Elements, 67.6% Coverage"},
      "children":[{"id":"BufferInfo8176","name":"BufferInfo","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/nio/util/BufferInfo.html#BufferInfo","title":
            "BufferInfo 0 Elements,  -  Coverage"},"children":[]},{"id":
          "ByteBufferAllocator8176","name":"ByteBufferAllocator","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/util/ByteBufferAllocator.html#ByteBufferAllocator",
            "title":"ByteBufferAllocator 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ContentInputBuffer8176","name":
          "ContentInputBuffer","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/util/ContentInputBuffer.html#ContentInputBuffer",
            "title":"ContentInputBuffer 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ContentOutputBuffer8176","name":
          "ContentOutputBuffer","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/util/ContentOutputBuffer.html#ContentOutputBuffer",
            "title":"ContentOutputBuffer 0 Elements,  -  Coverage"},
          "children":[]},{"id":"DirectByteBufferAllocator8176","name":
          "DirectByteBufferAllocator","data":{"$area":2.0,"$color":100.0,
            "path":
            "org/apache/http/nio/util/DirectByteBufferAllocator.html#DirectByteBufferAllocator",
            "title":"DirectByteBufferAllocator 2 Elements, 100% Coverage"},
          "children":[]},{"id":"ExpandableBuffer8178","name":
          "ExpandableBuffer","data":{"$area":75.0,"$color":69.333336,"path":
            "org/apache/http/nio/util/ExpandableBuffer.html#ExpandableBuffer",
            "title":"ExpandableBuffer 75 Elements, 69.3% Coverage"},
          "children":[]},{"id":"HeapByteBufferAllocator8253","name":
          "HeapByteBufferAllocator","data":{"$area":2.0,"$color":100.0,
            "path":
            "org/apache/http/nio/util/HeapByteBufferAllocator.html#HeapByteBufferAllocator",
            "title":"HeapByteBufferAllocator 2 Elements, 100% Coverage"},
          "children":[]},{"id":"SharedInputBuffer8255","name":
          "SharedInputBuffer","data":{"$area":166.0,"$color":59.03614,"path":
            
            "org/apache/http/nio/util/SharedInputBuffer.html#SharedInputBuffer",
            "title":"SharedInputBuffer 166 Elements, 59% Coverage"},
          "children":[]},{"id":"SharedOutputBuffer8421","name":
          "SharedOutputBuffer","data":{"$area":155.0,"$color":63.225807,
            "path":
            "org/apache/http/nio/util/SharedOutputBuffer.html#SharedOutputBuffer",
            "title":"SharedOutputBuffer 155 Elements, 63.2% Coverage"},
          "children":[]},{"id":"SimpleInputBuffer8576","name":
          "SimpleInputBuffer","data":{"$area":64.0,"$color":96.875,"path":
            "org/apache/http/nio/util/SimpleInputBuffer.html#SimpleInputBuffer",
            "title":"SimpleInputBuffer 64 Elements, 96.9% Coverage"},
          "children":[]},{"id":"SimpleOutputBuffer8640","name":
          "SimpleOutputBuffer","data":{"$area":49.0,"$color":67.34694,"path":
            
            "org/apache/http/nio/util/SimpleOutputBuffer.html#SimpleOutputBuffer",
            "title":"SimpleOutputBuffer 49 Elements, 67.3% Coverage"},
          "children":[]}]},{"id":"org.apache.http.nio3838","name":
      "org.apache.http.nio","data":{"$area":14.0,"$color":100.0,"title":
        "org.apache.http.nio 14 Elements, 100% Coverage"},"children":[{"id":
          "ContentDecoder3838","name":"ContentDecoder","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/nio/ContentDecoder.html#ContentDecoder","title":
            "ContentDecoder 0 Elements,  -  Coverage"},"children":[]},{"id":
          "ContentDecoderChannel3838","name":"ContentDecoderChannel","data":{
            "$area":7.0,"$color":100.0,"path":
            "org/apache/http/nio/ContentDecoderChannel.html#ContentDecoderChannel",
            "title":"ContentDecoderChannel 7 Elements, 100% Coverage"},
          "children":[]},{"id":"ContentEncoder3845","name":
          "ContentEncoder","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/ContentEncoder.html#ContentEncoder","title":
            "ContentEncoder 0 Elements,  -  Coverage"},"children":[]},{"id":
          "ContentEncoderChannel3845","name":"ContentEncoderChannel","data":{
            "$area":7.0,"$color":100.0,"path":
            "org/apache/http/nio/ContentEncoderChannel.html#ContentEncoderChannel",
            "title":"ContentEncoderChannel 7 Elements, 100% Coverage"},
          "children":[]},{"id":"FileContentDecoder3852","name":
          "FileContentDecoder","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/FileContentDecoder.html#FileContentDecoder",
            "title":"FileContentDecoder 0 Elements,  -  Coverage"},
          "children":[]},{"id":"FileContentEncoder3852","name":
          "FileContentEncoder","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/FileContentEncoder.html#FileContentEncoder",
            "title":"FileContentEncoder 0 Elements,  -  Coverage"},
          "children":[]},{"id":"IOControl3852","name":"IOControl","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/IOControl.html#IOControl","title":
            "IOControl 0 Elements,  -  Coverage"},"children":[]},{"id":
          "NHttpClientConnection3852","name":"NHttpClientConnection","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/NHttpClientConnection.html#NHttpClientConnection",
            "title":"NHttpClientConnection 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpClientEventHandler3852","name":
          "NHttpClientEventHandler","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/nio/NHttpClientEventHandler.html#NHttpClientEventHandler",
            "title":"NHttpClientEventHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpClientHandler3852","name":
          "NHttpClientHandler","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/NHttpClientHandler.html#NHttpClientHandler",
            "title":"NHttpClientHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpClientIOTarget3852","name":
          "NHttpClientIOTarget","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/NHttpClientIOTarget.html#NHttpClientIOTarget",
            "title":"NHttpClientIOTarget 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpConnection3852","name":
          "NHttpConnection","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/NHttpConnection.html#NHttpConnection",
            "title":"NHttpConnection 0 Elements,  -  Coverage"},"children":[]},
        {"id":"NHttpConnectionFactory3852","name":"NHttpConnectionFactory",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/NHttpConnectionFactory.html#NHttpConnectionFactory",
            "title":"NHttpConnectionFactory 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpMessageParser3852","name":
          "NHttpMessageParser","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/NHttpMessageParser.html#NHttpMessageParser",
            "title":"NHttpMessageParser 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpMessageWriter3852","name":
          "NHttpMessageWriter","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/NHttpMessageWriter.html#NHttpMessageWriter",
            "title":"NHttpMessageWriter 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpServerConnection3852","name":
          "NHttpServerConnection","data":{"$area":0.0,"$color":-100.0,"path":
            
            "org/apache/http/nio/NHttpServerConnection.html#NHttpServerConnection",
            "title":"NHttpServerConnection 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpServerEventHandler3852","name":
          "NHttpServerEventHandler","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/nio/NHttpServerEventHandler.html#NHttpServerEventHandler",
            "title":"NHttpServerEventHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpServerIOTarget3852","name":
          "NHttpServerIOTarget","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/NHttpServerIOTarget.html#NHttpServerIOTarget",
            "title":"NHttpServerIOTarget 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpServiceHandler3852","name":
          "NHttpServiceHandler","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/NHttpServiceHandler.html#NHttpServiceHandler",
            "title":"NHttpServiceHandler 0 Elements,  -  Coverage"},
          "children":[]}]},{"id":"org.apache.http.nio.params4247","name":
      "org.apache.http.nio.params","data":{"$area":56.0,"$color":0.0,"title":
        "org.apache.http.nio.params 56 Elements, 0% Coverage"},"children":[{
          "id":"NIOReactorPNames4247","name":"NIOReactorPNames","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/params/NIOReactorPNames.html#NIOReactorPNames",
            "title":"NIOReactorPNames 0 Elements,  -  Coverage"},"children":[]},
        {"id":"NIOReactorParamBean4247","name":"NIOReactorParamBean","data":{
            "$area":6.0,"$color":0.0,"path":
            "org/apache/http/nio/params/NIOReactorParamBean.html#NIOReactorParamBean",
            "title":"NIOReactorParamBean 6 Elements, 0% Coverage"},
          "children":[]},{"id":"NIOReactorParams4253","name":
          "NIOReactorParams","data":{"$area":50.0,"$color":0.0,"path":
            "org/apache/http/nio/params/NIOReactorParams.html#NIOReactorParams",
            "title":"NIOReactorParams 50 Elements, 0% Coverage"},"children":[]}]},
    {"id":"org.apache.http.nio.reactor.ssl7822","name":
      "org.apache.http.nio.reactor.ssl","data":{"$area":354.0,"$color":
        69.49152,"title":
        "org.apache.http.nio.reactor.ssl 354 Elements, 69.5% Coverage"},
      "children":[{"id":"SSLIOSession7822","name":"SSLIOSession","data":{
            "$area":346.0,"$color":69.94219,"path":
            "org/apache/http/nio/reactor/ssl/SSLIOSession.html#SSLIOSession",
            "title":"SSLIOSession 346 Elements, 69.9% Coverage"},"children":[]},
        {"id":"SSLIOSession.InternalByteChannel8168","name":
          "SSLIOSession.InternalByteChannel","data":{"$area":8.0,"$color":
            50.0,"path":
            "org/apache/http/nio/reactor/ssl/SSLIOSession.html#SSLIOSession.InternalByteChannel",
            "title":
            "SSLIOSession.InternalByteChannel 8 Elements, 50% Coverage"},
          "children":[]},{"id":"SSLMode8176","name":"SSLMode","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/ssl/SSLMode.html#SSLMode","title":
            "SSLMode 0 Elements,  -  Coverage"},"children":[]},{"id":
          "SSLSetupHandler8176","name":"SSLSetupHandler","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/ssl/SSLSetupHandler.html#SSLSetupHandler",
            "title":"SSLSetupHandler 0 Elements,  -  Coverage"},"children":[]}]},
    {"id":"org.apache.http.nio.reactor7814","name":
      "org.apache.http.nio.reactor","data":{"$area":8.0,"$color":62.5,
        "title":"org.apache.http.nio.reactor 8 Elements, 62.5% Coverage"},
      "children":[{"id":"ConnectingIOReactor7814","name":
          "ConnectingIOReactor","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/ConnectingIOReactor.html#ConnectingIOReactor",
            "title":"ConnectingIOReactor 0 Elements,  -  Coverage"},
          "children":[]},{"id":"EventMask7814","name":"EventMask","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/EventMask.html#EventMask","title":
            "EventMask 0 Elements,  -  Coverage"},"children":[]},{"id":
          "IOEventDispatch7814","name":"IOEventDispatch","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/IOEventDispatch.html#IOEventDispatch",
            "title":"IOEventDispatch 0 Elements,  -  Coverage"},"children":[]},
        {"id":"IOReactor7814","name":"IOReactor","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/nio/reactor/IOReactor.html#IOReactor","title":
            "IOReactor 0 Elements,  -  Coverage"},"children":[]},{"id":
          "IOReactorException7814","name":"IOReactorException","data":{
            "$area":8.0,"$color":62.5,"path":
            "org/apache/http/nio/reactor/IOReactorException.html#IOReactorException",
            "title":"IOReactorException 8 Elements, 62.5% Coverage"},
          "children":[]},{"id":"IOReactorExceptionHandler7822","name":
          "IOReactorExceptionHandler","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/nio/reactor/IOReactorExceptionHandler.html#IOReactorExceptionHandler",
            "title":"IOReactorExceptionHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"IOReactorStatus7822","name":
          "IOReactorStatus","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/IOReactorStatus.html#IOReactorStatus",
            "title":"IOReactorStatus 0 Elements,  -  Coverage"},"children":[]},
        {"id":"IOSession7822","name":"IOSession","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/nio/reactor/IOSession.html#IOSession","title":
            "IOSession 0 Elements,  -  Coverage"},"children":[]},{"id":
          "ListenerEndpoint7822","name":"ListenerEndpoint","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/ListenerEndpoint.html#ListenerEndpoint",
            "title":"ListenerEndpoint 0 Elements,  -  Coverage"},"children":[]},
        {"id":"ListeningIOReactor7822","name":"ListeningIOReactor","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/ListeningIOReactor.html#ListeningIOReactor",
            "title":"ListeningIOReactor 0 Elements,  -  Coverage"},
          "children":[]},{"id":"SessionBufferStatus7822","name":
          "SessionBufferStatus","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/SessionBufferStatus.html#SessionBufferStatus",
            "title":"SessionBufferStatus 0 Elements,  -  Coverage"},
          "children":[]},{"id":"SessionInputBuffer7822","name":
          "SessionInputBuffer","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/SessionInputBuffer.html#SessionInputBuffer",
            "title":"SessionInputBuffer 0 Elements,  -  Coverage"},
          "children":[]},{"id":"SessionOutputBuffer7822","name":
          "SessionOutputBuffer","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/SessionOutputBuffer.html#SessionOutputBuffer",
            "title":"SessionOutputBuffer 0 Elements,  -  Coverage"},
          "children":[]},{"id":"SessionRequest7822","name":
          "SessionRequest","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/SessionRequest.html#SessionRequest",
            "title":"SessionRequest 0 Elements,  -  Coverage"},"children":[]},
        {"id":"SessionRequestCallback7822","name":"SessionRequestCallback",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/SessionRequestCallback.html#SessionRequestCallback",
            "title":"SessionRequestCallback 0 Elements,  -  Coverage"},
          "children":[]},{"id":"SocketAccessor7822","name":
          "SocketAccessor","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/reactor/SocketAccessor.html#SocketAccessor",
            "title":"SocketAccessor 0 Elements,  -  Coverage"},"children":[]}]},
    {"id":"org.apache.http.nio.protocol4863","name":
      "org.apache.http.nio.protocol","data":{"$area":2951.0,"$color":
        77.600815,"title":
        "org.apache.http.nio.protocol 2951 Elements, 77.6% Coverage"},
      "children":[{"id":"AbstractAsyncRequestConsumer4863","name":
          "AbstractAsyncRequestConsumer","data":{"$area":46.0,"$color":
            89.13044,"path":
            "org/apache/http/nio/protocol/AbstractAsyncRequestConsumer.html#AbstractAsyncRequestConsumer",
            "title":
            "AbstractAsyncRequestConsumer 46 Elements, 89.1% Coverage"},
          "children":[]},{"id":"AbstractAsyncResponseConsumer4909","name":
          "AbstractAsyncResponseConsumer","data":{"$area":51.0,"$color":
            98.039215,"path":
            "org/apache/http/nio/protocol/AbstractAsyncResponseConsumer.html#AbstractAsyncResponseConsumer",
            "title":
            "AbstractAsyncResponseConsumer 51 Elements, 98% Coverage"},
          "children":[]},{"id":"AsyncNHttpClientHandler4960","name":
          "AsyncNHttpClientHandler","data":{"$area":245.0,"$color":
            66.12245,"path":
            "org/apache/http/nio/protocol/AsyncNHttpClientHandler.html#AsyncNHttpClientHandler",
            "title":"AsyncNHttpClientHandler 245 Elements, 66.1% Coverage"},
          "children":[]},{"id":
          "AsyncNHttpClientHandler.ClientConnState5205","name":
          "AsyncNHttpClientHandler.ClientConnState","data":{"$area":49.0,
            "$color":95.918365,"path":
            "org/apache/http/nio/protocol/AsyncNHttpClientHandler.html#AsyncNHttpClientHandler.ClientConnState",
            "title":
            "AsyncNHttpClientHandler.ClientConnState 49 Elements, 95.9% Coverage"},
          "children":[]},{"id":"AsyncNHttpServiceHandler5254","name":
          "AsyncNHttpServiceHandler","data":{"$area":295.0,"$color":
            66.77966,"path":
            "org/apache/http/nio/protocol/AsyncNHttpServiceHandler.html#AsyncNHttpServiceHandler",
            "title":
            "AsyncNHttpServiceHandler 295 Elements, 66.8% Coverage"},
          "children":[]},{"id":
          "AsyncNHttpServiceHandler.ServerConnState5549","name":
          "AsyncNHttpServiceHandler.ServerConnState","data":{"$area":61.0,
            "$color":83.60656,"path":
            "org/apache/http/nio/protocol/AsyncNHttpServiceHandler.html#AsyncNHttpServiceHandler.ServerConnState",
            "title":
            "AsyncNHttpServiceHandler.ServerConnState 61 Elements, 83.6% Coverage"},
          "children":[]},{"id":
          "AsyncNHttpServiceHandler.ResponseTriggerImpl5610","name":
          "AsyncNHttpServiceHandler.ResponseTriggerImpl","data":{"$area":
            32.0,"$color":56.25,"path":
            "org/apache/http/nio/protocol/AsyncNHttpServiceHandler.html#AsyncNHttpServiceHandler.ResponseTriggerImpl",
            "title":
            "AsyncNHttpServiceHandler.ResponseTriggerImpl 32 Elements, 56.2% Coverage"},
          "children":[]},{"id":"BasicAsyncRequestConsumer5642","name":
          "BasicAsyncRequestConsumer","data":{"$area":27.0,"$color":
            85.18519,"path":
            "org/apache/http/nio/protocol/BasicAsyncRequestConsumer.html#BasicAsyncRequestConsumer",
            "title":
            "BasicAsyncRequestConsumer 27 Elements, 85.2% Coverage"},
          "children":[]},{"id":"BasicAsyncRequestExecutionHandler5669",
          "name":"BasicAsyncRequestExecutionHandler","data":{"$area":110.0,
            "$color":99.09091,"path":
            "org/apache/http/nio/protocol/BasicAsyncRequestExecutionHandler.html#BasicAsyncRequestExecutionHandler",
            "title":
            "BasicAsyncRequestExecutionHandler 110 Elements, 99.1% Coverage"},
          "children":[]},{"id":"BasicAsyncRequestHandler5779","name":
          "BasicAsyncRequestHandler","data":{"$area":12.0,"$color":100.0,
            "path":
            "org/apache/http/nio/protocol/BasicAsyncRequestHandler.html#BasicAsyncRequestHandler",
            "title":"BasicAsyncRequestHandler 12 Elements, 100% Coverage"},
          "children":[]},{"id":"BasicAsyncRequestProducer5791","name":
          "BasicAsyncRequestProducer","data":{"$area":69.0,"$color":
            86.95652,"path":
            "org/apache/http/nio/protocol/BasicAsyncRequestProducer.html#BasicAsyncRequestProducer",
            "title":"BasicAsyncRequestProducer 69 Elements, 87% Coverage"},
          "children":[]},{"id":"BasicAsyncResponseConsumer5860","name":
          "BasicAsyncResponseConsumer","data":{"$area":27.0,"$color":
            85.18519,"path":
            "org/apache/http/nio/protocol/BasicAsyncResponseConsumer.html#BasicAsyncResponseConsumer",
            "title":
            "BasicAsyncResponseConsumer 27 Elements, 85.2% Coverage"},
          "children":[]},{"id":"BasicAsyncResponseProducer5887","name":
          "BasicAsyncResponseProducer","data":{"$area":47.0,"$color":
            85.106384,"path":
            "org/apache/http/nio/protocol/BasicAsyncResponseProducer.html#BasicAsyncResponseProducer",
            "title":
            "BasicAsyncResponseProducer 47 Elements, 85.1% Coverage"},
          "children":[]},{"id":"BufferingHttpClientHandler5934","name":
          "BufferingHttpClientHandler","data":{"$area":24.0,"$color":0.0,
            "path":
            "org/apache/http/nio/protocol/BufferingHttpClientHandler.html#BufferingHttpClientHandler",
            "title":"BufferingHttpClientHandler 24 Elements, 0% Coverage"},
          "children":[]},{"id":
          "BufferingHttpClientHandler.ExecutionHandlerAdaptor5958","name":
          "BufferingHttpClientHandler.ExecutionHandlerAdaptor","data":{
            "$area":13.0,"$color":0.0,"path":
            "org/apache/http/nio/protocol/BufferingHttpClientHandler.html#BufferingHttpClientHandler.ExecutionHandlerAdaptor",
            "title":
            "BufferingHttpClientHandler.ExecutionHandlerAdaptor 13 Elements, 0% Coverage"},
          "children":[]},{"id":"BufferingHttpServiceHandler5971","name":
          "BufferingHttpServiceHandler","data":{"$area":30.0,"$color":
            66.66667,"path":
            "org/apache/http/nio/protocol/BufferingHttpServiceHandler.html#BufferingHttpServiceHandler",
            "title":
            "BufferingHttpServiceHandler 30 Elements, 66.7% Coverage"},
          "children":[]},{"id":
          "BufferingHttpServiceHandler.RequestHandlerResolverAdaptor6001",
          "name":
          "BufferingHttpServiceHandler.RequestHandlerResolverAdaptor","data":
          {"$area":7.0,"$color":71.42857,"path":
            "org/apache/http/nio/protocol/BufferingHttpServiceHandler.html#BufferingHttpServiceHandler.RequestHandlerResolverAdaptor",
            "title":
            "BufferingHttpServiceHandler.RequestHandlerResolverAdaptor 7 Elements, 71.4% Coverage"},
          "children":[]},{"id":
          "BufferingHttpServiceHandler.RequestHandlerAdaptor6008","name":
          "BufferingHttpServiceHandler.RequestHandlerAdaptor","data":{
            "$area":7.0,"$color":71.42857,"path":
            "org/apache/http/nio/protocol/BufferingHttpServiceHandler.html#BufferingHttpServiceHandler.RequestHandlerAdaptor",
            "title":
            "BufferingHttpServiceHandler.RequestHandlerAdaptor 7 Elements, 71.4% Coverage"},
          "children":[]},{"id":"ErrorResponseProducer6015","name":
          "ErrorResponseProducer","data":{"$area":24.0,"$color":95.83333,
            "path":
            "org/apache/http/nio/protocol/ErrorResponseProducer.html#ErrorResponseProducer",
            "title":"ErrorResponseProducer 24 Elements, 95.8% Coverage"},
          "children":[]},{"id":"EventListener6039","name":"EventListener",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/protocol/EventListener.html#EventListener",
            "title":"EventListener 0 Elements,  -  Coverage"},"children":[]},
        {"id":"HttpAsyncExchange6039","name":"HttpAsyncExchange","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/protocol/HttpAsyncExchange.html#HttpAsyncExchange",
            "title":"HttpAsyncExchange 0 Elements,  -  Coverage"},"children":
          []},{"id":"HttpAsyncExpectationVerifier6039","name":
          "HttpAsyncExpectationVerifier","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/nio/protocol/HttpAsyncExpectationVerifier.html#HttpAsyncExpectationVerifier",
            "title":
            "HttpAsyncExpectationVerifier 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpAsyncRequestConsumer6039","name":
          "HttpAsyncRequestConsumer","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/nio/protocol/HttpAsyncRequestConsumer.html#HttpAsyncRequestConsumer",
            "title":"HttpAsyncRequestConsumer 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpAsyncRequestExecutionHandler6039","name":
          "HttpAsyncRequestExecutionHandler","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/nio/protocol/HttpAsyncRequestExecutionHandler.html#HttpAsyncRequestExecutionHandler",
            "title":
            "HttpAsyncRequestExecutionHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpAsyncRequestExecutor6039","name":
          "HttpAsyncRequestExecutor","data":{"$area":239.0,"$color":
            95.39749,"path":
            "org/apache/http/nio/protocol/HttpAsyncRequestExecutor.html#HttpAsyncRequestExecutor",
            "title":
            "HttpAsyncRequestExecutor 239 Elements, 95.4% Coverage"},
          "children":[]},{"id":"HttpAsyncRequestExecutor.State6278","name":
          "HttpAsyncRequestExecutor.State","data":{"$area":55.0,"$color":
            98.18182,"path":
            "org/apache/http/nio/protocol/HttpAsyncRequestExecutor.html#HttpAsyncRequestExecutor.State",
            "title":
            "HttpAsyncRequestExecutor.State 55 Elements, 98.2% Coverage"},
          "children":[]},{"id":"HttpAsyncRequestHandler6333","name":
          "HttpAsyncRequestHandler","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/nio/protocol/HttpAsyncRequestHandler.html#HttpAsyncRequestHandler",
            "title":"HttpAsyncRequestHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpAsyncRequestHandlerRegistry6333","name":
          "HttpAsyncRequestHandlerRegistry","data":{"$area":12.0,"$color":
            50.0,"path":
            "org/apache/http/nio/protocol/HttpAsyncRequestHandlerRegistry.html#HttpAsyncRequestHandlerRegistry",
            "title":
            "HttpAsyncRequestHandlerRegistry 12 Elements, 50% Coverage"},
          "children":[]},{"id":"HttpAsyncRequestHandlerResolver6345","name":
          "HttpAsyncRequestHandlerResolver","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/nio/protocol/HttpAsyncRequestHandlerResolver.html#HttpAsyncRequestHandlerResolver",
            "title":
            "HttpAsyncRequestHandlerResolver 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpAsyncRequestProducer6345","name":
          "HttpAsyncRequestProducer","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/nio/protocol/HttpAsyncRequestProducer.html#HttpAsyncRequestProducer",
            "title":"HttpAsyncRequestProducer 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpAsyncRequester6345","name":
          "HttpAsyncRequester","data":{"$area":65.0,"$color":96.92308,"path":
            
            "org/apache/http/nio/protocol/HttpAsyncRequester.html#HttpAsyncRequester",
            "title":"HttpAsyncRequester 65 Elements, 96.9% Coverage"},
          "children":[]},{"id":
          "HttpAsyncRequester.ConnRequestCallback6409","name":
          "HttpAsyncRequester.ConnRequestCallback","data":{"$area":35.0,
            "$color":94.28571,"path":
            "org/apache/http/nio/protocol/HttpAsyncRequester.html#HttpAsyncRequester.ConnRequestCallback",
            "title":
            "HttpAsyncRequester.ConnRequestCallback 35 Elements, 94.3% Coverage"},
          "children":[]},{"id":
          "HttpAsyncRequester.RequestExecutionCallback6444","name":
          "HttpAsyncRequester.RequestExecutionCallback","data":{"$area":
            17.0,"$color":100.0,"path":
            "org/apache/http/nio/protocol/HttpAsyncRequester.html#HttpAsyncRequester.RequestExecutionCallback",
            "title":
            "HttpAsyncRequester.RequestExecutionCallback 17 Elements, 100% Coverage"},
          "children":[]},{"id":"HttpAsyncResponseConsumer6462","name":
          "HttpAsyncResponseConsumer","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/nio/protocol/HttpAsyncResponseConsumer.html#HttpAsyncResponseConsumer",
            "title":"HttpAsyncResponseConsumer 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpAsyncResponseProducer6462","name":
          "HttpAsyncResponseProducer","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/nio/protocol/HttpAsyncResponseProducer.html#HttpAsyncResponseProducer",
            "title":"HttpAsyncResponseProducer 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpAsyncService6462","name":
          "HttpAsyncService","data":{"$area":350.0,"$color":94.28571,"path":
            "org/apache/http/nio/protocol/HttpAsyncService.html#HttpAsyncService",
            "title":"HttpAsyncService 350 Elements, 94.3% Coverage"},
          "children":[]},{"id":"HttpAsyncService.State6812","name":
          "HttpAsyncService.State","data":{"$area":71.0,"$color":100.0,
            "path":
            "org/apache/http/nio/protocol/HttpAsyncService.html#HttpAsyncService.State",
            "title":"HttpAsyncService.State 71 Elements, 100% Coverage"},
          "children":[]},{"id":"HttpAsyncService.Exchange6883","name":
          "HttpAsyncService.Exchange","data":{"$area":49.0,"$color":
            77.55102,"path":
            "org/apache/http/nio/protocol/HttpAsyncService.html#HttpAsyncService.Exchange",
            "title":
            "HttpAsyncService.Exchange 49 Elements, 77.6% Coverage"},
          "children":[]},{"id":"HttpRequestExecutionHandler6932","name":
          "HttpRequestExecutionHandler","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/nio/protocol/HttpRequestExecutionHandler.html#HttpRequestExecutionHandler",
            "title":"HttpRequestExecutionHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"MessageState6932","name":"MessageState",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/protocol/MessageState.html#MessageState",
            "title":"MessageState 0 Elements,  -  Coverage"},"children":[]},{
          "id":"NHttpHandlerBase6932","name":"NHttpHandlerBase","data":{
            "$area":56.0,"$color":41.07143,"path":
            "org/apache/http/nio/protocol/NHttpHandlerBase.html#NHttpHandlerBase",
            "title":"NHttpHandlerBase 56 Elements, 41.1% Coverage"},
          "children":[]},{"id":"NHttpRequestExecutionHandler6988","name":
          "NHttpRequestExecutionHandler","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/nio/protocol/NHttpRequestExecutionHandler.html#NHttpRequestExecutionHandler",
            "title":
            "NHttpRequestExecutionHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpRequestHandler6988","name":
          "NHttpRequestHandler","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/protocol/NHttpRequestHandler.html#NHttpRequestHandler",
            "title":"NHttpRequestHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpRequestHandlerRegistry6988","name":
          "NHttpRequestHandlerRegistry","data":{"$area":12.0,"$color":0.0,
            "path":
            "org/apache/http/nio/protocol/NHttpRequestHandlerRegistry.html#NHttpRequestHandlerRegistry",
            "title":"NHttpRequestHandlerRegistry 12 Elements, 0% Coverage"},
          "children":[]},{"id":"NHttpRequestHandlerResolver7000","name":
          "NHttpRequestHandlerResolver","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/nio/protocol/NHttpRequestHandlerResolver.html#NHttpRequestHandlerResolver",
            "title":"NHttpRequestHandlerResolver 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NHttpResponseTrigger7000","name":
          "NHttpResponseTrigger","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/protocol/NHttpResponseTrigger.html#NHttpResponseTrigger",
            "title":"NHttpResponseTrigger 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NullNHttpEntity7000","name":
          "NullNHttpEntity","data":{"$area":19.0,"$color":57.894737,"path":
            "org/apache/http/nio/protocol/NullNHttpEntity.html#NullNHttpEntity",
            "title":"NullNHttpEntity 19 Elements, 57.9% Coverage"},
          "children":[]},{"id":"NullRequestConsumer7019","name":
          "NullRequestConsumer","data":{"$area":23.0,"$color":52.173912,
            "path":
            "org/apache/http/nio/protocol/NullRequestConsumer.html#NullRequestConsumer",
            "title":"NullRequestConsumer 23 Elements, 52.2% Coverage"},
          "children":[]},{"id":"NullRequestHandler7042","name":
          "NullRequestHandler","data":{"$area":8.0,"$color":100.0,"path":
            "org/apache/http/nio/protocol/NullRequestHandler.html#NullRequestHandler",
            "title":"NullRequestHandler 8 Elements, 100% Coverage"},
          "children":[]},{"id":"SimpleNHttpRequestHandler7050","name":
          "SimpleNHttpRequestHandler","data":{"$area":3.0,"$color":100.0,
            "path":
            "org/apache/http/nio/protocol/SimpleNHttpRequestHandler.html#SimpleNHttpRequestHandler",
            "title":"SimpleNHttpRequestHandler 3 Elements, 100% Coverage"},
          "children":[]},{"id":"ThrottlingHttpClientHandler7053","name":
          "ThrottlingHttpClientHandler","data":{"$area":308.0,"$color":
            65.25974,"path":
            "org/apache/http/nio/protocol/ThrottlingHttpClientHandler.html#ThrottlingHttpClientHandler",
            "title":
            "ThrottlingHttpClientHandler 308 Elements, 65.3% Coverage"},
          "children":[]},{"id":
          "ThrottlingHttpClientHandler.ClientConnState7361","name":
          "ThrottlingHttpClientHandler.ClientConnState","data":{"$area":
            52.0,"$color":90.38461,"path":
            "org/apache/http/nio/protocol/ThrottlingHttpClientHandler.html#ThrottlingHttpClientHandler.ClientConnState",
            "title":
            "ThrottlingHttpClientHandler.ClientConnState 52 Elements, 90.4% Coverage"},
          "children":[]},{"id":"ThrottlingHttpServiceHandler7413","name":
          "ThrottlingHttpServiceHandler","data":{"$area":352.0,"$color":
            62.215908,"path":
            "org/apache/http/nio/protocol/ThrottlingHttpServiceHandler.html#ThrottlingHttpServiceHandler",
            "title":
            "ThrottlingHttpServiceHandler 352 Elements, 62.2% Coverage"},
          "children":[]},{"id":
          "ThrottlingHttpServiceHandler.ServerConnState7765","name":
          "ThrottlingHttpServiceHandler.ServerConnState","data":{"$area":
            49.0,"$color":81.63265,"path":
            "org/apache/http/nio/protocol/ThrottlingHttpServiceHandler.html#ThrottlingHttpServiceHandler.ServerConnState",
            "title":
            "ThrottlingHttpServiceHandler.ServerConnState 49 Elements, 81.6% Coverage"},
          "children":[]}]},{"id":"org.apache.http.nio.pool4303","name":
      "org.apache.http.nio.pool","data":{"$area":560.0,"$color":92.32143,
        "title":"org.apache.http.nio.pool 560 Elements, 92.3% Coverage"},
      "children":[{"id":"AbstractNIOConnPool4303","name":
          "AbstractNIOConnPool","data":{"$area":401.0,"$color":91.27182,
            "path":
            "org/apache/http/nio/pool/AbstractNIOConnPool.html#AbstractNIOConnPool",
            "title":"AbstractNIOConnPool 401 Elements, 91.3% Coverage"},
          "children":[]},{"id":
          "AbstractNIOConnPool.InternalSessionRequestCallback4704","name":
          "AbstractNIOConnPool.InternalSessionRequestCallback","data":{
            "$area":8.0,"$color":25.0,"path":
            "org/apache/http/nio/pool/AbstractNIOConnPool.html#AbstractNIOConnPool.InternalSessionRequestCallback",
            "title":
            "AbstractNIOConnPool.InternalSessionRequestCallback 8 Elements, 25% Coverage"},
          "children":[]},{"id":"LeaseRequest4712","name":"LeaseRequest",
          "data":{"$area":27.0,"$color":100.0,"path":
            "org/apache/http/nio/pool/LeaseRequest.html#LeaseRequest",
            "title":"LeaseRequest 27 Elements, 100% Coverage"},"children":[]},
        {"id":"NIOConnFactory4739","name":"NIOConnFactory","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/nio/pool/NIOConnFactory.html#NIOConnFactory",
            "title":"NIOConnFactory 0 Elements,  -  Coverage"},"children":[]},
        {"id":"RouteSpecificPool4739","name":"RouteSpecificPool","data":{
            "$area":124.0,"$color":98.3871,"path":
            "org/apache/http/nio/pool/RouteSpecificPool.html#RouteSpecificPool",
            "title":"RouteSpecificPool 124 Elements, 98.4% Coverage"},
          "children":[]}]},{"id":"org.apache.http.impl.nio.pool1777","name":
      "org.apache.http.impl.nio.pool","data":{"$area":57.0,"$color":
        78.94737,"title":
        "org.apache.http.impl.nio.pool 57 Elements, 78.9% Coverage"},
      "children":[{"id":"BasicNIOConnFactory1777","name":
          "BasicNIOConnFactory","data":{"$area":29.0,"$color":72.41379,
            "path":
            "org/apache/http/impl/nio/pool/BasicNIOConnFactory.html#BasicNIOConnFactory",
            "title":"BasicNIOConnFactory 29 Elements, 72.4% Coverage"},
          "children":[]},{"id":"BasicNIOConnPool1806","name":
          "BasicNIOConnPool","data":{"$area":21.0,"$color":80.952385,"path":
            "org/apache/http/impl/nio/pool/BasicNIOConnPool.html#BasicNIOConnPool",
            "title":"BasicNIOConnPool 21 Elements, 81% Coverage"},"children":
          []},{"id":"BasicNIOPoolEntry1827","name":"BasicNIOPoolEntry",
          "data":{"$area":7.0,"$color":100.0,"path":
            "org/apache/http/impl/nio/pool/BasicNIOPoolEntry.html#BasicNIOPoolEntry",
            "title":"BasicNIOPoolEntry 7 Elements, 100% Coverage"},
          "children":[]}]},{"id":"org.apache.http.nio.entity3852","name":
      "org.apache.http.nio.entity","data":{"$area":395.0,"$color":
        34.177216,"title":
        "org.apache.http.nio.entity 395 Elements, 34.2% Coverage"},
      "children":[{"id":"BufferingNHttpEntity3852","name":
          "BufferingNHttpEntity","data":{"$area":39.0,"$color":46.153847,
            "path":
            "org/apache/http/nio/entity/BufferingNHttpEntity.html#BufferingNHttpEntity",
            "title":"BufferingNHttpEntity 39 Elements, 46.2% Coverage"},
          "children":[]},{"id":"ConsumingNHttpEntity3891","name":
          "ConsumingNHttpEntity","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/entity/ConsumingNHttpEntity.html#ConsumingNHttpEntity",
            "title":"ConsumingNHttpEntity 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ConsumingNHttpEntityTemplate3891","name":
          "ConsumingNHttpEntityTemplate","data":{"$area":17.0,"$color":0.0,
            "path":
            "org/apache/http/nio/entity/ConsumingNHttpEntityTemplate.html#ConsumingNHttpEntityTemplate",
            "title":
            "ConsumingNHttpEntityTemplate 17 Elements, 0% Coverage"},
          "children":[]},{"id":"ContentBufferEntity3908","name":
          "ContentBufferEntity","data":{"$area":16.0,"$color":62.5,"path":
            "org/apache/http/nio/entity/ContentBufferEntity.html#ContentBufferEntity",
            "title":"ContentBufferEntity 16 Elements, 62.5% Coverage"},
          "children":[]},{"id":"ContentInputStream3924","name":
          "ContentInputStream","data":{"$area":29.0,"$color":72.41379,"path":
            
            "org/apache/http/nio/entity/ContentInputStream.html#ContentInputStream",
            "title":"ContentInputStream 29 Elements, 72.4% Coverage"},
          "children":[]},{"id":"ContentListener3953","name":
          "ContentListener","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/entity/ContentListener.html#ContentListener",
            "title":"ContentListener 0 Elements,  -  Coverage"},"children":[]},
        {"id":"ContentOutputStream3953","name":"ContentOutputStream","data":{
            "$area":20.0,"$color":70.0,"path":
            "org/apache/http/nio/entity/ContentOutputStream.html#ContentOutputStream",
            "title":"ContentOutputStream 20 Elements, 70% Coverage"},
          "children":[]},{"id":"EntityAsyncContentProducer3973","name":
          "EntityAsyncContentProducer","data":{"$area":32.0,"$color":18.75,
            "path":
            "org/apache/http/nio/entity/EntityAsyncContentProducer.html#EntityAsyncContentProducer",
            "title":
            "EntityAsyncContentProducer 32 Elements, 18.8% Coverage"},
          "children":[]},{"id":"HttpAsyncContentProducer4005","name":
          "HttpAsyncContentProducer","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/nio/entity/HttpAsyncContentProducer.html#HttpAsyncContentProducer",
            "title":"HttpAsyncContentProducer 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NByteArrayEntity4005","name":
          "NByteArrayEntity","data":{"$area":65.0,"$color":46.153847,"path":
            "org/apache/http/nio/entity/NByteArrayEntity.html#NByteArrayEntity",
            "title":"NByteArrayEntity 65 Elements, 46.2% Coverage"},
          "children":[]},{"id":"NFileEntity4070","name":"NFileEntity","data":
          {"$area":83.0,"$color":0.0,"path":
            "org/apache/http/nio/entity/NFileEntity.html#NFileEntity",
            "title":"NFileEntity 83 Elements, 0% Coverage"},"children":[]},{
          "id":"NHttpEntityWrapper4153","name":"NHttpEntityWrapper","data":{
            "$area":26.0,"$color":0.0,"path":
            "org/apache/http/nio/entity/NHttpEntityWrapper.html#NHttpEntityWrapper",
            "title":"NHttpEntityWrapper 26 Elements, 0% Coverage"},
          "children":[]},{"id":"NStringEntity4179","name":"NStringEntity",
          "data":{"$area":53.0,"$color":67.92453,"path":
            "org/apache/http/nio/entity/NStringEntity.html#NStringEntity",
            "title":"NStringEntity 53 Elements, 67.9% Coverage"},"children":[]},
        {"id":"ProducingNHttpEntity4232","name":"ProducingNHttpEntity",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/nio/entity/ProducingNHttpEntity.html#ProducingNHttpEntity",
            "title":"ProducingNHttpEntity 0 Elements,  -  Coverage"},
          "children":[]},{"id":"SkipContentListener4232","name":
          "SkipContentListener","data":{"$area":15.0,"$color":0.0,"path":
            "org/apache/http/nio/entity/SkipContentListener.html#SkipContentListener",
            "title":"SkipContentListener 15 Elements, 0% Coverage"},
          "children":[]}]}]}

 ); 