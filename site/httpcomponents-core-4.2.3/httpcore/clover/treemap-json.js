processTreeMapJson (  {"id":"Clover database Wed Nov 28 2012 22:34:46 CET0","name":"","data":{
    "$area":7106.0,"$color":80.692375,"title":
    " 7106 Elements, 80.7% Coverage"},"children":[{"id":
      "org.apache.http.impl.entity1540","name":
      "org.apache.http.impl.entity","data":{"$area":204.0,"$color":
        95.588234,"title":
        "org.apache.http.impl.entity 204 Elements, 95.6% Coverage"},
      "children":[{"id":"DisallowIdentityContentLengthStrategy1540","name":
          "DisallowIdentityContentLengthStrategy","data":{"$area":10.0,
            "$color":100.0,"path":
            "org/apache/http/impl/entity/DisallowIdentityContentLengthStrategy.html#DisallowIdentityContentLengthStrategy",
            "title":
            "DisallowIdentityContentLengthStrategy 10 Elements, 100% Coverage"},
          "children":[]},{"id":"EntityDeserializer1550","name":
          "EntityDeserializer","data":{"$area":46.0,"$color":95.652176,
            "path":
            "org/apache/http/impl/entity/EntityDeserializer.html#EntityDeserializer",
            "title":"EntityDeserializer 46 Elements, 95.7% Coverage"},
          "children":[]},{"id":"EntitySerializer1596","name":
          "EntitySerializer","data":{"$area":34.0,"$color":94.117645,"path":
            "org/apache/http/impl/entity/EntitySerializer.html#EntitySerializer",
            "title":"EntitySerializer 34 Elements, 94.1% Coverage"},
          "children":[]},{"id":"LaxContentLengthStrategy1630","name":
          "LaxContentLengthStrategy","data":{"$area":72.0,"$color":
            95.83333,"path":
            "org/apache/http/impl/entity/LaxContentLengthStrategy.html#LaxContentLengthStrategy",
            "title":"LaxContentLengthStrategy 72 Elements, 95.8% Coverage"},
          "children":[]},{"id":"StrictContentLengthStrategy1702","name":
          "StrictContentLengthStrategy","data":{"$area":42.0,"$color":
            95.2381,"path":
            "org/apache/http/impl/entity/StrictContentLengthStrategy.html#StrictContentLengthStrategy",
            "title":
            "StrictContentLengthStrategy 42 Elements, 95.2% Coverage"},
          "children":[]}]},{"id":"org.apache.http.io2871","name":
      "org.apache.http.io","data":{"$area":0.0,"$color":-100.0,"title":
        "org.apache.http.io 0 Elements,  -  Coverage"},"children":[{"id":
          "BufferInfo2871","name":"BufferInfo","data":{"$area":0.0,"$color":
            -100.0,"path":"org/apache/http/io/BufferInfo.html#BufferInfo",
            "title":"BufferInfo 0 Elements,  -  Coverage"},"children":[]},{
          "id":"EofSensor2871","name":"EofSensor","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/io/EofSensor.html#EofSensor","title":
            "EofSensor 0 Elements,  -  Coverage"},"children":[]},{"id":
          "HttpMessageParser2871","name":"HttpMessageParser","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/io/HttpMessageParser.html#HttpMessageParser",
            "title":"HttpMessageParser 0 Elements,  -  Coverage"},"children":
          []},{"id":"HttpMessageWriter2871","name":"HttpMessageWriter",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/io/HttpMessageWriter.html#HttpMessageWriter",
            "title":"HttpMessageWriter 0 Elements,  -  Coverage"},"children":
          []},{"id":"HttpTransportMetrics2871","name":
          "HttpTransportMetrics","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/io/HttpTransportMetrics.html#HttpTransportMetrics",
            "title":"HttpTransportMetrics 0 Elements,  -  Coverage"},
          "children":[]},{"id":"SessionInputBuffer2871","name":
          "SessionInputBuffer","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/io/SessionInputBuffer.html#SessionInputBuffer",
            "title":"SessionInputBuffer 0 Elements,  -  Coverage"},
          "children":[]},{"id":"SessionOutputBuffer2871","name":
          "SessionOutputBuffer","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/io/SessionOutputBuffer.html#SessionOutputBuffer",
            "title":"SessionOutputBuffer 0 Elements,  -  Coverage"},
          "children":[]}]},{"id":"org.apache.http.message2871","name":
      "org.apache.http.message","data":{"$area":1653.0,"$color":88.38475,
        "title":"org.apache.http.message 1653 Elements, 88.4% Coverage"},
      "children":[{"id":"AbstractHttpMessage2871","name":
          "AbstractHttpMessage","data":{"$area":65.0,"$color":100.0,"path":
            "org/apache/http/message/AbstractHttpMessage.html#AbstractHttpMessage",
            "title":"AbstractHttpMessage 65 Elements, 100% Coverage"},
          "children":[]},{"id":"BasicHeader2936","name":"BasicHeader","data":
          {"$area":22.0,"$color":100.0,"path":
            "org/apache/http/message/BasicHeader.html#BasicHeader","title":
            "BasicHeader 22 Elements, 100% Coverage"},"children":[]},{"id":
          "BasicHeaderElement2958","name":"BasicHeaderElement","data":{
            "$area":77.0,"$color":100.0,"path":
            "org/apache/http/message/BasicHeaderElement.html#BasicHeaderElement",
            "title":"BasicHeaderElement 77 Elements, 100% Coverage"},
          "children":[]},{"id":"BasicHeaderElementIterator3035","name":
          "BasicHeaderElementIterator","data":{"$area":82.0,"$color":
            84.14634,"path":
            "org/apache/http/message/BasicHeaderElementIterator.html#BasicHeaderElementIterator",
            "title":
            "BasicHeaderElementIterator 82 Elements, 84.1% Coverage"},
          "children":[]},{"id":"BasicHeaderIterator3117","name":
          "BasicHeaderIterator","data":{"$area":39.0,"$color":100.0,"path":
            "org/apache/http/message/BasicHeaderIterator.html#BasicHeaderIterator",
            "title":"BasicHeaderIterator 39 Elements, 100% Coverage"},
          "children":[]},{"id":"BasicHeaderValueFormatter3156","name":
          "BasicHeaderValueFormatter","data":{"$area":194.0,"$color":
            93.29897,"path":
            "org/apache/http/message/BasicHeaderValueFormatter.html#BasicHeaderValueFormatter",
            "title":
            "BasicHeaderValueFormatter 194 Elements, 93.3% Coverage"},
          "children":[]},{"id":"BasicHeaderValueParser3350","name":
          "BasicHeaderValueParser","data":{"$area":230.0,"$color":
            87.391304,"path":
            "org/apache/http/message/BasicHeaderValueParser.html#BasicHeaderValueParser",
            "title":"BasicHeaderValueParser 230 Elements, 87.4% Coverage"},
          "children":[]},{"id":"BasicHttpEntityEnclosingRequest3580","name":
          "BasicHttpEntityEnclosingRequest","data":{"$area":13.0,"$color":
            100.0,"path":
            "org/apache/http/message/BasicHttpEntityEnclosingRequest.html#BasicHttpEntityEnclosingRequest",
            "title":
            "BasicHttpEntityEnclosingRequest 13 Elements, 100% Coverage"},
          "children":[]},{"id":"BasicHttpRequest3593","name":
          "BasicHttpRequest","data":{"$area":35.0,"$color":94.28571,"path":
            "org/apache/http/message/BasicHttpRequest.html#BasicHttpRequest",
            "title":"BasicHttpRequest 35 Elements, 94.3% Coverage"},
          "children":[]},{"id":"BasicHttpResponse3628","name":
          "BasicHttpResponse","data":{"$area":58.0,"$color":58.62069,"path":
            "org/apache/http/message/BasicHttpResponse.html#BasicHttpResponse",
            "title":"BasicHttpResponse 58 Elements, 58.6% Coverage"},
          "children":[]},{"id":"BasicLineFormatter3686","name":
          "BasicLineFormatter","data":{"$area":121.0,"$color":98.34711,
            "path":
            "org/apache/http/message/BasicLineFormatter.html#BasicLineFormatter",
            "title":"BasicLineFormatter 121 Elements, 98.3% Coverage"},
          "children":[]},{"id":"BasicLineParser3807","name":
          "BasicLineParser","data":{"$area":249.0,"$color":69.87952,"path":
            "org/apache/http/message/BasicLineParser.html#BasicLineParser",
            "title":"BasicLineParser 249 Elements, 69.9% Coverage"},
          "children":[]},{"id":"BasicListHeaderIterator4056","name":
          "BasicListHeaderIterator","data":{"$area":52.0,"$color":84.61539,
            "path":
            "org/apache/http/message/BasicListHeaderIterator.html#BasicListHeaderIterator",
            "title":"BasicListHeaderIterator 52 Elements, 84.6% Coverage"},
          "children":[]},{"id":"BasicNameValuePair4108","name":
          "BasicNameValuePair","data":{"$area":41.0,"$color":100.0,"path":
            "org/apache/http/message/BasicNameValuePair.html#BasicNameValuePair",
            "title":"BasicNameValuePair 41 Elements, 100% Coverage"},
          "children":[]},{"id":"BasicRequestLine4149","name":
          "BasicRequestLine","data":{"$area":27.0,"$color":100.0,"path":
            "org/apache/http/message/BasicRequestLine.html#BasicRequestLine",
            "title":"BasicRequestLine 27 Elements, 100% Coverage"},
          "children":[]},{"id":"BasicStatusLine4176","name":
          "BasicStatusLine","data":{"$area":23.0,"$color":100.0,"path":
            "org/apache/http/message/BasicStatusLine.html#BasicStatusLine",
            "title":"BasicStatusLine 23 Elements, 100% Coverage"},"children":
          []},{"id":"BasicTokenIterator4199","name":"BasicTokenIterator",
          "data":{"$area":135.0,"$color":100.0,"path":
            "org/apache/http/message/BasicTokenIterator.html#BasicTokenIterator",
            "title":"BasicTokenIterator 135 Elements, 100% Coverage"},
          "children":[]},{"id":"BufferedHeader4334","name":
          "BufferedHeader","data":{"$area":35.0,"$color":100.0,"path":
            "org/apache/http/message/BufferedHeader.html#BufferedHeader",
            "title":"BufferedHeader 35 Elements, 100% Coverage"},"children":[]},
        {"id":"HeaderGroup4369","name":"HeaderGroup","data":{"$area":114.0,
            "$color":94.73685,"path":
            "org/apache/http/message/HeaderGroup.html#HeaderGroup","title":
            "HeaderGroup 114 Elements, 94.7% Coverage"},"children":[]},{"id":
          "HeaderValueFormatter4483","name":"HeaderValueFormatter","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/message/HeaderValueFormatter.html#HeaderValueFormatter",
            "title":"HeaderValueFormatter 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HeaderValueParser4483","name":
          "HeaderValueParser","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/message/HeaderValueParser.html#HeaderValueParser",
            "title":"HeaderValueParser 0 Elements,  -  Coverage"},"children":
          []},{"id":"LineFormatter4483","name":"LineFormatter","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/message/LineFormatter.html#LineFormatter",
            "title":"LineFormatter 0 Elements,  -  Coverage"},"children":[]},
        {"id":"LineParser4483","name":"LineParser","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/message/LineParser.html#LineParser","title":
            "LineParser 0 Elements,  -  Coverage"},"children":[]},{"id":
          "ParserCursor4483","name":"ParserCursor","data":{"$area":41.0,
            "$color":51.219513,"path":
            "org/apache/http/message/ParserCursor.html#ParserCursor","title":
            "ParserCursor 41 Elements, 51.2% Coverage"},"children":[]}]},{
      "id":"org.apache.http.impl.io1744","name":"org.apache.http.impl.io",
      "data":{"$area":1077.0,"$color":92.014854,"title":
        "org.apache.http.impl.io 1077 Elements, 92% Coverage"},"children":[{
          "id":"AbstractMessageParser1744","name":"AbstractMessageParser",
          "data":{"$area":104.0,"$color":92.30769,"path":
            "org/apache/http/impl/io/AbstractMessageParser.html#AbstractMessageParser",
            "title":"AbstractMessageParser 104 Elements, 92.3% Coverage"},
          "children":[]},{"id":"AbstractMessageWriter1848","name":
          "AbstractMessageWriter","data":{"$area":24.0,"$color":79.16667,
            "path":
            "org/apache/http/impl/io/AbstractMessageWriter.html#AbstractMessageWriter",
            "title":"AbstractMessageWriter 24 Elements, 79.2% Coverage"},
          "children":[]},{"id":"AbstractSessionInputBuffer1872","name":
          "AbstractSessionInputBuffer","data":{"$area":248.0,"$color":
            97.98387,"path":
            "org/apache/http/impl/io/AbstractSessionInputBuffer.html#AbstractSessionInputBuffer",
            "title":
            "AbstractSessionInputBuffer 248 Elements, 98% Coverage"},
          "children":[]},{"id":"AbstractSessionOutputBuffer2120","name":
          "AbstractSessionOutputBuffer","data":{"$area":153.0,"$color":
            99.346405,"path":
            "org/apache/http/impl/io/AbstractSessionOutputBuffer.html#AbstractSessionOutputBuffer",
            "title":
            "AbstractSessionOutputBuffer 153 Elements, 99.3% Coverage"},
          "children":[]},{"id":"ChunkedInputStream2273","name":
          "ChunkedInputStream","data":{"$area":143.0,"$color":95.1049,"path":
            
            "org/apache/http/impl/io/ChunkedInputStream.html#ChunkedInputStream",
            "title":"ChunkedInputStream 143 Elements, 95.1% Coverage"},
          "children":[]},{"id":"ChunkedOutputStream2416","name":
          "ChunkedOutputStream","data":{"$area":64.0,"$color":100.0,"path":
            "org/apache/http/impl/io/ChunkedOutputStream.html#ChunkedOutputStream",
            "title":"ChunkedOutputStream 64 Elements, 100% Coverage"},
          "children":[]},{"id":"ContentLengthInputStream2480","name":
          "ContentLengthInputStream","data":{"$area":95.0,"$color":
            93.68421,"path":
            "org/apache/http/impl/io/ContentLengthInputStream.html#ContentLengthInputStream",
            "title":"ContentLengthInputStream 95 Elements, 93.7% Coverage"},
          "children":[]},{"id":"ContentLengthOutputStream2575","name":
          "ContentLengthOutputStream","data":{"$area":47.0,"$color":100.0,
            "path":
            "org/apache/http/impl/io/ContentLengthOutputStream.html#ContentLengthOutputStream",
            "title":"ContentLengthOutputStream 47 Elements, 100% Coverage"},
          "children":[]},{"id":"DefaultHttpRequestParser2622","name":
          "DefaultHttpRequestParser","data":{"$area":18.0,"$color":100.0,
            "path":
            "org/apache/http/impl/io/DefaultHttpRequestParser.html#DefaultHttpRequestParser",
            "title":"DefaultHttpRequestParser 18 Elements, 100% Coverage"},
          "children":[]},{"id":"DefaultHttpResponseParser2640","name":
          "DefaultHttpResponseParser","data":{"$area":18.0,"$color":100.0,
            "path":
            "org/apache/http/impl/io/DefaultHttpResponseParser.html#DefaultHttpResponseParser",
            "title":"DefaultHttpResponseParser 18 Elements, 100% Coverage"},
          "children":[]},{"id":"HttpRequestParser2658","name":
          "HttpRequestParser","data":{"$area":18.0,"$color":0.0,"path":
            "org/apache/http/impl/io/HttpRequestParser.html#HttpRequestParser",
            "title":"HttpRequestParser 18 Elements, 0% Coverage"},"children":
          []},{"id":"HttpRequestWriter2676","name":"HttpRequestWriter",
          "data":{"$area":5.0,"$color":100.0,"path":
            "org/apache/http/impl/io/HttpRequestWriter.html#HttpRequestWriter",
            "title":"HttpRequestWriter 5 Elements, 100% Coverage"},
          "children":[]},{"id":"HttpResponseParser2681","name":
          "HttpResponseParser","data":{"$area":18.0,"$color":0.0,"path":
            "org/apache/http/impl/io/HttpResponseParser.html#HttpResponseParser",
            "title":"HttpResponseParser 18 Elements, 0% Coverage"},
          "children":[]},{"id":"HttpResponseWriter2699","name":
          "HttpResponseWriter","data":{"$area":5.0,"$color":100.0,"path":
            "org/apache/http/impl/io/HttpResponseWriter.html#HttpResponseWriter",
            "title":"HttpResponseWriter 5 Elements, 100% Coverage"},
          "children":[]},{"id":"HttpTransportMetricsImpl2704","name":
          "HttpTransportMetricsImpl","data":{"$area":10.0,"$color":
            60.000004,"path":
            "org/apache/http/impl/io/HttpTransportMetricsImpl.html#HttpTransportMetricsImpl",
            "title":"HttpTransportMetricsImpl 10 Elements, 60% Coverage"},
          "children":[]},{"id":"IdentityInputStream2714","name":
          "IdentityInputStream","data":{"$area":27.0,"$color":92.59259,
            "path":
            "org/apache/http/impl/io/IdentityInputStream.html#IdentityInputStream",
            "title":"IdentityInputStream 27 Elements, 92.6% Coverage"},
          "children":[]},{"id":"IdentityOutputStream2741","name":
          "IdentityOutputStream","data":{"$area":29.0,"$color":100.0,"path":
            "org/apache/http/impl/io/IdentityOutputStream.html#IdentityOutputStream",
            "title":"IdentityOutputStream 29 Elements, 100% Coverage"},
          "children":[]},{"id":"SocketInputBuffer2770","name":
          "SocketInputBuffer","data":{"$area":36.0,"$color":77.77778,"path":
            "org/apache/http/impl/io/SocketInputBuffer.html#SocketInputBuffer",
            "title":"SocketInputBuffer 36 Elements, 77.8% Coverage"},
          "children":[]},{"id":"SocketOutputBuffer2806","name":
          "SocketOutputBuffer","data":{"$area":15.0,"$color":73.333336,
            "path":
            "org/apache/http/impl/io/SocketOutputBuffer.html#SocketOutputBuffer",
            "title":"SocketOutputBuffer 15 Elements, 73.3% Coverage"},
          "children":[]}]},{"id":"org.apache.http.protocol5514","name":
      "org.apache.http.protocol","data":{"$area":935.0,"$color":78.18182,
        "title":"org.apache.http.protocol 935 Elements, 78.2% Coverage"},
      "children":[{"id":"BasicHttpContext5514","name":"BasicHttpContext",
          "data":{"$area":51.0,"$color":78.43137,"path":
            "org/apache/http/protocol/BasicHttpContext.html#BasicHttpContext",
            "title":"BasicHttpContext 51 Elements, 78.4% Coverage"},
          "children":[]},{"id":"BasicHttpProcessor5565","name":
          "BasicHttpProcessor","data":{"$area":117.0,"$color":23.076923,
            "path":
            "org/apache/http/protocol/BasicHttpProcessor.html#BasicHttpProcessor",
            "title":"BasicHttpProcessor 117 Elements, 23.1% Coverage"},
          "children":[]},{"id":"DefaultedHttpContext5682","name":
          "DefaultedHttpContext","data":{"$area":27.0,"$color":0.0,"path":
            "org/apache/http/protocol/DefaultedHttpContext.html#DefaultedHttpContext",
            "title":"DefaultedHttpContext 27 Elements, 0% Coverage"},
          "children":[]},{"id":"ExecutionContext5709","name":
          "ExecutionContext","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/protocol/ExecutionContext.html#ExecutionContext",
            "title":"ExecutionContext 0 Elements,  -  Coverage"},"children":[]},
        {"id":"HTTP5709","name":"HTTP","data":{"$area":3.0,"$color":
            66.66667,"path":"org/apache/http/protocol/HTTP.html#HTTP",
            "title":"HTTP 3 Elements, 66.7% Coverage"},"children":[]},{"id":
          "HttpContext5712","name":"HttpContext","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/protocol/HttpContext.html#HttpContext","title":
            "HttpContext 0 Elements,  -  Coverage"},"children":[]},{"id":
          "HttpDateGenerator5712","name":"HttpDateGenerator","data":{"$area":
            12.0,"$color":100.0,"path":
            "org/apache/http/protocol/HttpDateGenerator.html#HttpDateGenerator",
            "title":"HttpDateGenerator 12 Elements, 100% Coverage"},
          "children":[]},{"id":"HttpExpectationVerifier5724","name":
          "HttpExpectationVerifier","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/protocol/HttpExpectationVerifier.html#HttpExpectationVerifier",
            "title":"HttpExpectationVerifier 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpProcessor5724","name":"HttpProcessor",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/protocol/HttpProcessor.html#HttpProcessor",
            "title":"HttpProcessor 0 Elements,  -  Coverage"},"children":[]},
        {"id":"HttpRequestExecutor5724","name":"HttpRequestExecutor","data":{
            "$area":145.0,"$color":100.0,"path":
            "org/apache/http/protocol/HttpRequestExecutor.html#HttpRequestExecutor",
            "title":"HttpRequestExecutor 145 Elements, 100% Coverage"},
          "children":[]},{"id":"HttpRequestHandler5869","name":
          "HttpRequestHandler","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/protocol/HttpRequestHandler.html#HttpRequestHandler",
            "title":"HttpRequestHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpRequestHandlerRegistry5869","name":
          "HttpRequestHandlerRegistry","data":{"$area":20.0,"$color":100.0,
            "path":
            "org/apache/http/protocol/HttpRequestHandlerRegistry.html#HttpRequestHandlerRegistry",
            "title":
            "HttpRequestHandlerRegistry 20 Elements, 100% Coverage"},
          "children":[]},{"id":"HttpRequestHandlerResolver5889","name":
          "HttpRequestHandlerResolver","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/protocol/HttpRequestHandlerResolver.html#HttpRequestHandlerResolver",
            "title":"HttpRequestHandlerResolver 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpRequestInterceptorList5889","name":
          "HttpRequestInterceptorList","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/protocol/HttpRequestInterceptorList.html#HttpRequestInterceptorList",
            "title":"HttpRequestInterceptorList 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpResponseInterceptorList5889","name":
          "HttpResponseInterceptorList","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/protocol/HttpResponseInterceptorList.html#HttpResponseInterceptorList",
            "title":"HttpResponseInterceptorList 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpService5889","name":"HttpService","data":
          {"$area":147.0,"$color":78.23129,"path":
            "org/apache/http/protocol/HttpService.html#HttpService","title":
            "HttpService 147 Elements, 78.2% Coverage"},"children":[]},{"id":
          "ImmutableHttpProcessor6036","name":"ImmutableHttpProcessor",
          "data":{"$area":58.0,"$color":62.068962,"path":
            "org/apache/http/protocol/ImmutableHttpProcessor.html#ImmutableHttpProcessor",
            "title":"ImmutableHttpProcessor 58 Elements, 62.1% Coverage"},
          "children":[]},{"id":"RequestConnControl6094","name":
          "RequestConnControl","data":{"$area":16.0,"$color":100.0,"path":
            "org/apache/http/protocol/RequestConnControl.html#RequestConnControl",
            "title":"RequestConnControl 16 Elements, 100% Coverage"},
          "children":[]},{"id":"RequestContent6110","name":
          "RequestContent","data":{"$area":50.0,"$color":100.0,"path":
            "org/apache/http/protocol/RequestContent.html#RequestContent",
            "title":"RequestContent 50 Elements, 100% Coverage"},"children":[]},
        {"id":"RequestDate6160","name":"RequestDate","data":{"$area":12.0,
            "$color":100.0,"path":
            "org/apache/http/protocol/RequestDate.html#RequestDate","title":
            "RequestDate 12 Elements, 100% Coverage"},"children":[]},{"id":
          "RequestExpectContinue6172","name":"RequestExpectContinue","data":{
            "$area":19.0,"$color":100.0,"path":
            "org/apache/http/protocol/RequestExpectContinue.html#RequestExpectContinue",
            "title":"RequestExpectContinue 19 Elements, 100% Coverage"},
          "children":[]},{"id":"RequestTargetHost6191","name":
          "RequestTargetHost","data":{"$area":43.0,"$color":100.0,"path":
            "org/apache/http/protocol/RequestTargetHost.html#RequestTargetHost",
            "title":"RequestTargetHost 43 Elements, 100% Coverage"},
          "children":[]},{"id":"RequestUserAgent6234","name":
          "RequestUserAgent","data":{"$area":15.0,"$color":100.0,"path":
            "org/apache/http/protocol/RequestUserAgent.html#RequestUserAgent",
            "title":"RequestUserAgent 15 Elements, 100% Coverage"},
          "children":[]},{"id":"ResponseConnControl6249","name":
          "ResponseConnControl","data":{"$area":45.0,"$color":100.0,"path":
            "org/apache/http/protocol/ResponseConnControl.html#ResponseConnControl",
            "title":"ResponseConnControl 45 Elements, 100% Coverage"},
          "children":[]},{"id":"ResponseContent6294","name":
          "ResponseContent","data":{"$area":50.0,"$color":100.0,"path":
            "org/apache/http/protocol/ResponseContent.html#ResponseContent",
            "title":"ResponseContent 50 Elements, 100% Coverage"},"children":
          []},{"id":"ResponseDate6344","name":"ResponseDate","data":{"$area":
            13.0,"$color":100.0,"path":
            "org/apache/http/protocol/ResponseDate.html#ResponseDate",
            "title":"ResponseDate 13 Elements, 100% Coverage"},"children":[]},
        {"id":"ResponseServer6357","name":"ResponseServer","data":{"$area":
            15.0,"$color":100.0,"path":
            "org/apache/http/protocol/ResponseServer.html#ResponseServer",
            "title":"ResponseServer 15 Elements, 100% Coverage"},"children":[]},
        {"id":"SyncBasicHttpContext6372","name":"SyncBasicHttpContext",
          "data":{"$area":12.0,"$color":0.0,"path":
            "org/apache/http/protocol/SyncBasicHttpContext.html#SyncBasicHttpContext",
            "title":"SyncBasicHttpContext 12 Elements, 0% Coverage"},
          "children":[]},{"id":"UriPatternMatcher6384","name":
          "UriPatternMatcher","data":{"$area":65.0,"$color":86.15385,"path":
            "org/apache/http/protocol/UriPatternMatcher.html#UriPatternMatcher",
            "title":"UriPatternMatcher 65 Elements, 86.2% Coverage"},
          "children":[]}]},{"id":"org.apache.http.pool4916","name":
      "org.apache.http.pool","data":{"$area":598.0,"$color":91.638794,
        "title":"org.apache.http.pool 598 Elements, 91.6% Coverage"},
      "children":[{"id":"AbstractConnPool4916","name":"AbstractConnPool",
          "data":{"$area":308.0,"$color":92.20779,"path":
            "org/apache/http/pool/AbstractConnPool.html#AbstractConnPool",
            "title":"AbstractConnPool 308 Elements, 92.2% Coverage"},
          "children":[]},{"id":"ConnFactory5224","name":"ConnFactory","data":
          {"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/pool/ConnFactory.html#ConnFactory","title":
            "ConnFactory 0 Elements,  -  Coverage"},"children":[]},{"id":
          "ConnPool5224","name":"ConnPool","data":{"$area":0.0,"$color":
            -100.0,"path":"org/apache/http/pool/ConnPool.html#ConnPool",
            "title":"ConnPool 0 Elements,  -  Coverage"},"children":[]},{
          "id":"ConnPoolControl5224","name":"ConnPoolControl","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/pool/ConnPoolControl.html#ConnPoolControl",
            "title":"ConnPoolControl 0 Elements,  -  Coverage"},"children":[]},
        {"id":"PoolEntry5224","name":"PoolEntry","data":{"$area":69.0,
            "$color":97.10145,"path":
            "org/apache/http/pool/PoolEntry.html#PoolEntry","title":
            "PoolEntry 69 Elements, 97.1% Coverage"},"children":[]},{"id":
          "PoolEntryFuture5293","name":"PoolEntryFuture","data":{"$area":
            76.0,"$color":84.210526,"path":
            "org/apache/http/pool/PoolEntryFuture.html#PoolEntryFuture",
            "title":"PoolEntryFuture 76 Elements, 84.2% Coverage"},
          "children":[]},{"id":"PoolStats5369","name":"PoolStats","data":{
            "$area":26.0,"$color":53.846157,"path":
            "org/apache/http/pool/PoolStats.html#PoolStats","title":
            "PoolStats 26 Elements, 53.8% Coverage"},"children":[]},{"id":
          "RouteSpecificPool5395","name":"RouteSpecificPool","data":{"$area":
            119.0,"$color":100.0,"path":
            "org/apache/http/pool/RouteSpecificPool.html#RouteSpecificPool",
            "title":"RouteSpecificPool 119 Elements, 100% Coverage"},
          "children":[]}]},{"id":"org.apache.http.impl785","name":
      "org.apache.http.impl","data":{"$area":755.0,"$color":59.60265,"title":
        "org.apache.http.impl 755 Elements, 59.6% Coverage"},"children":[{
          "id":"AbstractHttpClientConnection785","name":
          "AbstractHttpClientConnection","data":{"$area":97.0,"$color":
            71.13402,"path":
            "org/apache/http/impl/AbstractHttpClientConnection.html#AbstractHttpClientConnection",
            "title":
            "AbstractHttpClientConnection 97 Elements, 71.1% Coverage"},
          "children":[]},{"id":"AbstractHttpServerConnection882","name":
          "AbstractHttpServerConnection","data":{"$area":86.0,"$color":
            69.76744,"path":
            "org/apache/http/impl/AbstractHttpServerConnection.html#AbstractHttpServerConnection",
            "title":
            "AbstractHttpServerConnection 86 Elements, 69.8% Coverage"},
          "children":[]},{"id":"DefaultConnectionReuseStrategy968","name":
          "DefaultConnectionReuseStrategy","data":{"$area":70.0,"$color":
            97.14286,"path":
            "org/apache/http/impl/DefaultConnectionReuseStrategy.html#DefaultConnectionReuseStrategy",
            "title":
            "DefaultConnectionReuseStrategy 70 Elements, 97.1% Coverage"},
          "children":[]},{"id":"DefaultHttpClientConnection1038","name":
          "DefaultHttpClientConnection","data":{"$area":21.0,"$color":
            71.42857,"path":
            "org/apache/http/impl/DefaultHttpClientConnection.html#DefaultHttpClientConnection",
            "title":
            "DefaultHttpClientConnection 21 Elements, 71.4% Coverage"},
          "children":[]},{"id":"DefaultHttpRequestFactory1059","name":
          "DefaultHttpRequestFactory","data":{"$area":44.0,"$color":50.0,
            "path":
            "org/apache/http/impl/DefaultHttpRequestFactory.html#DefaultHttpRequestFactory",
            "title":"DefaultHttpRequestFactory 44 Elements, 50% Coverage"},
          "children":[]},{"id":"DefaultHttpResponseFactory1103","name":
          "DefaultHttpResponseFactory","data":{"$area":26.0,"$color":
            76.92308,"path":
            "org/apache/http/impl/DefaultHttpResponseFactory.html#DefaultHttpResponseFactory",
            "title":
            "DefaultHttpResponseFactory 26 Elements, 76.9% Coverage"},
          "children":[]},{"id":"DefaultHttpServerConnection1129","name":
          "DefaultHttpServerConnection","data":{"$area":21.0,"$color":
            71.42857,"path":
            "org/apache/http/impl/DefaultHttpServerConnection.html#DefaultHttpServerConnection",
            "title":
            "DefaultHttpServerConnection 21 Elements, 71.4% Coverage"},
          "children":[]},{"id":"EnglishReasonPhraseCatalog1150","name":
          "EnglishReasonPhraseCatalog","data":{"$area":67.0,"$color":100.0,
            "path":
            "org/apache/http/impl/EnglishReasonPhraseCatalog.html#EnglishReasonPhraseCatalog",
            "title":
            "EnglishReasonPhraseCatalog 67 Elements, 100% Coverage"},
          "children":[]},{"id":"HttpConnectionMetricsImpl1217","name":
          "HttpConnectionMetricsImpl","data":{"$area":76.0,"$color":
            15.789473,"path":
            "org/apache/http/impl/HttpConnectionMetricsImpl.html#HttpConnectionMetricsImpl",
            "title":
            "HttpConnectionMetricsImpl 76 Elements, 15.8% Coverage"},
          "children":[]},{"id":"NoConnectionReuseStrategy1293","name":
          "NoConnectionReuseStrategy","data":{"$area":10.0,"$color":100.0,
            "path":
            "org/apache/http/impl/NoConnectionReuseStrategy.html#NoConnectionReuseStrategy",
            "title":"NoConnectionReuseStrategy 10 Elements, 100% Coverage"},
          "children":[]},{"id":"SocketHttpClientConnection1303","name":
          "SocketHttpClientConnection","data":{"$area":118.0,"$color":
            41.525425,"path":
            "org/apache/http/impl/SocketHttpClientConnection.html#SocketHttpClientConnection",
            "title":
            "SocketHttpClientConnection 118 Elements, 41.5% Coverage"},
          "children":[]},{"id":"SocketHttpServerConnection1421","name":
          "SocketHttpServerConnection","data":{"$area":119.0,"$color":
            36.134453,"path":
            "org/apache/http/impl/SocketHttpServerConnection.html#SocketHttpServerConnection",
            "title":
            "SocketHttpServerConnection 119 Elements, 36.1% Coverage"},
          "children":[]}]},{"id":"org.apache.http.params4524","name":
      "org.apache.http.params","data":{"$area":392.0,"$color":39.795918,
        "title":"org.apache.http.params 392 Elements, 39.8% Coverage"},
      "children":[{"id":"AbstractHttpParams4524","name":
          "AbstractHttpParams","data":{"$area":50.0,"$color":52.0,"path":
            "org/apache/http/params/AbstractHttpParams.html#AbstractHttpParams",
            "title":"AbstractHttpParams 50 Elements, 52% Coverage"},
          "children":[]},{"id":"BasicHttpParams4574","name":
          "BasicHttpParams","data":{"$area":45.0,"$color":35.555557,"path":
            "org/apache/http/params/BasicHttpParams.html#BasicHttpParams",
            "title":"BasicHttpParams 45 Elements, 35.6% Coverage"},
          "children":[]},{"id":"CoreConnectionPNames4619","name":
          "CoreConnectionPNames","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/params/CoreConnectionPNames.html#CoreConnectionPNames",
            "title":"CoreConnectionPNames 0 Elements,  -  Coverage"},
          "children":[]},{"id":"CoreProtocolPNames4619","name":
          "CoreProtocolPNames","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/params/CoreProtocolPNames.html#CoreProtocolPNames",
            "title":"CoreProtocolPNames 0 Elements,  -  Coverage"},
          "children":[]},{"id":"DefaultedHttpParams4619","name":
          "DefaultedHttpParams","data":{"$area":38.0,"$color":73.68421,
            "path":
            "org/apache/http/params/DefaultedHttpParams.html#DefaultedHttpParams",
            "title":"DefaultedHttpParams 38 Elements, 73.7% Coverage"},
          "children":[]},{"id":"HttpAbstractParamBean4657","name":
          "HttpAbstractParamBean","data":{"$area":6.0,"$color":0.0,"path":
            "org/apache/http/params/HttpAbstractParamBean.html#HttpAbstractParamBean",
            "title":"HttpAbstractParamBean 6 Elements, 0% Coverage"},
          "children":[]},{"id":"HttpConnectionParamBean4663","name":
          "HttpConnectionParamBean","data":{"$area":14.0,"$color":0.0,"path":
            
            "org/apache/http/params/HttpConnectionParamBean.html#HttpConnectionParamBean",
            "title":"HttpConnectionParamBean 14 Elements, 0% Coverage"},
          "children":[]},{"id":"HttpConnectionParams4677","name":
          "HttpConnectionParams","data":{"$area":98.0,"$color":24.489796,
            "path":
            "org/apache/http/params/HttpConnectionParams.html#HttpConnectionParams",
            "title":"HttpConnectionParams 98 Elements, 24.5% Coverage"},
          "children":[]},{"id":"HttpParams4775","name":"HttpParams","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/params/HttpParams.html#HttpParams","title":
            "HttpParams 0 Elements,  -  Coverage"},"children":[]},{"id":
          "HttpParamsNames4775","name":"HttpParamsNames","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/params/HttpParamsNames.html#HttpParamsNames",
            "title":"HttpParamsNames 0 Elements,  -  Coverage"},"children":[]},
        {"id":"HttpProtocolParamBean4775","name":"HttpProtocolParamBean",
          "data":{"$area":12.0,"$color":0.0,"path":
            "org/apache/http/params/HttpProtocolParamBean.html#HttpProtocolParamBean",
            "title":"HttpProtocolParamBean 12 Elements, 0% Coverage"},
          "children":[]},{"id":"HttpProtocolParams4787","name":
          "HttpProtocolParams","data":{"$area":111.0,"$color":50.45045,
            "path":
            "org/apache/http/params/HttpProtocolParams.html#HttpProtocolParams",
            "title":"HttpProtocolParams 111 Elements, 50.5% Coverage"},
          "children":[]},{"id":"SyncBasicHttpParams4898","name":
          "SyncBasicHttpParams","data":{"$area":18.0,"$color":33.333336,
            "path":
            "org/apache/http/params/SyncBasicHttpParams.html#SyncBasicHttpParams",
            "title":"SyncBasicHttpParams 18 Elements, 33.3% Coverage"},
          "children":[]}]},{"id":"org.apache.http.entity287","name":
      "org.apache.http.entity","data":{"$area":498.0,"$color":86.34538,
        "title":"org.apache.http.entity 498 Elements, 86.3% Coverage"},
      "children":[{"id":"AbstractHttpEntity287","name":
          "AbstractHttpEntity","data":{"$area":29.0,"$color":96.55172,"path":
            
            "org/apache/http/entity/AbstractHttpEntity.html#AbstractHttpEntity",
            "title":"AbstractHttpEntity 29 Elements, 96.6% Coverage"},
          "children":[]},{"id":"BasicHttpEntity316","name":
          "BasicHttpEntity","data":{"$area":38.0,"$color":81.57895,"path":
            "org/apache/http/entity/BasicHttpEntity.html#BasicHttpEntity",
            "title":"BasicHttpEntity 38 Elements, 81.6% Coverage"},
          "children":[]},{"id":"BufferedHttpEntity354","name":
          "BufferedHttpEntity","data":{"$area":35.0,"$color":100.0,"path":
            "org/apache/http/entity/BufferedHttpEntity.html#BufferedHttpEntity",
            "title":"BufferedHttpEntity 35 Elements, 100% Coverage"},
          "children":[]},{"id":"ByteArrayEntity389","name":
          "ByteArrayEntity","data":{"$area":53.0,"$color":84.90566,"path":
            "org/apache/http/entity/ByteArrayEntity.html#ByteArrayEntity",
            "title":"ByteArrayEntity 53 Elements, 84.9% Coverage"},
          "children":[]},{"id":"ContentLengthStrategy442","name":
          "ContentLengthStrategy","data":{"$area":0.0,"$color":-100.0,"path":
            
            "org/apache/http/entity/ContentLengthStrategy.html#ContentLengthStrategy",
            "title":"ContentLengthStrategy 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ContentProducer442","name":
          "ContentProducer","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/entity/ContentProducer.html#ContentProducer",
            "title":"ContentProducer 0 Elements,  -  Coverage"},"children":[]},
        {"id":"ContentType442","name":"ContentType","data":{"$area":87.0,
            "$color":96.55172,"path":
            "org/apache/http/entity/ContentType.html#ContentType","title":
            "ContentType 87 Elements, 96.6% Coverage"},"children":[]},{"id":
          "EntityTemplate529","name":"EntityTemplate","data":{"$area":23.0,
            "$color":100.0,"path":
            "org/apache/http/entity/EntityTemplate.html#EntityTemplate",
            "title":"EntityTemplate 23 Elements, 100% Coverage"},"children":[]},
        {"id":"FileEntity552","name":"FileEntity","data":{"$area":51.0,
            "$color":60.784317,"path":
            "org/apache/http/entity/FileEntity.html#FileEntity","title":
            "FileEntity 51 Elements, 60.8% Coverage"},"children":[]},{"id":
          "HttpEntityWrapper603","name":"HttpEntityWrapper","data":{"$area":
            25.0,"$color":92.0,"path":
            "org/apache/http/entity/HttpEntityWrapper.html#HttpEntityWrapper",
            "title":"HttpEntityWrapper 25 Elements, 92% Coverage"},
          "children":[]},{"id":"InputStreamEntity628","name":
          "InputStreamEntity","data":{"$area":52.0,"$color":88.46153,"path":
            "org/apache/http/entity/InputStreamEntity.html#InputStreamEntity",
            "title":"InputStreamEntity 52 Elements, 88.5% Coverage"},
          "children":[]},{"id":"SerializableEntity680","name":
          "SerializableEntity","data":{"$area":46.0,"$color":100.0,"path":
            "org/apache/http/entity/SerializableEntity.html#SerializableEntity",
            "title":"SerializableEntity 46 Elements, 100% Coverage"},
          "children":[]},{"id":"StringEntity726","name":"StringEntity",
          "data":{"$area":59.0,"$color":64.406784,"path":
            "org/apache/http/entity/StringEntity.html#StringEntity","title":
            "StringEntity 59 Elements, 64.4% Coverage"},"children":[]}]},{
      "id":"org.apache.http.annotation202","name":
      "org.apache.http.annotation","data":{"$area":0.0,"$color":-100.0,
        "title":"org.apache.http.annotation 0 Elements,  -  Coverage"},
      "children":[{"id":"GuardedBy202","name":"GuardedBy","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/annotation/GuardedBy.html#GuardedBy","title":
            "GuardedBy 0 Elements,  -  Coverage"},"children":[]},{"id":
          "Immutable202","name":"Immutable","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/annotation/Immutable.html#Immutable","title":
            "Immutable 0 Elements,  -  Coverage"},"children":[]},{"id":
          "NotThreadSafe202","name":"NotThreadSafe","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/annotation/NotThreadSafe.html#NotThreadSafe",
            "title":"NotThreadSafe 0 Elements,  -  Coverage"},"children":[]},
        {"id":"ThreadSafe202","name":"ThreadSafe","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/annotation/ThreadSafe.html#ThreadSafe","title":
            "ThreadSafe 0 Elements,  -  Coverage"},"children":[]}]},{"id":
      "org.apache.http.impl.pool2821","name":"org.apache.http.impl.pool",
      "data":{"$area":50.0,"$color":86.0,"title":
        "org.apache.http.impl.pool 50 Elements, 86% Coverage"},"children":[{
          "id":"BasicConnFactory2821","name":"BasicConnFactory","data":{
            "$area":37.0,"$color":91.89189,"path":
            "org/apache/http/impl/pool/BasicConnFactory.html#BasicConnFactory",
            "title":"BasicConnFactory 37 Elements, 91.9% Coverage"},
          "children":[]},{"id":"BasicConnPool2858","name":"BasicConnPool",
          "data":{"$area":6.0,"$color":66.66667,"path":
            "org/apache/http/impl/pool/BasicConnPool.html#BasicConnPool",
            "title":"BasicConnPool 6 Elements, 66.7% Coverage"},"children":[]},
        {"id":"BasicPoolEntry2864","name":"BasicPoolEntry","data":{"$area":
            7.0,"$color":71.42857,"path":
            "org/apache/http/impl/pool/BasicPoolEntry.html#BasicPoolEntry",
            "title":"BasicPoolEntry 7 Elements, 71.4% Coverage"},"children":[]}]},
    {"id":"org.apache.http.util6449","name":"org.apache.http.util","data":{
        "$area":657.0,"$color":71.99391,"title":
        "org.apache.http.util 657 Elements, 72% Coverage"},"children":[{"id":
          "ByteArrayBuffer6449","name":"ByteArrayBuffer","data":{"$area":
            129.0,"$color":98.449615,"path":
            "org/apache/http/util/ByteArrayBuffer.html#ByteArrayBuffer",
            "title":"ByteArrayBuffer 129 Elements, 98.4% Coverage"},
          "children":[]},{"id":"CharArrayBuffer6578","name":
          "CharArrayBuffer","data":{"$area":182.0,"$color":97.8022,"path":
            "org/apache/http/util/CharArrayBuffer.html#CharArrayBuffer",
            "title":"CharArrayBuffer 182 Elements, 97.8% Coverage"},
          "children":[]},{"id":"EncodingUtils6760","name":"EncodingUtils",
          "data":{"$area":53.0,"$color":94.33962,"path":
            "org/apache/http/util/EncodingUtils.html#EncodingUtils","title":
            "EncodingUtils 53 Elements, 94.3% Coverage"},"children":[]},{
          "id":"EntityUtils6813","name":"EntityUtils","data":{"$area":
            127.0,"$color":65.35433,"path":
            "org/apache/http/util/EntityUtils.html#EntityUtils","title":
            "EntityUtils 127 Elements, 65.4% Coverage"},"children":[]},{"id":
          "ExceptionUtils6940","name":"ExceptionUtils","data":{"$area":
            12.0,"$color":0.0,"path":
            "org/apache/http/util/ExceptionUtils.html#ExceptionUtils",
            "title":"ExceptionUtils 12 Elements, 0% Coverage"},"children":[]},
        {"id":"LangUtils6952","name":"LangUtils","data":{"$area":36.0,
            "$color":97.22222,"path":
            "org/apache/http/util/LangUtils.html#LangUtils","title":
            "LangUtils 36 Elements, 97.2% Coverage"},"children":[]},{"id":
          "VersionInfo6988","name":"VersionInfo","data":{"$area":118.0,
            "$color":0.0,"path":
            "org/apache/http/util/VersionInfo.html#VersionInfo","title":
            "VersionInfo 118 Elements, 0% Coverage"},"children":[]}]},{"id":
      "org.apache.http0","name":"org.apache.http","data":{"$area":202.0,
        "$color":88.11881,"title":
        "org.apache.http 202 Elements, 88.1% Coverage"},"children":[{"id":
          "ConnectionClosedException0","name":"ConnectionClosedException",
          "data":{"$area":2.0,"$color":100.0,"path":
            "org/apache/http/ConnectionClosedException.html#ConnectionClosedException",
            "title":"ConnectionClosedException 2 Elements, 100% Coverage"},
          "children":[]},{"id":"ConnectionReuseStrategy2","name":
          "ConnectionReuseStrategy","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/ConnectionReuseStrategy.html#ConnectionReuseStrategy",
            "title":"ConnectionReuseStrategy 0 Elements,  -  Coverage"},
          "children":[]},{"id":"Consts2","name":"Consts","data":{"$area":
            1.0,"$color":0.0,"path":"org/apache/http/Consts.html#Consts",
            "title":"Consts 1 Elements, 0% Coverage"},"children":[]},{"id":
          "ContentTooLongException3","name":"ContentTooLongException","data":
          {"$area":2.0,"$color":0.0,"path":
            "org/apache/http/ContentTooLongException.html#ContentTooLongException",
            "title":"ContentTooLongException 2 Elements, 0% Coverage"},
          "children":[]},{"id":"FormattedHeader5","name":"FormattedHeader",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/FormattedHeader.html#FormattedHeader","title":
            "FormattedHeader 0 Elements,  -  Coverage"},"children":[]},{"id":
          "Header5","name":"Header","data":{"$area":0.0,"$color":-100.0,
            "path":"org/apache/http/Header.html#Header","title":
            "Header 0 Elements,  -  Coverage"},"children":[]},{"id":
          "HeaderElement5","name":"HeaderElement","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/HeaderElement.html#HeaderElement","title":
            "HeaderElement 0 Elements,  -  Coverage"},"children":[]},{"id":
          "HeaderElementIterator5","name":"HeaderElementIterator","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/HeaderElementIterator.html#HeaderElementIterator",
            "title":"HeaderElementIterator 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HeaderIterator5","name":"HeaderIterator",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/HeaderIterator.html#HeaderIterator","title":
            "HeaderIterator 0 Elements,  -  Coverage"},"children":[]},{"id":
          "HttpClientConnection5","name":"HttpClientConnection","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/HttpClientConnection.html#HttpClientConnection",
            "title":"HttpClientConnection 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpConnection5","name":"HttpConnection",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/HttpConnection.html#HttpConnection","title":
            "HttpConnection 0 Elements,  -  Coverage"},"children":[]},{"id":
          "HttpConnectionMetrics5","name":"HttpConnectionMetrics","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/HttpConnectionMetrics.html#HttpConnectionMetrics",
            "title":"HttpConnectionMetrics 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpEntity5","name":"HttpEntity","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/HttpEntity.html#HttpEntity","title":
            "HttpEntity 0 Elements,  -  Coverage"},"children":[]},{"id":
          "HttpEntityEnclosingRequest5","name":
          "HttpEntityEnclosingRequest","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/HttpEntityEnclosingRequest.html#HttpEntityEnclosingRequest",
            "title":"HttpEntityEnclosingRequest 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpException5","name":"HttpException",
          "data":{"$area":7.0,"$color":100.0,"path":
            "org/apache/http/HttpException.html#HttpException","title":
            "HttpException 7 Elements, 100% Coverage"},"children":[]},{"id":
          "HttpHeaders12","name":"HttpHeaders","data":{"$area":1.0,"$color":
            0.0,"path":"org/apache/http/HttpHeaders.html#HttpHeaders",
            "title":"HttpHeaders 1 Elements, 0% Coverage"},"children":[]},{
          "id":"HttpHost13","name":"HttpHost","data":{"$area":68.0,"$color":
            97.05882,"path":"org/apache/http/HttpHost.html#HttpHost","title":
            "HttpHost 68 Elements, 97.1% Coverage"},"children":[]},{"id":
          "HttpInetConnection81","name":"HttpInetConnection","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/HttpInetConnection.html#HttpInetConnection",
            "title":"HttpInetConnection 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpMessage81","name":"HttpMessage","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/HttpMessage.html#HttpMessage","title":
            "HttpMessage 0 Elements,  -  Coverage"},"children":[]},{"id":
          "HttpRequest81","name":"HttpRequest","data":{"$area":0.0,"$color":
            -100.0,"path":"org/apache/http/HttpRequest.html#HttpRequest",
            "title":"HttpRequest 0 Elements,  -  Coverage"},"children":[]},{
          "id":"HttpRequestFactory81","name":"HttpRequestFactory","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/HttpRequestFactory.html#HttpRequestFactory",
            "title":"HttpRequestFactory 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpRequestInterceptor81","name":
          "HttpRequestInterceptor","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/HttpRequestInterceptor.html#HttpRequestInterceptor",
            "title":"HttpRequestInterceptor 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpResponse81","name":"HttpResponse","data":
          {"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/HttpResponse.html#HttpResponse","title":
            "HttpResponse 0 Elements,  -  Coverage"},"children":[]},{"id":
          "HttpResponseFactory81","name":"HttpResponseFactory","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/HttpResponseFactory.html#HttpResponseFactory",
            "title":"HttpResponseFactory 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpResponseInterceptor81","name":
          "HttpResponseInterceptor","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/HttpResponseInterceptor.html#HttpResponseInterceptor",
            "title":"HttpResponseInterceptor 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpServerConnection81","name":
          "HttpServerConnection","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/HttpServerConnection.html#HttpServerConnection",
            "title":"HttpServerConnection 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpStatus81","name":"HttpStatus","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/HttpStatus.html#HttpStatus","title":
            "HttpStatus 0 Elements,  -  Coverage"},"children":[]},{"id":
          "HttpVersion81","name":"HttpVersion","data":{"$area":23.0,"$color":
            82.608696,"path":
            "org/apache/http/HttpVersion.html#HttpVersion","title":
            "HttpVersion 23 Elements, 82.6% Coverage"},"children":[]},{"id":
          "MalformedChunkCodingException104","name":
          "MalformedChunkCodingException","data":{"$area":4.0,"$color":
            100.0,"path":
            "org/apache/http/MalformedChunkCodingException.html#MalformedChunkCodingException",
            "title":
            "MalformedChunkCodingException 4 Elements, 100% Coverage"},
          "children":[]},{"id":"MethodNotSupportedException108","name":
          "MethodNotSupportedException","data":{"$area":4.0,"$color":100.0,
            "path":
            "org/apache/http/MethodNotSupportedException.html#MethodNotSupportedException",
            "title":
            "MethodNotSupportedException 4 Elements, 100% Coverage"},
          "children":[]},{"id":"NameValuePair112","name":"NameValuePair",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/NameValuePair.html#NameValuePair","title":
            "NameValuePair 0 Elements,  -  Coverage"},"children":[]},{"id":
          "NoHttpResponseException112","name":"NoHttpResponseException",
          "data":{"$area":2.0,"$color":100.0,"path":
            "org/apache/http/NoHttpResponseException.html#NoHttpResponseException",
            "title":"NoHttpResponseException 2 Elements, 100% Coverage"},
          "children":[]},{"id":"ParseException114","name":"ParseException",
          "data":{"$area":4.0,"$color":50.0,"path":
            "org/apache/http/ParseException.html#ParseException","title":
            "ParseException 4 Elements, 50% Coverage"},"children":[]},{"id":
          "ProtocolException118","name":"ProtocolException","data":{"$area":
            6.0,"$color":100.0,"path":
            "org/apache/http/ProtocolException.html#ProtocolException",
            "title":"ProtocolException 6 Elements, 100% Coverage"},
          "children":[]},{"id":"ProtocolVersion124","name":
          "ProtocolVersion","data":{"$area":72.0,"$color":83.33333,"path":
            "org/apache/http/ProtocolVersion.html#ProtocolVersion","title":
            "ProtocolVersion 72 Elements, 83.3% Coverage"},"children":[]},{
          "id":"ReasonPhraseCatalog196","name":"ReasonPhraseCatalog","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/ReasonPhraseCatalog.html#ReasonPhraseCatalog",
            "title":"ReasonPhraseCatalog 0 Elements,  -  Coverage"},
          "children":[]},{"id":"RequestLine196","name":"RequestLine","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/RequestLine.html#RequestLine","title":
            "RequestLine 0 Elements,  -  Coverage"},"children":[]},{"id":
          "StatusLine196","name":"StatusLine","data":{"$area":0.0,"$color":
            -100.0,"path":"org/apache/http/StatusLine.html#StatusLine",
            "title":"StatusLine 0 Elements,  -  Coverage"},"children":[]},{
          "id":"TokenIterator196","name":"TokenIterator","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/TokenIterator.html#TokenIterator","title":
            "TokenIterator 0 Elements,  -  Coverage"},"children":[]},{"id":
          "TruncatedChunkException196","name":"TruncatedChunkException",
          "data":{"$area":2.0,"$color":100.0,"path":
            "org/apache/http/TruncatedChunkException.html#TruncatedChunkException",
            "title":"TruncatedChunkException 2 Elements, 100% Coverage"},
          "children":[]},{"id":"UnsupportedHttpVersionException198","name":
          "UnsupportedHttpVersionException","data":{"$area":4.0,"$color":
            100.0,"path":
            "org/apache/http/UnsupportedHttpVersionException.html#UnsupportedHttpVersionException",
            "title":
            "UnsupportedHttpVersionException 4 Elements, 100% Coverage"},
          "children":[]}]},{"id":"org.apache.http.concurrent202","name":
      "org.apache.http.concurrent","data":{"$area":85.0,"$color":91.76471,
        "title":"org.apache.http.concurrent 85 Elements, 91.8% Coverage"},
      "children":[{"id":"BasicFuture202","name":"BasicFuture","data":{
            "$area":85.0,"$color":91.76471,"path":
            "org/apache/http/concurrent/BasicFuture.html#BasicFuture",
            "title":"BasicFuture 85 Elements, 91.8% Coverage"},"children":[]},
        {"id":"Cancellable287","name":"Cancellable","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/concurrent/Cancellable.html#Cancellable",
            "title":"Cancellable 0 Elements,  -  Coverage"},"children":[]},{
          "id":"FutureCallback287","name":"FutureCallback","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/concurrent/FutureCallback.html#FutureCallback",
            "title":"FutureCallback 0 Elements,  -  Coverage"},"children":[]}]}]}

 ); 