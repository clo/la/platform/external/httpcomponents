processTreeMapJson (  {"id":"Clover database Mon Jan 14 2013 22:50:36 CET0","name":"","data":{
    "$area":12584.0,"$color":64.756836,"title":
    " 12584 Elements, 64.8% Coverage"},"children":[{"id":
      "org.apache.http.client.entity523","name":
      "org.apache.http.client.entity","data":{"$area":93.0,"$color":
        66.66667,"title":
        "org.apache.http.client.entity 93 Elements, 66.7% Coverage"},
      "children":[{"id":"DecompressingEntity523","name":
          "DecompressingEntity","data":{"$area":32.0,"$color":87.5,"path":
            "org/apache/http/client/entity/DecompressingEntity.html#DecompressingEntity",
            "title":"DecompressingEntity 32 Elements, 87.5% Coverage"},
          "children":[]},{"id":"DeflateDecompressingEntity555","name":
          "DeflateDecompressingEntity","data":{"$area":41.0,"$color":
            68.29269,"path":
            "org/apache/http/client/entity/DeflateDecompressingEntity.html#DeflateDecompressingEntity",
            "title":
            "DeflateDecompressingEntity 41 Elements, 68.3% Coverage"},
          "children":[]},{"id":"GzipDecompressingEntity596","name":
          "GzipDecompressingEntity","data":{"$area":8.0,"$color":75.0,"path":
            
            "org/apache/http/client/entity/GzipDecompressingEntity.html#GzipDecompressingEntity",
            "title":"GzipDecompressingEntity 8 Elements, 75% Coverage"},
          "children":[]},{"id":"UrlEncodedFormEntity604","name":
          "UrlEncodedFormEntity","data":{"$area":12.0,"$color":0.0,"path":
            "org/apache/http/client/entity/UrlEncodedFormEntity.html#UrlEncodedFormEntity",
            "title":"UrlEncodedFormEntity 12 Elements, 0% Coverage"},
          "children":[]}]},{"id":"org.apache.http.conn.scheme3159","name":
      "org.apache.http.conn.scheme","data":{"$area":306.0,"$color":
        44.11765,"title":
        "org.apache.http.conn.scheme 306 Elements, 44.1% Coverage"},
      "children":[{"id":"HostNameResolver3159","name":"HostNameResolver",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/conn/scheme/HostNameResolver.html#HostNameResolver",
            "title":"HostNameResolver 0 Elements,  -  Coverage"},"children":[]},
        {"id":"LayeredSchemeSocketFactory3159","name":
          "LayeredSchemeSocketFactory","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/conn/scheme/LayeredSchemeSocketFactory.html#LayeredSchemeSocketFactory",
            "title":"LayeredSchemeSocketFactory 0 Elements,  -  Coverage"},
          "children":[]},{"id":"LayeredSocketFactory3159","name":
          "LayeredSocketFactory","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/conn/scheme/LayeredSocketFactory.html#LayeredSocketFactory",
            "title":"LayeredSocketFactory 0 Elements,  -  Coverage"},
          "children":[]},{"id":"LayeredSocketFactoryAdaptor3159","name":
          "LayeredSocketFactoryAdaptor","data":{"$area":5.0,"$color":0.0,
            "path":
            "org/apache/http/conn/scheme/LayeredSocketFactoryAdaptor.html#LayeredSocketFactoryAdaptor",
            "title":"LayeredSocketFactoryAdaptor 5 Elements, 0% Coverage"},
          "children":[]},{"id":"PlainSocketFactory3164","name":
          "PlainSocketFactory","data":{"$area":66.0,"$color":50.0,"path":
            "org/apache/http/conn/scheme/PlainSocketFactory.html#PlainSocketFactory",
            "title":"PlainSocketFactory 66 Elements, 50% Coverage"},
          "children":[]},{"id":"Scheme3230","name":"Scheme","data":{"$area":
            98.0,"$color":64.28571,"path":
            "org/apache/http/conn/scheme/Scheme.html#Scheme","title":
            "Scheme 98 Elements, 64.3% Coverage"},"children":[]},{"id":
          "SchemeLayeredSocketFactory3328","name":
          "SchemeLayeredSocketFactory","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/conn/scheme/SchemeLayeredSocketFactory.html#SchemeLayeredSocketFactory",
            "title":"SchemeLayeredSocketFactory 0 Elements,  -  Coverage"},
          "children":[]},{"id":"SchemeLayeredSocketFactoryAdaptor3328",
          "name":"SchemeLayeredSocketFactoryAdaptor","data":{"$area":5.0,
            "$color":0.0,"path":
            "org/apache/http/conn/scheme/SchemeLayeredSocketFactoryAdaptor.html#SchemeLayeredSocketFactoryAdaptor",
            "title":
            "SchemeLayeredSocketFactoryAdaptor 5 Elements, 0% Coverage"},
          "children":[]},{"id":"SchemeLayeredSocketFactoryAdaptor23333",
          "name":"SchemeLayeredSocketFactoryAdaptor2","data":{"$area":11.0,
            "$color":0.0,"path":
            "org/apache/http/conn/scheme/SchemeLayeredSocketFactoryAdaptor2.html#SchemeLayeredSocketFactoryAdaptor2",
            "title":
            "SchemeLayeredSocketFactoryAdaptor2 11 Elements, 0% Coverage"},
          "children":[]},{"id":"SchemeRegistry3344","name":
          "SchemeRegistry","data":{"$area":46.0,"$color":84.78261,"path":
            "org/apache/http/conn/scheme/SchemeRegistry.html#SchemeRegistry",
            "title":"SchemeRegistry 46 Elements, 84.8% Coverage"},"children":
          []},{"id":"SchemeSocketFactory3390","name":"SchemeSocketFactory",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/conn/scheme/SchemeSocketFactory.html#SchemeSocketFactory",
            "title":"SchemeSocketFactory 0 Elements,  -  Coverage"},
          "children":[]},{"id":"SchemeSocketFactoryAdaptor3390","name":
          "SchemeSocketFactoryAdaptor","data":{"$area":36.0,"$color":0.0,
            "path":
            "org/apache/http/conn/scheme/SchemeSocketFactoryAdaptor.html#SchemeSocketFactoryAdaptor",
            "title":"SchemeSocketFactoryAdaptor 36 Elements, 0% Coverage"},
          "children":[]},{"id":"SocketFactory3426","name":"SocketFactory",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/conn/scheme/SocketFactory.html#SocketFactory",
            "title":"SocketFactory 0 Elements,  -  Coverage"},"children":[]},
        {"id":"SocketFactoryAdaptor3426","name":"SocketFactoryAdaptor",
          "data":{"$area":39.0,"$color":0.0,"path":
            "org/apache/http/conn/scheme/SocketFactoryAdaptor.html#SocketFactoryAdaptor",
            "title":"SocketFactoryAdaptor 39 Elements, 0% Coverage"},
          "children":[]}]},{"id":"org.apache.http.conn.params2579","name":
      "org.apache.http.conn.params","data":{"$area":157.0,"$color":
        48.407642,"title":
        "org.apache.http.conn.params 157 Elements, 48.4% Coverage"},
      "children":[{"id":"ConnConnectionPNames2579","name":
          "ConnConnectionPNames","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/conn/params/ConnConnectionPNames.html#ConnConnectionPNames",
            "title":"ConnConnectionPNames 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ConnConnectionParamBean2579","name":
          "ConnConnectionParamBean","data":{"$area":4.0,"$color":0.0,"path":
            "org/apache/http/conn/params/ConnConnectionParamBean.html#ConnConnectionParamBean",
            "title":"ConnConnectionParamBean 4 Elements, 0% Coverage"},
          "children":[]},{"id":"ConnManagerPNames2583","name":
          "ConnManagerPNames","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/conn/params/ConnManagerPNames.html#ConnManagerPNames",
            "title":"ConnManagerPNames 0 Elements,  -  Coverage"},"children":
          []},{"id":"ConnManagerParamBean2583","name":
          "ConnManagerParamBean","data":{"$area":8.0,"$color":0.0,"path":
            "org/apache/http/conn/params/ConnManagerParamBean.html#ConnManagerParamBean",
            "title":"ConnManagerParamBean 8 Elements, 0% Coverage"},
          "children":[]},{"id":"ConnManagerParams2591","name":
          "ConnManagerParams","data":{"$area":43.0,"$color":27.906979,"path":
            
            "org/apache/http/conn/params/ConnManagerParams.html#ConnManagerParams",
            "title":"ConnManagerParams 43 Elements, 27.9% Coverage"},
          "children":[]},{"id":"ConnPerRoute2634","name":"ConnPerRoute",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/conn/params/ConnPerRoute.html#ConnPerRoute",
            "title":"ConnPerRoute 0 Elements,  -  Coverage"},"children":[]},{
          "id":"ConnPerRouteBean2634","name":"ConnPerRouteBean","data":{
            "$area":46.0,"$color":36.95652,"path":
            "org/apache/http/conn/params/ConnPerRouteBean.html#ConnPerRouteBean",
            "title":"ConnPerRouteBean 46 Elements, 37% Coverage"},"children":
          []},{"id":"ConnRoutePNames2680","name":"ConnRoutePNames","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/conn/params/ConnRoutePNames.html#ConnRoutePNames",
            "title":"ConnRoutePNames 0 Elements,  -  Coverage"},"children":[]},
        {"id":"ConnRouteParamBean2680","name":"ConnRouteParamBean","data":{
            "$area":8.0,"$color":0.0,"path":
            "org/apache/http/conn/params/ConnRouteParamBean.html#ConnRouteParamBean",
            "title":"ConnRouteParamBean 8 Elements, 0% Coverage"},"children":
          []},{"id":"ConnRouteParams2688","name":"ConnRouteParams","data":{
            "$area":48.0,"$color":97.91667,"path":
            "org/apache/http/conn/params/ConnRouteParams.html#ConnRouteParams",
            "title":"ConnRouteParams 48 Elements, 97.9% Coverage"},
          "children":[]}]},{"id":"org.apache.http.cookie.params4135","name":
      "org.apache.http.cookie.params","data":{"$area":6.0,"$color":0.0,
        "title":"org.apache.http.cookie.params 6 Elements, 0% Coverage"},
      "children":[{"id":"CookieSpecPNames4135","name":"CookieSpecPNames",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/cookie/params/CookieSpecPNames.html#CookieSpecPNames",
            "title":"CookieSpecPNames 0 Elements,  -  Coverage"},"children":[]},
        {"id":"CookieSpecParamBean4135","name":"CookieSpecParamBean","data":{
            "$area":6.0,"$color":0.0,"path":
            "org/apache/http/cookie/params/CookieSpecParamBean.html#CookieSpecParamBean",
            "title":"CookieSpecParamBean 6 Elements, 0% Coverage"},
          "children":[]}]},{"id":"org.apache.http.conn.ssl3465","name":
      "org.apache.http.conn.ssl","data":{"$area":497.0,"$color":48.69215,
        "title":"org.apache.http.conn.ssl 497 Elements, 48.7% Coverage"},
      "children":[{"id":"AbstractVerifier3465","name":"AbstractVerifier",
          "data":{"$area":168.0,"$color":86.90476,"path":
            "org/apache/http/conn/ssl/AbstractVerifier.html#AbstractVerifier",
            "title":"AbstractVerifier 168 Elements, 86.9% Coverage"},
          "children":[]},{"id":"AllowAllHostnameVerifier3633","name":
          "AllowAllHostnameVerifier","data":{"$area":3.0,"$color":
            33.333336,"path":
            "org/apache/http/conn/ssl/AllowAllHostnameVerifier.html#AllowAllHostnameVerifier",
            "title":"AllowAllHostnameVerifier 3 Elements, 33.3% Coverage"},
          "children":[]},{"id":"BrowserCompatHostnameVerifier3636","name":
          "BrowserCompatHostnameVerifier","data":{"$area":4.0,"$color":
            50.0,"path":
            "org/apache/http/conn/ssl/BrowserCompatHostnameVerifier.html#BrowserCompatHostnameVerifier",
            "title":
            "BrowserCompatHostnameVerifier 4 Elements, 50% Coverage"},
          "children":[]},{"id":"SSLInitializationException3640","name":
          "SSLInitializationException","data":{"$area":2.0,"$color":0.0,
            "path":
            "org/apache/http/conn/ssl/SSLInitializationException.html#SSLInitializationException",
            "title":"SSLInitializationException 2 Elements, 0% Coverage"},
          "children":[]},{"id":"SSLSocketFactory3642","name":
          "SSLSocketFactory","data":{"$area":301.0,"$color":27.906979,"path":
            
            "org/apache/http/conn/ssl/SSLSocketFactory.html#SSLSocketFactory",
            "title":"SSLSocketFactory 301 Elements, 27.9% Coverage"},
          "children":[]},{"id":"StrictHostnameVerifier3943","name":
          "StrictHostnameVerifier","data":{"$area":4.0,"$color":50.0,"path":
            "org/apache/http/conn/ssl/StrictHostnameVerifier.html#StrictHostnameVerifier",
            "title":"StrictHostnameVerifier 4 Elements, 50% Coverage"},
          "children":[]},{"id":"TrustManagerDecorator3947","name":
          "TrustManagerDecorator","data":{"$area":13.0,"$color":53.846157,
            "path":
            "org/apache/http/conn/ssl/TrustManagerDecorator.html#TrustManagerDecorator",
            "title":"TrustManagerDecorator 13 Elements, 53.8% Coverage"},
          "children":[]},{"id":"TrustSelfSignedStrategy3960","name":
          "TrustSelfSignedStrategy","data":{"$area":2.0,"$color":0.0,"path":
            "org/apache/http/conn/ssl/TrustSelfSignedStrategy.html#TrustSelfSignedStrategy",
            "title":"TrustSelfSignedStrategy 2 Elements, 0% Coverage"},
          "children":[]},{"id":"TrustStrategy3962","name":"TrustStrategy",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/conn/ssl/TrustStrategy.html#TrustStrategy",
            "title":"TrustStrategy 0 Elements,  -  Coverage"},"children":[]},
        {"id":"X509HostnameVerifier3962","name":"X509HostnameVerifier",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/conn/ssl/X509HostnameVerifier.html#X509HostnameVerifier",
            "title":"X509HostnameVerifier 0 Elements,  -  Coverage"},
          "children":[]}]},{"id":"org.apache.http.client.utils1469","name":
      "org.apache.http.client.utils","data":{"$area":812.0,"$color":
        85.46798,"title":
        "org.apache.http.client.utils 812 Elements, 85.5% Coverage"},
      "children":[{"id":"CloneUtils1469","name":"CloneUtils","data":{"$area":
            24.0,"$color":62.5,"path":
            "org/apache/http/client/utils/CloneUtils.html#CloneUtils",
            "title":"CloneUtils 24 Elements, 62.5% Coverage"},"children":[]},
        {"id":"HttpClientUtils1493","name":"HttpClientUtils","data":{"$area":
            16.0,"$color":87.5,"path":
            "org/apache/http/client/utils/HttpClientUtils.html#HttpClientUtils",
            "title":"HttpClientUtils 16 Elements, 87.5% Coverage"},
          "children":[]},{"id":"Idn1509","name":"Idn","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/client/utils/Idn.html#Idn","title":
            "Idn 0 Elements,  -  Coverage"},"children":[]},{"id":
          "JdkIdn1509","name":"JdkIdn","data":{"$area":12.0,"$color":
            58.333332,"path":
            "org/apache/http/client/utils/JdkIdn.html#JdkIdn","title":
            "JdkIdn 12 Elements, 58.3% Coverage"},"children":[]},{"id":
          "Punycode1521","name":"Punycode","data":{"$area":8.0,"$color":
            87.5,"path":
            "org/apache/http/client/utils/Punycode.html#Punycode","title":
            "Punycode 8 Elements, 87.5% Coverage"},"children":[]},{"id":
          "Rfc3492Idn1529","name":"Rfc3492Idn","data":{"$area":91.0,"$color":
            95.60439,"path":
            "org/apache/http/client/utils/Rfc3492Idn.html#Rfc3492Idn",
            "title":"Rfc3492Idn 91 Elements, 95.6% Coverage"},"children":[]},
        {"id":"URIBuilder1620","name":"URIBuilder","data":{"$area":208.0,
            "$color":87.980774,"path":
            "org/apache/http/client/utils/URIBuilder.html#URIBuilder",
            "title":"URIBuilder 208 Elements, 88% Coverage"},"children":[]},{
          "id":"URIUtils1828","name":"URIUtils","data":{"$area":196.0,
            "$color":72.44898,"path":
            "org/apache/http/client/utils/URIUtils.html#URIUtils","title":
            "URIUtils 196 Elements, 72.4% Coverage"},"children":[]},{"id":
          "URLEncodedUtils2024","name":"URLEncodedUtils","data":{"$area":
            257.0,"$color":92.99611,"path":
            "org/apache/http/client/utils/URLEncodedUtils.html#URLEncodedUtils",
            "title":"URLEncodedUtils 257 Elements, 93% Coverage"},"children":
          []}]},{"id":"org.apache.http.impl.conn8152","name":
      "org.apache.http.impl.conn","data":{"$area":1932.0,"$color":
        36.645966,"title":
        "org.apache.http.impl.conn 1932 Elements, 36.6% Coverage"},
      "children":[{"id":"AbstractClientConnAdapter8152","name":
          "AbstractClientConnAdapter","data":{"$area":170.0,"$color":
            5.294118,"path":
            "org/apache/http/impl/conn/AbstractClientConnAdapter.html#AbstractClientConnAdapter",
            "title":
            "AbstractClientConnAdapter 170 Elements, 5.3% Coverage"},
          "children":[]},{"id":"AbstractPoolEntry8322","name":
          "AbstractPoolEntry","data":{"$area":95.0,"$color":15.789473,"path":
            
            "org/apache/http/impl/conn/AbstractPoolEntry.html#AbstractPoolEntry",
            "title":"AbstractPoolEntry 95 Elements, 15.8% Coverage"},
          "children":[]},{"id":"AbstractPooledConnAdapter8417","name":
          "AbstractPooledConnAdapter","data":{"$area":70.0,"$color":
            4.2857146,"path":
            "org/apache/http/impl/conn/AbstractPooledConnAdapter.html#AbstractPooledConnAdapter",
            "title":"AbstractPooledConnAdapter 70 Elements, 4.3% Coverage"},
          "children":[]},{"id":"BasicClientConnectionManager8487","name":
          "BasicClientConnectionManager","data":{"$area":155.0,"$color":
            63.225807,"path":
            "org/apache/http/impl/conn/BasicClientConnectionManager.html#BasicClientConnectionManager",
            "title":
            "BasicClientConnectionManager 155 Elements, 63.2% Coverage"},
          "children":[]},{"id":"ConnectionShutdownException8642","name":
          "ConnectionShutdownException","data":{"$area":2.0,"$color":0.0,
            "path":
            "org/apache/http/impl/conn/ConnectionShutdownException.html#ConnectionShutdownException",
            "title":"ConnectionShutdownException 2 Elements, 0% Coverage"},
          "children":[]},{"id":"DefaultClientConnection8644","name":
          "DefaultClientConnection","data":{"$area":122.0,"$color":
            58.196724,"path":
            "org/apache/http/impl/conn/DefaultClientConnection.html#DefaultClientConnection",
            "title":"DefaultClientConnection 122 Elements, 58.2% Coverage"},
          "children":[]},{"id":"DefaultClientConnectionOperator8766","name":
          "DefaultClientConnectionOperator","data":{"$area":119.0,"$color":
            45.37815,"path":
            "org/apache/http/impl/conn/DefaultClientConnectionOperator.html#DefaultClientConnectionOperator",
            "title":
            "DefaultClientConnectionOperator 119 Elements, 45.4% Coverage"},
          "children":[]},{"id":"DefaultHttpResponseParser8885","name":
          "DefaultHttpResponseParser","data":{"$area":36.0,"$color":
            88.88889,"path":
            "org/apache/http/impl/conn/DefaultHttpResponseParser.html#DefaultHttpResponseParser",
            "title":
            "DefaultHttpResponseParser 36 Elements, 88.9% Coverage"},
          "children":[]},{"id":"DefaultHttpRoutePlanner8921","name":
          "DefaultHttpRoutePlanner","data":{"$area":33.0,"$color":69.69697,
            "path":
            "org/apache/http/impl/conn/DefaultHttpRoutePlanner.html#DefaultHttpRoutePlanner",
            "title":"DefaultHttpRoutePlanner 33 Elements, 69.7% Coverage"},
          "children":[]},{"id":"DefaultResponseParser8954","name":
          "DefaultResponseParser","data":{"$area":37.0,"$color":0.0,"path":
            "org/apache/http/impl/conn/DefaultResponseParser.html#DefaultResponseParser",
            "title":"DefaultResponseParser 37 Elements, 0% Coverage"},
          "children":[]},{"id":"HttpConnPool8991","name":"HttpConnPool",
          "data":{"$area":8.0,"$color":100.0,"path":
            "org/apache/http/impl/conn/HttpConnPool.html#HttpConnPool",
            "title":"HttpConnPool 8 Elements, 100% Coverage"},"children":[]},
        {"id":"HttpConnPool.InternalConnFactory8999","name":
          "HttpConnPool.InternalConnFactory","data":{"$area":2.0,"$color":
            100.0,"path":
            "org/apache/http/impl/conn/HttpConnPool.html#HttpConnPool.InternalConnFactory",
            "title":
            "HttpConnPool.InternalConnFactory 2 Elements, 100% Coverage"},
          "children":[]},{"id":"HttpPoolEntry9001","name":"HttpPoolEntry",
          "data":{"$area":25.0,"$color":88.0,"path":
            "org/apache/http/impl/conn/HttpPoolEntry.html#HttpPoolEntry",
            "title":"HttpPoolEntry 25 Elements, 88% Coverage"},"children":[]},
        {"id":"IdleConnectionHandler9026","name":"IdleConnectionHandler",
          "data":{"$area":59.0,"$color":5.0847454,"path":
            "org/apache/http/impl/conn/IdleConnectionHandler.html#IdleConnectionHandler",
            "title":"IdleConnectionHandler 59 Elements, 5.1% Coverage"},
          "children":[]},{"id":"IdleConnectionHandler.TimeValues9085","name":
          "IdleConnectionHandler.TimeValues","data":{"$area":7.0,"$color":
            0.0,"path":
            "org/apache/http/impl/conn/IdleConnectionHandler.html#IdleConnectionHandler.TimeValues",
            "title":
            "IdleConnectionHandler.TimeValues 7 Elements, 0% Coverage"},
          "children":[]},{"id":"InMemoryDnsResolver9092","name":
          "InMemoryDnsResolver","data":{"$area":23.0,"$color":0.0,"path":
            "org/apache/http/impl/conn/InMemoryDnsResolver.html#InMemoryDnsResolver",
            "title":"InMemoryDnsResolver 23 Elements, 0% Coverage"},
          "children":[]},{"id":"LoggingSessionInputBuffer9115","name":
          "LoggingSessionInputBuffer","data":{"$area":61.0,"$color":0.0,
            "path":
            "org/apache/http/impl/conn/LoggingSessionInputBuffer.html#LoggingSessionInputBuffer",
            "title":"LoggingSessionInputBuffer 61 Elements, 0% Coverage"},
          "children":[]},{"id":"LoggingSessionOutputBuffer9176","name":
          "LoggingSessionOutputBuffer","data":{"$area":46.0,"$color":0.0,
            "path":
            "org/apache/http/impl/conn/LoggingSessionOutputBuffer.html#LoggingSessionOutputBuffer",
            "title":"LoggingSessionOutputBuffer 46 Elements, 0% Coverage"},
          "children":[]},{"id":"ManagedClientConnectionImpl9222","name":
          "ManagedClientConnectionImpl","data":{"$area":321.0,"$color":
            51.40187,"path":
            "org/apache/http/impl/conn/ManagedClientConnectionImpl.html#ManagedClientConnectionImpl",
            "title":
            "ManagedClientConnectionImpl 321 Elements, 51.4% Coverage"},
          "children":[]},{"id":"PoolingClientConnectionManager9543","name":
          "PoolingClientConnectionManager","data":{"$area":176.0,"$color":
            57.386364,"path":
            "org/apache/http/impl/conn/PoolingClientConnectionManager.html#PoolingClientConnectionManager",
            "title":
            "PoolingClientConnectionManager 176 Elements, 57.4% Coverage"},
          "children":[]},{"id":"ProxySelectorRoutePlanner9719","name":
          "ProxySelectorRoutePlanner","data":{"$area":88.0,"$color":
            70.454544,"path":
            "org/apache/http/impl/conn/ProxySelectorRoutePlanner.html#ProxySelectorRoutePlanner",
            "title":
            "ProxySelectorRoutePlanner 88 Elements, 70.5% Coverage"},
          "children":[]},{"id":"SchemeRegistryFactory9807","name":
          "SchemeRegistryFactory","data":{"$area":10.0,"$color":50.0,"path":
            "org/apache/http/impl/conn/SchemeRegistryFactory.html#SchemeRegistryFactory",
            "title":"SchemeRegistryFactory 10 Elements, 50% Coverage"},
          "children":[]},{"id":"SingleClientConnManager9817","name":
          "SingleClientConnManager","data":{"$area":159.0,"$color":
            16.981133,"path":
            "org/apache/http/impl/conn/SingleClientConnManager.html#SingleClientConnManager",
            "title":"SingleClientConnManager 159 Elements, 17% Coverage"},
          "children":[]},{"id":"SingleClientConnManager.PoolEntry9976",
          "name":"SingleClientConnManager.PoolEntry","data":{"$area":14.0,
            "$color":42.857143,"path":
            "org/apache/http/impl/conn/SingleClientConnManager.html#SingleClientConnManager.PoolEntry",
            "title":
            "SingleClientConnManager.PoolEntry 14 Elements, 42.9% Coverage"},
          "children":[]},{"id":"SingleClientConnManager.ConnAdapter9990",
          "name":"SingleClientConnManager.ConnAdapter","data":{"$area":4.0,
            "$color":0.0,"path":
            "org/apache/http/impl/conn/SingleClientConnManager.html#SingleClientConnManager.ConnAdapter",
            "title":
            "SingleClientConnManager.ConnAdapter 4 Elements, 0% Coverage"},
          "children":[]},{"id":"SystemDefaultDnsResolver9994","name":
          "SystemDefaultDnsResolver","data":{"$area":2.0,"$color":100.0,
            "path":
            "org/apache/http/impl/conn/SystemDefaultDnsResolver.html#SystemDefaultDnsResolver",
            "title":"SystemDefaultDnsResolver 2 Elements, 100% Coverage"},
          "children":[]},{"id":"Wire9996","name":"Wire","data":{"$area":
            88.0,"$color":0.0,"path":
            "org/apache/http/impl/conn/Wire.html#Wire","title":
            "Wire 88 Elements, 0% Coverage"},"children":[]}]},{"id":
      "org.apache.http.client491","name":"org.apache.http.client","data":{
        "$area":32.0,"$color":53.125,"title":
        "org.apache.http.client 32 Elements, 53.1% Coverage"},"children":[{
          "id":"AuthCache491","name":"AuthCache","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/client/AuthCache.html#AuthCache","title":
            "AuthCache 0 Elements,  -  Coverage"},"children":[]},{"id":
          "AuthenticationHandler491","name":"AuthenticationHandler","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/client/AuthenticationHandler.html#AuthenticationHandler",
            "title":"AuthenticationHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"AuthenticationStrategy491","name":
          "AuthenticationStrategy","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/client/AuthenticationStrategy.html#AuthenticationStrategy",
            "title":"AuthenticationStrategy 0 Elements,  -  Coverage"},
          "children":[]},{"id":"BackoffManager491","name":"BackoffManager",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/client/BackoffManager.html#BackoffManager",
            "title":"BackoffManager 0 Elements,  -  Coverage"},"children":[]},
        {"id":"CircularRedirectException491","name":
          "CircularRedirectException","data":{"$area":6.0,"$color":
            33.333336,"path":
            "org/apache/http/client/CircularRedirectException.html#CircularRedirectException",
            "title":"CircularRedirectException 6 Elements, 33.3% Coverage"},
          "children":[]},{"id":"ClientProtocolException497","name":
          "ClientProtocolException","data":{"$area":9.0,"$color":44.444447,
            "path":
            "org/apache/http/client/ClientProtocolException.html#ClientProtocolException",
            "title":"ClientProtocolException 9 Elements, 44.4% Coverage"},
          "children":[]},{"id":"ConnectionBackoffStrategy506","name":
          "ConnectionBackoffStrategy","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/client/ConnectionBackoffStrategy.html#ConnectionBackoffStrategy",
            "title":"ConnectionBackoffStrategy 0 Elements,  -  Coverage"},
          "children":[]},{"id":"CookieStore506","name":"CookieStore","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/client/CookieStore.html#CookieStore","title":
            "CookieStore 0 Elements,  -  Coverage"},"children":[]},{"id":
          "CredentialsProvider506","name":"CredentialsProvider","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/client/CredentialsProvider.html#CredentialsProvider",
            "title":"CredentialsProvider 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpClient506","name":"HttpClient","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/client/HttpClient.html#HttpClient","title":
            "HttpClient 0 Elements,  -  Coverage"},"children":[]},{"id":
          "HttpRequestRetryHandler506","name":"HttpRequestRetryHandler",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/client/HttpRequestRetryHandler.html#HttpRequestRetryHandler",
            "title":"HttpRequestRetryHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpResponseException506","name":
          "HttpResponseException","data":{"$area":5.0,"$color":100.0,"path":
            "org/apache/http/client/HttpResponseException.html#HttpResponseException",
            "title":"HttpResponseException 5 Elements, 100% Coverage"},
          "children":[]},{"id":"NonRepeatableRequestException511","name":
          "NonRepeatableRequestException","data":{"$area":6.0,"$color":
            66.66667,"path":
            "org/apache/http/client/NonRepeatableRequestException.html#NonRepeatableRequestException",
            "title":
            "NonRepeatableRequestException 6 Elements, 66.7% Coverage"},
          "children":[]},{"id":"RedirectException517","name":
          "RedirectException","data":{"$area":6.0,"$color":33.333336,"path":
            "org/apache/http/client/RedirectException.html#RedirectException",
            "title":"RedirectException 6 Elements, 33.3% Coverage"},
          "children":[]},{"id":"RedirectHandler523","name":
          "RedirectHandler","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/client/RedirectHandler.html#RedirectHandler",
            "title":"RedirectHandler 0 Elements,  -  Coverage"},"children":[]},
        {"id":"RedirectStrategy523","name":"RedirectStrategy","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/client/RedirectStrategy.html#RedirectStrategy",
            "title":"RedirectStrategy 0 Elements,  -  Coverage"},"children":[]},
        {"id":"RequestDirector523","name":"RequestDirector","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/client/RequestDirector.html#RequestDirector",
            "title":"RequestDirector 0 Elements,  -  Coverage"},"children":[]},
        {"id":"ResponseHandler523","name":"ResponseHandler","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/client/ResponseHandler.html#ResponseHandler",
            "title":"ResponseHandler 0 Elements,  -  Coverage"},"children":[]},
        {"id":"ServiceUnavailableRetryStrategy523","name":
          "ServiceUnavailableRetryStrategy","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/client/ServiceUnavailableRetryStrategy.html#ServiceUnavailableRetryStrategy",
            "title":
            "ServiceUnavailableRetryStrategy 0 Elements,  -  Coverage"},
          "children":[]},{"id":"UserTokenHandler523","name":
          "UserTokenHandler","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/client/UserTokenHandler.html#UserTokenHandler",
            "title":"UserTokenHandler 0 Elements,  -  Coverage"},"children":[]}]},
    {"id":"org.apache.http.conn.routing2736","name":
      "org.apache.http.conn.routing","data":{"$area":423.0,"$color":
        99.52718,"title":
        "org.apache.http.conn.routing 423 Elements, 99.5% Coverage"},
      "children":[{"id":"BasicRouteDirector2736","name":
          "BasicRouteDirector","data":{"$area":81.0,"$color":100.0,"path":
            "org/apache/http/conn/routing/BasicRouteDirector.html#BasicRouteDirector",
            "title":"BasicRouteDirector 81 Elements, 100% Coverage"},
          "children":[]},{"id":"HttpRoute2817","name":"HttpRoute","data":{
            "$area":152.0,"$color":98.68421,"path":
            "org/apache/http/conn/routing/HttpRoute.html#HttpRoute","title":
            "HttpRoute 152 Elements, 98.7% Coverage"},"children":[]},{"id":
          "HttpRouteDirector2969","name":"HttpRouteDirector","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/conn/routing/HttpRouteDirector.html#HttpRouteDirector",
            "title":"HttpRouteDirector 0 Elements,  -  Coverage"},"children":
          []},{"id":"HttpRoutePlanner2969","name":"HttpRoutePlanner","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/conn/routing/HttpRoutePlanner.html#HttpRoutePlanner",
            "title":"HttpRoutePlanner 0 Elements,  -  Coverage"},"children":[]},
        {"id":"RouteInfo2969","name":"RouteInfo","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/conn/routing/RouteInfo.html#RouteInfo","title":
            "RouteInfo 0 Elements,  -  Coverage"},"children":[]},{"id":
          "RouteInfo.TunnelType2969","name":"RouteInfo.TunnelType","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/conn/routing/RouteInfo.html#RouteInfo.TunnelType",
            "title":"RouteInfo.TunnelType 0 Elements,  -  Coverage"},
          "children":[]},{"id":"RouteInfo.LayerType2969","name":
          "RouteInfo.LayerType","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/conn/routing/RouteInfo.html#RouteInfo.LayerType",
            "title":"RouteInfo.LayerType 0 Elements,  -  Coverage"},
          "children":[]},{"id":"RouteTracker2969","name":"RouteTracker",
          "data":{"$area":190.0,"$color":100.0,"path":
            "org/apache/http/conn/routing/RouteTracker.html#RouteTracker",
            "title":"RouteTracker 190 Elements, 100% Coverage"},"children":[]}]},
    {"id":"org.apache.http.client.protocol899","name":
      "org.apache.http.client.protocol","data":{"$area":570.0,"$color":
        85.96491,"title":
        "org.apache.http.client.protocol 570 Elements, 86% Coverage"},
      "children":[{"id":"ClientContext899","name":"ClientContext","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/client/protocol/ClientContext.html#ClientContext",
            "title":"ClientContext 0 Elements,  -  Coverage"},"children":[]},
        {"id":"ClientContextConfigurer899","name":
          "ClientContextConfigurer","data":{"$area":14.0,"$color":0.0,"path":
            
            "org/apache/http/client/protocol/ClientContextConfigurer.html#ClientContextConfigurer",
            "title":"ClientContextConfigurer 14 Elements, 0% Coverage"},
          "children":[]},{"id":"RequestAcceptEncoding913","name":
          "RequestAcceptEncoding","data":{"$area":5.0,"$color":100.0,"path":
            "org/apache/http/client/protocol/RequestAcceptEncoding.html#RequestAcceptEncoding",
            "title":"RequestAcceptEncoding 5 Elements, 100% Coverage"},
          "children":[]},{"id":"RequestAddCookies918","name":
          "RequestAddCookies","data":{"$area":120.0,"$color":90.0,"path":
            "org/apache/http/client/protocol/RequestAddCookies.html#RequestAddCookies",
            "title":"RequestAddCookies 120 Elements, 90% Coverage"},
          "children":[]},{"id":"RequestAuthCache1038","name":
          "RequestAuthCache","data":{"$area":67.0,"$color":91.04478,"path":
            "org/apache/http/client/protocol/RequestAuthCache.html#RequestAuthCache",
            "title":"RequestAuthCache 67 Elements, 91% Coverage"},"children":
          []},{"id":"RequestAuthenticationBase1105","name":
          "RequestAuthenticationBase","data":{"$area":66.0,"$color":
            77.27273,"path":
            "org/apache/http/client/protocol/RequestAuthenticationBase.html#RequestAuthenticationBase",
            "title":
            "RequestAuthenticationBase 66 Elements, 77.3% Coverage"},
          "children":[]},{"id":"RequestClientConnControl1171","name":
          "RequestClientConnControl","data":{"$area":34.0,"$color":
            91.17647,"path":
            "org/apache/http/client/protocol/RequestClientConnControl.html#RequestClientConnControl",
            "title":"RequestClientConnControl 34 Elements, 91.2% Coverage"},
          "children":[]},{"id":"RequestDefaultHeaders1205","name":
          "RequestDefaultHeaders","data":{"$area":18.0,"$color":100.0,"path":
            
            "org/apache/http/client/protocol/RequestDefaultHeaders.html#RequestDefaultHeaders",
            "title":"RequestDefaultHeaders 18 Elements, 100% Coverage"},
          "children":[]},{"id":"RequestProxyAuthentication1223","name":
          "RequestProxyAuthentication","data":{"$area":37.0,"$color":
            86.48649,"path":
            "org/apache/http/client/protocol/RequestProxyAuthentication.html#RequestProxyAuthentication",
            "title":
            "RequestProxyAuthentication 37 Elements, 86.5% Coverage"},
          "children":[]},{"id":"RequestTargetAuthentication1260","name":
          "RequestTargetAuthentication","data":{"$area":31.0,"$color":
            93.548386,"path":
            "org/apache/http/client/protocol/RequestTargetAuthentication.html#RequestTargetAuthentication",
            "title":
            "RequestTargetAuthentication 31 Elements, 93.5% Coverage"},
          "children":[]},{"id":"ResponseAuthCache1291","name":
          "ResponseAuthCache","data":{"$area":84.0,"$color":83.33333,"path":
            "org/apache/http/client/protocol/ResponseAuthCache.html#ResponseAuthCache",
            "title":"ResponseAuthCache 84 Elements, 83.3% Coverage"},
          "children":[]},{"id":"ResponseContentEncoding1375","name":
          "ResponseContentEncoding","data":{"$area":35.0,"$color":94.28571,
            "path":
            "org/apache/http/client/protocol/ResponseContentEncoding.html#ResponseContentEncoding",
            "title":"ResponseContentEncoding 35 Elements, 94.3% Coverage"},
          "children":[]},{"id":"ResponseProcessCookies1410","name":
          "ResponseProcessCookies","data":{"$area":59.0,"$color":88.1356,
            "path":
            "org/apache/http/client/protocol/ResponseProcessCookies.html#ResponseProcessCookies",
            "title":"ResponseProcessCookies 59 Elements, 88.1% Coverage"},
          "children":[]}]},{"id":"org.apache.http.client.params811","name":
      "org.apache.http.client.params","data":{"$area":88.0,"$color":
        27.272728,"title":
        "org.apache.http.client.params 88 Elements, 27.3% Coverage"},
      "children":[{"id":"AllClientPNames811","name":"AllClientPNames","data":
          {"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/client/params/AllClientPNames.html#AllClientPNames",
            "title":"AllClientPNames 0 Elements,  -  Coverage"},"children":[]},
        {"id":"AuthPolicy811","name":"AuthPolicy","data":{"$area":2.0,
            "$color":0.0,"path":
            "org/apache/http/client/params/AuthPolicy.html#AuthPolicy",
            "title":"AuthPolicy 2 Elements, 0% Coverage"},"children":[]},{
          "id":"ClientPNames813","name":"ClientPNames","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/client/params/ClientPNames.html#ClientPNames",
            "title":"ClientPNames 0 Elements,  -  Coverage"},"children":[]},{
          "id":"ClientParamBean813","name":"ClientParamBean","data":{"$area":
            24.0,"$color":0.0,"path":
            "org/apache/http/client/params/ClientParamBean.html#ClientParamBean",
            "title":"ClientParamBean 24 Elements, 0% Coverage"},"children":[]},
        {"id":"CookiePolicy837","name":"CookiePolicy","data":{"$area":2.0,
            "$color":0.0,"path":
            "org/apache/http/client/params/CookiePolicy.html#CookiePolicy",
            "title":"CookiePolicy 2 Elements, 0% Coverage"},"children":[]},{
          "id":"HttpClientParams839","name":"HttpClientParams","data":{
            "$area":60.0,"$color":40.0,"path":
            "org/apache/http/client/params/HttpClientParams.html#HttpClientParams",
            "title":"HttpClientParams 60 Elements, 40% Coverage"},"children":
          []}]},{"id":"org.apache.http.impl.client5628","name":
      "org.apache.http.impl.client","data":{"$area":2524.0,"$color":
        58.28051,"title":
        "org.apache.http.impl.client 2524 Elements, 58.3% Coverage"},
      "children":[{"id":"AIMDBackoffManager5628","name":
          "AIMDBackoffManager","data":{"$area":64.0,"$color":87.5,"path":
            "org/apache/http/impl/client/AIMDBackoffManager.html#AIMDBackoffManager",
            "title":"AIMDBackoffManager 64 Elements, 87.5% Coverage"},
          "children":[]},{"id":"AbstractAuthenticationHandler5692","name":
          "AbstractAuthenticationHandler","data":{"$area":78.0,"$color":
            0.0,"path":
            "org/apache/http/impl/client/AbstractAuthenticationHandler.html#AbstractAuthenticationHandler",
            "title":
            "AbstractAuthenticationHandler 78 Elements, 0% Coverage"},
          "children":[]},{"id":"AbstractHttpClient5770","name":
          "AbstractHttpClient","data":{"$area":388.0,"$color":61.340206,
            "path":
            "org/apache/http/impl/client/AbstractHttpClient.html#AbstractHttpClient",
            "title":"AbstractHttpClient 388 Elements, 61.3% Coverage"},
          "children":[]},{"id":"AuthenticationStrategyAdaptor6158","name":
          "AuthenticationStrategyAdaptor","data":{"$area":84.0,"$color":
            0.0,"path":
            "org/apache/http/impl/client/AuthenticationStrategyAdaptor.html#AuthenticationStrategyAdaptor",
            "title":
            "AuthenticationStrategyAdaptor 84 Elements, 0% Coverage"},
          "children":[]},{"id":"AuthenticationStrategyImpl6242","name":
          "AuthenticationStrategyImpl","data":{"$area":162.0,"$color":
            91.358025,"path":
            "org/apache/http/impl/client/AuthenticationStrategyImpl.html#AuthenticationStrategyImpl",
            "title":
            "AuthenticationStrategyImpl 162 Elements, 91.4% Coverage"},
          "children":[]},{"id":"AutoRetryHttpClient6404","name":
          "AutoRetryHttpClient","data":{"$area":60.0,"$color":40.0,"path":
            "org/apache/http/impl/client/AutoRetryHttpClient.html#AutoRetryHttpClient",
            "title":"AutoRetryHttpClient 60 Elements, 40% Coverage"},
          "children":[]},{"id":"BasicAuthCache6464","name":
          "BasicAuthCache","data":{"$area":34.0,"$color":73.52941,"path":
            "org/apache/http/impl/client/BasicAuthCache.html#BasicAuthCache",
            "title":"BasicAuthCache 34 Elements, 73.5% Coverage"},"children":
          []},{"id":"BasicCookieStore6498","name":"BasicCookieStore","data":{
            "$area":39.0,"$color":51.282055,"path":
            "org/apache/http/impl/client/BasicCookieStore.html#BasicCookieStore",
            "title":"BasicCookieStore 39 Elements, 51.3% Coverage"},
          "children":[]},{"id":"BasicCredentialsProvider6537","name":
          "BasicCredentialsProvider","data":{"$area":38.0,"$color":
            84.210526,"path":
            "org/apache/http/impl/client/BasicCredentialsProvider.html#BasicCredentialsProvider",
            "title":"BasicCredentialsProvider 38 Elements, 84.2% Coverage"},
          "children":[]},{"id":"BasicResponseHandler6575","name":
          "BasicResponseHandler","data":{"$area":11.0,"$color":90.909096,
            "path":
            "org/apache/http/impl/client/BasicResponseHandler.html#BasicResponseHandler",
            "title":"BasicResponseHandler 11 Elements, 90.9% Coverage"},
          "children":[]},{"id":"ClientParamsStack6586","name":
          "ClientParamsStack","data":{"$area":54.0,"$color":38.88889,"path":
            "org/apache/http/impl/client/ClientParamsStack.html#ClientParamsStack",
            "title":"ClientParamsStack 54 Elements, 38.9% Coverage"},
          "children":[]},{"id":"Clock6640","name":"Clock","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/impl/client/Clock.html#Clock","title":
            "Clock 0 Elements,  -  Coverage"},"children":[]},{"id":
          "ContentEncodingHttpClient6640","name":
          "ContentEncodingHttpClient","data":{"$area":11.0,"$color":0.0,
            "path":
            "org/apache/http/impl/client/ContentEncodingHttpClient.html#ContentEncodingHttpClient",
            "title":"ContentEncodingHttpClient 11 Elements, 0% Coverage"},
          "children":[]},{"id":"DecompressingHttpClient6651","name":
          "DecompressingHttpClient","data":{"$area":64.0,"$color":89.0625,
            "path":
            "org/apache/http/impl/client/DecompressingHttpClient.html#DecompressingHttpClient",
            "title":"DecompressingHttpClient 64 Elements, 89.1% Coverage"},
          "children":[]},{"id":"DefaultBackoffStrategy6715","name":
          "DefaultBackoffStrategy","data":{"$area":4.0,"$color":100.0,"path":
            
            "org/apache/http/impl/client/DefaultBackoffStrategy.html#DefaultBackoffStrategy",
            "title":"DefaultBackoffStrategy 4 Elements, 100% Coverage"},
          "children":[]},{"id":"DefaultConnectionKeepAliveStrategy6719",
          "name":"DefaultConnectionKeepAliveStrategy","data":{"$area":18.0,
            "$color":100.0,"path":
            "org/apache/http/impl/client/DefaultConnectionKeepAliveStrategy.html#DefaultConnectionKeepAliveStrategy",
            "title":
            "DefaultConnectionKeepAliveStrategy 18 Elements, 100% Coverage"},
          "children":[]},{"id":"DefaultHttpClient6737","name":
          "DefaultHttpClient","data":{"$area":36.0,"$color":91.66667,"path":
            "org/apache/http/impl/client/DefaultHttpClient.html#DefaultHttpClient",
            "title":"DefaultHttpClient 36 Elements, 91.7% Coverage"},
          "children":[]},{"id":"DefaultHttpRequestRetryHandler6773","name":
          "DefaultHttpRequestRetryHandler","data":{"$area":64.0,"$color":
            75.0,"path":
            "org/apache/http/impl/client/DefaultHttpRequestRetryHandler.html#DefaultHttpRequestRetryHandler",
            "title":
            "DefaultHttpRequestRetryHandler 64 Elements, 75% Coverage"},
          "children":[]},{"id":"DefaultProxyAuthenticationHandler6837",
          "name":"DefaultProxyAuthenticationHandler","data":{"$area":23.0,
            "$color":0.0,"path":
            "org/apache/http/impl/client/DefaultProxyAuthenticationHandler.html#DefaultProxyAuthenticationHandler",
            "title":
            "DefaultProxyAuthenticationHandler 23 Elements, 0% Coverage"},
          "children":[]},{"id":"DefaultRedirectHandler6860","name":
          "DefaultRedirectHandler","data":{"$area":81.0,"$color":0.0,"path":
            "org/apache/http/impl/client/DefaultRedirectHandler.html#DefaultRedirectHandler",
            "title":"DefaultRedirectHandler 81 Elements, 0% Coverage"},
          "children":[]},{"id":"DefaultRedirectStrategy6941","name":
          "DefaultRedirectStrategy","data":{"$area":139.0,"$color":
            87.76978,"path":
            "org/apache/http/impl/client/DefaultRedirectStrategy.html#DefaultRedirectStrategy",
            "title":"DefaultRedirectStrategy 139 Elements, 87.8% Coverage"},
          "children":[]},{"id":"DefaultRedirectStrategyAdaptor7080","name":
          "DefaultRedirectStrategyAdaptor","data":{"$area":15.0,"$color":
            0.0,"path":
            "org/apache/http/impl/client/DefaultRedirectStrategyAdaptor.html#DefaultRedirectStrategyAdaptor",
            "title":
            "DefaultRedirectStrategyAdaptor 15 Elements, 0% Coverage"},
          "children":[]},{"id":"DefaultRequestDirector7095","name":
          "DefaultRequestDirector","data":{"$area":588.0,"$color":64.28571,
            "path":
            "org/apache/http/impl/client/DefaultRequestDirector.html#DefaultRequestDirector",
            "title":"DefaultRequestDirector 588 Elements, 64.3% Coverage"},
          "children":[]},{"id":
          "DefaultServiceUnavailableRetryStrategy7683","name":
          "DefaultServiceUnavailableRetryStrategy","data":{"$area":18.0,
            "$color":77.77778,"path":
            "org/apache/http/impl/client/DefaultServiceUnavailableRetryStrategy.html#DefaultServiceUnavailableRetryStrategy",
            "title":
            "DefaultServiceUnavailableRetryStrategy 18 Elements, 77.8% Coverage"},
          "children":[]},{"id":"DefaultTargetAuthenticationHandler7701",
          "name":"DefaultTargetAuthenticationHandler","data":{"$area":23.0,
            "$color":0.0,"path":
            "org/apache/http/impl/client/DefaultTargetAuthenticationHandler.html#DefaultTargetAuthenticationHandler",
            "title":
            "DefaultTargetAuthenticationHandler 23 Elements, 0% Coverage"},
          "children":[]},{"id":"DefaultUserTokenHandler7724","name":
          "DefaultUserTokenHandler","data":{"$area":36.0,"$color":75.0,
            "path":
            "org/apache/http/impl/client/DefaultUserTokenHandler.html#DefaultUserTokenHandler",
            "title":"DefaultUserTokenHandler 36 Elements, 75% Coverage"},
          "children":[]},{"id":"EntityEnclosingRequestWrapper7760","name":
          "EntityEnclosingRequestWrapper","data":{"$area":15.0,"$color":
            93.333336,"path":
            "org/apache/http/impl/client/EntityEnclosingRequestWrapper.html#EntityEnclosingRequestWrapper",
            "title":
            "EntityEnclosingRequestWrapper 15 Elements, 93.3% Coverage"},
          "children":[]},{"id":
          "EntityEnclosingRequestWrapper.EntityWrapper7775","name":
          "EntityEnclosingRequestWrapper.EntityWrapper","data":{"$area":
            11.0,"$color":45.454548,"path":
            "org/apache/http/impl/client/EntityEnclosingRequestWrapper.html#EntityEnclosingRequestWrapper.EntityWrapper",
            "title":
            "EntityEnclosingRequestWrapper.EntityWrapper 11 Elements, 45.5% Coverage"},
          "children":[]},{"id":"HttpAuthenticator7786","name":
          "HttpAuthenticator","data":{"$area":94.0,"$color":86.17021,"path":
            "org/apache/http/impl/client/HttpAuthenticator.html#HttpAuthenticator",
            "title":"HttpAuthenticator 94 Elements, 86.2% Coverage"},
          "children":[]},{"id":"LaxRedirectStrategy7880","name":
          "LaxRedirectStrategy","data":{"$area":7.0,"$color":0.0,"path":
            "org/apache/http/impl/client/LaxRedirectStrategy.html#LaxRedirectStrategy",
            "title":"LaxRedirectStrategy 7 Elements, 0% Coverage"},
          "children":[]},{"id":"NullBackoffStrategy7887","name":
          "NullBackoffStrategy","data":{"$area":4.0,"$color":100.0,"path":
            "org/apache/http/impl/client/NullBackoffStrategy.html#NullBackoffStrategy",
            "title":"NullBackoffStrategy 4 Elements, 100% Coverage"},
          "children":[]},{"id":"ProxyAuthenticationStrategy7891","name":
          "ProxyAuthenticationStrategy","data":{"$area":2.0,"$color":100.0,
            "path":
            "org/apache/http/impl/client/ProxyAuthenticationStrategy.html#ProxyAuthenticationStrategy",
            "title":
            "ProxyAuthenticationStrategy 2 Elements, 100% Coverage"},
          "children":[]},{"id":"ProxyClient7893","name":"ProxyClient","data":
          {"$area":96.0,"$color":0.0,"path":
            "org/apache/http/impl/client/ProxyClient.html#ProxyClient",
            "title":"ProxyClient 96 Elements, 0% Coverage"},"children":[]},{
          "id":"ProxyClient.ProxyConnection7989","name":
          "ProxyClient.ProxyConnection","data":{"$area":11.0,"$color":0.0,
            "path":
            "org/apache/http/impl/client/ProxyClient.html#ProxyClient.ProxyConnection",
            "title":"ProxyClient.ProxyConnection 11 Elements, 0% Coverage"},
          "children":[]},{"id":"RedirectLocations8000","name":
          "RedirectLocations","data":{"$area":26.0,"$color":100.0,"path":
            "org/apache/http/impl/client/RedirectLocations.html#RedirectLocations",
            "title":"RedirectLocations 26 Elements, 100% Coverage"},
          "children":[]},{"id":"RequestWrapper8026","name":
          "RequestWrapper","data":{"$area":70.0,"$color":77.14286,"path":
            "org/apache/http/impl/client/RequestWrapper.html#RequestWrapper",
            "title":"RequestWrapper 70 Elements, 77.1% Coverage"},"children":
          []},{"id":"RoutedRequest8096","name":"RoutedRequest","data":{
            "$area":8.0,"$color":100.0,"path":
            "org/apache/http/impl/client/RoutedRequest.html#RoutedRequest",
            "title":"RoutedRequest 8 Elements, 100% Coverage"},"children":[]},
        {"id":"StandardHttpRequestRetryHandler8104","name":
          "StandardHttpRequestRetryHandler","data":{"$area":15.0,"$color":
            0.0,"path":
            "org/apache/http/impl/client/StandardHttpRequestRetryHandler.html#StandardHttpRequestRetryHandler",
            "title":
            "StandardHttpRequestRetryHandler 15 Elements, 0% Coverage"},
          "children":[]},{"id":"SystemClock8119","name":"SystemClock","data":
          {"$area":2.0,"$color":0.0,"path":
            "org/apache/http/impl/client/SystemClock.html#SystemClock",
            "title":"SystemClock 2 Elements, 0% Coverage"},"children":[]},{
          "id":"SystemDefaultHttpClient8121","name":
          "SystemDefaultHttpClient","data":{"$area":24.0,"$color":0.0,"path":
            
            "org/apache/http/impl/client/SystemDefaultHttpClient.html#SystemDefaultHttpClient",
            "title":"SystemDefaultHttpClient 24 Elements, 0% Coverage"},
          "children":[]},{"id":"TargetAuthenticationStrategy8145","name":
          "TargetAuthenticationStrategy","data":{"$area":2.0,"$color":
            100.0,"path":
            "org/apache/http/impl/client/TargetAuthenticationStrategy.html#TargetAuthenticationStrategy",
            "title":
            "TargetAuthenticationStrategy 2 Elements, 100% Coverage"},
          "children":[]},{"id":"TunnelRefusedException8147","name":
          "TunnelRefusedException","data":{"$area":5.0,"$color":0.0,"path":
            "org/apache/http/impl/client/TunnelRefusedException.html#TunnelRefusedException",
            "title":"TunnelRefusedException 5 Elements, 0% Coverage"},
          "children":[]}]},{"id":"org.apache.http.auth.params468","name":
      "org.apache.http.auth.params","data":{"$area":23.0,"$color":
        34.782608,"title":
        "org.apache.http.auth.params 23 Elements, 34.8% Coverage"},
      "children":[{"id":"AuthPNames468","name":"AuthPNames","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/auth/params/AuthPNames.html#AuthPNames","title":
            "AuthPNames 0 Elements,  -  Coverage"},"children":[]},{"id":
          "AuthParamBean468","name":"AuthParamBean","data":{"$area":4.0,
            "$color":0.0,"path":
            "org/apache/http/auth/params/AuthParamBean.html#AuthParamBean",
            "title":"AuthParamBean 4 Elements, 0% Coverage"},"children":[]},{
          "id":"AuthParams472","name":"AuthParams","data":{"$area":19.0,
            "$color":42.105263,"path":
            "org/apache/http/auth/params/AuthParams.html#AuthParams","title":
            "AuthParams 19 Elements, 42.1% Coverage"},"children":[]}]},{"id":
      "org.apache.http.conn2281","name":"org.apache.http.conn","data":{
        "$area":298.0,"$color":49.328857,"title":
        "org.apache.http.conn 298 Elements, 49.3% Coverage"},"children":[{
          "id":"BasicEofSensorWatcher2281","name":"BasicEofSensorWatcher",
          "data":{"$area":28.0,"$color":0.0,"path":
            "org/apache/http/conn/BasicEofSensorWatcher.html#BasicEofSensorWatcher",
            "title":"BasicEofSensorWatcher 28 Elements, 0% Coverage"},
          "children":[]},{"id":"BasicManagedEntity2309","name":
          "BasicManagedEntity","data":{"$area":75.0,"$color":68.0,"path":
            "org/apache/http/conn/BasicManagedEntity.html#BasicManagedEntity",
            "title":"BasicManagedEntity 75 Elements, 68% Coverage"},
          "children":[]},{"id":"ClientConnectionManager2384","name":
          "ClientConnectionManager","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/conn/ClientConnectionManager.html#ClientConnectionManager",
            "title":"ClientConnectionManager 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ClientConnectionManagerFactory2384","name":
          "ClientConnectionManagerFactory","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/conn/ClientConnectionManagerFactory.html#ClientConnectionManagerFactory",
            "title":
            "ClientConnectionManagerFactory 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ClientConnectionOperator2384","name":
          "ClientConnectionOperator","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/conn/ClientConnectionOperator.html#ClientConnectionOperator",
            "title":"ClientConnectionOperator 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ClientConnectionRequest2384","name":
          "ClientConnectionRequest","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/conn/ClientConnectionRequest.html#ClientConnectionRequest",
            "title":"ClientConnectionRequest 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ConnectTimeoutException2384","name":
          "ConnectTimeoutException","data":{"$area":4.0,"$color":100.0,
            "path":
            "org/apache/http/conn/ConnectTimeoutException.html#ConnectTimeoutException",
            "title":"ConnectTimeoutException 4 Elements, 100% Coverage"},
          "children":[]},{"id":"ConnectionKeepAliveStrategy2388","name":
          "ConnectionKeepAliveStrategy","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/conn/ConnectionKeepAliveStrategy.html#ConnectionKeepAliveStrategy",
            "title":"ConnectionKeepAliveStrategy 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ConnectionPoolTimeoutException2388","name":
          "ConnectionPoolTimeoutException","data":{"$area":4.0,"$color":
            100.0,"path":
            "org/apache/http/conn/ConnectionPoolTimeoutException.html#ConnectionPoolTimeoutException",
            "title":
            "ConnectionPoolTimeoutException 4 Elements, 100% Coverage"},
          "children":[]},{"id":"ConnectionReleaseTrigger2392","name":
          "ConnectionReleaseTrigger","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/conn/ConnectionReleaseTrigger.html#ConnectionReleaseTrigger",
            "title":"ConnectionReleaseTrigger 0 Elements,  -  Coverage"},
          "children":[]},{"id":"DnsResolver2392","name":"DnsResolver","data":
          {"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/conn/DnsResolver.html#DnsResolver","title":
            "DnsResolver 0 Elements,  -  Coverage"},"children":[]},{"id":
          "EofSensorInputStream2392","name":"EofSensorInputStream","data":{
            "$area":110.0,"$color":73.63637,"path":
            "org/apache/http/conn/EofSensorInputStream.html#EofSensorInputStream",
            "title":"EofSensorInputStream 110 Elements, 73.6% Coverage"},
          "children":[]},{"id":"EofSensorWatcher2502","name":
          "EofSensorWatcher","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/conn/EofSensorWatcher.html#EofSensorWatcher",
            "title":"EofSensorWatcher 0 Elements,  -  Coverage"},"children":[]},
        {"id":"HttpHostConnectException2502","name":
          "HttpHostConnectException","data":{"$area":6.0,"$color":0.0,"path":
            
            "org/apache/http/conn/HttpHostConnectException.html#HttpHostConnectException",
            "title":"HttpHostConnectException 6 Elements, 0% Coverage"},
          "children":[]},{"id":"HttpInetSocketAddress2508","name":
          "HttpInetSocketAddress","data":{"$area":11.0,"$color":63.636364,
            "path":
            "org/apache/http/conn/HttpInetSocketAddress.html#HttpInetSocketAddress",
            "title":"HttpInetSocketAddress 11 Elements, 63.6% Coverage"},
          "children":[]},{"id":"HttpRoutedConnection2519","name":
          "HttpRoutedConnection","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/conn/HttpRoutedConnection.html#HttpRoutedConnection",
            "title":"HttpRoutedConnection 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ManagedClientConnection2519","name":
          "ManagedClientConnection","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/conn/ManagedClientConnection.html#ManagedClientConnection",
            "title":"ManagedClientConnection 0 Elements,  -  Coverage"},
          "children":[]},{"id":"MultihomePlainSocketFactory2519","name":
          "MultihomePlainSocketFactory","data":{"$area":60.0,"$color":0.0,
            "path":
            "org/apache/http/conn/MultihomePlainSocketFactory.html#MultihomePlainSocketFactory",
            "title":"MultihomePlainSocketFactory 60 Elements, 0% Coverage"},
          "children":[]},{"id":"OperatedClientConnection2579","name":
          "OperatedClientConnection","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/conn/OperatedClientConnection.html#OperatedClientConnection",
            "title":"OperatedClientConnection 0 Elements,  -  Coverage"},
          "children":[]}]},{"id":"org.apache.http.cookie3971","name":
      "org.apache.http.cookie","data":{"$area":164.0,"$color":86.585365,
        "title":"org.apache.http.cookie 164 Elements, 86.6% Coverage"},
      "children":[{"id":"ClientCookie3971","name":"ClientCookie","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/cookie/ClientCookie.html#ClientCookie","title":
            "ClientCookie 0 Elements,  -  Coverage"},"children":[]},{"id":
          "Cookie3971","name":"Cookie","data":{"$area":0.0,"$color":-100.0,
            "path":"org/apache/http/cookie/Cookie.html#Cookie","title":
            "Cookie 0 Elements,  -  Coverage"},"children":[]},{"id":
          "CookieAttributeHandler3971","name":"CookieAttributeHandler",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/cookie/CookieAttributeHandler.html#CookieAttributeHandler",
            "title":"CookieAttributeHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"CookieIdentityComparator3971","name":
          "CookieIdentityComparator","data":{"$area":39.0,"$color":100.0,
            "path":
            "org/apache/http/cookie/CookieIdentityComparator.html#CookieIdentityComparator",
            "title":"CookieIdentityComparator 39 Elements, 100% Coverage"},
          "children":[]},{"id":"CookieOrigin4010","name":"CookieOrigin",
          "data":{"$area":47.0,"$color":72.34042,"path":
            "org/apache/http/cookie/CookieOrigin.html#CookieOrigin","title":
            "CookieOrigin 47 Elements, 72.3% Coverage"},"children":[]},{"id":
          "CookiePathComparator4057","name":"CookiePathComparator","data":{
            "$area":27.0,"$color":100.0,"path":
            "org/apache/http/cookie/CookiePathComparator.html#CookiePathComparator",
            "title":"CookiePathComparator 27 Elements, 100% Coverage"},
          "children":[]},{"id":"CookieRestrictionViolationException4084",
          "name":"CookieRestrictionViolationException","data":{"$area":4.0,
            "$color":50.0,"path":
            "org/apache/http/cookie/CookieRestrictionViolationException.html#CookieRestrictionViolationException",
            "title":
            "CookieRestrictionViolationException 4 Elements, 50% Coverage"},
          "children":[]},{"id":"CookieSpec4088","name":"CookieSpec","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/cookie/CookieSpec.html#CookieSpec","title":
            "CookieSpec 0 Elements,  -  Coverage"},"children":[]},{"id":
          "CookieSpecFactory4088","name":"CookieSpecFactory","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/cookie/CookieSpecFactory.html#CookieSpecFactory",
            "title":"CookieSpecFactory 0 Elements,  -  Coverage"},"children":
          []},{"id":"CookieSpecRegistry4088","name":"CookieSpecRegistry",
          "data":{"$area":41.0,"$color":82.92683,"path":
            "org/apache/http/cookie/CookieSpecRegistry.html#CookieSpecRegistry",
            "title":"CookieSpecRegistry 41 Elements, 82.9% Coverage"},
          "children":[]},{"id":"MalformedCookieException4129","name":
          "MalformedCookieException","data":{"$area":6.0,"$color":100.0,
            "path":
            "org/apache/http/cookie/MalformedCookieException.html#MalformedCookieException",
            "title":"MalformedCookieException 6 Elements, 100% Coverage"},
          "children":[]},{"id":"SM4135","name":"SM","data":{"$area":0.0,
            "$color":-100.0,"path":"org/apache/http/cookie/SM.html#SM",
            "title":"SM 0 Elements,  -  Coverage"},"children":[]},{"id":
          "SetCookie4135","name":"SetCookie","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/cookie/SetCookie.html#SetCookie","title":
            "SetCookie 0 Elements,  -  Coverage"},"children":[]},{"id":
          "SetCookie24135","name":"SetCookie2","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/cookie/SetCookie2.html#SetCookie2","title":
            "SetCookie2 0 Elements,  -  Coverage"},"children":[]}]},{"id":
      "org.apache.http.conn.util3962","name":"org.apache.http.conn.util",
      "data":{"$area":9.0,"$color":88.88889,"title":
        "org.apache.http.conn.util 9 Elements, 88.9% Coverage"},"children":[{
          "id":"InetAddressUtils3962","name":"InetAddressUtils","data":{
            "$area":9.0,"$color":88.88889,"path":
            "org/apache/http/conn/util/InetAddressUtils.html#InetAddressUtils",
            "title":"InetAddressUtils 9 Elements, 88.9% Coverage"},
          "children":[]}]},{"id":"org.apache.http.impl.cookie10882","name":
      "org.apache.http.impl.cookie","data":{"$area":1702.0,"$color":
        87.89659,"title":
        "org.apache.http.impl.cookie 1702 Elements, 87.9% Coverage"},
      "children":[{"id":"AbstractCookieAttributeHandler10882","name":
          "AbstractCookieAttributeHandler","data":{"$area":3.0,"$color":
            100.0,"path":
            "org/apache/http/impl/cookie/AbstractCookieAttributeHandler.html#AbstractCookieAttributeHandler",
            "title":
            "AbstractCookieAttributeHandler 3 Elements, 100% Coverage"},
          "children":[]},{"id":"AbstractCookieSpec10885","name":
          "AbstractCookieSpec","data":{"$area":24.0,"$color":100.0,"path":
            "org/apache/http/impl/cookie/AbstractCookieSpec.html#AbstractCookieSpec",
            "title":"AbstractCookieSpec 24 Elements, 100% Coverage"},
          "children":[]},{"id":"BasicClientCookie10909","name":
          "BasicClientCookie","data":{"$area":86.0,"$color":66.27907,"path":
            "org/apache/http/impl/cookie/BasicClientCookie.html#BasicClientCookie",
            "title":"BasicClientCookie 86 Elements, 66.3% Coverage"},
          "children":[]},{"id":"BasicClientCookie210995","name":
          "BasicClientCookie2","data":{"$area":23.0,"$color":100.0,"path":
            "org/apache/http/impl/cookie/BasicClientCookie2.html#BasicClientCookie2",
            "title":"BasicClientCookie2 23 Elements, 100% Coverage"},
          "children":[]},{"id":"BasicCommentHandler11018","name":
          "BasicCommentHandler","data":{"$area":8.0,"$color":100.0,"path":
            "org/apache/http/impl/cookie/BasicCommentHandler.html#BasicCommentHandler",
            "title":"BasicCommentHandler 8 Elements, 100% Coverage"},
          "children":[]},{"id":"BasicDomainHandler11026","name":
          "BasicDomainHandler","data":{"$area":73.0,"$color":100.0,"path":
            "org/apache/http/impl/cookie/BasicDomainHandler.html#BasicDomainHandler",
            "title":"BasicDomainHandler 73 Elements, 100% Coverage"},
          "children":[]},{"id":"BasicExpiresHandler11099","name":
          "BasicExpiresHandler","data":{"$area":18.0,"$color":100.0,"path":
            "org/apache/http/impl/cookie/BasicExpiresHandler.html#BasicExpiresHandler",
            "title":"BasicExpiresHandler 18 Elements, 100% Coverage"},
          "children":[]},{"id":"BasicMaxAgeHandler11117","name":
          "BasicMaxAgeHandler","data":{"$area":20.0,"$color":100.0,"path":
            "org/apache/http/impl/cookie/BasicMaxAgeHandler.html#BasicMaxAgeHandler",
            "title":"BasicMaxAgeHandler 20 Elements, 100% Coverage"},
          "children":[]},{"id":"BasicPathHandler11137","name":
          "BasicPathHandler","data":{"$area":45.0,"$color":100.0,"path":
            "org/apache/http/impl/cookie/BasicPathHandler.html#BasicPathHandler",
            "title":"BasicPathHandler 45 Elements, 100% Coverage"},
          "children":[]},{"id":"BasicSecureHandler11182","name":
          "BasicSecureHandler","data":{"$area":18.0,"$color":100.0,"path":
            "org/apache/http/impl/cookie/BasicSecureHandler.html#BasicSecureHandler",
            "title":"BasicSecureHandler 18 Elements, 100% Coverage"},
          "children":[]},{"id":"BestMatchSpec11200","name":"BestMatchSpec",
          "data":{"$area":140.0,"$color":90.71429,"path":
            "org/apache/http/impl/cookie/BestMatchSpec.html#BestMatchSpec",
            "title":"BestMatchSpec 140 Elements, 90.7% Coverage"},"children":
          []},{"id":"BestMatchSpecFactory11340","name":
          "BestMatchSpecFactory","data":{"$area":14.0,"$color":64.28571,
            "path":
            "org/apache/http/impl/cookie/BestMatchSpecFactory.html#BestMatchSpecFactory",
            "title":"BestMatchSpecFactory 14 Elements, 64.3% Coverage"},
          "children":[]},{"id":"BrowserCompatSpec11354","name":
          "BrowserCompatSpec","data":{"$area":103.0,"$color":89.32039,"path":
            
            "org/apache/http/impl/cookie/BrowserCompatSpec.html#BrowserCompatSpec",
            "title":"BrowserCompatSpec 103 Elements, 89.3% Coverage"},
          "children":[]},{"id":"BrowserCompatSpecFactory11457","name":
          "BrowserCompatSpecFactory","data":{"$area":13.0,"$color":
            76.92308,"path":
            "org/apache/http/impl/cookie/BrowserCompatSpecFactory.html#BrowserCompatSpecFactory",
            "title":"BrowserCompatSpecFactory 13 Elements, 76.9% Coverage"},
          "children":[]},{"id":"BrowserCompatVersionAttributeHandler11470",
          "name":"BrowserCompatVersionAttributeHandler","data":{"$area":
            15.0,"$color":73.333336,"path":
            "org/apache/http/impl/cookie/BrowserCompatVersionAttributeHandler.html#BrowserCompatVersionAttributeHandler",
            "title":
            "BrowserCompatVersionAttributeHandler 15 Elements, 73.3% Coverage"},
          "children":[]},{"id":"CookieSpecBase11485","name":
          "CookieSpecBase","data":{"$area":66.0,"$color":98.48485,"path":
            "org/apache/http/impl/cookie/CookieSpecBase.html#CookieSpecBase",
            "title":"CookieSpecBase 66 Elements, 98.5% Coverage"},"children":
          []},{"id":"DateParseException11551","name":"DateParseException",
          "data":{"$area":4.0,"$color":100.0,"path":
            "org/apache/http/impl/cookie/DateParseException.html#DateParseException",
            "title":"DateParseException 4 Elements, 100% Coverage"},
          "children":[]},{"id":"DateUtils11555","name":"DateUtils","data":{
            "$area":53.0,"$color":94.33962,"path":
            "org/apache/http/impl/cookie/DateUtils.html#DateUtils","title":
            "DateUtils 53 Elements, 94.3% Coverage"},"children":[]},{"id":
          "DateUtils.DateFormatHolder11608","name":
          "DateUtils.DateFormatHolder","data":{"$area":20.0,"$color":75.0,
            "path":
            "org/apache/http/impl/cookie/DateUtils.html#DateUtils.DateFormatHolder",
            "title":"DateUtils.DateFormatHolder 20 Elements, 75% Coverage"},
          "children":[]},{"id":"IgnoreSpec11628","name":"IgnoreSpec","data":{
            "$area":8.0,"$color":0.0,"path":
            "org/apache/http/impl/cookie/IgnoreSpec.html#IgnoreSpec","title":
            "IgnoreSpec 8 Elements, 0% Coverage"},"children":[]},{"id":
          "IgnoreSpecFactory11636","name":"IgnoreSpecFactory","data":{
            "$area":2.0,"$color":0.0,"path":
            "org/apache/http/impl/cookie/IgnoreSpecFactory.html#IgnoreSpecFactory",
            "title":"IgnoreSpecFactory 2 Elements, 0% Coverage"},"children":[]},
        {"id":"NetscapeDomainHandler11638","name":"NetscapeDomainHandler",
          "data":{"$area":40.0,"$color":100.0,"path":
            "org/apache/http/impl/cookie/NetscapeDomainHandler.html#NetscapeDomainHandler",
            "title":"NetscapeDomainHandler 40 Elements, 100% Coverage"},
          "children":[]},{"id":"NetscapeDraftHeaderParser11678","name":
          "NetscapeDraftHeaderParser","data":{"$area":79.0,"$color":
            94.936714,"path":
            "org/apache/http/impl/cookie/NetscapeDraftHeaderParser.html#NetscapeDraftHeaderParser",
            "title":
            "NetscapeDraftHeaderParser 79 Elements, 94.9% Coverage"},
          "children":[]},{"id":"NetscapeDraftSpec11757","name":
          "NetscapeDraftSpec","data":{"$area":81.0,"$color":81.48148,"path":
            "org/apache/http/impl/cookie/NetscapeDraftSpec.html#NetscapeDraftSpec",
            "title":"NetscapeDraftSpec 81 Elements, 81.5% Coverage"},
          "children":[]},{"id":"NetscapeDraftSpecFactory11838","name":
          "NetscapeDraftSpecFactory","data":{"$area":13.0,"$color":
            76.92308,"path":
            "org/apache/http/impl/cookie/NetscapeDraftSpecFactory.html#NetscapeDraftSpecFactory",
            "title":"NetscapeDraftSpecFactory 13 Elements, 76.9% Coverage"},
          "children":[]},{"id":"PublicSuffixFilter11851","name":
          "PublicSuffixFilter","data":{"$area":52.0,"$color":82.69231,"path":
            
            "org/apache/http/impl/cookie/PublicSuffixFilter.html#PublicSuffixFilter",
            "title":"PublicSuffixFilter 52 Elements, 82.7% Coverage"},
          "children":[]},{"id":"PublicSuffixListParser11903","name":
          "PublicSuffixListParser","data":{"$area":62.0,"$color":90.32258,
            "path":
            "org/apache/http/impl/cookie/PublicSuffixListParser.html#PublicSuffixListParser",
            "title":"PublicSuffixListParser 62 Elements, 90.3% Coverage"},
          "children":[]},{"id":"RFC2109DomainHandler11965","name":
          "RFC2109DomainHandler","data":{"$area":74.0,"$color":100.0,"path":
            "org/apache/http/impl/cookie/RFC2109DomainHandler.html#RFC2109DomainHandler",
            "title":"RFC2109DomainHandler 74 Elements, 100% Coverage"},
          "children":[]},{"id":"RFC2109Spec12039","name":"RFC2109Spec",
          "data":{"$area":134.0,"$color":93.283585,"path":
            "org/apache/http/impl/cookie/RFC2109Spec.html#RFC2109Spec",
            "title":"RFC2109Spec 134 Elements, 93.3% Coverage"},"children":[]},
        {"id":"RFC2109SpecFactory12173","name":"RFC2109SpecFactory","data":{
            "$area":14.0,"$color":78.57143,"path":
            "org/apache/http/impl/cookie/RFC2109SpecFactory.html#RFC2109SpecFactory",
            "title":"RFC2109SpecFactory 14 Elements, 78.6% Coverage"},
          "children":[]},{"id":"RFC2109VersionHandler12187","name":
          "RFC2109VersionHandler","data":{"$area":27.0,"$color":100.0,"path":
            
            "org/apache/http/impl/cookie/RFC2109VersionHandler.html#RFC2109VersionHandler",
            "title":"RFC2109VersionHandler 27 Elements, 100% Coverage"},
          "children":[]},{"id":"RFC2965CommentUrlAttributeHandler12214",
          "name":"RFC2965CommentUrlAttributeHandler","data":{"$area":11.0,
            "$color":90.909096,"path":
            "org/apache/http/impl/cookie/RFC2965CommentUrlAttributeHandler.html#RFC2965CommentUrlAttributeHandler",
            "title":
            "RFC2965CommentUrlAttributeHandler 11 Elements, 90.9% Coverage"},
          "children":[]},{"id":"RFC2965DiscardAttributeHandler12225","name":
          "RFC2965DiscardAttributeHandler","data":{"$area":11.0,"$color":
            90.909096,"path":
            "org/apache/http/impl/cookie/RFC2965DiscardAttributeHandler.html#RFC2965DiscardAttributeHandler",
            "title":
            "RFC2965DiscardAttributeHandler 11 Elements, 90.9% Coverage"},
          "children":[]},{"id":"RFC2965DomainAttributeHandler12236","name":
          "RFC2965DomainAttributeHandler","data":{"$area":81.0,"$color":
            77.77778,"path":
            "org/apache/http/impl/cookie/RFC2965DomainAttributeHandler.html#RFC2965DomainAttributeHandler",
            "title":
            "RFC2965DomainAttributeHandler 81 Elements, 77.8% Coverage"},
          "children":[]},{"id":"RFC2965PortAttributeHandler12317","name":
          "RFC2965PortAttributeHandler","data":{"$area":82.0,"$color":
            84.14634,"path":
            "org/apache/http/impl/cookie/RFC2965PortAttributeHandler.html#RFC2965PortAttributeHandler",
            "title":
            "RFC2965PortAttributeHandler 82 Elements, 84.1% Coverage"},
          "children":[]},{"id":"RFC2965Spec12399","name":"RFC2965Spec",
          "data":{"$area":137.0,"$color":86.131386,"path":
            "org/apache/http/impl/cookie/RFC2965Spec.html#RFC2965Spec",
            "title":"RFC2965Spec 137 Elements, 86.1% Coverage"},"children":[]},
        {"id":"RFC2965SpecFactory12536","name":"RFC2965SpecFactory","data":{
            "$area":14.0,"$color":0.0,"path":
            "org/apache/http/impl/cookie/RFC2965SpecFactory.html#RFC2965SpecFactory",
            "title":"RFC2965SpecFactory 14 Elements, 0% Coverage"},
          "children":[]},{"id":"RFC2965VersionAttributeHandler12550","name":
          "RFC2965VersionAttributeHandler","data":{"$area":34.0,"$color":
            79.411766,"path":
            "org/apache/http/impl/cookie/RFC2965VersionAttributeHandler.html#RFC2965VersionAttributeHandler",
            "title":
            "RFC2965VersionAttributeHandler 34 Elements, 79.4% Coverage"},
          "children":[]}]},{"id":"org.apache.http.impl.auth4141","name":
      "org.apache.http.impl.auth","data":{"$area":1487.0,"$color":68.52724,
        "title":"org.apache.http.impl.auth 1487 Elements, 68.5% Coverage"},
      "children":[{"id":"AuthSchemeBase4141","name":"AuthSchemeBase","data":{
            "$area":64.0,"$color":87.5,"path":
            "org/apache/http/impl/auth/AuthSchemeBase.html#AuthSchemeBase",
            "title":"AuthSchemeBase 64 Elements, 87.5% Coverage"},"children":
          []},{"id":"BasicScheme4205","name":"BasicScheme","data":{"$area":
            52.0,"$color":78.84615,"path":
            "org/apache/http/impl/auth/BasicScheme.html#BasicScheme","title":
            "BasicScheme 52 Elements, 78.8% Coverage"},"children":[]},{"id":
          "BasicSchemeFactory4257","name":"BasicSchemeFactory","data":{
            "$area":2.0,"$color":100.0,"path":
            "org/apache/http/impl/auth/BasicSchemeFactory.html#BasicSchemeFactory",
            "title":"BasicSchemeFactory 2 Elements, 100% Coverage"},
          "children":[]},{"id":"DigestScheme4259","name":"DigestScheme",
          "data":{"$area":242.0,"$color":96.28099,"path":
            "org/apache/http/impl/auth/DigestScheme.html#DigestScheme",
            "title":"DigestScheme 242 Elements, 96.3% Coverage"},"children":[]},
        {"id":"DigestSchemeFactory4501","name":"DigestSchemeFactory","data":{
            "$area":2.0,"$color":100.0,"path":
            "org/apache/http/impl/auth/DigestSchemeFactory.html#DigestSchemeFactory",
            "title":"DigestSchemeFactory 2 Elements, 100% Coverage"},
          "children":[]},{"id":"GGSSchemeBase4503","name":"GGSSchemeBase",
          "data":{"$area":104.0,"$color":56.73077,"path":
            "org/apache/http/impl/auth/GGSSchemeBase.html#GGSSchemeBase",
            "title":"GGSSchemeBase 104 Elements, 56.7% Coverage"},"children":
          []},{"id":"GGSSchemeBase.State4503","name":"GGSSchemeBase.State",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/impl/auth/GGSSchemeBase.html#GGSSchemeBase.State",
            "title":"GGSSchemeBase.State 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpEntityDigester4607","name":
          "HttpEntityDigester","data":{"$area":26.0,"$color":92.30769,"path":
            
            "org/apache/http/impl/auth/HttpEntityDigester.html#HttpEntityDigester",
            "title":"HttpEntityDigester 26 Elements, 92.3% Coverage"},
          "children":[]},{"id":"KerberosScheme4633","name":
          "KerberosScheme","data":{"$area":20.0,"$color":0.0,"path":
            "org/apache/http/impl/auth/KerberosScheme.html#KerberosScheme",
            "title":"KerberosScheme 20 Elements, 0% Coverage"},"children":[]},
        {"id":"KerberosSchemeFactory4653","name":"KerberosSchemeFactory",
          "data":{"$area":9.0,"$color":55.555557,"path":
            "org/apache/http/impl/auth/KerberosSchemeFactory.html#KerberosSchemeFactory",
            "title":"KerberosSchemeFactory 9 Elements, 55.6% Coverage"},
          "children":[]},{"id":"NTLMEngine4662","name":"NTLMEngine","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/impl/auth/NTLMEngine.html#NTLMEngine","title":
            "NTLMEngine 0 Elements,  -  Coverage"},"children":[]},{"id":
          "NTLMEngineException4662","name":"NTLMEngineException","data":{
            "$area":6.0,"$color":0.0,"path":
            "org/apache/http/impl/auth/NTLMEngineException.html#NTLMEngineException",
            "title":"NTLMEngineException 6 Elements, 0% Coverage"},
          "children":[]},{"id":"NTLMEngineImpl4668","name":
          "NTLMEngineImpl","data":{"$area":230.0,"$color":73.91304,"path":
            "org/apache/http/impl/auth/NTLMEngineImpl.html#NTLMEngineImpl",
            "title":"NTLMEngineImpl 230 Elements, 73.9% Coverage"},
          "children":[]},{"id":"NTLMEngineImpl.CipherGen4746","name":
          "NTLMEngineImpl.CipherGen","data":{"$area":166.0,"$color":
            74.09639,"path":
            "org/apache/http/impl/auth/NTLMEngineImpl.html#NTLMEngineImpl.CipherGen",
            "title":
            "NTLMEngineImpl.CipherGen 166 Elements, 74.1% Coverage"},
          "children":[]},{"id":"NTLMEngineImpl.NTLMMessage5046","name":
          "NTLMEngineImpl.NTLMMessage","data":{"$area":77.0,"$color":
            35.064934,"path":
            "org/apache/http/impl/auth/NTLMEngineImpl.html#NTLMEngineImpl.NTLMMessage",
            "title":
            "NTLMEngineImpl.NTLMMessage 77 Elements, 35.1% Coverage"},
          "children":[]},{"id":"NTLMEngineImpl.Type1Message5123","name":
          "NTLMEngineImpl.Type1Message","data":{"$area":22.0,"$color":
            95.454544,"path":
            "org/apache/http/impl/auth/NTLMEngineImpl.html#NTLMEngineImpl.Type1Message",
            "title":
            "NTLMEngineImpl.Type1Message 22 Elements, 95.5% Coverage"},
          "children":[]},{"id":"NTLMEngineImpl.Type2Message5145","name":
          "NTLMEngineImpl.Type2Message","data":{"$area":37.0,"$color":0.0,
            "path":
            "org/apache/http/impl/auth/NTLMEngineImpl.html#NTLMEngineImpl.Type2Message",
            "title":"NTLMEngineImpl.Type2Message 37 Elements, 0% Coverage"},
          "children":[]},{"id":"NTLMEngineImpl.Type3Message5182","name":
          "NTLMEngineImpl.Type3Message","data":{"$area":103.0,"$color":
            75.72816,"path":
            "org/apache/http/impl/auth/NTLMEngineImpl.html#NTLMEngineImpl.Type3Message",
            "title":
            "NTLMEngineImpl.Type3Message 103 Elements, 75.7% Coverage"},
          "children":[]},{"id":"NTLMEngineImpl.MD45298","name":
          "NTLMEngineImpl.MD4","data":{"$area":106.0,"$color":100.0,"path":
            "org/apache/http/impl/auth/NTLMEngineImpl.html#NTLMEngineImpl.MD4",
            "title":"NTLMEngineImpl.MD4 106 Elements, 100% Coverage"},
          "children":[]},{"id":"NTLMEngineImpl.HMACMD55404","name":
          "NTLMEngineImpl.HMACMD5","data":{"$area":36.0,"$color":80.55556,
            "path":
            "org/apache/http/impl/auth/NTLMEngineImpl.html#NTLMEngineImpl.HMACMD5",
            "title":"NTLMEngineImpl.HMACMD5 36 Elements, 80.6% Coverage"},
          "children":[]},{"id":"NTLMScheme5445","name":"NTLMScheme","data":{
            "$area":58.0,"$color":0.0,"path":
            "org/apache/http/impl/auth/NTLMScheme.html#NTLMScheme","title":
            "NTLMScheme 58 Elements, 0% Coverage"},"children":[]},{"id":
          "NTLMScheme.State5445","name":"NTLMScheme.State","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/impl/auth/NTLMScheme.html#NTLMScheme.State",
            "title":"NTLMScheme.State 0 Elements,  -  Coverage"},"children":[]},
        {"id":"NTLMSchemeFactory5503","name":"NTLMSchemeFactory","data":{
            "$area":2.0,"$color":0.0,"path":
            "org/apache/http/impl/auth/NTLMSchemeFactory.html#NTLMSchemeFactory",
            "title":"NTLMSchemeFactory 2 Elements, 0% Coverage"},"children":[]},
        {"id":"NegotiateScheme5505","name":"NegotiateScheme","data":{"$area":
            48.0,"$color":0.0,"path":
            "org/apache/http/impl/auth/NegotiateScheme.html#NegotiateScheme",
            "title":"NegotiateScheme 48 Elements, 0% Coverage"},"children":[]},
        {"id":"NegotiateSchemeFactory5553","name":"NegotiateSchemeFactory",
          "data":{"$area":14.0,"$color":0.0,"path":
            "org/apache/http/impl/auth/NegotiateSchemeFactory.html#NegotiateSchemeFactory",
            "title":"NegotiateSchemeFactory 14 Elements, 0% Coverage"},
          "children":[]},{"id":"RFC2617Scheme5567","name":"RFC2617Scheme",
          "data":{"$area":26.0,"$color":100.0,"path":
            "org/apache/http/impl/auth/RFC2617Scheme.html#RFC2617Scheme",
            "title":"RFC2617Scheme 26 Elements, 100% Coverage"},"children":[]},
        {"id":"SPNegoScheme5593","name":"SPNegoScheme","data":{"$area":
            20.0,"$color":50.0,"path":
            "org/apache/http/impl/auth/SPNegoScheme.html#SPNegoScheme",
            "title":"SPNegoScheme 20 Elements, 50% Coverage"},"children":[]},
        {"id":"SPNegoSchemeFactory5613","name":"SPNegoSchemeFactory","data":{
            "$area":9.0,"$color":55.555557,"path":
            "org/apache/http/impl/auth/SPNegoSchemeFactory.html#SPNegoSchemeFactory",
            "title":"SPNegoSchemeFactory 9 Elements, 55.6% Coverage"},
          "children":[]},{"id":"SpnegoTokenGenerator5622","name":
          "SpnegoTokenGenerator","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/impl/auth/SpnegoTokenGenerator.html#SpnegoTokenGenerator",
            "title":"SpnegoTokenGenerator 0 Elements,  -  Coverage"},
          "children":[]},{"id":"UnsupportedDigestAlgorithmException5622",
          "name":"UnsupportedDigestAlgorithmException","data":{"$area":6.0,
            "$color":33.333336,"path":
            "org/apache/http/impl/auth/UnsupportedDigestAlgorithmException.html#UnsupportedDigestAlgorithmException",
            "title":
            "UnsupportedDigestAlgorithmException 6 Elements, 33.3% Coverage"},
          "children":[]}]},{"id":"org.apache.http.impl.conn.tsccm10084",
      "name":"org.apache.http.impl.conn.tsccm","data":{"$area":798.0,
        "$color":67.04261,"title":
        "org.apache.http.impl.conn.tsccm 798 Elements, 67% Coverage"},
      "children":[{"id":"AbstractConnPool10084","name":"AbstractConnPool",
          "data":{"$area":48.0,"$color":12.5,"path":
            "org/apache/http/impl/conn/tsccm/AbstractConnPool.html#AbstractConnPool",
            "title":"AbstractConnPool 48 Elements, 12.5% Coverage"},
          "children":[]},{"id":"BasicPoolEntry10132","name":
          "BasicPoolEntry","data":{"$area":51.0,"$color":54.901962,"path":
            "org/apache/http/impl/conn/tsccm/BasicPoolEntry.html#BasicPoolEntry",
            "title":"BasicPoolEntry 51 Elements, 54.9% Coverage"},"children":
          []},{"id":"BasicPoolEntryRef10183","name":"BasicPoolEntryRef",
          "data":{"$area":9.0,"$color":0.0,"path":
            "org/apache/http/impl/conn/tsccm/BasicPoolEntryRef.html#BasicPoolEntryRef",
            "title":"BasicPoolEntryRef 9 Elements, 0% Coverage"},"children":[]},
        {"id":"BasicPooledConnAdapter10192","name":
          "BasicPooledConnAdapter","data":{"$area":9.0,"$color":33.333336,
            "path":
            "org/apache/http/impl/conn/tsccm/BasicPooledConnAdapter.html#BasicPooledConnAdapter",
            "title":"BasicPooledConnAdapter 9 Elements, 33.3% Coverage"},
          "children":[]},{"id":"ConnPoolByRoute10201","name":
          "ConnPoolByRoute","data":{"$area":406.0,"$color":84.48276,"path":
            "org/apache/http/impl/conn/tsccm/ConnPoolByRoute.html#ConnPoolByRoute",
            "title":"ConnPoolByRoute 406 Elements, 84.5% Coverage"},
          "children":[]},{"id":"PoolEntryRequest10607","name":
          "PoolEntryRequest","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/impl/conn/tsccm/PoolEntryRequest.html#PoolEntryRequest",
            "title":"PoolEntryRequest 0 Elements,  -  Coverage"},"children":[]},
        {"id":"RouteSpecificPool10607","name":"RouteSpecificPool","data":{
            "$area":96.0,"$color":73.95833,"path":
            "org/apache/http/impl/conn/tsccm/RouteSpecificPool.html#RouteSpecificPool",
            "title":"RouteSpecificPool 96 Elements, 74% Coverage"},
          "children":[]},{"id":"ThreadSafeClientConnManager10703","name":
          "ThreadSafeClientConnManager","data":{"$area":121.0,"$color":
            30.578512,"path":
            "org/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager.html#ThreadSafeClientConnManager",
            "title":
            "ThreadSafeClientConnManager 121 Elements, 30.6% Coverage"},
          "children":[]},{"id":"WaitingThread10824","name":"WaitingThread",
          "data":{"$area":46.0,"$color":84.78261,"path":
            "org/apache/http/impl/conn/tsccm/WaitingThread.html#WaitingThread",
            "title":"WaitingThread 46 Elements, 84.8% Coverage"},"children":[]},
        {"id":"WaitingThreadAborter10870","name":"WaitingThreadAborter",
          "data":{"$area":12.0,"$color":66.66667,"path":
            "org/apache/http/impl/conn/tsccm/WaitingThreadAborter.html#WaitingThreadAborter",
            "title":"WaitingThreadAborter 12 Elements, 66.7% Coverage"},
          "children":[]}]},{"id":"org.apache.http.client.methods616","name":
      "org.apache.http.client.methods","data":{"$area":195.0,"$color":
        73.84615,"title":
        "org.apache.http.client.methods 195 Elements, 73.8% Coverage"},
      "children":[{"id":"AbortableHttpRequest616","name":
          "AbortableHttpRequest","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/client/methods/AbortableHttpRequest.html#AbortableHttpRequest",
            "title":"AbortableHttpRequest 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpDelete616","name":"HttpDelete","data":{
            "$area":10.0,"$color":50.0,"path":
            "org/apache/http/client/methods/HttpDelete.html#HttpDelete",
            "title":"HttpDelete 10 Elements, 50% Coverage"},"children":[]},{
          "id":"HttpEntityEnclosingRequestBase626","name":
          "HttpEntityEnclosingRequestBase","data":{"$area":16.0,"$color":
            100.0,"path":
            "org/apache/http/client/methods/HttpEntityEnclosingRequestBase.html#HttpEntityEnclosingRequestBase",
            "title":
            "HttpEntityEnclosingRequestBase 16 Elements, 100% Coverage"},
          "children":[]},{"id":"HttpGet642","name":"HttpGet","data":{"$area":
            10.0,"$color":80.0,"path":
            "org/apache/http/client/methods/HttpGet.html#HttpGet","title":
            "HttpGet 10 Elements, 80% Coverage"},"children":[]},{"id":
          "HttpHead652","name":"HttpHead","data":{"$area":10.0,"$color":
            80.0,"path":
            "org/apache/http/client/methods/HttpHead.html#HttpHead","title":
            "HttpHead 10 Elements, 80% Coverage"},"children":[]},{"id":
          "HttpOptions662","name":"HttpOptions","data":{"$area":25.0,
            "$color":60.000004,"path":
            "org/apache/http/client/methods/HttpOptions.html#HttpOptions",
            "title":"HttpOptions 25 Elements, 60% Coverage"},"children":[]},{
          "id":"HttpPatch687","name":"HttpPatch","data":{"$area":10.0,
            "$color":0.0,"path":
            "org/apache/http/client/methods/HttpPatch.html#HttpPatch",
            "title":"HttpPatch 10 Elements, 0% Coverage"},"children":[]},{
          "id":"HttpPost697","name":"HttpPost","data":{"$area":10.0,"$color":
            80.0,"path":
            "org/apache/http/client/methods/HttpPost.html#HttpPost","title":
            "HttpPost 10 Elements, 80% Coverage"},"children":[]},{"id":
          "HttpPut707","name":"HttpPut","data":{"$area":10.0,"$color":50.0,
            "path":"org/apache/http/client/methods/HttpPut.html#HttpPut",
            "title":"HttpPut 10 Elements, 50% Coverage"},"children":[]},{
          "id":"HttpRequestBase717","name":"HttpRequestBase","data":{"$area":
            84.0,"$color":84.52381,"path":
            "org/apache/http/client/methods/HttpRequestBase.html#HttpRequestBase",
            "title":"HttpRequestBase 84 Elements, 84.5% Coverage"},
          "children":[]},{"id":"HttpTrace801","name":"HttpTrace","data":{
            "$area":10.0,"$color":80.0,"path":
            "org/apache/http/client/methods/HttpTrace.html#HttpTrace",
            "title":"HttpTrace 10 Elements, 80% Coverage"},"children":[]},{
          "id":"HttpUriRequest811","name":"HttpUriRequest","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/client/methods/HttpUriRequest.html#HttpUriRequest",
            "title":"HttpUriRequest 0 Elements,  -  Coverage"},"children":[]}]},
    {"id":"org.apache.http.auth0","name":"org.apache.http.auth","data":{
        "$area":468.0,"$color":66.23932,"title":
        "org.apache.http.auth 468 Elements, 66.2% Coverage"},"children":[{
          "id":"AUTH0","name":"AUTH","data":{"$area":1.0,"$color":0.0,"path":
            "org/apache/http/auth/AUTH.html#AUTH","title":
            "AUTH 1 Elements, 0% Coverage"},"children":[]},{"id":
          "AuthOption1","name":"AuthOption","data":{"$area":18.0,"$color":
            66.66667,"path":
            "org/apache/http/auth/AuthOption.html#AuthOption","title":
            "AuthOption 18 Elements, 66.7% Coverage"},"children":[]},{"id":
          "AuthProtocolState19","name":"AuthProtocolState","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/auth/AuthProtocolState.html#AuthProtocolState",
            "title":"AuthProtocolState 0 Elements,  -  Coverage"},"children":
          []},{"id":"AuthScheme19","name":"AuthScheme","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/auth/AuthScheme.html#AuthScheme","title":
            "AuthScheme 0 Elements,  -  Coverage"},"children":[]},{"id":
          "AuthSchemeFactory19","name":"AuthSchemeFactory","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/http/auth/AuthSchemeFactory.html#AuthSchemeFactory",
            "title":"AuthSchemeFactory 0 Elements,  -  Coverage"},"children":
          []},{"id":"AuthSchemeRegistry19","name":"AuthSchemeRegistry",
          "data":{"$area":39.0,"$color":41.025642,"path":
            "org/apache/http/auth/AuthSchemeRegistry.html#AuthSchemeRegistry",
            "title":"AuthSchemeRegistry 39 Elements, 41% Coverage"},
          "children":[]},{"id":"AuthScope58","name":"AuthScope","data":{
            "$area":121.0,"$color":57.024796,"path":
            "org/apache/http/auth/AuthScope.html#AuthScope","title":
            "AuthScope 121 Elements, 57% Coverage"},"children":[]},{"id":
          "AuthState179","name":"AuthState","data":{"$area":72.0,"$color":
            47.22222,"path":
            "org/apache/http/auth/AuthState.html#AuthState","title":
            "AuthState 72 Elements, 47.2% Coverage"},"children":[]},{"id":
          "AuthenticationException251","name":"AuthenticationException",
          "data":{"$area":6.0,"$color":66.66667,"path":
            "org/apache/http/auth/AuthenticationException.html#AuthenticationException",
            "title":"AuthenticationException 6 Elements, 66.7% Coverage"},
          "children":[]},{"id":"BasicUserPrincipal257","name":
          "BasicUserPrincipal","data":{"$area":33.0,"$color":84.84849,"path":
            
            "org/apache/http/auth/BasicUserPrincipal.html#BasicUserPrincipal",
            "title":"BasicUserPrincipal 33 Elements, 84.8% Coverage"},
          "children":[]},{"id":"ChallengeState290","name":"ChallengeState",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/auth/ChallengeState.html#ChallengeState",
            "title":"ChallengeState 0 Elements,  -  Coverage"},"children":[]},
        {"id":"ContextAwareAuthScheme290","name":"ContextAwareAuthScheme",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/auth/ContextAwareAuthScheme.html#ContextAwareAuthScheme",
            "title":"ContextAwareAuthScheme 0 Elements,  -  Coverage"},
          "children":[]},{"id":"Credentials290","name":"Credentials","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/auth/Credentials.html#Credentials","title":
            "Credentials 0 Elements,  -  Coverage"},"children":[]},{"id":
          "InvalidCredentialsException290","name":
          "InvalidCredentialsException","data":{"$area":6.0,"$color":0.0,
            "path":
            "org/apache/http/auth/InvalidCredentialsException.html#InvalidCredentialsException",
            "title":"InvalidCredentialsException 6 Elements, 0% Coverage"},
          "children":[]},{"id":"MalformedChallengeException296","name":
          "MalformedChallengeException","data":{"$area":6.0,"$color":
            66.66667,"path":
            "org/apache/http/auth/MalformedChallengeException.html#MalformedChallengeException",
            "title":
            "MalformedChallengeException 6 Elements, 66.7% Coverage"},
          "children":[]},{"id":"NTCredentials302","name":"NTCredentials",
          "data":{"$area":72.0,"$color":87.5,"path":
            "org/apache/http/auth/NTCredentials.html#NTCredentials","title":
            "NTCredentials 72 Elements, 87.5% Coverage"},"children":[]},{
          "id":"NTUserPrincipal374","name":"NTUserPrincipal","data":{"$area":
            48.0,"$color":81.25,"path":
            "org/apache/http/auth/NTUserPrincipal.html#NTUserPrincipal",
            "title":"NTUserPrincipal 48 Elements, 81.2% Coverage"},
          "children":[]},{"id":"UsernamePasswordCredentials422","name":
          "UsernamePasswordCredentials","data":{"$area":46.0,"$color":
            89.13044,"path":
            "org/apache/http/auth/UsernamePasswordCredentials.html#UsernamePasswordCredentials",
            "title":
            "UsernamePasswordCredentials 46 Elements, 89.1% Coverage"},
          "children":[]}]}]}

 ); 