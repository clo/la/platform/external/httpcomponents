processTreeMapJson (  {"id":"Clover database Mon Jan 14 2013 22:52:16 CET0","name":"","data":{
    "$area":3116.0,"$color":86.553276,"title":
    " 3116 Elements, 86.6% Coverage"},"children":[{"id":
      "org.apache.http.client.cache0","name":
      "org.apache.http.client.cache","data":{"$area":75.0,"$color":
        89.33333,"title":
        "org.apache.http.client.cache 75 Elements, 89.3% Coverage"},
      "children":[{"id":"CacheResponseStatus0","name":
          "CacheResponseStatus","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/client/cache/CacheResponseStatus.html#CacheResponseStatus",
            "title":"CacheResponseStatus 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HeaderConstants0","name":"HeaderConstants",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/client/cache/HeaderConstants.html#HeaderConstants",
            "title":"HeaderConstants 0 Elements,  -  Coverage"},"children":[]},
        {"id":"HttpCacheEntry0","name":"HttpCacheEntry","data":{"$area":
            55.0,"$color":100.0,"path":
            "org/apache/http/client/cache/HttpCacheEntry.html#HttpCacheEntry",
            "title":"HttpCacheEntry 55 Elements, 100% Coverage"},"children":[]},
        {"id":"HttpCacheEntrySerializationException55","name":
          "HttpCacheEntrySerializationException","data":{"$area":5.0,
            "$color":0.0,"path":
            "org/apache/http/client/cache/HttpCacheEntrySerializationException.html#HttpCacheEntrySerializationException",
            "title":
            "HttpCacheEntrySerializationException 5 Elements, 0% Coverage"},
          "children":[]},{"id":"HttpCacheEntrySerializer60","name":
          "HttpCacheEntrySerializer","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/client/cache/HttpCacheEntrySerializer.html#HttpCacheEntrySerializer",
            "title":"HttpCacheEntrySerializer 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpCacheStorage60","name":
          "HttpCacheStorage","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/client/cache/HttpCacheStorage.html#HttpCacheStorage",
            "title":"HttpCacheStorage 0 Elements,  -  Coverage"},"children":[]},
        {"id":"HttpCacheUpdateCallback60","name":"HttpCacheUpdateCallback",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/client/cache/HttpCacheUpdateCallback.html#HttpCacheUpdateCallback",
            "title":"HttpCacheUpdateCallback 0 Elements,  -  Coverage"},
          "children":[]},{"id":"HttpCacheUpdateException60","name":
          "HttpCacheUpdateException","data":{"$area":5.0,"$color":40.0,
            "path":
            "org/apache/http/client/cache/HttpCacheUpdateException.html#HttpCacheUpdateException",
            "title":"HttpCacheUpdateException 5 Elements, 40% Coverage"},
          "children":[]},{"id":"InputLimit65","name":"InputLimit","data":{
            "$area":10.0,"$color":100.0,"path":
            "org/apache/http/client/cache/InputLimit.html#InputLimit",
            "title":"InputLimit 10 Elements, 100% Coverage"},"children":[]},{
          "id":"Resource75","name":"Resource","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/http/client/cache/Resource.html#Resource","title":
            "Resource 0 Elements,  -  Coverage"},"children":[]},{"id":
          "ResourceFactory75","name":"ResourceFactory","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/client/cache/ResourceFactory.html#ResourceFactory",
            "title":"ResourceFactory 0 Elements,  -  Coverage"},"children":[]}]},
    {"id":"org.apache.http.impl.client.cache.memcached2944","name":
      "org.apache.http.impl.client.cache.memcached","data":{"$area":172.0,
        "$color":91.860466,"title":
        "org.apache.http.impl.client.cache.memcached 172 Elements, 91.9% Coverage"},
      "children":[{"id":"KeyHashingScheme2944","name":"KeyHashingScheme",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/impl/client/cache/memcached/KeyHashingScheme.html#KeyHashingScheme",
            "title":"KeyHashingScheme 0 Elements,  -  Coverage"},"children":[]},
        {"id":"MemcachedCacheEntry2944","name":"MemcachedCacheEntry","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/impl/client/cache/memcached/MemcachedCacheEntry.html#MemcachedCacheEntry",
            "title":"MemcachedCacheEntry 0 Elements,  -  Coverage"},
          "children":[]},{"id":"MemcachedCacheEntryFactory2944","name":
          "MemcachedCacheEntryFactory","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/http/impl/client/cache/memcached/MemcachedCacheEntryFactory.html#MemcachedCacheEntryFactory",
            "title":"MemcachedCacheEntryFactory 0 Elements,  -  Coverage"},
          "children":[]},{"id":"MemcachedCacheEntryFactoryImpl2944","name":
          "MemcachedCacheEntryFactoryImpl","data":{"$area":4.0,"$color":
            50.0,"path":
            "org/apache/http/impl/client/cache/memcached/MemcachedCacheEntryFactoryImpl.html#MemcachedCacheEntryFactoryImpl",
            "title":
            "MemcachedCacheEntryFactoryImpl 4 Elements, 50% Coverage"},
          "children":[]},{"id":"MemcachedCacheEntryImpl2948","name":
          "MemcachedCacheEntryImpl","data":{"$area":33.0,"$color":93.93939,
            "path":
            "org/apache/http/impl/client/cache/memcached/MemcachedCacheEntryImpl.html#MemcachedCacheEntryImpl",
            "title":"MemcachedCacheEntryImpl 33 Elements, 93.9% Coverage"},
          "children":[]},{"id":"MemcachedHttpCacheStorage2981","name":
          "MemcachedHttpCacheStorage","data":{"$area":114.0,"$color":
            92.98246,"path":
            "org/apache/http/impl/client/cache/memcached/MemcachedHttpCacheStorage.html#MemcachedHttpCacheStorage",
            "title":"MemcachedHttpCacheStorage 114 Elements, 93% Coverage"},
          "children":[]},{"id":"MemcachedKeyHashingException3095","name":
          "MemcachedKeyHashingException","data":{"$area":2.0,"$color":
            100.0,"path":
            "org/apache/http/impl/client/cache/memcached/MemcachedKeyHashingException.html#MemcachedKeyHashingException",
            "title":
            "MemcachedKeyHashingException 2 Elements, 100% Coverage"},
          "children":[]},{"id":"MemcachedOperationTimeoutException3097",
          "name":"MemcachedOperationTimeoutException","data":{"$area":3.0,
            "$color":100.0,"path":
            "org/apache/http/impl/client/cache/memcached/MemcachedOperationTimeoutException.html#MemcachedOperationTimeoutException",
            "title":
            "MemcachedOperationTimeoutException 3 Elements, 100% Coverage"},
          "children":[]},{"id":"MemcachedSerializationException3100","name":
          "MemcachedSerializationException","data":{"$area":2.0,"$color":
            100.0,"path":
            "org/apache/http/impl/client/cache/memcached/MemcachedSerializationException.html#MemcachedSerializationException",
            "title":
            "MemcachedSerializationException 2 Elements, 100% Coverage"},
          "children":[]},{"id":"PrefixKeyHashingScheme3102","name":
          "PrefixKeyHashingScheme","data":{"$area":5.0,"$color":100.0,"path":
            
            "org/apache/http/impl/client/cache/memcached/PrefixKeyHashingScheme.html#PrefixKeyHashingScheme",
            "title":"PrefixKeyHashingScheme 5 Elements, 100% Coverage"},
          "children":[]},{"id":"SHA256KeyHashingScheme3107","name":
          "SHA256KeyHashingScheme","data":{"$area":9.0,"$color":77.77778,
            "path":
            "org/apache/http/impl/client/cache/memcached/SHA256KeyHashingScheme.html#SHA256KeyHashingScheme",
            "title":"SHA256KeyHashingScheme 9 Elements, 77.8% Coverage"},
          "children":[]}]},{"id":
      "org.apache.http.impl.client.cache.ehcache2895","name":
      "org.apache.http.impl.client.cache.ehcache","data":{"$area":49.0,
        "$color":95.918365,"title":
        "org.apache.http.impl.client.cache.ehcache 49 Elements, 95.9% Coverage"},
      "children":[{"id":"EhcacheHttpCacheStorage2895","name":
          "EhcacheHttpCacheStorage","data":{"$area":49.0,"$color":
            95.918365,"path":
            "org/apache/http/impl/client/cache/ehcache/EhcacheHttpCacheStorage.html#EhcacheHttpCacheStorage",
            "title":"EhcacheHttpCacheStorage 49 Elements, 95.9% Coverage"},
          "children":[]}]},{"id":"org.apache.http.impl.client.cache75",
      "name":"org.apache.http.impl.client.cache","data":{"$area":2820.0,
        "$color":85.992905,"title":
        "org.apache.http.impl.client.cache 2820 Elements, 86% Coverage"},
      "children":[{"id":"AsynchronousValidationRequest75","name":
          "AsynchronousValidationRequest","data":{"$area":16.0,"$color":
            100.0,"path":
            "org/apache/http/impl/client/cache/AsynchronousValidationRequest.html#AsynchronousValidationRequest",
            "title":
            "AsynchronousValidationRequest 16 Elements, 100% Coverage"},
          "children":[]},{"id":"AsynchronousValidator91","name":
          "AsynchronousValidator","data":{"$area":23.0,"$color":100.0,"path":
            
            "org/apache/http/impl/client/cache/AsynchronousValidator.html#AsynchronousValidator",
            "title":"AsynchronousValidator 23 Elements, 100% Coverage"},
          "children":[]},{"id":"BasicHttpCache114","name":"BasicHttpCache",
          "data":{"$area":160.0,"$color":96.25,"path":
            "org/apache/http/impl/client/cache/BasicHttpCache.html#BasicHttpCache",
            "title":"BasicHttpCache 160 Elements, 96.2% Coverage"},
          "children":[]},{"id":"BasicHttpCacheStorage274","name":
          "BasicHttpCacheStorage","data":{"$area":12.0,"$color":100.0,"path":
            
            "org/apache/http/impl/client/cache/BasicHttpCacheStorage.html#BasicHttpCacheStorage",
            "title":"BasicHttpCacheStorage 12 Elements, 100% Coverage"},
          "children":[]},{"id":"BasicIdGenerator286","name":
          "BasicIdGenerator","data":{"$area":24.0,"$color":75.0,"path":
            "org/apache/http/impl/client/cache/BasicIdGenerator.html#BasicIdGenerator",
            "title":"BasicIdGenerator 24 Elements, 75% Coverage"},"children":
          []},{"id":"CacheConfig310","name":"CacheConfig","data":{"$area":
            54.0,"$color":74.07407,"path":
            "org/apache/http/impl/client/cache/CacheConfig.html#CacheConfig",
            "title":"CacheConfig 54 Elements, 74.1% Coverage"},"children":[]},
        {"id":"CacheEntity364","name":"CacheEntity","data":{"$area":29.0,
            "$color":31.034481,"path":
            "org/apache/http/impl/client/cache/CacheEntity.html#CacheEntity",
            "title":"CacheEntity 29 Elements, 31% Coverage"},"children":[]},{
          "id":"CacheEntryUpdater393","name":"CacheEntryUpdater","data":{
            "$area":70.0,"$color":100.0,"path":
            "org/apache/http/impl/client/cache/CacheEntryUpdater.html#CacheEntryUpdater",
            "title":"CacheEntryUpdater 70 Elements, 100% Coverage"},
          "children":[]},{"id":"CacheInvalidator463","name":
          "CacheInvalidator","data":{"$area":150.0,"$color":93.333336,"path":
            
            "org/apache/http/impl/client/cache/CacheInvalidator.html#CacheInvalidator",
            "title":"CacheInvalidator 150 Elements, 93.3% Coverage"},
          "children":[]},{"id":"CacheKeyGenerator613","name":
          "CacheKeyGenerator","data":{"$area":85.0,"$color":95.29412,"path":
            "org/apache/http/impl/client/cache/CacheKeyGenerator.html#CacheKeyGenerator",
            "title":"CacheKeyGenerator 85 Elements, 95.3% Coverage"},
          "children":[]},{"id":"CacheMap698","name":"CacheMap","data":{
            "$area":5.0,"$color":100.0,"path":
            "org/apache/http/impl/client/cache/CacheMap.html#CacheMap",
            "title":"CacheMap 5 Elements, 100% Coverage"},"children":[]},{
          "id":"CacheValidityPolicy703","name":"CacheValidityPolicy","data":{
            "$area":188.0,"$color":96.80851,"path":
            "org/apache/http/impl/client/cache/CacheValidityPolicy.html#CacheValidityPolicy",
            "title":"CacheValidityPolicy 188 Elements, 96.8% Coverage"},
          "children":[]},{"id":"CacheableRequestPolicy891","name":
          "CacheableRequestPolicy","data":{"$area":33.0,"$color":100.0,
            "path":
            "org/apache/http/impl/client/cache/CacheableRequestPolicy.html#CacheableRequestPolicy",
            "title":"CacheableRequestPolicy 33 Elements, 100% Coverage"},
          "children":[]},{"id":"CachedHttpResponseGenerator924","name":
          "CachedHttpResponseGenerator","data":{"$area":73.0,"$color":
            95.89041,"path":
            "org/apache/http/impl/client/cache/CachedHttpResponseGenerator.html#CachedHttpResponseGenerator",
            "title":
            "CachedHttpResponseGenerator 73 Elements, 95.9% Coverage"},
          "children":[]},{"id":"CachedResponseSuitabilityChecker997","name":
          "CachedResponseSuitabilityChecker","data":{"$area":210.0,"$color":
            91.42857,"path":
            "org/apache/http/impl/client/cache/CachedResponseSuitabilityChecker.html#CachedResponseSuitabilityChecker",
            "title":
            "CachedResponseSuitabilityChecker 210 Elements, 91.4% Coverage"},
          "children":[]},{"id":"CachingHttpClient1207","name":
          "CachingHttpClient","data":{"$area":500.0,"$color":89.8,"path":
            "org/apache/http/impl/client/cache/CachingHttpClient.html#CachingHttpClient",
            "title":"CachingHttpClient 500 Elements, 89.8% Coverage"},
          "children":[]},{"id":"CombinedEntity1707","name":
          "CombinedEntity","data":{"$area":28.0,"$color":50.0,"path":
            "org/apache/http/impl/client/cache/CombinedEntity.html#CombinedEntity",
            "title":"CombinedEntity 28 Elements, 50% Coverage"},"children":[]},
        {"id":"CombinedEntity.ResourceStream1735","name":
          "CombinedEntity.ResourceStream","data":{"$area":6.0,"$color":
            100.0,"path":
            "org/apache/http/impl/client/cache/CombinedEntity.html#CombinedEntity.ResourceStream",
            "title":
            "CombinedEntity.ResourceStream 6 Elements, 100% Coverage"},
          "children":[]},{"id":"ConditionalRequestBuilder1741","name":
          "ConditionalRequestBuilder","data":{"$area":59.0,"$color":
            93.220345,"path":
            "org/apache/http/impl/client/cache/ConditionalRequestBuilder.html#ConditionalRequestBuilder",
            "title":
            "ConditionalRequestBuilder 59 Elements, 93.2% Coverage"},
          "children":[]},{"id":"DefaultHttpCacheEntrySerializer1800","name":
          "DefaultHttpCacheEntrySerializer","data":{"$area":11.0,"$color":
            90.909096,"path":
            "org/apache/http/impl/client/cache/DefaultHttpCacheEntrySerializer.html#DefaultHttpCacheEntrySerializer",
            "title":
            "DefaultHttpCacheEntrySerializer 11 Elements, 90.9% Coverage"},
          "children":[]},{"id":"FileResource1811","name":"FileResource",
          "data":{"$area":17.0,"$color":35.294117,"path":
            "org/apache/http/impl/client/cache/FileResource.html#FileResource",
            "title":"FileResource 17 Elements, 35.3% Coverage"},"children":[]},
        {"id":"FileResourceFactory1828","name":"FileResourceFactory","data":{
            "$area":48.0,"$color":68.75,"path":
            "org/apache/http/impl/client/cache/FileResourceFactory.html#FileResourceFactory",
            "title":"FileResourceFactory 48 Elements, 68.8% Coverage"},
          "children":[]},{"id":"HeapResource1876","name":"HeapResource",
          "data":{"$area":10.0,"$color":100.0,"path":
            "org/apache/http/impl/client/cache/HeapResource.html#HeapResource",
            "title":"HeapResource 10 Elements, 100% Coverage"},"children":[]},
        {"id":"HeapResourceFactory1886","name":"HeapResourceFactory","data":{
            "$area":26.0,"$color":76.92308,"path":
            "org/apache/http/impl/client/cache/HeapResourceFactory.html#HeapResourceFactory",
            "title":"HeapResourceFactory 26 Elements, 76.9% Coverage"},
          "children":[]},{"id":"HttpCache1912","name":"HttpCache","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/impl/client/cache/HttpCache.html#HttpCache",
            "title":"HttpCache 0 Elements,  -  Coverage"},"children":[]},{
          "id":"IOUtils1912","name":"IOUtils","data":{"$area":36.0,"$color":
            0.0,"path":
            "org/apache/http/impl/client/cache/IOUtils.html#IOUtils","title":
            "IOUtils 36 Elements, 0% Coverage"},"children":[]},{"id":
          "ManagedHttpCacheStorage1948","name":"ManagedHttpCacheStorage",
          "data":{"$area":90.0,"$color":32.22222,"path":
            "org/apache/http/impl/client/cache/ManagedHttpCacheStorage.html#ManagedHttpCacheStorage",
            "title":"ManagedHttpCacheStorage 90 Elements, 32.2% Coverage"},
          "children":[]},{"id":"OptionsHttp11Response2038","name":
          "OptionsHttp11Response","data":{"$area":43.0,"$color":0.0,"path":
            "org/apache/http/impl/client/cache/OptionsHttp11Response.html#OptionsHttp11Response",
            "title":"OptionsHttp11Response 43 Elements, 0% Coverage"},
          "children":[]},{"id":"RequestProtocolCompliance2081","name":
          "RequestProtocolCompliance","data":{"$area":233.0,"$color":
            93.99142,"path":
            "org/apache/http/impl/client/cache/RequestProtocolCompliance.html#RequestProtocolCompliance",
            "title":"RequestProtocolCompliance 233 Elements, 94% Coverage"},
          "children":[]},{"id":"RequestProtocolError2314","name":
          "RequestProtocolError","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/impl/client/cache/RequestProtocolError.html#RequestProtocolError",
            "title":"RequestProtocolError 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ResourceReference2314","name":
          "ResourceReference","data":{"$area":13.0,"$color":53.846157,"path":
            
            "org/apache/http/impl/client/cache/ResourceReference.html#ResourceReference",
            "title":"ResourceReference 13 Elements, 53.8% Coverage"},
          "children":[]},{"id":"ResponseCachingPolicy2327","name":
          "ResponseCachingPolicy","data":{"$area":171.0,"$color":97.66082,
            "path":
            "org/apache/http/impl/client/cache/ResponseCachingPolicy.html#ResponseCachingPolicy",
            "title":"ResponseCachingPolicy 171 Elements, 97.7% Coverage"},
          "children":[]},{"id":"ResponseProtocolCompliance2498","name":
          "ResponseProtocolCompliance","data":{"$area":152.0,"$color":
            94.73685,"path":
            "org/apache/http/impl/client/cache/ResponseProtocolCompliance.html#ResponseProtocolCompliance",
            "title":
            "ResponseProtocolCompliance 152 Elements, 94.7% Coverage"},
          "children":[]},{"id":"SizeLimitedResponseReader2650","name":
          "SizeLimitedResponseReader","data":{"$area":58.0,"$color":
            89.655174,"path":
            "org/apache/http/impl/client/cache/SizeLimitedResponseReader.html#SizeLimitedResponseReader",
            "title":
            "SizeLimitedResponseReader 58 Elements, 89.7% Coverage"},
          "children":[]},{"id":"Variant2708","name":"Variant","data":{
            "$area":10.0,"$color":80.0,"path":
            "org/apache/http/impl/client/cache/Variant.html#Variant","title":
            "Variant 10 Elements, 80% Coverage"},"children":[]},{"id":
          "WarningValue2718","name":"WarningValue","data":{"$area":177.0,
            "$color":85.31073,"path":
            "org/apache/http/impl/client/cache/WarningValue.html#WarningValue",
            "title":"WarningValue 177 Elements, 85.3% Coverage"},"children":[]}]}]}

 ); 