processTreeMapJson (  {"id":"Clover database Mon Jan 14 2013 22:52:04 CET0","name":"","data":{
    "$area":506.0,"$color":68.181816,"title":
    " 506 Elements, 68.2% Coverage"},"children":[{"id":
      "org.apache.http.entity.mime.content338","name":
      "org.apache.http.entity.mime.content","data":{"$area":168.0,"$color":
        64.88096,"title":
        "org.apache.http.entity.mime.content 168 Elements, 64.9% Coverage"},
      "children":[{"id":"AbstractContentBody338","name":
          "AbstractContentBody","data":{"$area":21.0,"$color":76.190475,
            "path":
            "org/apache/http/entity/mime/content/AbstractContentBody.html#AbstractContentBody",
            "title":"AbstractContentBody 21 Elements, 76.2% Coverage"},
          "children":[]},{"id":"ByteArrayBody359","name":"ByteArrayBody",
          "data":{"$area":20.0,"$color":0.0,"path":
            "org/apache/http/entity/mime/content/ByteArrayBody.html#ByteArrayBody",
            "title":"ByteArrayBody 20 Elements, 0% Coverage"},"children":[]},
        {"id":"ContentBody379","name":"ContentBody","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/http/entity/mime/content/ContentBody.html#ContentBody",
            "title":"ContentBody 0 Elements,  -  Coverage"},"children":[]},{
          "id":"ContentDescriptor379","name":"ContentDescriptor","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/http/entity/mime/content/ContentDescriptor.html#ContentDescriptor",
            "title":"ContentDescriptor 0 Elements,  -  Coverage"},"children":
          []},{"id":"FileBody379","name":"FileBody","data":{"$area":46.0,
            "$color":78.26087,"path":
            "org/apache/http/entity/mime/content/FileBody.html#FileBody",
            "title":"FileBody 46 Elements, 78.3% Coverage"},"children":[]},{
          "id":"InputStreamBody425","name":"InputStreamBody","data":{"$area":
            34.0,"$color":76.47059,"path":
            "org/apache/http/entity/mime/content/InputStreamBody.html#InputStreamBody",
            "title":"InputStreamBody 34 Elements, 76.5% Coverage"},
          "children":[]},{"id":"StringBody459","name":"StringBody","data":{
            "$area":47.0,"$color":65.95744,"path":
            "org/apache/http/entity/mime/content/StringBody.html#StringBody",
            "title":"StringBody 47 Elements, 66% Coverage"},"children":[]}]},
    {"id":"org.apache.http.entity.mime0","name":
      "org.apache.http.entity.mime","data":{"$area":338.0,"$color":
        69.82249,"title":
        "org.apache.http.entity.mime 338 Elements, 69.8% Coverage"},
      "children":[{"id":"FormBodyPart0","name":"FormBodyPart","data":{
            "$area":51.0,"$color":88.2353,"path":
            "org/apache/http/entity/mime/FormBodyPart.html#FormBodyPart",
            "title":"FormBodyPart 51 Elements, 88.2% Coverage"},"children":[]},
        {"id":"Header51","name":"Header","data":{"$area":90.0,"$color":
            27.777779,"path":
            "org/apache/http/entity/mime/Header.html#Header","title":
            "Header 90 Elements, 27.8% Coverage"},"children":[]},{"id":
          "HttpMultipart141","name":"HttpMultipart","data":{"$area":110.0,
            "$color":87.27273,"path":
            "org/apache/http/entity/mime/HttpMultipart.html#HttpMultipart",
            "title":"HttpMultipart 110 Elements, 87.3% Coverage"},"children":
          []},{"id":"HttpMultipartMode251","name":"HttpMultipartMode","data":
          {"$area":0.0,"$color":-100.0,"path":
            "org/apache/http/entity/mime/HttpMultipartMode.html#HttpMultipartMode",
            "title":"HttpMultipartMode 0 Elements,  -  Coverage"},"children":
          []},{"id":"MIME251","name":"MIME","data":{"$area":0.0,"$color":
            -100.0,"path":"org/apache/http/entity/mime/MIME.html#MIME",
            "title":"MIME 0 Elements,  -  Coverage"},"children":[]},{"id":
          "MinimalField251","name":"MinimalField","data":{"$area":14.0,
            "$color":57.14286,"path":
            "org/apache/http/entity/mime/MinimalField.html#MinimalField",
            "title":"MinimalField 14 Elements, 57.1% Coverage"},"children":[]},
        {"id":"MultipartEntity265","name":"MultipartEntity","data":{"$area":
            73.0,"$color":84.9315,"path":
            "org/apache/http/entity/mime/MultipartEntity.html#MultipartEntity",
            "title":"MultipartEntity 73 Elements, 84.9% Coverage"},
          "children":[]}]}]}

 ); 