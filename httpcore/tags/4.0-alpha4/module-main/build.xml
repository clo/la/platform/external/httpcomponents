<?xml version="1.0" encoding="UTF-8"?>
<!-- 
   $HeadURL$
   $Revision$
   $Date$

   ====================================================================
   Licensed to the Apache Software Foundation (ASF) under one
   or more contributor license agreements.  See the NOTICE file
   distributed with this work for additional information
   regarding copyright ownership.  The ASF licenses this file
   to you under the Apache License, Version 2.0 (the
   "License"); you may not use this file except in compliance
   with the License.  You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing,
   software distributed under the License is distributed on an
   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
   KIND, either express or implied.  See the License for the
   specific language governing permissions and limitations
   under the License.
   ====================================================================

   This software consists of voluntary contributions made by many
   individuals on behalf of the Apache Software Foundation.  For more
   information on the Apache Software Foundation, please see
   <http://www.apache.org/>.
 -->

<project name="HttpCoreMain" basedir="." default="compile-src">
<description>
Jakarta HttpComponents Core, Module "main"
This build file is typically imported by the HttpCore main build file.
</description>


<!-- Definition of the local environment.

        External dependencies for which there is a default location:

        External dependencies for which there is no default location:
          junit.jar           - for test and clover targets
          commons.cli.jar     - for contrib-bench
          spring.jar          - for contrib-spring
  -->
<property name="local.properties" location="../../project/build.properties" />
<property file="${local.properties}" />


<!-- external dependencies
        These are prime candidates for overriding in local.properties.
        The defaults assume again that you are building from the full tree
        of HTTP components, and that other components use the default
        build settings.
        The '.jar properties must be set to either to a JAR file in which
        the classes are packaged, or to a directory tree holding them.
  -->

<!-- no external dependencies with default location at this time -->


<!-- directory structure of the source tree -->
<property name="comp.core.home" location="${basedir}/.."/>

<property name="comp.core.main.home"
      location="${comp.core.home}/module-main"/>
<property name="comp.core.main.src"
      location="${comp.core.main.home}/src/main/java"/>
<property name="comp.core.main.tests"
      location="${comp.core.main.home}/src/test/java"/>
<property name="comp.core.main.xmpls"
      location="${comp.core.main.home}/src/examples"/>

<property name="comp.core.main.contr"
      location="${comp.core.home}/src/contrib"/>
<!-- @@@ contrib should be moved to module-main, or removed from this file -->


<!-- locations for intermediate and final build results
        For consistency with other builds, the temporary files are
        located by default below "target/", final results below "dist/".
  -->
<property name="build.core.main.home"
      location="${comp.core.main.home}/target"/>
<property name="build.core.main.classes"
      location="${build.core.main.home}/classes"/>
<property name="build.core.main.tests"
      location="${build.core.main.home}/tests"/>
<property name="build.core.main.xmpls"
      location="${build.core.main.home}/examples"/>
<property name="build.core.main.contr"
      location="${build.core.main.home}/contrib"/>
<!-- @@@ contrib should be moved to module-main, or removed from this file -->


<!-- compiler setup -->
<property name="compile.debug"       value="true"/>
<property name="compile.deprecation" value="true"/>
<property name="compile.optimize"    value="true"/>
<property name="compile.source"      value="1.4"/>
<property name="compile.target"      value="1.4"/>

<!-- versions are different for the main module source -->
<property name="compile.core.main.source" value="1.3"/>
<property name="compile.core.main.target" value="1.3"/>


<!-- build targets ======================================================== -->

<target name="echo-properties">
<echo>
Module "main"
  base directory    ${basedir}
  component base    ${comp.core.home}
  module base       ${comp.core.main.home}
  local properties  ${local.properties}

Source Tree
  src       ${comp.core.main.src}
  tests     ${comp.core.main.tests}
  examples  ${comp.core.main.xmpls}
  contrib   ${comp.core.main.contr}

Dependencies
  JUnit     ${junit.jar}

Output
  temp      ${build.core.main.home}
</echo>
</target>


<!-- compile and clean targets ================================================
        For compiling and recompiling specific parts of HttpCore "main" code.
        These targets do NOT define dependencies between eachother.
        Direct invocation of these targets is for those who know what they do!
  -->

<target name="compile-src" depends="build-init">

	<mkdir dir="${build.core.main.classes}" />
        <javac destdir     ="${build.core.main.classes}"
               debug       ="${compile.debug}"
               deprecation ="${compile.deprecation}"
               optimize    ="${compile.optimize}"
               encoding    ="UTF-8"
               source      ="${compile.core.main.source}"
               target      ="${compile.core.main.target}"
               sourcepath  =""
        >
          <src>
            <pathelement location="${comp.core.main.src}"/>
          </src>
          <exclude name="org/apache/http/impl/**" />
        </javac>

        <javac destdir     ="${build.core.main.classes}"
               debug       ="${compile.debug}"
               deprecation ="${compile.deprecation}"
               optimize    ="${compile.optimize}"
               encoding    ="UTF-8"
               source      ="${compile.core.main.source}"
               target      ="${compile.core.main.target}"
               sourcepath  =""
        >
          <src>
            <pathelement location="${comp.core.main.src}"/>
          </src>
          <include name="org/apache/http/impl/**" />
        </javac>
</target>
<target name="clean-src">
	<delete dir="${build.core.main.classes}" quiet="true" />
</target>


<target name="compile-tests" depends="build-init">

	<mkdir dir="${build.core.main.tests}" />
        <javac destdir     ="${build.core.main.tests}"
               debug       ="${compile.debug}"
               deprecation ="${compile.deprecation}"
               optimize    ="${compile.optimize}"
               encoding    ="UTF-8"
               source      ="${compile.core.main.source}"
               target      ="${compile.core.main.target}"
               sourcepath  =""
        >
          <src>
            <pathelement path="${comp.core.main.tests}"/>
          </src>
          <classpath>
            <pathelement location="${build.core.main.classes}"/>
            <pathelement location="${junit.jar}"/>
          </classpath>
        </javac>
        <copy todir="${build.core.main.tests}" filtering="on">
          <fileset dir="${comp.core.main.tests}" excludes="**/*.java" />
        </copy>
</target>
<target name="clean-tests">
	<delete dir="${build.core.main.tests}" quiet="true" />
</target>


<target name="compile-examples" depends="build-init">
	<mkdir dir="${build.core.main.xmpls}" />
        <javac destdir     ="${build.core.main.xmpls}"
               debug       ="${compile.debug}"
               deprecation ="${compile.deprecation}"
               optimize    ="${compile.optimize}"
               encoding    ="UTF-8"
               source      ="${compile.source}"
               target      ="${compile.target}"
               sourcepath  =""
        >
          <src>
            <pathelement path="${comp.core.main.xmpls}"/>
          </src>
          <classpath>
            <pathelement location="${build.core.main.classes}"/>
          </classpath>
        </javac>
</target>
<target name="clean-examples">
	<delete dir="${build.core.main.xmpls}" quiet="true" />
</target>


<target name="compile-contrib" depends="build-init">
	<mkdir dir="${build.core.main.contr}" />
        <javac destdir     ="${build.core.main.contr}"
               debug       ="${compile.debug}"
               deprecation ="${compile.deprecation}"
               optimize    ="${compile.optimize}"
               encoding    ="UTF-8"
               source      ="${compile.source}"
               target      ="${compile.target}"
               sourcepath  =""
        >
          <src>
            <pathelement path="${comp.core.main.contr}" />
          </src>

          <include name="org/apache/http/contrib/**" />
          <exclude name="org/apache/http/contrib/spring/**" />
          <exclude name="org/apache/http/contrib/benchmark/**" />

          <classpath>
            <pathelement location="${build.core.main.classes}"/>
          </classpath>
        </javac>
</target>
<target name="clean-contrib">
	<delete dir="${build.core.main.contr}" quiet="true" />
</target>


<target name="warn-no-bench" unless="commons.cli.jar">
<echo level="warning"
    message="Can not compile benchmark, ${commons.cli.jar} not defined."
/>
</target>
<target name="compile-contrib-bench" depends="build-init,warn-no-bench"
        if="commons.cli.jar"
>
	<mkdir dir="${build.core.main.contr}" />
        <javac destdir     ="${build.core.main.contr}"
               debug       ="${compile.debug}"
               deprecation ="${compile.deprecation}"
               optimize    ="${compile.optimize}"
               encoding    ="UTF-8"
               source      ="${compile.source}"
               target      ="${compile.target}"
               sourcepath  =""
        >
          <src>
            <pathelement path="${comp.core.main.contr}" />
          </src>

          <include name="org/apache/http/contrib/benchmark/**" />

          <classpath>
            <pathelement location="${build.core.main.classes}"/>
            <pathelement location="${commons.cli.jar}"/>
          </classpath>
        </javac>
</target>
<target name="clean-contrib-bench">
	<delete quiet="true"
             dir="${build.core.main.contr}/org/apache/http/contrib/benchmark"
        />
</target>


<target name="warn-no-spring" unless="spring.jar">
<echo level="warning"
    message="Can not compile Spring demo, ${spring.jar} not defined."
/>
</target>
<target name="compile-contrib-spring" depends="build-init,warn-no-spring"
        if="spring.jar"
>
	<mkdir dir="${build.core.main.contr}" />
        <javac destdir     ="${build.core.main.contr}"
               debug       ="${compile.debug}"
               deprecation ="${compile.deprecation}"
               optimize    ="${compile.optimize}"
               encoding    ="UTF-8"
               source      ="${compile.source}"
               target      ="${compile.target}"
               sourcepath  =""
        >
          <src>
            <pathelement path="${comp.core.main.contr}" />
          </src>

          <include name="org/apache/http/contrib/spring/**" />

          <classpath>
            <pathelement location="${build.core.main.classes}"/>
            <pathelement location="${spring.jar}"/>
          </classpath>
        </javac>
</target>
<target name="clean-contrib-spring">
	<delete dir="${build.core.main.contr}/org/apache/http/contrib/spring"
              quiet="true" />
</target>



<!-- verification targets ================================================= -->

<target name="run-tests"
     depends="build-init,compile-tests"
>
        <!-- don't use Ant optional JUnit tasks here -->

        <java classname="junit.textui.TestRunner"
              fork="yes" failonerror="yes"
        >
          <arg value="org.apache.http.TestAll"/>
          <classpath>
            <pathelement location="${junit.jar}"/>
            <pathelement location="${clover.jar}"/>
            <pathelement location="${build.core.main.classes}"/>
            <pathelement location="${build.core.main.tests}"/>
          </classpath>
        </java>
</target>
<target name="run-tests-fs"
     depends="clean-build,compile-src,run-tests"
/>


<!-- generic and helper targets =========================================== -->

<target name="build-init">
        <mkdir dir="${build.core.main.home}" />
</target>
<target name="clean-build">
        <delete dir="${build.core.main.home}" quiet="true" />
</target>


</project><!-- HttpCore, Module "main" -->
