    ----------
    HttpComponents HttpCore Overview
    ----------
    ----------
    ----------

HttpCore Overview

    HttpCore components implement the most fundamental aspects of the HTTP protocol. They are nonetheless 
    sufficient to develop basic client-side and server-side HTTP services with a minimal footprint and no 
    external dependencies. HttpCore components require only Java 1.3 compatible JVM.

Standards Compliance

    HttpCore components strive to conform to the following specifications endorsed by the Internet 
    Engineering Task Force (IETF) and the internet at large:

    * {{{http://www.ietf.org/rfc/rfc1945.txt}RFC 1945}} - Hypertext Transfer Protocol -- HTTP/1.0

    * {{{http://www.ietf.org/rfc/rfc2616.txt}RFC 2116}} - Hypertext Transfer Protocol -- HTTP/1.1
    
Examples

    Some examples of HttpCore components in action can be found {{{examples.html}here}}
