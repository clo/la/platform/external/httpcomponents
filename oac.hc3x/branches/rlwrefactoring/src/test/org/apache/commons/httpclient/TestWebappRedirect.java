/*
 * $Header: /home/jerenkrantz/tmp/commons/commons-convert/cvs/home/cvs/jakarta-commons//httpclient/src/test/org/apache/commons/httpclient/Attic/TestWebappRedirect.java,v 1.1.2.7 2001/10/01 16:58:05 rwaldhoff Exp $
 * $Revision: 1.1.2.7 $
 * $Date: 2001/10/01 16:58:05 $
 * ====================================================================
 *
 * The Apache Software License, Version 1.1
 *
 * Copyright (c) 1999 The Apache Software Foundation.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution, if
 *    any, must include the following acknowlegement:
 *       "This product includes software developed by the
 *        Apache Software Foundation (http://www.apache.org/)."
 *    Alternately, this acknowlegement may appear in the software itself,
 *    if and wherever such third-party acknowlegements normally appear.
 *
 * 4. The names "The Jakarta Project", "Tomcat", and "Apache Software
 *    Foundation" must not be used to endorse or promote products derived
 *    from this software without prior written permission. For written
 *    permission, please contact apache@apache.org.
 *
 * 5. Products derived from this software may not be called "Apache"
 *    nor may "Apache" appear in their names without prior written
 *    permission of the Apache Group.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 * [Additional notices, if required by prior licensing conditions]
 *
 */

package org.apache.commons.httpclient;

import junit.framework.*;
import org.apache.commons.httpclient.methods.*;
import java.net.URLEncoder;

/**
 * This suite of tests depends upon the httpclienttest webapp,
 * which is available in the httpclient/src/test-webapp
 * directory in the CVS tree.
 * <p>
 * The webapp should be deployed in the context "httpclienttest"
 * on a servlet engine running on port 8080 on the localhost
 * (IP 127.0.0.1).
 * <p>
 * You can change the assumed port by setting the
 * "httpclient.test.localPort" property.
 * You can change the assumed host by setting the
 * "httpclient.test.localHost" property.
 * You can change the assumed context by setting the
 * "httpclient.test.webappContext" property.
 *
 * @author Rodney Waldhoff
 * @version $Id: TestWebappRedirect.java,v 1.1.2.7 2001/10/01 16:58:05 rwaldhoff Exp $
 */
public class TestWebappRedirect extends TestWebappBase {

    public TestWebappRedirect(String testName) {
        super(testName);
    }

    public static Test suite() {
        TestSuite suite = new TestSuite(TestWebappRedirect.class);
        return suite;
    }

    public static void main(String args[]) {
        String[] testCaseName = { TestWebappRedirect.class.getName() };
        junit.textui.TestRunner.main(testCaseName);
    }

    // ------------------------------------------------------------------ Tests

    public void testRedirect() throws Exception {
        HttpClient client = new HttpClient();
        client.startSession(host, port);
        GetMethod method = new GetMethod("/" + context + "/redirect");
        method.setQueryString("to=" + URLEncoder.encode("http://" + host + ":" + port + "/" + context + "/params"));
        method.setUseDisk(false);
        try {
            client.executeMethod(method);
        } catch (Throwable t) {
            t.printStackTrace();
            fail("Unable to execute method : " + t.toString());
        }
        assertEquals(200,method.getStatusCode());
        assert(method.getResponseBodyAsString().indexOf("<title>Param Servlet: GET</title>") >= 0);
    }

    public void testRedirectWithQueryString() throws Exception {
        HttpClient client = new HttpClient();
        client.startSession(host, port);
        GetMethod method = new GetMethod("/" + context + "/redirect");
        method.setQueryString("to=" + URLEncoder.encode("http://" + host + ":" + port + "/" + context + "/params?foo=bar&bar=foo"));
        method.setUseDisk(false);
        try {
            client.executeMethod(method);
        } catch (Throwable t) {
            t.printStackTrace();
            fail("Unable to execute method : " + t.toString());
        }
        assertEquals(200,method.getStatusCode());
        assert(method.getResponseBodyAsString().indexOf("<title>Param Servlet: GET</title>") >= 0);
        assert(method.getResponseBodyAsString().indexOf("<p>QueryString=\"foo=bar&bar=foo\"</p>") >= 0);
    }

    public void testRecursiveRedirect() throws Exception {
        HttpClient client = new HttpClient();
        client.startSession(host, port);
        GetMethod method = new GetMethod("/" + context + "/redirect");

        String qs = URLEncoder.encode("http://" + host + ":" + port + "/" + context + "/params?foo=bar&bar=foo");
        for(int i=0;i<10;i++) {
            qs = "to=" + URLEncoder.encode("http://" + host + ":" + port + "/" + context + "/redirect?to=" + qs);
        }
        method.setQueryString(qs);
        method.setUseDisk(false);
        try {
            client.executeMethod(method);
        } catch (Throwable t) {
            t.printStackTrace();
            fail("Unable to execute method : " + t.toString());
        }
        assertEquals(200,method.getStatusCode());
        assert(method.getResponseBodyAsString().indexOf("<title>Param Servlet: GET</title>") >= 0);
        assert(method.getResponseBodyAsString().indexOf("<p>QueryString=\"foo=bar&bar=foo\"</p>") >= 0);
    }

    public void testDetectRedirectLoop() throws Exception {
        HttpClient client = new HttpClient();
        client.startSession(host, port);
        GetMethod method = new GetMethod("/" + context + "/redirect");
        method.setQueryString("loop=true");
        try {
            client.executeMethod(method);
            fail("Expected HTTPException");
        } catch (HttpException t) {
            // expected
        }
        assertEquals(302,method.getStatusCode());
        assert(null != method.getResponseHeader("location"));
        assert(null != (method.getResponseHeader("location")).getValue());
        assertEquals("http://" + host + (port == 80 ? "" : ":" + port) + "/" + context + "/redirect?loop=true",(method.getResponseHeader("location")).getValue());
    }

    public void testPostRedirect() throws Exception {
        HttpClient client = new HttpClient();
        client.startSession(host, port);
        PostMethod method = new PostMethod("/" + context + "/redirect");
        method.setQueryString("to=" + URLEncoder.encode("http://" + host + ":" + port + "/" + context + "/params?foo=bar&bar=foo"));
        method.addParameter("param","eter");
        method.addParameter("para","meter");
        method.setUseDisk(false);
        try {
            client.executeMethod(method);
        } catch (Throwable t) {
            t.printStackTrace();
            fail("Unable to execute method : " + t.toString());
        }
        assertEquals(200,method.getStatusCode());
        assert(method.getResponseBodyAsString().indexOf("<title>Param Servlet: POST</title>") >= 0);
        assert(method.getResponseBodyAsString().indexOf("<p>QueryString=\"foo=bar&bar=foo\"</p>") >= 0);
        assert(method.getResponseBodyAsString().indexOf("name=\"para\";value=\"meter\"") >= 0);
        assert(method.getResponseBodyAsString().indexOf("name=\"param\";value=\"eter\"") >= 0);
    }

    public void testPutRedirect() throws Exception {
        HttpClient client = new HttpClient();
        client.startSession(host, port);
        PutMethod method = new PutMethod("/" + context + "/redirect");
        method.setFollowRedirects(true);
        method.setQueryString("to=" + URLEncoder.encode("http://" + host + ":" + port + "/" + context + "/body?foo=bar&bar=foo"));
        method.setRequestBody("This is data to be sent in the body of an HTTP PUT.");
        try {
            client.executeMethod(method);
        } catch (Throwable t) {
            t.printStackTrace();
            fail("Unable to execute method : " + t.toString());
        }
        assert(method.getResponseBodyAsString(),method.getResponseBodyAsString().indexOf("<tt>This is data to be sent in the body of an HTTP PUT.</tt>") >= 0);
        assertEquals(200,method.getStatusCode());
    }
}

