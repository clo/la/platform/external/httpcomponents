<project name="HTTP Client" default="compile" basedir=".">
<!--
        "HTTP Client" component of the Jakarta Commons Subproject
        $Id: build.xml,v 1.6.2.3 2001/08/21 01:11:17 rwaldhoff Exp $
-->

<!-- ========== Properties: Property Files  =============================== -->

  <property file="${basedir}/build.properties"/>     <!-- Component local   -->
  <property file="${basedir}/../build.properties"/>  <!-- Commons local     -->
  <property file="${user.home}/build.properties"/>   <!-- User local        -->

<!-- ========== Properties: External Dependencies ========================= -->

  <property name="junit.home"              value="/usr/local/junit3.5"/>

<!-- ========== Properties: Derived Values ================================ -->

  <property name="junit.jar"               value="${junit.home}/junit.jar"/>

<!-- ========== Properties: Component Declarations ======================== -->

  <!-- The name of this component -->
  <property name="component.name"          value="httpclient"/>

  <!-- The title of this component -->
  <property name="component.title"         value="HTTP Client Library"/>

  <!-- The current version number of this component -->
  <property name="component.version"       value="1.0-dev"/>

<!-- ========== Properties: Source Directories ============================ -->

  <!-- The base directory for component configuration files -->
  <property name="conf.home"               value="src/conf"/>

  <!-- The base directory for component sources -->
  <property name="source.home"             value="src/java"/>

  <!-- The base directory for documenation -->
  <property name="docs.home"               value="docs"/>

  <!-- The base directory for unit test sources -->
  <property name="test.home"               value="src/test"/>

  <!-- The base directory for test webapp sources -->
  <property name="test-webapp.source.home"       value="src/test-webapp/src"/>

  <!-- The base directory for test webapp configuration files -->
  <property name="test-webapp.conf.home"         value="src/test-webapp/conf"/>

<!-- ========== Properties: Destination Directories ======================= -->

  <!-- The base directory for compilation targets -->
  <property name="build.home"              value="target"/>

  <!-- The base directory for distribution targets -->
  <property name="dist.home"               value="dist"/>

  <!-- The build destination for the test webapp -->
  <property name="test-webapp.dest"        value="${build.home}"/>

<!-- ========== Compiler Defaults ========================================= -->

  <!-- Should Java compilations set the 'debug' compiler option? -->
  <property name="compile.debug"           value="true"/>

  <!-- Should Java compilations set the 'deprecation' compiler option? -->
  <property name="compile.deprecation"     value="false"/>

  <!-- Should Java compilations set the 'optimize' compiler option? -->
  <property name="compile.optimize"        value="true"/>

  <!-- Construct compile classpath -->
  <path id="compile.classpath">
    <pathelement location="${build.home}/classes"/>
    <pathelement location="${junit.jar}"/>
    <pathelement location="${jsse.jar}"/>
    <pathelement location="${jnet.jar}"/>
    <pathelement location="${log4j.jar}"/>
  </path>

<!-- ========== Test Execution Defaults =================================== -->

  <!-- Construct unit test classpath -->
  <path id="test.classpath">
    <pathelement location="${build.home}/classes"/>
    <pathelement location="${build.home}/tests"/>
    <pathelement location="${junit.jar}"/>
    <pathelement location="${jsse.jar}"/>
    <pathelement location="${jcert.jar}"/>
    <pathelement location="${jnet.jar}"/>
    <pathelement location="${log4j.jar}"/>
    <pathelement location="${conf.home}"/>
    <pathelement location="${servlet.jar}"/>
  </path>

  <!-- Should all tests fail if one does? -->
  <property name="test.failonerror"    value="true"/>

  <!-- The root test to execute -->
  <property name="test.runner"         value="junit.textui.TestRunner"/>
  <property name="test.entry"          value="org.apache.commons.httpclient.TestAll"/>
  <property name="test-local.entry"    value="org.apache.commons.httpclient.TestAllLocal"/>
  <property name="test-external.entry" value="org.apache.commons.httpclient.TestAllExternal"/>
  <property name="test-nohost.entry"   value="org.apache.commons.httpclient.TestNoHost"/>

  <!-- HTTPS protocol handler, needed for HTTPS unit tests -->
  <property name="java.protocol.handler.pkgs" value="com.sun.net.ssl.internal.www.protocol"/>

  <!-- log type to use -->
  <property name="httpclient.log" value="org.apache.commons.httpclient.log.NoOpLog"/>

  <!-- The default context name for the test webapp -->
  <property name="httpclient.test.webappContext" value="httpclienttest"/>

  <!-- Anakia props -->

  <property name="docs.src" value="./xdocs"/>
  <property name="docs.dest" value="./docs"/>

<!-- ========== Targets =================================================== -->

<!-- ========== Targets: "Internal" Targets =============================== -->

  <target name="init"
          description="Initialize and evaluate conditionals">
    <echo message="-------- ${component.title} ${component.version} --------"/>
    <filter token="name"    value="${component.name}"/>
    <filter token="version" value="${component.version}"/>
  </target>

  <target name="prepare" depends="init"
          description="Prepare build directory">
    <mkdir dir="${build.home}"/>
    <mkdir dir="${build.home}/classes"/>
    <mkdir dir="${build.home}/conf"/>
    <mkdir dir="${build.home}/docs"/>
    <mkdir dir="${build.home}/docs/api"/>
    <mkdir dir="${build.home}/tests"/>
    <mkdir dir="${test-webapp.dest}"/>
    <mkdir dir="${test-webapp.dest}/${httpclient.test.webappContext}"/>
    <mkdir dir="${test-webapp.dest}/${httpclient.test.webappContext}/WEB-INF"/>
    <mkdir dir="${test-webapp.dest}/${httpclient.test.webappContext}/WEB-INF/classes"/>
  </target>

  <target name="static" depends="prepare"
          description="Copy static files to build directory">
    <tstamp/>
    <copy todir="${build.home}/conf" filtering="on">
      <fileset dir="${conf.home}" includes="*.MF"/>
      <fileset dir="${conf.home}" includes="*.properties"/>
    </copy>
  </target>

<!-- ========== Targets: "External" Targets =============================== -->

  <target name="dist" depends="compile,doc"
          description="Create binary distribution">
    <mkdir dir="${dist.home}"/>
    <copy file="../LICENSE" todir="${dist.home}"/>
    <copy file="README.txt" todir="${dist.home}"/>
    <jar jarfile  ="${dist.home}/commons-${component.name}.jar"
         basedir  ="${build.home}/classes"
         manifest ="${build.home}/conf/MANIFEST.MF"/>
    <mkdir dir="${dist.home}/src"/>
    <copy todir="${dist.home}/src" filtering="on">
      <fileset dir="${source.home}"/>
    </copy>
  </target>

<!-- ========== Targets: "External" Targets: Clean-up ===================== -->

  <target name="clean"
          description="Clean build and distribution directories">
    <delete dir="${build.home}"/>
    <delete dir="${dist.home}"/>
  </target>

  <target name="all" depends="clean,compile"
          description="Clean and compile all components"/>

  <target name="clean.test-webapp"
          description="Clean test web app directories">
    <delete dir="${test-webapp.dest}/${httpclient.test.webappContext}"/>
  </target>

<!-- ========== Targets: "External" Targets: Compilation ================== -->

  <target name="compile" depends="static"
          description="Compile shareable components">
    <javac srcdir      ="${source.home}"
           destdir     ="${build.home}/classes"
           debug       ="${compile.debug}"
           deprecation ="${compile.deprecation}"
           optimize    ="${compile.optimize}">
      <classpath refid="compile.classpath"/>
    </javac>
    <copy todir="${build.home}/classes" filtering="on">
      <fileset dir="${source.home}" excludes="**/*.java"/>
    </copy>
  </target>

  <target name="compile.tests" depends="compile"
          description="Compile unit test cases">
    <javac srcdir      ="${test.home}"
           destdir     ="${build.home}/tests"
           debug       ="${compile.debug}"
           deprecation ="${compile.deprecation}"
           optimize    ="${compile.optimize}">
      <classpath refid="test.classpath"/>
    </javac>
    <copy todir="${build.home}/tests" filtering="on">
      <fileset dir="${test.home}" excludes="**/*.java"/>
    </copy>
  </target>

  <target name="compile.test-webapp"
          description="Compile test webapp used by unit tests">
    <javac srcdir      ="${test-webapp.source.home}"
           destdir     ="${test-webapp.dest}/${httpclient.test.webappContext}/WEB-INF/classes"
           debug       ="${compile.debug}"
           deprecation ="${compile.deprecation}"
           optimize    ="${compile.optimize}">
      <classpath refid="test.classpath"/>
    </javac>
    <copy todir="${test-webapp.dest}/${httpclient.test.webappContext}/WEB-INF/classes"
          filtering="off">
      <fileset dir="${test-webapp.source.home}" excludes="**/*.java"/>
    </copy>
    <copy todir="${test-webapp.dest}/${httpclient.test.webappContext}/WEB-INF/"
          filtering="off">
      <fileset dir="${test-webapp.conf.home}"/>
    </copy>
  </target>

<!-- ========== Targets: "External" Targets: Testing ====================== -->

  <target name="test" depends="compile.tests" if="test.entry"
          description="Run all unit test cases">
      <java classname="${test.runner}" fork="yes" failonerror="${test.failonerror}">
        <jvmarg value="-Djava.protocol.handler.pkgs=${java.protocol.handler.pkgs}"/>
        <jvmarg value="-Dhttpclient.log=${httpclient.log}"/>
        <arg value="${test.entry}"/>
        <classpath refid="test.classpath"/>
      </java>
  </target>

  <target name="test-nohost" depends="compile.tests" if="test-nohost.entry"
          description="Run all test cases that run in the local VM only.">
      <java classname="${test.runner}" fork="yes" failonerror="${test.failonerror}">
        <jvmarg value="-Djava.protocol.handler.pkgs=${java.protocol.handler.pkgs}"/>
        <jvmarg value="-Dhttpclient.log=${httpclient.log}"/>
        <arg value="${test-nohost.entry}"/>
        <classpath refid="test.classpath"/>
      </java>
  </target>

  <target name="test-local" depends="compile.tests" if="test-local.entry"
          description="Run all test cases that depend upon the local webserver">
      <java classname="${test.runner}" fork="yes" failonerror="${test.failonerror}">
        <jvmarg value="-Djava.protocol.handler.pkgs=${java.protocol.handler.pkgs}"/>
        <jvmarg value="-Dhttpclient.log=${httpclient.log}"/>
        <arg value="${test-local.entry}"/>
        <classpath refid="test.classpath"/>
      </java>
  </target>

  <target name="test-external" depends="compile.tests" if="test-external.entry"
          description="Run all test cases that depend upon an external internet connection.">
      <java classname="${test.runner}" fork="yes" failonerror="${test.failonerror}">
        <jvmarg value="-Djava.protocol.handler.pkgs=${java.protocol.handler.pkgs}"/>
        <jvmarg value="-Dhttpclient.log=${httpclient.log}"/>
        <arg value="${test-external.entry}"/>
        <classpath refid="test.classpath"/>
      </java>
  </target>

<!-- ========== Targets: "External" Targets: Documenation ================= -->

  <target name="doc" depends="javadoc"
          description="Create component documentation.">
    <mkdir dir="${dist.home}"/>
    <mkdir dir="${dist.home}/docs"/>
    <copy todir="${dist.home}/docs" filtering="off">
      <fileset dir="docs"/>
    </copy>
  </target>

  <target name="javadoc" depends="compile"
          description="Create component Javadoc documentation">
    <mkdir dir="${dist.home}"/>
    <mkdir dir="${dist.home}/docs"/>
    <mkdir dir="${dist.home}/docs/api"/>
    <javadoc sourcepath   ="${source.home}"
             destdir      ="${dist.home}/docs/api"
             packagenames ="org.apache.commons.*"
             author       ="true"
             private      ="true"
             version      ="true"
             doctitle     ="&lt;h1&gt;${component.title}&lt;/h1&gt;"
             windowtitle  ="${component.title} (Version ${component.version})"
             bottom       ="Copyright (c) 2001 - Apache Software Foundation">
      <classpath refid="test.classpath"/>
    </javadoc>
  </target>

  <target name="xdoc.fetch-stylesheet" unless="localstylesheet">
      <echo>
       ####################################################################
       #
       #  Fetching the latest stylesheet from jakarta-site2
       #
       #  NOTE : As this build target is meant for developers, this requires
       #    a properly setup CVS.  But you are encouraged to use this to
       #    experiment with Anakia - if the fetch fails, it may be because
       #    you haven't yet logged into CVS. The way to do it, assuming you
       #    have a resonable CVS client setup is
       #
       #  $ cvs -d :pserver:anoncvs@cvs.apache.org:/home/cvspublic login
       #  password: anoncvs
       #
       #  and that should solve it.
       #
       #  See http://jakarta.apache.org/site/cvsindex.html for more
       #      information, or http://www.cvshome.org/
       #
       #  Ant really is the bee's knees. http://jakarta.apache.org/ant/
       #
       ######################################################################
       </echo>

      <cvs cvsRoot=":pserver:anoncvs@cvs.apache.org:/home/cvspublic"
          command="checkout -p jakarta-site2/xdocs/stylesheets/site.vsl"
          output="${docs.src}/stylesheets/site.vsl"
      />
	</target>

  <target name="xdoc" depends="xdoc.fetch-stylesheet"
          description="Generates HTML documentation from XML source">
      <taskdef name="anakia" classname="org.apache.velocity.anakia.AnakiaTask">
          <classpath location="${velocity.jar}"/>
          <classpath location="${jdom.jar}"/>
          <classpath location="${xerces.jar}"/>
      </taskdef>

      <echo>
       #######################################################
       #
       #  Now using Anakia to transform our XML documentation
       #  to HTML.
       #
       #######################################################
       </echo>

      <anakia basedir="${docs.src}" destdir="${docs.dest}/"
           extension=".html" style="./site.vsl"
           projectFile="stylesheets/project.xml"
           excludes="**/stylesheets/** empty.xml"
           includes="**/*.xml"
           lastModifiedCheck="true"
           templatePath="xdocs/stylesheets">
      </anakia>

      <copy todir="${docs.dest}/images" filtering="no">
          <fileset dir="${docs.src}/images">
              <include name="**/*.gif"/>
              <include name="**/*.jpeg"/>
              <include name="**/*.jpg"/>
          </fileset>
      </copy>
  </target>

</project>