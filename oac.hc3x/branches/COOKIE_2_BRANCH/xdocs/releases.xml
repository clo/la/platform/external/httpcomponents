<?xml version="1.0" encoding="ISO-8859-1"?>

<document>

  <properties>
    <title>Creating HttpClient Package Releases</title>
    <author email="commons-dev@jakarta.apache.org">Commons Documentation Team</author>
    <author email="jsdever@apache.org">Jeff Dever</author>
    <revision>$Id$</revision>
  </properties>

  <body>

    <section name="Introduction">

      <p>The <em>Jakarta Commons HttpClient</em> project is following the new
        wave of projects that are using Maven to assist in building 
        the packages and all the documentation including javadoc and the 
        website.  These instructions describe the release process for 
        <em>HttpClient</em>.  While other commons projects may be similar,
        these instructions are <em>HttpClient</em> specific, but can be
        adapted to suit other projects.
      </p>

    </section>


    <section name="Step By Step Instructions">

      <p>The following steps are required to create and deploy a release version
        of a Commons library package.  The <code>example text</code> consistently
        assumes that we are releasing version <em>2.0</em> of the <em>httpclient</em>
        package.</p>

      <ol>

        <li>Announce your proposed release of a particular package to the
          <strong>commons-httpclient-dev@jakarta.apache.org</strong> mailing list,
          and ask for a vote.  Per the Commons Project charter, votes of
          committers on the particular package in question (as listed in the
          <code>project.xml</code> file) are binding.<br/><br/></li>

        <li>Check out and thoroughly test the package code that you plan to
          release.<br/><br/></li>

        <li>Update release_notes.txt.  This file should be updated in CVS when
          patches are committed, but it is frequently not.  Be sure all relevant
          changes since the last release are included.  This file should be published
          along with the release source and binary distributions.<br/><br/></li>

        <li>Update the project version number in the <code>build.xml</code>, 
          <code>project.xml</code>, <code>status.xml</code>, <code>downloads.xml</code>, 
          and the HttpMethodBase useragent string. There is an Ant property named 
          <code>component.version</code> that would be updated to <code>2.0</code>.  
          Check in any files you have modified.<br/><br/></li>

        <li>In your local repository (or on cvs.apache.org) tag <strong>only</strong> the 
          files in the subdirectory for this package with the package name (in caps) 
          and version number for the package you are creating.  For example,
          <pre>
            cd $JAKARTA_COMMONS_HOME/httpclient
            cvs tag HTTPCLIENT_2_0_BETA1
          </pre>
        </li>

        <li>Regenerate the binary distribution of the code by running
          <code>maven dist</code>.  Ensure that you use the lowest reasonable jdk
          to do the build, 1.2.2.  Review the generated documentation
          to ensure that it correctly reflects the functionality (and the
          version number) of this code.<br/><br/></li>

        <li>Create a news item.  This should be added to httpclient/xdocs/news.xml and 
          the jakarta-site2/xdocs/site/news.xml as well as a one liner in
          jakarta-site2/xdocs/index.html<br/><br/></li>

        <li>Run <code>maven site:generate</code> to generate the website documentation.  Browse
          the generated docs in the target directory to ensure that they
          are correct and complete.  Modify the xdocs as required and when
          satisfied run <code>maven -Dmaven.username=your.apache.id@ site:deploy</code>
          to deploy the site to daedalus.<br/><br/></li>

        <li>SSH to daedalus (aka jakarta.apache.org) and create a new subdirectory for the
          release you are about to create.  For example:
          <pre>
            cd /www/jakarta.apache.org/builds/jakarta-commons/release/commons-httpclient/
            mkdir v2.0
          </pre>
          NOTE: Make sure that the directory you create is group writable.</li>

        <li>Upload the binary and source distribution files to the newly created directory
          on daedalus.
          <pre>
            scp target/distributions/* \
            your_apache_id@jakarta.apache.org:\
            /www/jakarta.apache.org/builds/jakarta-commons/release/commons-httpclient/v2.0/
          </pre>
          NOTE: Make sure that the files you copy are group writable.</li>

        <li>The release packages must also be uploaded to www.apache.org which is also
          hosted by daedalus.
          <pre>
            scp target/distributions/commons-httpclient-2.0-src.* \
              your_apache_id@jakarta.apache.org:\
              /www/www.apache.org/dist/jakarta/commons/httpclient/source
            scp target/distributions/commons-httpclient-2.0.* \
              your_apache_id@jakarta.apache.org:\
              /www/www.apache.org/dist/jakarta/commons/httpclient/binary
          </pre>
          NOTE: Make sure that the files you copy are group writable.</li>

        <li>Follow standard procedures to update the Jakarta web site (stored in
          CVS repository <code>jakarta-site2</code> to reflect the availability
          of the new release.  Generally, you will be updating the following
          pages:
          <ul>
            <li><code>xdocs/site/binindex.xml</code> - Create a link to the
              release directory under the <strong>Release Builds</strong>
              heading.</li>
            <li><code>xdocs/site/sourceindex.xml</code> - Create a link to the
              release directory under the <strong>Release Builds</strong>
              heading.</li>
            <li><code>xdocs/site/news.xml</code> - Create a news item that
              describes the new release, and includes hyperlinks to the
              release directory.</li>
            <li><code>xdocs/index.xml</code> - Create a one line news item
              that links to the full item in site/news.xml.</li>
          </ul>
          <ul>
            <li>Then run ant at the base to generate the new docs and commit the changes to jakarta-site2.
              You will be committing the updated xdocs and html.
              <pre>
                ant
                cvs commit -m "update to reflect release of commons-httpclient"
            </pre></li>
            <li>If you have an account on daedalus, log in and update the web site:
              <pre>
                cd /www/jakarta.apache.org
                cvs update index.html site
            </pre></li>
          </ul>
        </li>

        <li>Update components.xml in the jakarta-commons/xdocs CVS and then build the 
          docs by running ant (this may require some configuration).  CVS commit, 
          then if you have an account on daedalus, update the commons website with a 
          cvs udpate in /www/jakarta.apache.org/commons.<br/></li>

        <li>Announce the availability of the new package on the following mailing lists:
          <ul>
            <li>announcements@jakarta.apache.org</li>
            <li>commons-dev@jakarta.apache.org</li>
            <li>commons-user@jakarta.apache.org</li>
            <li>commons-httpclient-dev@jakarta.apache.org</li>
        </ul><br/></li>

        <li>Check in bugzilla for all bugs which have been marked <code>LATER</code> 
          and update their status appropriately. If you need to have some changes made 
          to bugzilla you can contact <a href="mailto:mvdb@apache.org">mvdb@apache.org</a><br/><br/></li>

        <li>Deploy the jar file to the ibiblio repository.
          <pre>
            maven jar:deploy
          </pre>
        </li>
      </ol>


    </section>

  </body>
</document>
