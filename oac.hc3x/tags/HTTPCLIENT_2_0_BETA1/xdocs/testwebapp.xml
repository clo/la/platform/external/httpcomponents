<?xml version="1.0" encoding="ISO-8859-1"?>

<document>

  <properties>
    <title>Webapp Test HowTo</title>
    <author email="olegk@apache.org">Oleg Kalnichevski</author>
    <author email="jsdever@apache.org">Jeff Dever</author>
    <revision>$Id: testwebapp.xml,v 1.3 2003/02/25 02:10:18 jsdever Exp $</revision>
  </properties>

  <body>
    <section name="Rationale">
      <p>Complex software development efforts
        with many developers collaborating on the same code base require a
        means of ensuring that changes introduced in one module do not
        produce a rippling effect on other modules by breaking some
        dependencies or assumptions. One of the widely employed approaches to
        achieve such goal calls for simultaneous development of software
        units and corresponding test cases. The latter can be executed
        against the former in order to ensure that the software units comply
        with their contract or programming interface. Such suite of test
        cases helps developers ensure formal validity and integrity of the
        their software artifact while it evolves through multiple development
        cycles.
      </p>
      <p>JUnit, a popular open-source unit
        testing framework, has been employed in the HttpClient project.
        Please refer to the JUnit web site for more detailed information
        <A HREF="http://www.junit.org/index.htm">http://www.junit.org/index.htm</A>
      </p>
      <p>Different types of unit tests in the HttpClient project
        <ul>
          <li>Test cases that have no external dependencies in order to
            execute;</li>
          <li>Test cases that require access to external public resources
            (usually high profile web sites);</li>
          <li>Test cases that must be run against a local web application
            that implements test specific functions and produces specific HTTP
            responses. These tests are the most critical component of the entire
            test suite as they allow for testing of most arcane aspects of the
            HTTP protocol that are not easily reproducible with external
            resources.</li>
        </ul>
      </p>
      <p>It is absolutely crucial that patches
        are tested for compliance with the entire test  suite in order to be
        considered for acceptance into the main CVS trunk</p>

      <p>The purpose of this document is to help
        potential HttpClient contributors compile and deploy the test web
        application (later referred as Test WebApp)</p>
    </section>

    <section name="Test WebApp external dependencies">

      <p>In order to successfully compile the
        Test WebApp we need satisfy the following external dependencies 
      </p>
      <ul>
        <li><a HREF="http://jakarta.apache.org/tomcat/">Apache Jakarta Tomcat</a><br/> 
          servlet engine version 4.0.3 or higher.  Technically any Servlet 2.3 and JSP
          1.2 compliant servlet engine should do. However, the Test WebApp has
          been mostly used with Tomcat servlet engine.
          If you need assistance in deploying the Tomcat servlet engine please refer to this 
          <a HREF="http://jakarta.apache.org/tomcat/tomcat-4.0-doc/RUNNING.txt">document</a>
        </li>

        <li><a HREF="http://jakarta.apache.org/ant/index.html">Apache Jakarta Ant</a><br/>
          build tool version 1.5.1 or higher
        </li>

        <li><a HREF="http://www.junit.org/index.htm">JUnit</a><br/>
          unit testing framework version 3.7 or higher
        </li>

        <li><a href="http://java.sun.com/products/jsse/">Java<sup>TM</sup> Secure Socket Extension (JSSE)</a><br/>
          version 1.0.3 or higher
        </li>

      </ul>

    </section>

    <section name="Development environment">

      <p>HttpClient is a Java based framework and so is not tied to any particular platform.
        However setup instructions such as these must choose a particular enviroment
        to describe.  This document will describe the setup for j2sdk1.4 on Unix.
      </p>

      <ol>
        <li>Specify Test WebApp build properties
          <p>
            Update references in the file
            build.properties to refer to correct locations on your local system.
            The  build.properties is located in the root directory of the
            HttpClient source tree. Most importantly the location of the
            following packages must be specified: <b>junit.jar, servlet.jar</b></p>
          <p>
            For example, you can copy the build.properties.sample to your home directory 
            as build.properties and modify the following lines to match your system configuration:</p>

          <ul>
            <li>servlet.jar=/usr/local/jakarta-tomcat/common/lib/servlet.jar</li>
            <li>junit.jar=/usr/local/junit/junit.jar</li>
          </ul>
        </li>
      </ol>
    </section>

    <section name="Build Test WebApp">
      <p>
        From the root directory of the HttpClient source tree, run the following
        command:
        <blockquote>/opt/ant test-webapp-war</blockquote>
      </p>
      <p>If all external dependencies have been
        correctly specified the execution of the Ant task should result in
        creation of web archive target/httpclienttest.war.
      </p>
    </section>

    <section name="Deploy Test WebApp">
      <ul>
        <li>Shutdown Tomcat 
          <p>Run the shutdown script: <tt>/usr/local/jakarta-tomcat/bin/shutdown.sh</tt></p>
        </li>
        <li>Copy the httpclienttest.war file into the webapps directory
          <p>Your tomcat installations webapp directory might be in either
            /var/tomcat/webapps or /usr/local/jakarta-tomcat/webapps.
          </p>
          <p><b>Note:</b> You can control where ant <tt>test-webapp-war</tt> target puts the
            generated <tt>httpclienttest.war</tt> file by modifying the <tt>test-webapp.dest</tt>
            property.  This can be set to create the war file in the tomcat 
            webapps directory so you can skip this manual step.
          </p>
        </li>
        <li>Startup Tomcap
          <p>Run the startup script: /usr/local/jakarta-tomcat/bin/shutdown.sh
          </p>
          <p>Tomcat will check for new web archives in its base directory and 
            automatically deploy them.
          </p>
        </li>
        <li>Test the deployment of the httpclienttest web archive
          <p>Point your browser to the following
            URL: <A HREF="http://127.0.0.1:8080/httpclienttest/headers">
              http://127.0.0.1:8080/httpclienttest/headers</A>
          </p>
          <p>If the Test WebApp has been
            properly deployed and is ready for use you should see similar
            response in your browser</p>


          <p>This is a response to an HTTP GET request.<br/>
            <blockquote>
              Request Headers:<br/>
              name=&quot;user-agent&quot;;value=&quot;Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.3a) Phoenix/0.5&quot;<br/>
              name=&quot;accept&quot;;value=&quot;text/xml,application/xml,application/xhtml+xml,text/html<br/>
              name=&quot;accept-language&quot;;value=&quot;en-us,en;q=0.5&quot;<br/>
              name=&quot;accept-encoding&quot;;value=&quot;gzip,deflate,compress;q=0.9&quot;<br/>
              name=&quot;accept-charset&quot;;value=&quot;ISO-8859-1,utf-8;q=0.7,*;q=0.7&quot;<br/>
              name=&quot;keep-alive&quot;;value=&quot;300&quot;<br/>
              name=&quot;connection&quot;;value=&quot;keep-alive&quot;<br/>
            </blockquote>
          </p>
        </li>
      </ul>
    </section>

    <section name="Run test cases">
      <p>In case you run a non-default installation of the Tomcat servlet
        engine you may need to update the following properties in the
        build.properties file</p>

      <ul>
        <li>httpclient.test.localHost</li>
        <li>httpclient.test.localPort</li>
        <li>httpclient.test.webappContext</li>
      </ul>
      <p>run the webapp tests using Ant:<br/>
        <code>ant test-local</code> 
      </p>
      <p>You should receive BUILD SUCCESSFUL message if all test cases succeeded</p>

      <p>Disregard WARN messages. The test cases run through valid and invalid cases and as such 
        warnings are part of the HttpClient test sequence.</p>

    </section>
  </body>
</document>
