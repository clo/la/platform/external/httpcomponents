<?xml version="1.0" encoding="ISO-8859-1"?>

<document>

  <properties>
    <title>HttpClient Authentication Guide</title>
    <author email="jsdever@apache.org">Jeff Dever</author>
    <author email="adrian.sutton@ephox.com">Adrian Sutton</author>
    <revision>$Id: authentication.xml,v 1.5.2.3 2003/08/21 16:07:31 oglueck Exp $</revision>
  </properties>

  <body>

    <section name="Introduction">
      <p>HttpClient supports three different types of http authentication schemes:
      Basic, Digest and NTLM.  These can be used to authenticate with http servers
      or proxies.</p>
    </section>

	<section name="Server Authentication">
		<p>HttpClient handles authenticating with servers almost transparently,
		the only thing a developer must do is actually provide the login
		credentials.  These credentials are stored in the HttpState instance
		and can be set or retrieved using the <code>setCredentials(String realm,
		Credentials cred)</code> and <code>getCredentials(String realm)</code>
		methods.</p>

		<p><i>Note:</i> To set default Credentials for any realm that has not been
		explicitly specified, pass in <code>null</code> as the value of
		<code>realm</code>.</p>

		<p>The automatic authorization built in to HttpClient can be disabled
		with the method <code>setDoAuthentication(boolean doAuthentication)</code>
		in the HttpMethod class.  The change only affects that method instance.</p>

		<subsection name="Preemptive Authentication">
		<p>Preemptive authentication can be enabled within HttpClient.  In this
		mode HttpClient will send the basic authentication response even before
		the server gives an unauthorized response in certain situations, thus reducing the overhead
		of making the connection.  To enable this use the following:</p>

    <source>client.getState().setAuthenticationPreemptive(true);</source>

    <p>To enable preemptive authentication by default for all newly created
    <tt>HttpState</tt>'s, a system property can be set, as shown below.</p>

		<source>setSystemProperty(Authenticator.PREEMPTIVE_PROPERTY, "true");</source>

		<p>The preemptive authentication in HttpClient conforms to rfc2617:</p>

		<blockquote>A client SHOULD assume that all paths at or deeper than the depth
		of the last symbolic element in the path field of the Request-URI also
		are within the protection space specified by the Basic realm value
		of the current challenge.  A client MAY preemptively send the 
		corresponding Authorization header with requests for resources in
		that space without receipt of another challenge from the server.
		Similarly, when a client sends a request to a proxy, it may reuse
		a userid and password in the Proxy-Authorization header field without
		receiving another challenge from the proxy server.</blockquote>
		</subsection>
	</section>

	<section name="Proxy Authentication">
		<p>Proxy authentication in HttpClient is almost identical to server
		authentication with the exception that the credentials for each are
		stored independantly.  So for proxy authentication you must use
		<code>setProxyCredentials(String realm, Credentials cred)</code> and
		<code>getProxyCredentials(String realm)</code>.  As with server
		authentication, passing <code>null</code> as the realm sets or returns
		the default credentials.</p>
	</section>

	<section name="Authentication Schemes">
	<p>The following authentication schemes are supported by HttpClient.</p>
    <subsection name="Basic">
	<p>Basic authentication is the original and most compatible authentication
	scheme for HTTP.  Unfortunately, it is also the least secure as it sends
	the username and password unencrypted to the server.  Basic authentication
	requires an instance of UsernamePasswordCredentials (which NTCredentials
	extends) to be available, either for the specific realm specified by the
	server or as the default credentials.</p>
    </subsection>

    <subsection name="Digest">
		<p>Digest authentication was added in the HTTP 1.1 protocol and while
		not being as widely supported as Basic authentication there is a great
		deal of support for it.  Digest authentication is significantly more
		secure than basic authentication as it never transfers the actual
		password across the network, but instead uses it to encrypt a "nonce"
		value sent from the server.</p>

		<p>Digest authentication requires an instance of
		UsernamePasswordCredentials (which NTCredentials extends) to be
		available either for the specific realm specified by the server or as
		the default credentials.</p>
    </subsection>

    <subsection name="NTLM">
		<p>NTLM is the most complex of the authentication protocols supported
		by HttpClient.  It is a proprietary protocol designed by Microsoft
		with no publicly available specification.  Early version of NTLM were
		less secure than Digest authentication due to faults in the design,
		however these were fixed in a service pack for Windows NT 4 and the
		protocol is now considered more secure than Digest authentication.</p>

		<p>NTLM authentication requires an instance of NTCredentials be
		available for the <i>domain name</i> of the server or the default
		credentials.  Note that since NTLM does not use the notion of realms
		HttpClient uses the domain name of the server as the name of the realm.
		Also note that the username provided to the NTCredentials should not
		be prefixed with the domain - ie: "adrian" is correct whereas
		"DOMAIN\adrian" is not correct.</p>

		<p>There are some significant differences in the way that NTLM works
		compared with basic and digest authentication.  These differences
		are generally handled by HttpClient, however having an
		understanding of these differences can help avoid problems when using
		NTLM authentication.</p>

      <p>
        <ol>
          <li>NTLM authentication works almost exactly the same as any other form of
            authentication in terms of the HttpClient API.  The only difference is that
            you need to supply 'NTCredentials' instead of 'UsernamePasswordCredentials'
            (NTCredentials actually extends UsernamePasswordCredentials so you can use
            NTCredentials right throughout your application if need be).</li>

          <li>The realm for NTLM authentication is the domain name of the computer
            being connected to, this can be troublesome as servers often have
			multiple domain names that refer to them.  Only the domain name
			that HttpClient connects to (as specified by the HostConfiguration)
			is used to look up the credentials.
            It is generally advised that while initially testing NTLM
			authentication, you pass the realm in as null which is used as
			the default.</li>

          <li>NTLM authenticates a connection and not a request, so you need to
            authenticate every time a new connection is made and keeping the connection
            open during authentication is vital.  Due to this, NTLM cannot
			be used to authenticate with both a proxy and the server, nor can
			NTLM be used with HTTP 1.0 connections or servers that do not
			support HTTP keep-alives.</li>
        </ol>
      </p>
	  
      <p>For a detailed explanation of how NTLM authentication works, please see
      <a href="http://davenport.sourceforge.net/ntlm.html">
      http://davenport.sourceforge.net/ntlm.html</a>.</p>
    </subsection>
	</section>

  <section name="Examples">
      <p>There is an <a href="http://cvs.apache.org/viewcvs.cgi/*checkout*/jakarta-commons/httpclient/src/examples/BasicAuthenticatonExample.java?rev=HEAD&amp;content-type=text/plain">example</a> 
      of basic authentication available in the       
      <a href="http://cvs.apache.org/viewcvs.cgi/jakarta-commons/httpclient/src/examples/">example directory</a> in CVS.
      </p>
  </section>
  
  <section name="Troubleshooting">
      <p>Some authentication schemes may use cryptographic algorithms. It is recommended to include the
         <a href="http://java.sun.com/products/jce/" target="_blank">Java Cryptography Extension</a> in
         your runtime environment prior to JDK 1.4.  Also note that you must register the JCE
         implementation manually as HttpClient will not do so automatically.  For instance to
         register the Sun JCE implementation, you should execute the following code before attempting
         to use HttpClient.
      </p>

	  <source>
String secProviderName = "com.sun.crypto.provider.SunJCE");
java.security.Provider secProvider = 
    (java.security.Provider)Class.forName(secProviderName).newInstance();
Security.addProvider(secProvider);
	  </source>
  </section>
  </body>

</document>
