$Id: TESTING.txt,v 1.2 2001/10/04 17:49:15 rwaldhoff Exp $
================================================================
Testing HTTP Client
================================================================

DEPENDENCIES:

For testing changes the component, you will need JUnit
(version 3.4 or later), which can be obtrained from:

  http://www.junit.org/

To help the test process find JUnit, you may make a copy of the
build.properties.sample file, rename to build.properties, and
modify it to reflect the location of junit.jar.

TESTING:

You may use the provided Ant script to run the tests. There
are three primary test targets you'll want to consider:

* test-nohost
   - Runs all the "pure" unit tests--those that don't rely upon
     anything outside of the JUnit VM.

* test-local
   - Runs all of the tests that don't rely upon an external
     Internet connection. To run these tests you'll need
     a web server, and a servlet engine with the test web
     application installed.

* test
   - Runs all of the tests, including those that rely upon
     a "local" web server/servlet engine, as well as those
     that rely upon an external Internet connection.
     Developer's are encouraged to ensure that all of the
     "test" test cases pass before submitting or committing
     patches to httpclient.

BUILDING AND DEPLOYING THE TEST WEB APPLICATION:

Many of the JUnit tests make use of a web application developed
specifically for testing HTTP client.  To build the web
application, you'll need the Servlet classes (e.g., j2ee.jar
or servlet.jar).  You can obtain the Servlet API from:

  http://java.sun.com/products/servlets

To help the build and test process find the servlet API, you may
make a copy of the build.properties.sample file, rename to
build.properties, and modify it to reflect the location of
servlet.jar on your computer.

To build the web application, run the "compile.test-webapp"
ant target.  This will generate a web application directory
ready to deploy to your favorite servlet engine.

For testing purposes, this web application is assumed to
be running in the context "httpclienttest" on the web server
running on port 8080 on 127.0.0.1.  You may change these
assumptions by setting system properties to be passed to
test suite.  See TestWebapp for details.

This web application has been tested with Tomcat 3.2 and
4.0.

ADDING TESTS:

To add a new Junit TestCase to one of the
"standard" Suites, you should add it to one
(and only one) of the following files:

 * TestNoHost.java
    - use this file if the test has no
      external dependencies (i.e., it
      runs entirely within the JUnit VM)

 * TestExternalHost.java
    - use this file if the test depends
      upon a connection to the public Internet

 * TestLocalHost.java
    - use this file if the test depends upon
      a local webserver (running on
      ${httpclient.test.localHost} and
      ${httpclient.test.localPost})

 * TestWebapp.java
    - use this file if the test depends upon
      the test webapp
