Jakarta Commons HTTP Client
===========================
Welcome to the HTTP Client component of the Jakarta Commons
project.

See the docs directory, or visit us online at
   http://jakarta.apache.org/commons/httpclient
for more information.

See docs/status.html, or
   http://jakarta.apache.org/commons/httpclient/status.html
for the status of the project.