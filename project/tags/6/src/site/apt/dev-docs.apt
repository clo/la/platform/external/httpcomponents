~~ ====================================================================
~~ Licensed to the Apache Software Foundation (ASF) under one
~~ or more contributor license agreements.  See the NOTICE file
~~ distributed with this work for additional information
~~ regarding copyright ownership.  The ASF licenses this file
~~ to you under the Apache License, Version 2.0 (the
~~ "License"); you may not use this file except in compliance
~~ with the License.  You may obtain a copy of the License at
~~ 
~~   http://www.apache.org/licenses/LICENSE-2.0
~~ 
~~ Unless required by applicable law or agreed to in writing,
~~ software distributed under the License is distributed on an
~~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
~~ KIND, either express or implied.  See the License for the
~~ specific language governing permissions and limitations
~~ under the License.
~~ ====================================================================
~~ 
~~ This software consists of voluntary contributions made by many
~~ individuals on behalf of the Apache Software Foundation.  For more
~~ information on the Apache Software Foundation, please see
~~ <http://www.apache.org/>.

   -----------
   Developer Documentation Index

Developer Documentation

   You are a developer of HttpComponents if you are developing code or
   documentation for the components themselves.

   Unfortunately, this page is currently mostly a dummy. It will eventually
   point you to information about setting up a development environment,
   submitting patches, and updating information on the website.
   For the time being, the documentation on cutting releases may include
   some interesting pieces of information for you.

Release Process

   * {{{http://wiki.apache.org/HttpComponents/HttpComponentsCoreReleaseProcess}
        HttpComponents Core Release Process}}

   * {{{http://wiki.apache.org/HttpComponents/CommonsHttpClientReleaseProcess}
        Commons HttpClient 3.1 Process}}

Web Site Updates

   Instructions for updating the various sections of our website are
   {{{http://wiki.apache.org/HttpComponents/WebSite}
     in the Wiki}}.

Maven Skin details

   The HC project has created a Maven skin to enable customisation of page footers.
   This is intended to be used to add Trademark notices to pages. 

   To use the skin, add the following to the <<<src/site/site.xml>>> file:

+-------------------------------------------
<project ...

  <skin>
    <groupId>org.apache.httpcomponents</groupId>
    <artifactId>maven-site-skin</artifactId>
    <version>1.1</version>
  </skin>
+-------------------------------------------

   The skin contains an updated version of the default Maven template. The template
   checks the site.xml file for the presence of the following custom tag:

+-------------------------------------------
   <custom>
      <footer>
      Optional HTML-formatted text goes here
      </footer>
   </custom>
+--------------------------------------------

   If the tag is found, its contents are added to the footer just before the copyright statement.
   Example usage:
   
+--------------------------------------------
 <custom>
    <footer>
      <div class="xleft">Apache HttpComponents, Commons HttpClient, Apache, the Apache feather logo, 
      and the Apache HttpComponents project logo are trademarks of The Apache Software Foundation.</div>
      <br/>
      <div class="xleft">All other marks mentioned may be trademarks or registered trademarks of their respective owners.</div>
    </footer>
  </custom>
+--------------------------------------------

Editting APT documents

   The APT syntax is documented at {{http://maven.apache.org/doxia/references/apt-format.html}}