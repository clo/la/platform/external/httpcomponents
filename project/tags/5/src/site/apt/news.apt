~~ ====================================================================
~~ Licensed to the Apache Software Foundation (ASF) under one
~~ or more contributor license agreements.  See the NOTICE file
~~ distributed with this work for additional information
~~ regarding copyright ownership.  The ASF licenses this file
~~ to you under the Apache License, Version 2.0 (the
~~ "License"); you may not use this file except in compliance
~~ with the License.  You may obtain a copy of the License at
~~ 
~~   http://www.apache.org/licenses/LICENSE-2.0
~~ 
~~ Unless required by applicable law or agreed to in writing,
~~ software distributed under the License is distributed on an
~~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
~~ KIND, either express or implied.  See the License for the
~~ specific language governing permissions and limitations
~~ under the License.
~~ ====================================================================
~~ 
~~ This software consists of voluntary contributions made by many
~~ individuals on behalf of the Apache Software Foundation.  For more
~~ information on the Apache Software Foundation, please see
~~ <http://www.apache.org/>.

    ----------
    HttpComponents News
    ----------
    ----------
    ----------

HttpComponents Project News 

* 19 August 2011 - HttpComponents HttpCore 4.2-alpha1 released

    This is the first ALPHA release of the 4.2 development branch. The most notable feature 
    included in this release is support for connection pools of blocking and non-blocking HTTP 
    connections. Connection pool components are based on mature code migrated from HttpClient and 
    HttpAsyncClient modules but have a slightly different API that makes a better use of Java 
    standard concurrent primitives. Support for connection pools in HttpCore is expected to make 
    development of client and proxy HTTP services easier and less error prone.

* 7 August 2011 - HttpComponents HttpClient 4.1.2 (GA) released

    HttpClient 4.1.2 is a bug fix release that addresses a number of non-critical issues reported 
    since release 4.1.1.

* 31 July 2011 - HttpComponents HttpCore 4.1.3 (GA) released

    HttpCore 4.1.3 is a patch release that fixes a critical regression in the non-blocking SSL I/O
    session code introduced in the 4.1.2 release.

* 18 July 2011 - HttpComponents HttpCore 4.1.2 (GA) released

    HttpCore 4.1.2 is a patch release that fixes a number of non-critical issues found since 
    release 4.1.1.

* 24 May 2011 - HttpComponents HttpAsyncClient 4.0-alpha2 released

    The second ALPHA release of HttpAsyncClient 4.0 comes with a number of important improvements 
    and enhancements. As of this version HttpAsyncClient fully supports HTTP state management 
    (cookies) and HTTP authentication (basic, digest, NTLM, spnego/kerberos). Connection management 
    classes have been thoroughly reworked and improved. This version also improves support for zero 
    copy file upload / download operations.

* 20 May 2011 - HttpComponents HttpCore 4.1.1 (GA) released

    HttpCore 4.1.1 is a patch release that fixes a number of non-critical issues found since 
    release 4.1.

    This release marks the end of support for Java 1.3. As of release 4.2 HttpCore will require 
    Java 1.5 for all its components.

* 20 March 2011 - HttpComponents HttpClient 4.1.1 (GA) released

    HttpClient 4.1.1 is a bug fix release that addresses a number of issues reported since 
    release 4.1, including one critical security issue.

* 23 January 2011 - HttpComponents HttpClient 4.1 (GA) released

    The HttpClient 4.1 release builds upon the stable foundation laid by HttpClient 4.0 and adds 
    several functional improvements and popular features.

    * Response caching conditionally compliant with HTTP/1.1 specification (full compliance with
      MUST requirements, partial compliance with SHOULD requirements)

    * Full support for NTLMv1, NTLMv2, and NTLM2 Session authentication. The NTLM protocol code 
      was kindly contributed by the Lucene Connector Framework project.

    * Support for SPNEGO/Kerberos authentication.

    * Persistence of authentication data between request executions within the same execution context.

    * Support for preemptive authentication for BASIC and DIGEST schemes.
  
    * Support for transparent content encoding. Please note transparent content encoding is not 
      enabled per default in order to avoid conflicts with already existing custom content encoding 
      solutions.

    * Mechanism to bypass the standard certificate trust verification (useful when dealing with 
      self-signed certificates).

    * Simplified configuration for connection managers.

    * Transparent support for host multihoming.

* 18 January 2011 - HttpComponents HttpAsyncClient 4.0-alpha1 released

    This is the first public release of HttpAsyncClient. The HttpAsyncClient 4.0 API is considered 
    very experimental and is expected to change in the course of the ALPHA development phase. This
    release is primarily intended for early adopters who may be interested in contributing to
    the project and in helping shape the new API.

* 21 November 2010 - HttpComponents HttpClient 4.1-beta1 released

    This release finalizes the 4.1 API and brings a number of major improvements to the HTTP 
    caching module. This release also adds full support for NTLMv1, NTLMv2, and NTLM2 Session 
    authentication schemes. The NTLM protocol code was kindly contributed by the Lucene Connector 
    Framework project.

* 19 November 2010 - HttpComponents HttpCore 4.1 (GA) released

    This is the first stable release of HttpCore 4.1. This release provides a compatibility mode 
    with JREs that have a naive (broken) implementation of SelectionKey API and also improves
    compatibility with the Google Android platform. There has also been a number of performance 
    related improvements and bug fixes in both blocking and non-blocking components.

* 19 September 2010 - HttpComponents HttpClient 4.0.3 (GA) released

    This is an emergency release fixing a critical regression in the SSL
    connection management code.

* 9 September 2010 - HttpComponents HttpClient 4.0.2 (GA) released

    This is a maintenance release that fixes a number of bugs found since 4.0.1. 
    This is likely to be the last release in the 4.0.x branch.

* 30 August 2010 - HttpComponents HttpCore 4.1-beta2 released

    This release addresses fixes a number of non-critical bugs. It is likely to be 
    the last BETA release in the 4.1 branch.

* 19 May 2010 - HttpComponents HttpClient 4.1-alpha2 released

    This release fixes a number of non-severe bugs discovered since  the last release 
    and introduces support for two frequently requested features:

    * HTTP/1.1 response caching

    * transparent support for host multihoming

    * a mechanism to bypass the standard certificate trust verification (useful when 
      dealing with self-signed certificates)

* 3 April 2010 - HttpComponents HttpCore 4.1-beta1 released

    This release finalizes the API introduced in the 4.1 development branch. It also 
    fixes a number of bugs discovered since the previous release and delivers a number 
    of performance optimizations in the blocking HTTP transport components. The blocking 
    HTTP transport is expected to be 5% to 10% faster compared to previous releases.

* 11 December 2009 - HttpComponents HttpClient 4.1-alpha1 released

    This release builds on the stable 4.0 release and adds several functionality 
    improvements and new features.

    * Simplified configuration of connection managers.

    * Persistence of authentication data between request executions within 
    the same execution context.

    * Support for SPNEGO/Kerberos authentication scheme

    * Support for transparent content encoding. Please note transparent content 
    encoding is not enabled per default in order to avoid conflicts with
    already existing custom content encoding solutions.

* 11 December 2009 - HttpComponents HttpClient 4.0.1 (GA) released

    This is a bug fix release that addresses a number of issues discovered since 
    the previous stable release. None of the fixed bugs is considered critical. 
    Most notably this release eliminates dependency on JCIP annotations.

    This release is also expected to improve performance by 5 to 10% due to
    elimination of unnecessary Log object lookups by short-lived components.
    
* 12 September 2009 - HttpComponents HttpCore 4.1-alpha1 released

    This is the first public release from the 4.1 branch of HttpCore. This release 
    adds a number of new features, most notable being introduction of compatibility 
    mode with IBM JREs and other JREs with naive (broken) implementation 
    of SelectionKey API.

* 14 August 2009 - HttpComponents HttpClient 4.0 (GA) released

    This the first stable (GA) release in the 4.x code line. This release completes 
    the rewrite of HttpClient and delivers a complete API documentation and fixes 
    a few minor bugs reported since the previous release.

* 22 June 2009 - HttpComponents HttpCore 4.0.1 (GA) released

    This is a patch release addressing a number of issues discovered since the 4.0 
    release.

* 26 February 2009 - HttpComponents HttpCore 4.0 (GA) released

    This the first stable (GA) release in the 4.x code line. This release delivers 
    complete API documentation and fixes a few minor bugs reported since 
    the previous release.

* 20 December 2008 - HttpComponents HttpClient 4.0-beta2 released

    The second BETA of HttpComponents HttpClient addresses a number of issues 
    discovered since the previous release. 

    The only significant new feature is an addition of an OSGi compliant bundle 
    combining HttpClient and HttpMime jars.

    All upstream projects are strongly encouraged to upgrade.

* 19 October 2008 - HttpComponents HttpCore 4.0-beta3 released

    The third BETA version of HttpComponents Core addresses a number of issues 
    discovered since the previous release. 

    The only significant new feature is an addition of an OSGi compliant bundle
    combining HttpCore and HttpCore NIO jars.

* 12 September 2008 - HttpClient is one of the best open source development tools

    HttpClient is among the 60 winners of InfoWorlds 
    {{{http://www.infoworld.com/article/08/08/04/32TC-bossies-2008_1.html}
    "Best of Open Source Software Awards 2008"}}.

    HttpClient was selected as one of the 
    {{{http://www.infoworld.com/slideshow/2008/08/166-best_of_open_so-4.html}
    best open source development tools}}.

* 29 August 2008 - HttpComponents HttpClient 4.0-beta1 released

    The first BETA brings yet another round of API enhancements and 
    improvements in the area of connection management. Among the most notable
    ones is the capability to handle stateful connections such as persistent 
    NTLM connections and private key authenticated SSL connections.

    This is the first API stable release of HttpClient 4.0. All further 
    releases in the 4.0 code line will maintain API compatibility with this
    release.

* 22 June 2008 - HttpComponents HttpCore 4.0-beta2 released

    The second BETA version of HttpComponents Core added a number of improvements 
    to the NIO components, most notable being improved asynchronous client side and 
    server side protocol handlers.

* 09 May 2008 - HttpComponents HttpClient 4.0-alpha4 released

    The fourth ALPHA marks the completion of the overhaul of the connection 
    management code in HttpClient. All known shortcomings of the old HttpClient 
    3.x connection management API have been addressed.

* 03 May 2008 - Welcome new HttpComponents committer Sam Berlin 

    By 6 binding votes in favor and none against Sam Berlin has been voted in as a new 
    HttpComponents committer. Sam made several valuable contributions to both core 
    and client components in the course of the past several months.

    Welcome on board, Sam!

* 26 February 2008 - HttpComponents HttpClient 4.0-alpha3 released 

    The third ALPHA release brings another round of API refinements and improvements 
    in functionality. As of this release HttpClient requires Java 5 compatible 
    runtime environment and takes full advantage of generics and new concurrency
    primitives.     

    This release also introduces new default cookie policy that selects a cookie 
    specification depending on the format of cookies sent by the target host. 
    It is no longer necessary to know beforehand what kind of HTTP cookie support 
    the target host provides. HttpClient is now able to pick up either a lenient 
    or a strict cookie policy depending on the compliance level of the target host.

    Another notable improvement is a completely reworked support for multipart 
    entities based on Apache mime4j library.

* 24 January 2008 - HttpComponents HttpCore 4.0-beta1 released 

    The first BETA version of HttpComponents Core has been released. This release 
    can be considered a major milestone, as it marks the end of API instability 
    in HttpCore. As of this release the API compatibility between minor releases
    in 4.x codeline will be maintained.

    This release includes several major improvements such as enhanced HTTP message 
    parsing API and optimized parser implementations, Java 5.0 compatibility
    for HttpCore NIO extensions.

    The focus of the development efforts will be gradually shifting towards
    providing better test coverage, documentation and performance optimizations. 

* 15 November 2007 - HttpComponents becomes TLP

    The ASF board had approved HttpComponents 'graduation' from Jakarta to a TLP of its own.

    We are now Apache HttpComponents Project!

* 7 November 2007 - HttpComponents HttpClient 4.0-alpha2 released 

    The second ALPHA release is another important milestone in the redesign of HttpClient. The 
    release includes a number of improvements since ALPHA1, among which are improved connection 
    pooling, support for proxy chains, redesigned HTTP state and authentication credentials 
    management API, improved RFC 2965 cookie specification.  

* 9 October 2007 - HttpComponents HttpCore 4.0-alpha6 released 

    The sixth ALPHA version of HttpComponents Core has been released. This release sports an 
    improved message parsing and formatting API in the base module and lots of incremental 
    improvements and bug fixes in the NIO and NIOSSL modules. Based on the improved API, it is now 
    possible to send and receive SIP messages with HttpComponents Core.

* 20 July 2007 - HttpComponents HttpClient 4.0-alpha1 released 

    This release represents a complete, ground-up redesign and almost a complete rewrite of the old 
    HttpClient 3.x codeline. This release finally addresses several design flaws that existed since 
    the 1.0 release and could not be fixed without a major code overhaul and breaking API 
    compatibility. 

    Notable changes and enhancements: 

    * Redesign of the HttpClient internals addressing all known major architectural shortcomings of 
    the 3.x codeline  

    * Cleaner, more flexible and expressive API   

    * Better performance and smaller memory footprint due to a more efficient HTTP transport based 
    on HttpCore. HttpClient 4.0 is expected to be 10% to 25% faster than HttpClient 3.x codeline

    * More modular structure   

    * Pluggable redirect and authentication handlers   

    * Support for protocol incerceptors

    * Improved connection management 

    * Improved support for sending requests via a proxy or a chain of proxies 

    * Improved handling redirects of entity enclosing requests

    * More flexible SSL context customization 

    * Reduced intermediate garbage in the process of generating HTTP requests and parsing HTTP 
    responses  

* 4 July 2007 - HttpComponents HttpCore 4.0-alpha5 released 

   The fifth ALPHA version of HttpComponents Core has been released. This release delivers a number 
   of incremental improvements across the board in all modules and adds several performance oriented 
   features such as ability to transfer data directly between a file and a socket NIO channels. 

* 30 March 2007 - HttpComponents HttpCore 4.0-alpha4 released 

    The fourth ALPHA version fixes a number of bugs and adds a number of improvements to HttpCore 
    base and the HttpCore NIO extensions. This release also introduces NIOSSL extensions that can be 
    used to extend HttpCore non-blocking transport components with the ability to transparently encrypt 
    data in transit using SSL/TLS.

* 6 December 2006 - HttpComponents HttpCore 4.0-alpha3 released 

    The third ALPHA version of HttpCore has been released. The ALPHA3 release includes a number of API
    optimizations and improvements and introduces a set of NIO extensions to the HttpCore API. 
    NIO extensions can be used to build HTTP services intended to handle thousands of simultaneous 
    connections with a small number of I/O threads.

* 9 June 2006 - HttpComponents HttpCore 4.0-alpha2 released 

    The second ALPHA version of HttpCore  has been released, which addresses a number of non-critical 
    problems found in the previous release. The upstream projects are strongly encouraged use this 
    release as a dependency while HttpCore undergoes another round of reviews and optimization in the 
    SVN trunk.

* 12 May 2006 - HttpClient issue tracking migrated to Jira

    HttpClient issue tracking has migrated from Bugzilla to Jira. Please use 
    {{{http://issues.apache.org/jira/browse/HTTPCLIENT}this project}} in Jira 
    to report new issues against HttpClient and search for reported ones. All existing 
    issue reports can be accessed in Jira by their original Bugzilla bug id. 

* 29 April 2006 - New Project Logo  

    HttpComponents project now has a brand new logo kindly contributed by Regula Wernli.

    Many thanks, Regula! 

* 23 April 2006 - HttpComponents HttpCore 4.0-alpha1 released 

    This is the first ALPHA release of HttpCore intended for API review and use in 
    experimental projects. The HttpCore API is still deemed unstable and it can still 
    undergo significant changes based on the feedback from early adopters. 

* 12 February 2006 - Welcome new HttpComponents committer Roland Weber 

    By 5 binding votes in favor and none against Roland Weber has been voted in as a new 
    HttpComponents committer. Roland has been an invaluable contributor to the Jakarta 
    Commons HttpClient project for many years and he is the very first committer to
    join the Jakarta HttpComponents project.

    Welcome, Roland

* 31 October 2005 - Jakarta HttpClient becomes Jakarta HttpComponents

    By the count 15 votes in favor, Jakarta HttpClient as been renamed as Jakarta HttpComponents. 
    The Jakarta PMC has approved the new project charter and the new project scope.

* 16 April 2004 - Welcome Jakarta HttpClient!

    By the count 26 votes in favor, none against, Jakarta Commons HttpClient as been promoted to 
    the Jakarta sub-project level 
